﻿using DividaBill.Models;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    public class HouseViewModel
    {

        public string CreationDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string[] Services { get; set; }
        public string[] ServiceInitials { get; set; }

        public int TenantCount { get; set; }

        public HouseViewModel(HouseModel house)
        {
            CreationDate = house.RegisteredDate.ToShortDateString();
            StartDate = house.StartDate.ToShortDateString();
            EndDate = house.EndDate.ToShortDateString();

            TenantCount = house.Tenants.Count;

            Services = ServiceType.All
                .Where(s => house.HasService(s))
                .Select(s => s.ToString())
                .ToArray();

            ServiceInitials = ServiceType.All
                .Where(s => house.HasService(s))
                .Select(s => s.Abbreviation)
                .ToArray();
        }

    }
}