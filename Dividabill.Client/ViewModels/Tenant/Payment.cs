﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels.Tenant
{
    public class PaymentViewModel
    {
        public string AccountNumber { get; set; }
        public string AccountSortCode { get; set; }

        public string PaymentMethod { get; set; } = "Direct Debit";
        public string PaymentType { get; set; } = "Recurring";

        public string LastPayment { get; set; } = "N/A";
        public string NextPayment { get; set; } = "N/A";

        private PaymentViewModel() { }


        public PaymentViewModel(User user)
        {
            AccountNumber = "XXXXX" + string.Concat(user.AccountNumber.Skip(5));
            AccountSortCode = "XX-XX-" + string.Concat(user.AccountSortCode.Skip(4));

            var h = user.House;

            if(h != null)
            {
                var lastPaidBill = h.PaidHouseBills
                    .OrderByDescending(b => b.PaymentDate)
                    .FirstOrDefault();
                if (lastPaidBill != null)
                    LastPayment = "£" + (lastPaidBill.TotalBill / h.Tenants.Count);

                var firstUnpaidBill = h.UnpaidHouseBills
                    .OrderBy(b => b.PaymentDate)
                    .FirstOrDefault();
                if(firstUnpaidBill != null)
                    LastPayment = "£" + (firstUnpaidBill.TotalBill / h.Tenants.Count);
            }
        }
    }
}
