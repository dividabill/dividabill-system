﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    public class TenantSettingsViewModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string EMail { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public TenantSettingsViewModel()
        {

        }

        public TenantSettingsViewModel(User tenant)
        {
            FullName = string.Format("{0} {1}", tenant.FirstName, tenant.LastName).Trim();
            EMail = tenant.Email;
            PhoneNumber = tenant.PhoneNumber;

            StartDate = tenant.StartDate.ToShortDateString();
            EndDate = tenant.EndDate.ToShortDateString();
        }
    }
}