﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    public class TenantViewModel
    {

        public readonly string StartDate;
        public readonly string EndDate;

        public readonly int Duration;

        public TenantViewModel(User tenant)
        {
            StartDate = tenant.StartDate.ToShortDateString();
            EndDate = tenant.EndDate.ToShortDateString();
            Duration = (int)(tenant.EndDate - tenant.StartDate).TotalDays;
        }
    }
}