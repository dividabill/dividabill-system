﻿using DividaBill.Models;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    /// <summary>
    /// Displays summary info for all services. 
    /// </summary>
    public class ServicesSummaryViewModel
    {
        public string LastBillPayment { get; set; } = "N/A";

        public string CurrentBill { get; set; } = "N/A";

        public string PreviousBill { get; set; } = "N/A";

        private ServicesSummaryViewModel() { }

        public ServicesSummaryViewModel(HouseModel house)
        {
            var firstUnpaidBill = house.UnpaidHouseBills
                .OrderBy(b => b.StartDate)
                .FirstOrDefault();

            var lastPaidBill = house.PaidHouseBills
                .OrderByDescending(b => b.StartDate)
                .FirstOrDefault();

            if (firstUnpaidBill != null)
            {
                CurrentBill = "£" + firstUnpaidBill.TotalBill;
            }

            if(lastPaidBill != null)
            {
                LastBillPayment = lastPaidBill.PaymentDate.ToShortDateString();
                PreviousBill = "£" + lastPaidBill.TotalBill;
            }
        }
    }
}