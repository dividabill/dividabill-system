﻿(function (App) {
    'use strict';

    var GasBillView = Backbone.Marionette.LayoutView.extend({
        template: '#gas-page-tpl',
        className: "white-bg",

        events: {
            'click .bills': 'showBills',
            'click .usage': 'showUsage',
            'click .contract': 'showContract',
            'click .billing': 'showBilling'
        },

        regions: {
            SecondNav: "#second-nav",
            PageItem: "#page-item"
        },

        initialize: function () {
        },

        onShow: function () {
            this.SecondNav.show(new App.View.BillSecondNav());
            this.showBills();
        },

        showBills: function () {
            this.PageItem.show(new App.View.GasBillTable());
        },

        showUsage: function (e) {
            e.preventDefault();
            this.PageItem.show(new App.View.Usage());
        },

        showContract: function (e) {
            e.preventDefault();
            var contract = new App.Model.Contract({ id: "2" }); //gas
            this.PageItem.show(new App.View.Contract({ model: contract }));
        },

        showBilling: function (e) {
            //e.preventDefault();
            //this.PageItem.empty();
            this.PageItem.show(new App.View.Billing());
        }
    });


    App.View.GasBillView = GasBillView;

})(window.App);