﻿(function (App) {
    'use strict';

    var EditPaymentDetails = Backbone.Marionette.ItemView.extend({
        template: '#edit-paydetails-tpl',

        initialize: function () {
            console.log("Show Edit Details");
        }
    });

    App.View.EditPaymentDetails = EditPaymentDetails;

})(window.App);
