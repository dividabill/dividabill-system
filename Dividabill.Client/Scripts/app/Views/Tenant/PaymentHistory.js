﻿(function (App) {
    'use strict';

    var PaymentHistory = Backbone.Marionette.ItemView.extend({
        template: '#payment-history-tpl',
        initialize: function () {
            console.log("Init PaymentHistory");
        },

        onShow: function () {
            console.log("PaymentHistory show");
        }

    });

    App.View.PaymentHistory = PaymentHistory;
})(window.App);
