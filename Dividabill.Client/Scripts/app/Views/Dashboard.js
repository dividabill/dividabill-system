﻿(function (App) {
    'use strict';

    var Dashboard = Backbone.Marionette.LayoutView.extend({
        template: '#dashboard-tpl',

        regions: {
            Notifications: '#notifications',
            HouseAccount: '#house-account',
            TenantSettingsTile: '#tenant-settings',
            Services: '#services'
        },

        initialize: function () {

            //console.log("Init Dashboard");
        },

        onShow: function () {
            $("main").removeClass("white-bg");

            this.Notifications.empty();
            this.Notifications.show(new App.View.Notifications());

            this.HouseAccount.empty();
            this.HouseAccount.show(new App.View.HouseTile());

            this.TenantSettingsTile.empty();
            this.TenantSettingsTile.show(new App.View.TenantTile());

            this.Services.empty();
            this.Services.show(new App.View.ServicesTile());

            App.Router.navigate('')
            console.log("Dashboard show");
        }

    });

    App.View.Dashboard = Dashboard;
})(window.App);
