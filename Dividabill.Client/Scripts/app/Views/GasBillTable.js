﻿(function (App) {
    'use strict';

    var GasBillTable = Backbone.Marionette.LayoutView.extend({
        template: '#bill-table-tpl',

        regions: {
            BillsTable: "#bill-table",
            SearchOptions: "#search-options"
        },

        initialize: function () {
            this.billCollection = new App.Model.GasBillCollection();
            this.billCollection.fetch();
        },

        onShow: function () {
            var billTableView = new App.View.BillTableView({ collection: this.billCollection });
            this.BillsTable.show(billTableView);
            this.SearchOptions.show(new App.View.BillSearchOptions());
        }
    });


    App.View.GasBillTable = GasBillTable;

})(window.App);