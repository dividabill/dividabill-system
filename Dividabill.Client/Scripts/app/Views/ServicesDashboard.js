﻿(function (App) {
    'use strict';

    var ServicesDashboard = Backbone.Marionette.LayoutView.extend({
        template: '#services-dashboard-tpl',

        events: {
            'click .electricity-tile':'showElPage',
            'click .gas-tile': "showGasPage",
            'click .water-tile': "showWaterPage",

            'click .bb-tile': "showBbPage",
            'click .sky-tile': "showSkyPage",
            'click .tv-tile': "showTvPage",
            'click .phone-tile': "showPhonePage",
        },

        regions: {
            PageHeader: "#page-header",
            PageItem: "#page-item"
        },

        initialize: function () {
            //init & fetch model
            this.model = new App.Model.ServiceCollection();
            this.model.fetch().done(this.render);


            console.log("Init ServicesDashboard");
        },

        checkServiceActive: function (id) {
            var is_active = this.model.attributes.Services[id].IsActive;

            if (!is_active)
                bootbox.alert({
                    title: "<p class='text-center'> Unavailable Feature </p>",
                    message: '<p class="text-center"> The feature for adding unselected service is not available yet. <Br/> It will be integrated after a few weeks. </p>'
                });

            return is_active;
        },

        onRender: function () {
            this.PageHeader.show(new App.View.PageHeader({
                backRoute: '#',
            }));

            App.Router.navigate('Services');

            console.log("ServicesDashboard show");
        },

        showElPage: function () {
            if (this.checkServiceActive(1))
                App.vent.trigger('show:showService', 'electricity');
        },

        showGasPage: function () {
            if (this.checkServiceActive(2))
                App.vent.trigger('show:showService', 'gas');
        },

        showWaterPage: function () {
            if (this.checkServiceActive(3))
                App.vent.trigger('show:showService', 'water');
        },


        showBbPage: function () {
            if (this.checkServiceActive(4))
                App.vent.trigger('show:showService', 'broadband');
        },

        showSkyPage: function () {
            if (this.checkServiceActive(5))
                App.vent.trigger('show:showService', 'sky tv');
        },

        showTvPage: function () {
            if (this.checkServiceActive(6))
                App.vent.trigger('show:showService', 'tv license');
        },

        showPhonePage: function () {
            if (this.checkServiceActive(7))
                App.vent.trigger('show:showService', 'phone');
        },

        

    });

    App.View.ServicesDashboard = ServicesDashboard;
})(window.App);
