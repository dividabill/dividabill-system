﻿(function (App) {
    'use strict';

    var PageHeader = Backbone.Marionette.ItemView.extend({
        template: '#page-header-tpl',

        events:
        {
            'click .body-header-back': 'goBack'
        },

        initialize: function () {

            if (!this.options.backRoute)
                console.warn("No back route given for PageHeader!");

            var title = (this.options.title) ? this.options.title : "";
            var menu = (this.options.menu) ? this.options.menu : null;

            this.model = new App.Model.PageHeader({
                backRoute: this.options.backRoute,
                PageHeaderTitle: title,
                PageHeaderMenu: menu,
            });

            console.log("Page Header init");
        },

        onShow: function () {
            console.log("Page Header show");
        },

        goBack: function () {
            var route = this.model.attributes.backRoute;

            console.log("Backing to: " + route)
            if (route)
                App.Router.navigate(route, { trigger: true });
        }

    });

    App.View.PageHeader = PageHeader;
})(window.App);
