﻿(function (App) {
    'use strict';

    var Contract = Backbone.Marionette.ItemView.extend({
        template: '#contract-item-tpl',
        className: 'gas-body-full',

        initialize: function () {
            console.log("Init Contract");
            var that = this;
            var fetching = this.model.fetch();

            fetching.done(function () { that.render(); });
        }
    });

    App.View.Contract = Contract;

})(window.App);
