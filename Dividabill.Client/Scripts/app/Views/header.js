﻿(function (App) {
    'use strict';

    var HeaderBar = Backbone.Marionette.ItemView.extend({
        template: '#header-tpl',

        initialize: function () {
            //console.log("Init header");
        },

        templateHelpers: {
            events: function () {
                var date = new Date();
                var today = ("0" + (date.getMonth() + 1)).slice(-2) + ("0" + (date.getDate())).slice(-2);
                if (today == "1231" || today == "0101") {
                    return 'newyear';
                } else if (today >= "1218" || today <= "0103") {
                    return 'xmas';
                } else if (today >= "1027" && today <= "1103") {
                    return 'halloween';
                } else if (today == "0220") {
                    return 'pt_anniv';
                } else if (today == "0224") {
                    return 'stvalentine';
                } else if (today == "0317") {
                    return 'stpatrick';
                } else if (today == "0401") {
                    return 'aprilsfool';
                };
            }
        },

        onShow: function () {
            console.log("Header show");
            $('.tooltipped').tooltip({
                delay: {
                    'show': 800,
                    'hide': 100
                }
            });
        }

    });

    App.View.HeaderBar = HeaderBar;
})(window.App);
