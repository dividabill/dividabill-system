﻿(function (App) {
    'use strict';

    var TenantManagement = Backbone.Marionette.LayoutView.extend({
        template: '#tenantManagement-tpl',
        regions: {
            Table: "#tenant-table"
        },
        initialize: function () {

            //load the model
            this.model = new App.Model.HouseTenants();
            this.model.fetch().done(this.render);


            console.log("HouseTenants init");
        },
       
        onRender: function () {

            console.log("HouseTenants show");
            this.Table.show(new App.View.TenantTableView({
                model: this.model,  
            }));


        }

    });

    App.View.TenantManagement = TenantManagement;
})(window.App);
