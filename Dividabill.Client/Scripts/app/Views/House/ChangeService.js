﻿(function (App) {
    'use strict';

    var ChangeService = Backbone.Marionette.ItemView.extend({
        template: '#changeServices-tpl',
        initialize: function () {

            this.model = new App.Model.House();
            this.model.fetch().done(this.render);

            console.log("HouseSettings init");
        },

        onShow: function () {
            console.log("HouseSettings show");
        }

    });

    App.View.ChangeService = ChangeService;
})(window.App);
