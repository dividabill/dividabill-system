﻿(function (App) {
    'use strict';

    var TenantTableView = Backbone.Marionette.CompositeView.extend({
        template: '#house-table-tpl',
        tagName: 'section',

        initialize: function () {
            console.log("Init TableView");
        },
    });


    App.View.TenantTableView = TenantTableView;

})(window.App);
