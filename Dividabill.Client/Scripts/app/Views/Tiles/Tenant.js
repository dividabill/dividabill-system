﻿(function (App) {
    'use strict';

    var TenantTile = Backbone.Marionette.ItemView.extend({
        template: '#tenant-tile-tpl',
        events: {
            'click .front': 'flipFront',
            'click .backButton': 'flipBack',
            'click #tenantAccount': 'tenantAccount',
            'click #paymentDetails': 'paymentDetails',
        },

        initialize: function () {
            //load the model
            this.model = new App.Model.Tenant();
            this.model.fetch().done(this.render);
        },

        onRender: function () {
            $('#card', this.$el).flip({ trigger: 'manual' });
        },

        onShow: function () {
            console.log("Tenant tile show");
        },

        flipFront: function (e) {
            this.$el.find('#card').flip(true);
        },

        flipBack: function (e) {
        this.$el.find('#card').flip(false);
        },

        tenantAccount: function () {
            App.vent.trigger('show:tenantAccount');
        },

        paymentDetails: function () {
            App.vent.trigger('show:paymentDetails');
        }

    });

    App.View.TenantTile = TenantTile;
})(window.App);
