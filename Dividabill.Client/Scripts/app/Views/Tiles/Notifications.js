﻿(function (App) {
    'use strict';

    var Notifications = Backbone.Marionette.ItemView.extend({
        template: '#notifications-tpl',

        initialize: function () {
            //console.log("Notifications init");
        },

        onShow: function () {
            console.log("Notification Tile show");
        }

    });

    App.View.Notifications = Notifications;
})(window.App);
