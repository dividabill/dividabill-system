﻿(function (App) {
    'use strict';

    App.View.ServicesTile = Backbone.Marionette.ItemView.extend({
        template: '#services-tpl',
        events: {
            'click': 'showServicesDashboard',
        },
        initialize: function () {
            this.model = new App.Model.Services();
            this.model.fetch().done(this.render);
            //console.log("Init Services tile");
        },

        onShow: function () {
            console.log("Services tile show");
        },

        showServicesDashboard: function () {
            App.vent.trigger('show:servicesDashboard');
        },

    });
})(window.App);
