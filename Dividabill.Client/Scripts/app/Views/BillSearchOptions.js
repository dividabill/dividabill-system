﻿/*
    Displays the search bar in a service's 'Bills' page. 
*/

(function (App) {
    'use strict';

    var BillSearchOptions = Backbone.Marionette.ItemView.extend({
        template: '#search-options-tpl',
        tagName: 'section',
        className: 'col-lg-12',

        initialize: function () {
            console.log("Init SearchOptions");
        }
    });

    App.View.BillSearchOptions = BillSearchOptions;

})(window.App);
