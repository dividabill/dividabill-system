﻿(function (App) {
    'use strict';

    //DB Data - 
    var MenuItem = Backbone.Model.extend({
        defaults: {
            "linkTitle": "",
            'id': ""
        }
    });

    App.Model.MenuItem = MenuItem;
})(window.App);