﻿(function (App) {
    'use strict';
    var defaultData = _.times(1, function (i) {
        {
            return {
                "FullName": "...",
                "EndDate": "...",
                "PaymentStatus": "...",
                "LastPaymentAmount": "...",
                "StartDate": "...",
            }
        }
    })
    App.Model.HouseTenants = Backbone.Model.extend({
        url: "/api/house",
        defaults: {
           Tenants: defaultData,
           TenantCount : "...",
        },
        url: "/api/HouseTenants"
    });
})(window.App);