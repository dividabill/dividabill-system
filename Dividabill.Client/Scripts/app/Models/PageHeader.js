﻿(function (App) {
    'use strict';

    //DB Data - 
    var PageHeader = Backbone.Model.extend({
        defaults: {
            "PageHeaderTitle": "",
            "PageHeaderMenu" : null
        }
    });

    App.Model.PageHeader = PageHeader;
})(window.App);