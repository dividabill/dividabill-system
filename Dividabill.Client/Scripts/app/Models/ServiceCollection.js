﻿(function (App) {
    'use strict';

    var defaultData = _.times(10, function (i)
    {
        return {
            "Service": "...",
            "IsActive": false,
            "EstimatedBill": "...",
            "BillStatus": "...",
        };
    });

    //DB Data - id,name,number,date,status,amount
    var ServiceCollection = Backbone.Model.extend({
        defaults: {
            Services: defaultData,
        },
        url: "/api/Service",
    })

    App.Model.ServiceCollection = ServiceCollection;
})(window.App);