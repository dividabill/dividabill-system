﻿(function (App) {
    'use strict';

    //DB Data - id,name,number,date,status,amount
    var Service = Backbone.Model.extend({
        defaults: {
            "IsActive": "!!?",
            "EstimatedBill": "!?!",
            "BillStatus": "?!?",
        },
        urlRoot: '/api/service/'
    });

    App.Model.Service = Service;
})(window.App);