﻿(function (App) {
    'use strict';

    //DB Data - id,name,number,date,status,amount

    App.Model.ElectricityBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/1",
    });

    App.Model.GasBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/2",
    });

    App.Model.WaterBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/3"
    });

    //test
    App.Model.BbBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/4"
    });

    App.Model.SkyBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/6"
    });

    App.Model.TvBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/7"
    });

    App.Model.PhoneBillCollection = Backbone.Collection.extend({
        model: App.Model.Bill,
        url: "/api/bills/8"
    });

})(window.App);