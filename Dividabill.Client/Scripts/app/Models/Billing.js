﻿(function (App) {
    'use strict';

    //DB Data - 
    var Billing = Backbone.Model.extend({
        urlRoot: '/api/Billing',
        defaults: {
            "LastPayment": "...",
            "NextPayment": "..."
        }
    });

    App.Model.Billing = Billing;
})(window.App);