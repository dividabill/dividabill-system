﻿using System.Web;
using System.Web.Optimization;

namespace DividaBill.Client
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                    "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/sitejs").Include(new[]
                {
                    "~/Scripts/underscore.js",

                    "~/Scripts/Q.js",
                    "~/Scripts/backbone.js",
                    "~/Scripts/backbone.marionette.js",
                    "~/Scripts/jquery.flip.js",
                    "~/Scripts/site.js",
                    "~/Scripts/app/app.js",
                    "~/Scripts/app/Router.js",

                    //Tiles
                    "~/Scripts/app/Views/Tiles/Tenant.js",
                    "~/Scripts/app/Views/Tiles/House.js",
                    "~/Scripts/app/Views/Tiles/Services.js",
                    "~/Scripts/app/Views/Tiles/Notifications.js",

                    //House Account
                    "~/Scripts/app/Views/House/HouseSettings.js",
                    "~/Scripts/app/Views/House/ChangeService.js",
                    "~/Scripts/app/Views/House/TenantManagement.js",
                    "~/Scripts/app/Views/House/TenantTableView.js",

                    //Tenant Settings
                    "~/Scripts/app/Views/Tenant/TenantSettings.js",
                    "~/Scripts/app/Views/Tenant/TenantAccount.js",
                    "~/Scripts/app/Views/Tenant/PaymentDetails.js",
                    "~/Scripts/app/Views/Tenant/PaymentHistory.js",
                    "~/Scripts/app/Views/Tenant/TenantDetails.js",
                    "~/Scripts/app/Views/Tenant/EditTenantDetails.js",
                    "~/Scripts/app/Views/Tenant/EditPaymentDetails.js",
                    "~/Scripts/app/Views/Tenant/PaymentAccount.js",

                    "~/Scripts/app/Views/header.js",
                    "~/Scripts/app/Views/Dashboard.js",
                    "~/Scripts/app/Views/main_window.js",
                    "~/Scripts/app/Views/PageHeaderItemView.js",
                    "~/Scripts/app/Views/ServicesDashboard.js",
                    "~/Scripts/app/Views/BillTableItemView.js",
                    "~/Scripts/app/Views/BillSearchOptions.js",
                    "~/Scripts/app/Views/BillSecondNav.js",
                    "~/Scripts/app/Views/BillTableView.js",
                    "~/Scripts/app/Views/Billing.js",
                    "~/Scripts/app/Views/Usage.js",
                    "~/Scripts/app/Views/Contract.js",
                    "~/Scripts/app/Views/BillPanelView.js",
                    "~/Scripts/app/Views/BillTablePanel.js",


                    // models
                    "~/Scripts/app/Models/Tiles/Tenant.js",
                    "~/Scripts/app/Models/Tiles/House.js",
                    "~/Scripts/app/Models/Tiles/Services.js",
                    "~/Scripts/app/Models/PaymentDetails.js",
                    "~/Scripts/app/Models/MenuItem.js",
                    "~/Scripts/app/Models/PageHeader.js",
                    "~/Scripts/app/Models/PageHeaderMenu.js",
                    "~/Scripts/app/Models/Contract.js",
                    "~/Scripts/app/Models/Billing.js",
                    "~/Scripts/app/Models/Bill.js",
                    "~/Scripts/app/Models/BillCollections.js",
                    "~/Scripts/app/Models/ServiceCollection.js",
                    "~/Scripts/app/Models/Service.js",
                    "~/Scripts/app/Models/TenantSettings.js",
                    "~/Scripts/app/Models/HouseTenants.js",

                    // app
                    "~/Scripts/app/bootstrap.js",
                }));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootbox.js"));

            bundles.Add(new StyleBundle("~/Content/sitecss").Include(
                      "~/Content/bootstrap.css",
                      // "~/Content/site.css",
                      "~/Content/normalize.css",
                      "~/Content/main.css",
                      "~/Content/style.css",
                      "~/Content/responsive.css",
                      "~/Content/font-awesome.css"
                      ));

            //Login Page
            bundles.Add(new ScriptBundle("~/bundles/metronic-login").Include(
                "~/Scripts/app2.js",
                "~/Scripts/login.js",
                "~/Scripts/jquery.backstretch.js",
                "~/Scripts/select2.js"
                ));

            bundles.Add(new StyleBundle("~/Content/metronic-login").Include(
                "~/Content/metronic/pages/login-soft.css",
                "~/Content/select2.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/metronic-core").Include(
"~/Scripts/bootstrap-hover-dropdown.js",
"~/Scripts/jquery.slimscroll.js",
"~/Scripts/jquery.blockUI.js",
"~/Scripts/jquery.uniform.js",
"~/Scripts/select2.js"));

            bundles.Add(new StyleBundle("~/Content/metronic-core")
            .Include(
          "~/Content/bootstrap.css",
          "~/Content/font-awesome.css",
          "~/Content/metronic/style-metronic.css",
          "~/Content/metronic/style.css",
          "~/Content/metronic/style-responsive.css",
          "~/Content/metronic/plugins.css",
          "~/Content/metronic/pages/tasks.css",
          "~/Content/metronic/themes/default.css",
          "~/Content/metronic/custom.css",
          "~/Content/themes/uniformjs/default/css/uniform.default.css",
          "~/Content/select2.css",
          "~/Content/select2-metronic.css"));
        }
    }
}
