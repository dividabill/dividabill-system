﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DividaBill.Library;
using DividaBill.Services.Impl;
using DividaBill.Services.NewBillingSystem.Impl;

namespace DividaBill.Client.Controllers
{
    [Authorize]
    public class ServicesController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        static Dictionary<User, Dictionary<ServiceType, int>> RequestedServices = new Dictionary<User, Dictionary<ServiceType, int>>();
        static Dictionary<User, Dictionary<ServiceType, decimal>> HousePrices = new Dictionary<User, Dictionary<ServiceType, decimal>>();
        static Dictionary<User, Dictionary<ServiceType, decimal>> TenantPrices = new Dictionary<User, Dictionary<ServiceType, decimal>>();


        public ServicesController(IUnitOfWork unit)
        {
            this.unitOfWork = unit;
        }

        public ActionResult Done()
        {
            var u = this.GetCurrentUser();
            if (!NeedsRedirect(u))
                return RedirectToAction("Index", "Home");

            
            var h = u.House;
            var dataz = RequestedServices[u];
            var pricez = HousePrices[u];

            //create zhe contract shit
            var dur = (ContractLength)(h.EndDate.GetMonthsPassed() - h.StartDate.GetMonthsPassed());
            var providerService = new ProviderService(unitOfWork);
            var srv = new ContractService(unitOfWork, providerService);
            var ps = unitOfWork.ProviderRepository.Get().ToArray();
            foreach (var kvp in dataz)
                if (kvp.Value > 0)
                {
                    var pr = ps.First(p => p.Type.Any(k => k.Id == kvp.Key.Id));
                    srv.Create(new ContractRequest
                    {
                        Service = kvp.Key,
                        PackageRaw = kvp.Value,
                        Duration = dur,
                        RequestedStartDate = h.StartDate,
                        Provider = pr,
                        TriggeringTenant = u,
                    });
                }
            unitOfWork.Save();

            //display zhe shit

            fillViewBag(u);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirm(FormCollection frm)
        {
            var u = this.GetCurrentUser();
            if (!NeedsRedirect(u))
                return RedirectToAction("Index", "Home");
            var h = u.House;
            var hardCodedEstimatedUtilityPricesService = new HardCodedEstimatedUtilityPricesService();
            var ps = new PriceService(unitOfWork, new CalculateEstimatesPricesService(hardCodedEstimatedUtilityPricesService),hardCodedEstimatedUtilityPricesService );


            var requestedServices = 
                RequestedServices[u] = new Dictionary<ServiceType, int>
                {
                    { ServiceType.Electricity, frm["Electricity"] == "1" ? 1 : 0 },
                    { ServiceType.Gas, frm["Gas"] == "1" ? 1 : 0 },
                    { ServiceType.Water, frm["Water"] == "1" ? 1 : 0 },
                    { ServiceType.SkyTV, frm["Sky"] == "1" ? 1 : 0 },
                    { ServiceType.Broadband, (frm["Internet"] == "2") ? 2 : (frm["Internet"] == "1" ? 1 : 0) },
                    { ServiceType.LandlinePhone, (frm["Landline"] == "2") ? 2 : (frm["Landline"] == "1" ? 1 : 0) },
                };

            //fix house type
            h.BroadbandType = (BroadbandType)requestedServices[ServiceType.Broadband];
            h.LandlineType = (LandlineType)requestedServices[ServiceType.LandlinePhone];

            HousePrices[u] = ps.GetMonthlyEstdPrice(h).ToDictionary(
                    kvp => kvp.Key,
                    kvp => decimal.Round(
                        requestedServices.ContainsKey(kvp.Key) && requestedServices[kvp.Key] > 0 
                        ? kvp.Value 
                        : 0, 
                        2));

            TenantPrices[u] = HousePrices[u].ToDictionary(
                kvp => kvp.Key,
                kvp => (kvp.Value / h.Housemates).BusinessRound());

            fillViewBag(u);
            return View();
        }

        void fillViewBag(User u)
        {
            ViewBag.HousePrices = HousePrices[u];
            ViewBag.TenantPrices = TenantPrices[u];

            ViewBag.Services = RequestedServices[u];

            ViewBag.HouseTotal = HousePrices[u].Sum(kvp => kvp.Value);
            ViewBag.TenantTotal = TenantPrices[u].Sum(kvp => kvp.Value);
        }

        public ActionResult Add()
        {
            var u = this.GetCurrentUser();
            if (!NeedsRedirect(u))
                return RedirectToAction("Index", "Home");

            return View();
        }
        

        public static bool NeedsRedirect(User u)
        {
            var h = u.House;
            var hasContracts = h.GetRequestedContracts().Any();
            var isPrimaryUser = h.Tenants.First().Id == u.Id;
            return !hasContracts && isPrimaryUser;
        }

    }
} 