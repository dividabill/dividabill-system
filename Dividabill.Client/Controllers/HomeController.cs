﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace DividaBill.Client.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController(IUnitOfWork u)
        {
        }

        public ActionResult Index()
        {
            var u = this.GetCurrentUser();
            if (ServicesController.NeedsRedirect(u))
                return RedirectToAction("Add", "Services");

            //var user = this.GetCurrentUser();

            ViewBag.PersonGreet = "Hello," + u.FirstName;
            ViewBag.PersonWelcome = "Welcome to your house account. ";
            
            return View();
        }
    }
}