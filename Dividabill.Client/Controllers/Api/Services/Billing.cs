﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Client.ViewModels.Services;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.Services
{
    [Authorize]
    public class BillingController : BaseApiController
    {
        public BillingController(IHouseService houseService, IUnitOfWork unitOfWork) 
            : base(houseService, unitOfWork)
        {
        }

        // GET: api/Billing/1
        public BillingViewModel Get(ServiceType id)
        {
            return new BillingViewModel(this.CurrentHouse(), id);
        }
    }
}