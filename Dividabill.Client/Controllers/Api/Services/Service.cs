﻿using DividaBill.DAL.Interfaces;
using DividaBill.Client.ViewModels.Services;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api
{
    [Authorize]
    public class ServiceController : BaseApiController
    {
        public ServiceController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }


        // GET: api/Service/1
        // Lists data for a service. 
        public ServiceViewModel Get(int id)
        {
            ServiceType ty = id;
            return new ServiceViewModel(CurrentHouse(), id);
        }

        // GET: api/Service/
        // Lists all services' data. 
        public IHttpActionResult Get()
        {
            var h = CurrentHouse();
            var services = ServiceType.All
                .ToDictionary(
                    ty => ty.Id, 
                    ty => new ServiceViewModel(h, ty));

            return Json(new
            {
                Services = services,
            });
        }
    }
}