﻿using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using DividaBill.Models;
using DividaBill.Client.ViewModels;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api
{
    [Authorize]
    public class ContractController : BaseApiController
    {
        public ContractController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }


        // GET: api/Contract/1
        public ContractViewModel Get(int id)
        {
            checkHaveHouse();

            var h = CurrentHouse();
            var contracts = h.Contracts
                .Where(c => c.ServiceNew == id);

            if(!contracts.Any())
                throw new Exception(string.Format("User '{0}' has no contract for service '{1}'!", CurrentUser.Id, id.ToString()));

            var contract = contracts.SingleOrDefault();

            if (contract == null)
                throw new Exception(string.Format("User '{0}' has more than one contract for service '{1}'!", CurrentUser.Id, id.ToString()));

            return new ContractViewModel(contract, id);
        }
    }
}
