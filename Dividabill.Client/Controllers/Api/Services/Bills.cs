﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.Services
{
    [Authorize]
    public class BillsController : BaseApiController
    {
        public BillsController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork) {  }

        // GET: api/bills/1
        // GET: api/bills/1?max=20&start=20
        public IEnumerable<BillViewModel> Get(int id, int max = 0, int start = 0)
        {
            checkHaveHouse();

            var ty = (ServiceType)id;

            var bills = this.CurrentHouse().Contracts
                .SingleOrDefault(c => c.ServiceNew == ty)
                ?.Bills
                ?.Select(b => new BillViewModel(b, ty));

            if (start > 0)
                bills = bills.Skip(start);

            if(max > 0)
                bills = bills.Take(max);

            return bills;
        }
    }
}