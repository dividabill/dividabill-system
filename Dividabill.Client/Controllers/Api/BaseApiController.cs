﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using DividaBill.Services;
using System.IO;
using DividaBill_Library;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api
{
    public class BaseApiController : ApiController
    {
        User _currentUser;

        protected readonly IUnitOfWork unitOfWork;
        protected readonly IHouseService _houseService;

        protected User CurrentUser
        {
            get
            {
                var userIdentity = User?.Identity?.GetUserId() ?? "mn slao da ima takaf user, manqk";
                if (userIdentity != _currentUser?.Id)
                    _currentUser = unitOfWork.UserRepository.Get().SingleOrDefault(u => u.Id == userIdentity);

                return _currentUser;
            }
        }

        protected HouseModel CurrentHouse()
        {
                if (CurrentUser == null)
                    return null;
                return _houseService.GetHouse(CurrentUser.House_ID.Value);
        }

        public BaseApiController() { }

        public BaseApiController(IHouseService houseService, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this._houseService = houseService;
            //Retry.Do(() =>
            //{
            //    File.AppendAllText(@"E:\lololog.txt", string.Format("[{0}] Controller: {1}\tUnit: {2}\r\n", DateTime.Now.ToShortTimeString(), GetType().Name, unitOfWork.GetHashCode()));
            //}, TimeSpan.FromSeconds(0.01), 5);
        }

        protected void checkHaveHouse()
        {
            if (this.CurrentHouse() == null)
                throw new InvalidOperationException("This user has no house!");
        }
    }
}
