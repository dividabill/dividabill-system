﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using DividaBill.DAL;
using System.Web.Mvc;

namespace DividaBill.Client.Controllers
{

    public static class ControllerHelper
    {
        private static IUnitOfWork UnitOfWork;


        public static void Init(IUnitOfWork _unitOfWork)
        {
            UnitOfWork = _unitOfWork;
        }

        /// <summary>
        /// Gets the <see cref="DividaBill.Models.User"/> for the currently logged-in account. 
        /// Returns null if the user has not logged in. 
        /// </summary>
        public static User GetCurrentUser(this ApiController c)
        {
            if (c == null || c.User == null)
                return null;

            return getUser(c.User.Identity);
        }
        public static User GetCurrentUser(this Controller c)
        {
            if (c == null || c.User == null)
                return null;

            return getUser(c.User.Identity);
        }

        static User getUser(System.Security.Principal.IIdentity userIdentity)
        {
            if (userIdentity == null || !userIdentity.IsAuthenticated)
                return null;

            var userId = userIdentity.GetUserId();
            return UnitOfWork.UserRepository.Get()
                .Single(u => u.Id == userId);
        }

    }
}