﻿namespace DividaBill.API.Models
{
    public class AssemblyInformation
    {
        public AssemblyInformation(string assemblyVersionInfo)
        {
            AssemblyVersionInfo = assemblyVersionInfo;
        }

        public string AssemblyVersionInfo { get; set; }
    }
}