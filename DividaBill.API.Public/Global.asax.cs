﻿using System;
using System.Web;
using System.Web.Http;
using DividaBill.Models.Errors;
using DividaBill.Services.AutoMapper;
using DividaBill.Services.Emails.Exceptions;

namespace DividaBill.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            UnityConfig.GetConfiguredContainer();
            AutoMapperConfiguration.Configure();

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy =
                IncludeErrorDetailPolicy.Always;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            var exception = Server.GetLastError();

            ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(exception, typeof (WebApiApplication).Assembly));
        }

        protected void Application_End()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }
    }
}