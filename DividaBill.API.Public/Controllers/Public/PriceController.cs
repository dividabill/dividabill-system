﻿using DividaBill.Services;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.Services.NewBillingSystem;

namespace DividaBill.API.Controllers.Public
{
    /// <summary>
    /// Reports the prices for a specified postcode. 
    /// </summary>
    [RouteParent("Public")]
    public class PriceController : ApiController
    {
        private readonly IPriceService priceService;

        public PriceController(IPriceService priceService)
        {
            this.priceService = priceService;
        }
        // GET: v1/Price/AA111AA
        [HttpGet]
        [ResponseType(typeof(UnitEstimatedPrice))]
        //[Route("Unit")]
        public IHttpActionResult UnitPricing(String id)
        {
            // sanitize the postcode: trim, uppercase,
            // put a space before the last 3 chars
            var postCode = id.ToUpper().Replace(" ", "");   
            postCode = postCode.Insert(postCode.Length - 3, " ");   

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling =
                Newtonsoft.Json.PreserveReferencesHandling.None;

            return Ok(priceService.GetPriceByPostCode(postCode));
        }

    }
}
