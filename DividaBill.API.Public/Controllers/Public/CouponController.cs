﻿using DividaBill.Services;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.Services.NewBillingSystem;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class CouponController : ApiController
    {
        private readonly IPriceService priceService;

        public CouponController(IPriceService priceService)
        {
            this.priceService = priceService;
        }

        [ResponseType(typeof(EstimatedPriceViewModel))]
        // GET: api/Coupon/5
        public IHttpActionResult Get([FromUri]CouponSearchViewModel id)
        {
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling =
                Newtonsoft.Json.PreserveReferencesHandling.None;

            return Ok(priceService.GetEstimatedPrice(id, null));
        }

    }
}
