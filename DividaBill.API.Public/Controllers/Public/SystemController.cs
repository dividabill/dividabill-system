﻿using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.API.Models;
using DividaBill.Library;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class SystemController : ApiController
    {
        [HttpGet]
        [ResponseType(typeof (AssemblyInformation))]
        public IHttpActionResult GetAssemblyVersion()
        {
            var assemblyInfo =
                new AssemblyInformation(Functions.GetAssemblyVersionInfo(typeof (WebApiApplication).Assembly));
            return Ok(assemblyInfo);
        }
    }
}