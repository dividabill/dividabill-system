﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class AccountController : ApiController
    {
        private readonly IAuthService _authService;
        private readonly IUserManager _userManager;
        private readonly IBankAccountRegistrationService _bankAccountRegistrationService;

        public AccountController(IBankAccountRegistrationService bankAccountRegistrationService, IUserManager userManager, IAuthService authService)
        {
            _bankAccountRegistrationService = bankAccountRegistrationService;
            _userManager = userManager;
            _authService = authService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Create(SignUpViewModel signUpViewModel)
        {
            // validate base model
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //register the account
            var registerResult = await _bankAccountRegistrationService.RegisterAccount(_userManager, signUpViewModel);

            if (registerResult.Errors.Any())
            {
                foreach (var error in registerResult.Errors)
                {
                    ModelState.AddModelError(error.Key, error.ErrorMessage);
                }
                return BadRequest(ModelState);
            }
            await _authService.SignInAsync(registerResult.User, false);

            await _userManager.GenerateEmailConfirmationTokenAsync(registerResult.User.Id);

            return Ok();
        }
    }
}