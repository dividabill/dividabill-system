using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models.Emails
{
    public class EmailSentMessage : IIdentityEntity, IChangeableEntity, IEntityModel
    {
        [MaxLength(256)]
        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        public string SendToEmail { get; set; }

        public int EmailTemplateId { get; set; }

        [ForeignKey(nameof(EmailTemplateId))]
        public virtual EmailTemplate EmailTemplate { get; set; }

        public string Subject { get; set; }

        public string SentFromEmail { get; set; }
        public string SentFromEmailDisplayName { get; set; }

        public int EmailTypeId { get; set; }

        [ForeignKey(nameof(EmailTypeId))]
        public virtual EmailType EmailType { get; set; }

        public string TemplateSubstitutedValues { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
    }
}