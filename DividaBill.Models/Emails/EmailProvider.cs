using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models.Emails
{
    public class EmailProvider : IIdentityEntity, INamedEntity
    {
        public static readonly EmailProvider SendGridProvider = new EmailProvider {ID = 1, Name = "SendGrid", Description = "https://sendgrid.com/"};

        public static EmailProvider[] All =
        {
            SendGridProvider
        };

        public int ID { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public bool Equals(EmailProvider other)
        {
            if (other == null)
                return false;

            return ID == other.ID;
        }

        public override string ToString()
        {
            return Name;
        }

        public static implicit operator int(EmailProvider ty)
        {
            return ty.ID;
        }

        public static implicit operator EmailProvider(int id)
        {
            if (id <= 0 || id > All.Length)
                throw new ArgumentOutOfRangeException(nameof(id));
            return All[id - 1];
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EmailProvider))
                return false;
            return ((EmailProvider) obj).ID == ID;
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(EmailProvider a, EmailProvider b)
        {
            if ((object) a == null)
                return (object) b == null;
            return a.Equals(b);
        }

        public static bool operator !=(EmailProvider a, EmailProvider b)
        {
            return !(a == b);
        }
    }
}