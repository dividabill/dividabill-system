﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public class HouseEstimatedPrice : IEstimatedPrice
    {
        [Key]
        public int HouseID { get; set; }
        [ForeignKey("HouseID")]
        public virtual HouseModel House { get; set; }

        public decimal Electricity { get; set; }

        public decimal ElectricityStandingCharge { get; set; }

        public decimal Gas { get; set; }

        public decimal GasStandingCharge { get; set; }

        public decimal Water { get; set; }
        public decimal WaterStandingCharge { get; set; }

        public decimal BroadbandADSL { get; set; }
        
         public decimal BroadbandFible38 { get; set; }
        
         public decimal BroadbandFible76 { get; set; }

         public decimal LandlinePhoneBasic { get; set; }

         public decimal LandlinePhoneMedium { get; set; }

         public decimal LineRental { get; set; }

         public decimal SkyTV { get; set; }

         public decimal NetflixTV { get; set; }

         public decimal TVLicense { get; set; }

         public decimal BroadbandADSLMargin { get; set; }

         public decimal BroadbandADSLRouter { get; set; }

         public decimal BroadbandFible38Router { get; set; }

         public decimal BroadbandFible38Margin { get; set; }

         public decimal LineRentalMargin { get; set; }

         public decimal LineRentalInstallation { get; set; }

         public decimal SkyTVMargin { get; set; }

         public decimal SkyTVSTB { get; set; }

         public decimal SkyTVSatellite { get; set; }

         public decimal SkyTVInstallation { get; set; }

         public decimal LandlinePhoneBasicMargin { get; set; }

         public decimal LandlinePhoneMediumMargin { get; set; }

         public decimal BroadbandADSLRouterMargin { get; set; }

         public decimal BroadbandFibre40RouterMargin { get; set; }

         public decimal SkyTVInstallationMargin { get; set; }

         public decimal SkyTVSTBMargin { get; set; }

         public decimal SkyTVSatelliteMargin { get; set; }
}
}