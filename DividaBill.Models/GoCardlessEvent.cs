﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    public class GoCardlessEvent
    {
        [Key]
        public string id { get; set; }
        public string created_at { get; set; }
        public string eventMessage { get; set; }
    }
}
