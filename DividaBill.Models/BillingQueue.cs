using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;
using DividaBill.Models.Bills;

namespace DividaBill.Models
{
    [Table("BillingQueue")]
    public class BillingQueue : IIdentityEntity, IChangeableEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int HouseId { get; set; }

        public int? HouseholdBillId { get; set; }

        public int DistributedProcessorId { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        public int BillingRunId { get; set; }

        [ForeignKey("BillingRunId")]
        public virtual BillingRun BillingRun { get; set; }

        [ForeignKey("HouseId")]
        public virtual HouseModel House { get; set; }

        [ForeignKey("HouseholdBillId")]
        public virtual HouseholdBill HouseholdBill { get; set; }

        public virtual ICollection<BillingQueue> BillingQueues { get; set; }
    }
}