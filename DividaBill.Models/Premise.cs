﻿using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    [Serializable]
    public class Premise : IEntityModel
    {
        [Key]
        public int Id { get; set; }

        public double PAId { get; set; }
        public string StreetAddress { get; set; }
        public string Place { get; set; }
        public string Postcode { get; set; }
    }
}