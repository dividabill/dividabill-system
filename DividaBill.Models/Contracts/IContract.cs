﻿using System;
namespace DividaBill.Models
{
    interface IContract
    {
        DateTimeOffset EndDate { get; set; }
        HouseModel House { get; set; }
        int House_ID { get; set; }
        int ID { get; set; }
        ContractLength Length { get; set; }
        Provider Provider { get; set; }
        int Provider_ID { get; set; }
        User RequestedBy { get; set; }
        string RequestedBy_ID { get; set; }
        ServiceType ServiceNew { get; set; }
        DateTimeOffset StartDate { get; set; }
    }
}
