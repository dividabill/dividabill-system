﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models
{
    public abstract class PowerContract : Contract
    {

        protected PowerContract() { }

        public PowerContract(ContractRequest req)
        : base(req) { }

    }

    public abstract class TVContract : Contract
    {

        protected TVContract() { }
        public TVContract(ContractRequest req)
        : base(req)
        {
            if(!StartDate.HasValue)
                StartDate = req.RequestedStartDate;
        }
    }

    //[TrackChanges]
    [Table("ElectricityContracts")]
    public class ElectricityContract : PowerContract
    {
        public virtual IList<ElectricityBill> Bills { get; set; }

        protected ElectricityContract() { }
        public ElectricityContract(ContractRequest req)
        : base(req)
        {

        }


    }

    //[TrackChanges]
    [Table("GasContracts")]
    public class GasContract : PowerContract
    {
        public virtual IList<GasBill> Bills { get; set; }

        protected GasContract() { }
        public GasContract(ContractRequest req)
        : base(req)
        {

        }
    }

    //[TrackChanges]
    [Table("WaterContracts")]
    public class WaterContract : Contract
    {
        public virtual IList<WaterBill> Bills { get; set; }

        protected WaterContract() { }
        public WaterContract(ContractRequest req)
        : base(req)
        {
            if (!StartDate.HasValue)
                StartDate = req.RequestedStartDate;
        }
    }

    //[TrackChanges]
    [Table("BroadbandContracts")]
    public class BroadbandContract : Contract
    {
        public BroadbandType Speed
        {
            get { return (BroadbandType)PackageRaw; }
        }

        public virtual IList<BroadbandBill> Bills { get; set; }

        protected BroadbandContract() { }
        public BroadbandContract(ContractRequest req)
        : base(req)
        {

        }

    }

    //[TrackChanges]
    [Table("SkyTVContracts")]
    public class SkyTVContract : TVContract
    {
        public virtual IList<SkyTVBill> Bills { get; set; }

        protected SkyTVContract() { }
        public SkyTVContract(ContractRequest req)
        : base(req)
        {

        }
    }

    //[TrackChanges]
    [Table("NetflixTVContracts")]
    public class NetflixTVContract : TVContract
    {
        public virtual IList<NetflixTVBill> Bills { get; set; }

        protected NetflixTVContract() { }
        public NetflixTVContract(ContractRequest req)
        : base(req)
        {

        }
    }

    //[TrackChanges]
    [Table("TVLicenseContracts")]
    public class TVLicenseContract : TVContract
    {
        public virtual IList<TVLicenseBill> Bills { get; set; }

        protected TVLicenseContract() { }
        public TVLicenseContract(ContractRequest req)
        : base(req)
        {

        }
    }

    //[TrackChanges]
    [Table("LandlinePhoneContracts")]
    public class LandlinePhoneContract : Contract
    {
        public LandlineType Package
        {
            get { return (LandlineType)PackageRaw; }
        }

        public virtual IList<LandlinePhoneBill> Bills { get; set; }


        protected LandlinePhoneContract() { }
        public LandlinePhoneContract(ContractRequest req)
        : base(req)
        {

        }
    }

    //[TrackChanges]
    [Table("LineRentalContracts")]
    public class LineRentalContract : Contract
    {
        public virtual IList<LineRentalBill> Bills { get; set; }

        protected LineRentalContract() { }
        public LineRentalContract(ContractRequest req)
        : base(req)
        {

        }
    }
}
