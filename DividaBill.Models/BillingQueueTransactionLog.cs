using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models
{
    public class BillingQueueTransactionLog: TransactionLog
    {
        public int BillingQueueId { get; set; }

        public BillingQueueStage BillingQueueStage { get; set; }

        [ForeignKey("BillingQueueId")]
        public virtual BillingQueue BillingQueue { get; set; }
    }
}