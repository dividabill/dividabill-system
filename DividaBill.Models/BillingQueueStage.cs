namespace DividaBill.Models
{
    public enum BillingQueueStage
    {
        //Never change the number assigned to these, just add to them and set a new number,
        //Otherwise the entire system will function incorrectly.
        AssigningHousesToBeBilled = 0,
        GenerateBills = 1,
        CalculateEstimatedBills = 2,
        ReconcileActualBillsWithEstimates = 3,
        ReconcilePayments = 4,
        ReviewBills = 5,
        ApprovalOfBills = 6,
        SendBillsToGoCardless = 7,
        SendBillEmails = 8,

    }
}