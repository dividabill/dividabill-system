﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    //[TrackChanges]
    public class UserPercentage
    {
        #region IDs
        [Key, Column(Order = 0)]
        public int HousePercentage_Id { get; set; }

        [Key, Column(Order = 1)]
        public string User_Id { get; set; }
        #endregion


        [ForeignKey(nameof(HousePercentage_Id))]
        public virtual HouseholdPercentage HousePercentage { get; set; }

        [ForeignKey(nameof(User_Id))]
        public virtual User User { get; set; }


        /// <summary>
        /// Gets the percentage of the bill this payment is responsible for as a value between 0 and 1. 
        /// </summary>
        [Range(0.0, 1.0), DataType(DataType.Currency)]
        public decimal PercentageOfBill { get; set; }

        /// <summary>
        /// Gets the date the user made a decision, if he did at all. 
        /// </summary>
        public DateTimeOffset? DecisionDate { get; set; } = null;

        /// <summary>
        /// Gets whether the user approved this percentage allocation. 
        /// If he already decided, that is. 
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Gets whether the user has made a decision on this proposal. 
        /// </summary>
        public bool HasMadeDecision
        {
            get { return DecisionDate != null; }
        }

        public UserPercentage(HouseholdPercentage housePerc, User usr, decimal perc)
        {
            HousePercentage = housePerc;
            User = usr;
            PercentageOfBill = perc;
        }

        public void MakeDecision(bool doesTheUserAgree)
        {
            if (DecisionDate != null)
                throw new InvalidOperationException("User {0} has already made a decision on proposal #{1}");

            DecisionDate = DateTime.Now;
            IsApproved = doesTheUserAgree;
        }
    }
}
