﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public abstract class EstimatedPrice : IEstimatedPrice
    {
        public decimal Water
        {
            get;
            set;
        }
        public decimal Gas
        {
            get;
            set;
        }
        public decimal Electricity
        {
            get;
            set;
        }


        public decimal ElectricityStandingCharge { get; set; }
        public decimal GasStandingCharge { get; set; }
        [NotMapped]
        public decimal WaterStandingCharge { get; set; }
        [NotMapped]
        public decimal BroadbandADSL { get; set; }
        [NotMapped]
        public decimal BroadbandFible38 { get; set; }
        [NotMapped]
        public decimal BroadbandFible76 { get; set; }
        [NotMapped]
        public decimal LandlinePhoneBasic { get; set; }
        [NotMapped]
        public decimal LandlinePhoneMedium { get; set; }
        [NotMapped]
        public decimal LineRental { get; set; }
        [NotMapped]
        public decimal SkyTV { get; set; }
        [NotMapped]
        public decimal NetflixTV { get; set; }
        [NotMapped]
        public decimal TVLicense { get; set; }
        [NotMapped]
        public decimal BroadbandADSLMargin { get; set; }
        [NotMapped]
        public decimal BroadbandADSLRouter { get; set; }
        [NotMapped]
        public decimal BroadbandFible38Router { get; set; }
        [NotMapped]
        public decimal BroadbandFible38Margin { get; set; }
        [NotMapped]
        public decimal LineRentalMargin { get; set; }
        [NotMapped]
        public decimal LineRentalInstallation { get; set; }
        [NotMapped]
        public decimal SkyTVMargin { get; set; }
        [NotMapped]
        public decimal SkyTVSTB { get; set; }
        [NotMapped]
        public decimal SkyTVSatellite { get; set; }
        [NotMapped]
        public decimal SkyTVInstallation { get; set; }
        [NotMapped]
        public decimal LandlinePhoneBasicMargin { get; set; }
        [NotMapped]
        public decimal LandlinePhoneMediumMargin { get; set; }
        [NotMapped]
        public decimal BroadbandADSLRouterMargin { get; set; }
        [NotMapped]
        public decimal BroadbandFibre40RouterMargin { get; set; }
        [NotMapped]
        public decimal SkyTVInstallationMargin { get; set; }
        [NotMapped]
        public decimal SkyTVSTBMargin { get; set; }
        [NotMapped]
        public decimal SkyTVSatelliteMargin { get; set; }
    }



    public class DefaultEstimatedPrice : EstimatedPrice
    {
        /// <summary>
        /// Define average estimated prices here. 
        /// </summary>
        public DefaultEstimatedPrice()
        {
            this.Electricity = 11.05m;
            this.ElectricityStandingCharge = 27.31m;
            this.Gas = 3.30m;
            this.GasStandingCharge = 26.64m;
        }
    }
}