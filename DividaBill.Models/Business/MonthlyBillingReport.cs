namespace DividaBill.Models.Business
{
    public class MonthlyBillingReport
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int? BilledHouseCount { get; set; }
        public int? Housemates { get; set; }
        public decimal? TotalAmountBilled { get; set; }
        public int? NbOfHousesWithElectricityContract { get; set; }
        public decimal? TotalAmountBilledForElectricity { get; set; }
        public int? NbOfHousesWithGasContract { get; set; }
        public decimal? TotalAmountBilledForGas { get; set; }
        public int? NbOfHousesWithWaterContract { get; set; }
        public decimal? TotalAmountBilledForWater { get; set; }
        public int? NbOfHousesWithBroadbandContract { get; set; }
        public decimal? TotalAmountBilledForBroadband { get; set; }
        public int? NbOfHousesWithNetflixTvContract { get; set; }
        public decimal? TotalAmountBilledForNetflixTv { get; set; }
        public int? NbOfHousesWithSkyTvContract { get; set; }
        public decimal? TotalAmountBilledForSkyTv { get; set; }
        public int? NbOfHousesWithTvLicenseContract { get; set; }
        public decimal? TotalAmountBilledForTvLicense { get; set; }
        public int? NbOfHousesWithLandlinePhoneContract { get; set; }
        public decimal? TotalAmountBilledForLandlinePhone { get; set; }
        public int? NbOfHousesWithLineRentalContract { get; set; }
        public decimal? TotalAmountBilledForLineRental { get; set; }
    }
}