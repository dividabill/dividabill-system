﻿namespace DividaBill.Models
{
    public class UIError
    {
        public UIError(string key, string errorMessage)
        {
            Key = key;
            ErrorMessage = errorMessage;
        }

        public string Key { get; private set; }
        public string ErrorMessage { get; private set; }
    }
}