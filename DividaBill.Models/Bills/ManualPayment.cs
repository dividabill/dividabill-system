﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models.Bills
{
    //[TrackChanges]
    class ManualPayment
    {
        [Key]
        public string User_Id { get; set; }


        [ForeignKey(nameof(User_Id))]
        public virtual User User { get; set; }

        /// <summary>
        /// Gets the amount of this payment. 
        /// Positive amounts indicate a payment by the user to the company. 
        /// Negative amounts indicate a payment by the company to the user. 
        /// </summary>
        public decimal Amount { get; set; }

        private ManualPayment() { }

        private ManualPayment(User user, decimal amount, bool isPayment)
        {
            User = user;
            Amount = amount;

            if ((Amount > 0) != isPayment)
                throw new ArgumentOutOfRangeException("amount", "Invalid voucher or payment amount. ");
        }

        public static ManualPayment CreatePayment(User user, decimal amount)
        {
            return new ManualPayment(user, amount, true);
        }

        public static ManualPayment CreateVoucher(User user, decimal amount)
        {
            return new ManualPayment(user, -amount, false);
        }
    }
}
