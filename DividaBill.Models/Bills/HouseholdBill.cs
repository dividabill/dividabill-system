﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DividaBill.Library;

namespace DividaBill.Models.Bills
{
    //[TrackChanges]
    public class HouseholdBill
    {
        protected HouseholdBill()
        {
        }

        /// <summary>
        ///     Creates an empty household bill for the specified house, year, and a month.
        /// </summary>
        public HouseholdBill(HouseModel house, int year, int month)
        {
            Month = month;
            Year = year;
            House = house;
            CreatedDate = DateTime.Now;
        }

        [ForeignKey(nameof(House_ID))]
        public virtual HouseModel House { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public virtual ICollection<UtilityBill> UtilityBills { get; set; }

        public IEnumerable<BillPayment> Payments
        {
            get { return UtilityBills.SelectMany(b => b.Payments); }
        }

        public decimal TotalAmount
        {
            get { return UtilityBills.Sum(b => b.Ammount); }
        }

        public DateTime StartDate
        {
            get
            {
                var dayOfMonth = Math.Min(28, House.StartDate.Day);
                return new DateTime(Year, Month, dayOfMonth);
            }
        }

        public DateTime EndDate
        {
            get
            {
                // a month after the start date. 
                return StartDate.AddMonths(1);
            }
        }

        public int DurationDays
        {
            get { return (int) Math.Round((EndDate - StartDate).TotalDays); }
        }

        //TODO: change to a field
        public DateTime PaymentDate
        {
            get { return StartDate.AddBusinessDays(3); }
        }


        /// <summary>
        ///     Gets the sum of all utility bills' amounts for this household bill.
        /// </summary>
        public decimal TotalBill
        {
            get { return UtilityBills.Sum(b => b.Ammount); }
        }

        /// <summary>
        ///     Gets whether all bills for all utilities have been fully paid.
        /// </summary>
        public bool IsPaid
        {
            get { return UtilityBills.All(b => b.IsPaid); }
        }

        public UtilityBill GetBill(ServiceType ty)
        {
            return UtilityBills.SingleOrDefault(b => b.Service == ty);
        }

        public T GetBill<T>()
            where T : UtilityBill
        {
            return UtilityBills.OfType<T>().SingleOrDefault();
        }

        public override string ToString()
        {
            return string.Format("HouseBill #{0}", ID);
        }

        #region IDs

        [Key]
        public int ID { get; set; }

        public int House_ID { get; set; }


        public int Month { get; private set; }

        public int Year { get; private set; }

        #endregion

        #region Bill References

        public int? ElectricityBill_Id { get; set; }

        [ForeignKey("ElectricityBill_Id"), Obsolete]
        public ElectricityBill ElectricityBill { get; set; }

        public int? GasBill_Id { get; set; }

        [ForeignKey("GasBill_Id"), Obsolete]
        public GasBill GasBill { get; set; }

        public int? WaterBill_Id { get; set; }

        [ForeignKey("WaterBill_Id"), Obsolete]
        public WaterBill WaterBill { get; set; }

        public int? NetflixTVBill_Id { get; set; }

        [ForeignKey("NetflixTVBill_Id"), Obsolete]
        public NetflixTVBill NetflixTVBill { get; set; }

        public int? SkyTVBill_Id { get; set; }

        [ForeignKey("SkyTVBill_Id"), Obsolete]
        public SkyTVBill SkyTVBill { get; set; }

        public int? TVLicenseBill_Id { get; set; }

        [ForeignKey("TVLicenseBill_Id"), Obsolete]
        public TVLicenseBill TVLicenseBill { get; set; }

        public int? BroadbandBill_Id { get; set; }

        [ForeignKey("BroadbandBill_Id"), Obsolete]
        public BroadbandBill BroadbandBill { get; set; }

        public int? LandlinePhoneBill_Id { get; set; }

        [ForeignKey("LandlinePhoneBill_Id"), Obsolete]
        public LandlinePhoneBill LandlinePhoneBill { get; set; }

        public int? LineRentalBill_Id { get; set; }

        [ForeignKey("LineRentalBill_Id"), Obsolete]
        public LineRentalBill LineRentalBill { get; set; }


        public int? DeliveryBill_Id { get; set; }

        [ForeignKey("DeliveryBill_Id"), Obsolete]
        public DeliveryBill DeliveryBill { get; set; }

        #endregion
    }
}