﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    [Table("ElectricityBills")]
    public class ElectricityBill : UtilityBill
    {
        public int ElectricityContract_ID { get; set; }
        [ForeignKey("ElectricityContract_ID")]
        public ElectricityContract Contract { get; set; }
    }

    [Table("GasBills")]
    public class GasBill : UtilityBill
    {
        public int GasContract_ID { get; set; }
        [ForeignKey("GasContract_ID")]
        public GasContract Contract { get; set; }
    }

    [Table("WaterBills")]
    public class WaterBill : UtilityBill
    {
        public int WaterContract_ID { get; set; }
        [ForeignKey("WaterContract_ID")]
        public WaterContract Contract { get; set; }
    }

    [Table("DeliveryBills")]
    public class DeliveryBill : UtilityBill
    {
    }

    [Table("NetflixTVBills")]
    public class NetflixTVBill : UtilityBill
    {
        public int NetflixTVContract_ID { get; set; }
        [ForeignKey("NetflixTVContract_ID")]
        public NetflixTVContract Contract { get; set; }
    }

    [Table("SkyTVBills")]
    public class SkyTVBill : UtilityBill
    {
        public int SkyTVContract_ID { get; set; }
        [ForeignKey("SkyTVContract_ID")]
        public SkyTVContract Contract { get; set; }
    }

    [Table("TVLicenseBills")]
    public class TVLicenseBill : UtilityBill
    {
        public int TVLicenseContract_ID { get; set; }
        [ForeignKey("TVLicenseContract_ID")]
        public TVLicenseContract Contract { get; set; }
    }

    [Table("BroadbandBills")]
    public class BroadbandBill : UtilityBill
    {
        public int BroadbandContract_ID { get; set; }
        [ForeignKey("BroadbandContract_ID")]
        public BroadbandContract Contract { get; set; }
    }

    [Table("LandlinePhoneBills")]
    public class LandlinePhoneBill : UtilityBill
    {
        public int LandlinePhoneContract_ID { get; set; }
        [ForeignKey("LandlinePhoneContract_ID")]
        public LandlinePhoneContract Contract { get; set; }
    }

    [Table("LineRentalBills")]
    public class LineRentalBill : UtilityBill
    {
        public int LineRentalContract_ID { get; set; }
        [ForeignKey("LineRentalContract_ID")]
        public LineRentalContract Contract { get; set; }
    }
}
