﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    public enum ContractLength
    {
        SixMonths = 6,
        NineMonths = 9,
        TenMonths = 10,
        ElevenMonths = 11,
        TwelveMonths = 12,
        Ongoing = 0
    }

    public static class ContractLengthExt
    {
        public static DateTimeOffset AddContractLength(this DateTimeOffset dt, ContractLength len)
        {
            if (len == ContractLength.Ongoing)
                return DateTimeOffset.MaxValue;
            return dt.AddMonths((int)len);
        }
        public static DateTime AddContractLength(this DateTime dt, ContractLength len)
        {
            if (len == ContractLength.Ongoing)
                return DateTime.MaxValue;
            return dt.AddMonths((int)len);
        }
    }

    public enum BroadbandType
    {
        None = 0,
        ADSL20 = 1,
        Fibre40 = 2,
        Fibre80 = 3
    }

    public enum LandlineType
    {
        None = 0,
        Basic = 1,
        Medium = 2
    }

    public enum ProviderSubmissionStatus
    {
        Submitted = 1,
        New = 0
    }
}
