﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DividaBill.Models
{
    public class ServiceType : IEquatable<ServiceType>
    {
        public static readonly ServiceType Electricity = new ServiceType {Id = 1, Name = "Electricity", Abbreviation = "E"};
        public static readonly ServiceType Gas = new ServiceType {Id = 2, Name = "Gas", Abbreviation = "G"};
        public static readonly ServiceType Water = new ServiceType {Id = 3, Name = "Water", Abbreviation = "W"};
        public static readonly ServiceType Broadband = new ServiceType {Id = 4, Name = "Broadband", Abbreviation = "BB"};
        public static readonly ServiceType NetflixTV = new ServiceType {Id = 5, Name = "NetflixTV", Abbreviation = "N"};
        public static readonly ServiceType SkyTV = new ServiceType {Id = 6, Name = "SkyTV", Abbreviation = "Sky"};
        public static readonly ServiceType TVLicense = new ServiceType {Id = 7, Name = "TVLicense", Abbreviation = "TVL"};
        public static readonly ServiceType LandlinePhone = new ServiceType {Id = 8, Name = "LandlinePhone", Abbreviation = "Ph"};
        public static readonly ServiceType LineRental = new ServiceType {Id = 9, Name = "LineRental", Abbreviation = "LR"};

        public static ServiceType[] All =
        {
            Electricity,
            Gas,
            Water,
            Broadband,
            NetflixTV,
            SkyTV,
            TVLicense,
            LandlinePhone,
            LineRental
        };

        public static ServiceType GetById(int id)
        {
            return All.FirstOrDefault(item => item.Id == id);
        }

        protected ServiceType()
        {
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Abbreviation { get; set; }

        public virtual ICollection<Provider> Providers { get; set; }

        public bool Equals(ServiceType other)
        {
            if ((object) other == null)
                return false;

            return Id == other.Id;
        }

        public static implicit operator ServiceType(int id)
        {
            if (id <= 0 || id > All.Length)
                throw new ArgumentOutOfRangeException("id");
            return All[id - 1];
        }

        public static implicit operator int(ServiceType ty)
        {
            return ty.Id;
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ServiceType))
                return false;
            return ((ServiceType) obj).Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static bool operator ==(ServiceType a, ServiceType b)
        {
            if ((object) a == null)
                return (object) b == null;
            return a.Equals(b);
        }

        public static bool operator !=(ServiceType a, ServiceType b)
        {
            return !(a == b);
        }
    }
}