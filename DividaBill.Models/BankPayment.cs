﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models
{
    public class BankPayment
    {
        [Key]
        public string id { get; set; }
        public string created_at { get; set; }
        public string status { get; set; }
        public string amount_refunded { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string charge_date { get; set; }
        public string reference { get; set; }
        [ForeignKey("Mandate")]
        public string Mandate_ID { get; set; }
        public virtual BankMandate Mandate { get; set; }
    }
}
