﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models
{
    public class Coupon
    {
        [Key]
        public int Id { get; set; }

        public String Code { get; set; }
        public String Text { get; set; }
        public CouponType Type { get; set; }

        public virtual User User { get; set; }
    }

    public enum CouponType
    {
        User, Partner
    }
}