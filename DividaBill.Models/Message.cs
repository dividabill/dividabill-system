﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DividaBill.Models
{
    public class Message
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Subject { get; set; }
        public string MessageText { get; set; }
        public DateTime Created { get; set; }
    }
}