using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public class Tenancy : IEntityModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public string UserId { get; set; }

        [ForeignKey("HouseId")]
        public virtual HouseModel House { get; set; }

        public int HouseId { get; set; }

        [DataType(DataType.Date)]
        public DateTime? JoiningDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? LeavingDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime RequestedDate { get; set; }

        public string RequestedBy { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
    }
}