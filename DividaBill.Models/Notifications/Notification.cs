﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models.Notifications
{
    public class Notification
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }


        /// <summary>
        /// Gets the date and time at which this notification was created. 
        /// </summary>
        public DateTime Timestamp { get; private set; } = DateTime.Today;

        /// <summary>
        /// Gets whether the user has seen this message. 
        /// </summary>
        public bool IsSeen { get; set; } = false;

        /// <summary>
        /// Gets the type of the notification. 
        /// </summary>
        public NotificationType Type { get; set; }

        /// <summary>
        /// Gets the status of this notification. 
        /// </summary>
        public NotificationStatus Status { get; set; } = NotificationStatus.Open;

        /// <summary>
        /// Gets the user who receives this notification. 
        /// </summary>
        public User NotifiedUser { get; set; }

        /// <summary>
        /// Gets the user who triggered this notification. 
        /// </summary>
        public User TriggerUser { get; set; }


        public string Message { get; set; }





        /// <summary>
        /// Marks this notification as seen by the user. 
        /// </summary>
        public void MarkAsSeen()
        {
            if (IsSeen)
                throw new InvalidOperationException("This notification has already been seen!");
            IsSeen = true;
        }
    }
}
