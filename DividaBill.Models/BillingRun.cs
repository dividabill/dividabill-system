using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;
using DividaBill.Models.Emails;

namespace DividaBill.Models
{
    [Table("BillingRun")]
    public class BillingRun : IIdentityEntity, IChangeableEntity, INamedEntity, IEntityModel
    {
        public DateTime? Period { get; set; }

        public string InitiatedByUserName { get; set; }
        public string InitiatedByUserId { get; set; }

        public int? EmailTemplateId { get; set; }

        [ForeignKey("EmailTemplateId")]
        public virtual EmailTemplate EmailTemplate { get; set; }

        public string EmailSubject { get; set; }
        public string EmailSendFrom { get; set; }
        public string EmailSendFromDisplayName { get; set; }

        public virtual ICollection<HouseModel> BillingQueues { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }
    }
}