﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models.GoCardless
{
    public class GCPayment : NewPayment
    {
        public string id { get; set; }
        public string created_at { get; set; }
        public string status { get; set; }
        public string amount_refunded { get; set; }
    }
    public class NewPayment
    {
        public NewPayment()
        {
            metadata = new Dictionary<string, string>();
            links = new Dictionary<string, string>();
        }
        public int amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string charge_date { get; set; }
        public string reference { get; set; }
        public Dictionary<string, string> metadata { get; set; }
        public Dictionary<string, string> links { get; set; }
    }
}
