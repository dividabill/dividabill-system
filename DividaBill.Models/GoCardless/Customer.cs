﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models.GoCardless
{
    public class Customer : NewCustomerInfo
    {
        public string id { get; set; }
        public string created_at { get; set; }

        public Customer() { }

        public Customer(User u)
            : base(u)
        {

        }
    }

    /// <summary>
    /// The object sent to GoCardless to add a new customer. 
    /// </summary>
    public class NewCustomerInfo
    {
        public string email { get; set; }
        [Required]
        public string given_name { get; set; }
        [Required]
        public string family_name { get; set; }
        [Required]
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string address_line3 { get; set; }
        [Required]
        public string city { get; set; }
        public string region { get; set; }
        [Required]
        public string postal_code { get; set; }
        [Required]
        public string country_code { get; set; }
        public Dictionary<string,string> metadata { get; private set; } = new Dictionary<string, string>();

        public NewCustomerInfo() { }

        public NewCustomerInfo(User user)
        {
            given_name = user.FirstName;
            family_name = user.LastName;
            address_line1 = user.House.Address.Line1;
            address_line2 = user.House.Address.Line2;
            address_line3 = user.House.Address.Line3;
            city = user.House.Address.City;
            postal_code = user.House.Address.Postcode;
            email = user.Email;
            country_code = "GB";
        }
    }
}
