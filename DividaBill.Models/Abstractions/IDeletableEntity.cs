namespace DividaBill.Models.Abstractions
{
    public interface IDeletableEntity
    {
        bool Enabled { get; set; }
        bool Deleted { get; set; }
    }
}