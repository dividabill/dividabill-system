using System;

namespace DividaBill.Models.Abstractions
{
    public interface IUsedForPeriodEntity
    {
        DateTime UsedFrom { get; set; }
        DateTime? UsedTo { get; set; }
    }
}