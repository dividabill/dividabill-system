﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    public abstract class Event
    {
        [Key]
        public string id { get; set; }
        public string created_at { get; set; }
        public string action { get; set; }
        public string resource_type { get; set; }
        public string details { get; set; }
    }

    public class BankPaymentEvent : Event
    {
        [ForeignKey("BankPayment")]
        public string Payment_ID { get; set; }
        public virtual BankPayment BankPayment { get; set; }
    }

    public class BankMandateEvent : Event
    {
        [ForeignKey("Mandate")]
        public string Mandate_ID { get; set; }
        public virtual BankMandate Mandate { get; set; }
    }
}
