﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;
using DividaBill.Models.GoCardless;

namespace DividaBill.Models
{
    /// <summary>
    ///     Represents the local data kept for a user's bank account?!
    /// </summary>
    public class BankAccount : IEntityModel, IIdentityEntity2, IChangeableEntity
    {
        [Key]
        public string Id { get; set; }

        public string created_at { get; set; }
        public bool enabled { get; set; }
        public string account_number { get; set; }
        public string sort_code { get; set; }

        [Required]
        public string country_code { get; set; }

        public string currency { get; set; }

        [Required]
        public string account_holder_name { get; set; }

        [ForeignKey("Owner")]
        public string Owner_ID { get; set; }

        public virtual User Owner { get; set; }
        public string GoCardlessCustomerID { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }
    }
}