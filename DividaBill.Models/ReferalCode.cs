﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models
{
    public class ReferalCode
    {
        [Key]
        public String ID { get; set; }
        public virtual Coupon DiscountCode { get; set; }
        public virtual User Owner { get; set; }
    }
}