using System;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public class TransactionLog : IIdentityEntity, IChangeableEntity, ITransactionLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Changes { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        public TransactionLogStatus TransactionLogStatus { get; set; }
    }
}