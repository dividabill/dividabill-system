﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models
{
    public class MediaRequest
    {
        [Key]
        public int ID { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        [DataType(DataType.MultilineText)]
        public String Address { get; set; }
        public String RequestDate { get; set; }

        public MediaProvider provider { get; set; }

        public DateTime Created { get; set; }
    }

    public enum MediaProvider
    {
        Virgin, plusNet, Both
    }
}