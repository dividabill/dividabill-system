﻿using Quartz;
using System;

namespace Scheduler.Jobs
{
    public class BillGenerationJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            JobKey key = context.JobDetail.Key;

            JobDataMap dataMap = context.JobDetail.JobDataMap;

            //DateTimeOffset StartDate = dataMap.GetDateTimeOffset("StartDate");
            //DateTimeOffset EndDate = dataMap.GetDateTimeOffset("EndDate");
            //DateTimeOffset PaymentDate = dataMap.GetDateTimeOffset("PaymentDate");

            var olddate = DateTimeOffset.Now.AddMonths(1);
            var nextMonth = new DateTimeOffset(olddate.Year, olddate.Month, 1, 0, 0, 0, TimeSpan.Zero);

            var StartDate = nextMonth;
            var EndDate = nextMonth.AddDays(DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month));
            var PaymentDate = nextMonth;


            throw new NotImplementedException();
            //IUnitOfWork unitOfWork = new UnitOfWork();
            //List<HouseModel> dbActiveHouses = unitOfWork.ActiveHouses.GetHouses(p => p.SetUpDate <= DateTime.Today && p.StartDate <= DateTime.Today && p.Housemates == p.Tenants.Count(), null, "Tenants").ToList();
            //BillService _service = new BillService(unitOfWork);
            //foreach (HouseModel house in dbActiveHouses)
            //{
            //    _service.GenerateMonthlyBill(StartDate.Date, EndDate.Date, house.ID, PaymentDate.Date);
            //}
        }
    }
}
