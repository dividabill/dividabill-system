﻿//using DividaBill.Services;
//using DividaBill.Services.Interfaces;
//using Quartz;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Scheduler.Jobs
//{
//    public class NetflixRegistrationJob : IJob
//    {
//        const string RecipientEmail = @"markov@dividabill.co.uk";

//        private readonly IEmailService mailService;
//        private readonly IHouseService houseService;

//        public NetflixRegistrationJob(IEmailService emailService, IHouseService houseService)
//        {
//            throw new NotImplementedException();

//            this.mailService = emailService;
//            this.houseService = houseService;
//        }

//        public void Execute(IJobExecutionContext context)
//        {

//            Console.WriteLine("Starting execution of NetflixRegistrationJob...");
//            var today = DateTimeOffset.Now.Date;

//            // from yesterday's 12:00 to today's 12:00
//            var csv = houseService.GenerateNetflixOnboardingForm(today);

//            //the e-mail body and subject
//            var body = "";
//            var subject = string.Format("Netflix Registrations - {0}", today.ToShortDateString());
//            Console.WriteLine("Sending Netflix registered houses for {0} ...", today.ToShortDateString());
//            mailService.SendCsvFormEmail(csv, subject, RecipientEmail, body);
//            Console.WriteLine("Finish execution of NetflixRegistrationJob...");
//        }
//    }
//}
