using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Interfaces;
using DividaBill.Services.Unity;
using GoCardless_API.api;
using Microsoft.Practices.Unity;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;

namespace DividaBill.Services.IntegrationTests.Impl
{
    [Timeout(0)]
    [TestFixture]
    public class GoCardlessBankMandateServiceTests
    {
        [Test]
        public void GivenLast10Tenants_WhenUpdateAllExistingBankMandatesFromGcInvoked_ThenAllMandatesUpdatedInDbFromGc()
        {
            var container = new UnityContainer();
            container.RegisterDAL();
            container.RegisterServices();

            var unitOfWork = container.Resolve<IUnitOfWork>();
            var goCardlessApiClient = container.Resolve<IGoCardlessApiClient>();
            var bankMandateService = container.Resolve<IBankMandateService>();
            var classUnderTest = container.Resolve<IGoCardlessBankMandateService>();

            var tenantsList = unitOfWork.UserRepository.Get().
                Where(x => x.MandateID != null).
                OrderByDescending(user => user.SignupDate).Take(20).
                ToList()
                ;

            foreach (var tenant in tenantsList)
            {
                classUnderTest.UpdateAllExistingBankMandatesFromGc(tenant).Wait();

                var bankMandates = bankMandateService.GetAllMandatesIDsForUser(tenant).Select(x => x.Id);

                Assert.NotNull(bankMandates);
                Assert.Greater(bankMandates.Count(), 0);

                foreach (var bankMandateId in bankMandates)
                {
                    var bankMandate = unitOfWork.BankMandateRepository.Get().First(x => x.Id == bankMandateId);
                    Assert.AreEqual(tenant.Id, bankMandate.User_ID);
                    Assert.AreEqual(tenant.BankAccountID, bankMandate.BankAccount_ID);

                    var gcMandate = goCardlessApiClient.GetMandateAsync(bankMandateId).Result;
                    Assert.AreEqual(gcMandate.id, bankMandate.Id);
                    Assert.AreEqual(gcMandate.created_at, bankMandate.created_at);
                    Assert.AreEqual(gcMandate.next_possible_charge_date, bankMandate.next_possible_charge_date);
                    Assert.AreEqual(gcMandate.status, bankMandate.status);
                    Assert.AreEqual(gcMandate.reference, bankMandate.reference);
                    Assert.AreEqual(gcMandate.scheme, bankMandate.scheme);
                }
            }

            unitOfWork.Save();
        }

        [Test]
        public async Task GivenLast10TenantsWithBankAccount_WhenSyncBankMandateAsyncInvoked_ThenAllMandatesUpdatedInDbFromGc()
        {
            var container = new UnityContainer();
            container.RegisterDAL();
            container.RegisterServices();

            var newGcMandate = new GCMandate
            {
                id = "integration test mandate id",
                scheme = "integration test mandate scheme"
            };

            var goCardlessApiClientReal = new GoCardlessApiClient();

            var mocker = new AutoMocker();
            var goCardlessApiClientMocked = mocker.GetMock<IGoCardlessApiClient>();
            goCardlessApiClientMocked.Setup(x => x.CreateMandateAsync(It.IsAny<NewMandate>()))
                .Returns(Task.FromResult(newGcMandate));

            goCardlessApiClientMocked.Setup(x => x.GetMandateAsync(It.IsAny<string>()))
                .Returns((string mandateId) => goCardlessApiClientReal.GetMandateAsync(mandateId));
            goCardlessApiClientMocked.Setup(x => x.GetMandatesForBankAccount(It.IsAny<string>()))
                .Returns((string bankAccountId) => goCardlessApiClientReal.GetMandatesForBankAccount(bankAccountId));

            container.RegisterType<IGoCardlessApiClient, GoCardlessApiClient>(new InjectionFactory(uc => goCardlessApiClientMocked.Object));

            var unitOfWork = container.Resolve<IUnitOfWork>();
            var classUnderTest = container.Resolve<IGoCardlessBankMandateService>();
            var bankMandateService = container.Resolve<IBankMandateService>();

            var tenantsList = unitOfWork.UserRepository.Get().
                Where(x => x.BankAccountID != null).
                OrderByDescending(user => user.SignupDate).Take(20).
                ToList()
                ;

            foreach (var tenant in tenantsList)
            {
                var result = await classUnderTest.SyncBankMandateAsync(tenant);

                var bankMandates = bankMandateService.GetAllMandatesIDsForUser(tenant).Select(x => x.Id);

                Assert.NotNull(bankMandates);
                Assert.Greater(bankMandates.Count(), 0);

                foreach (var bankMandateId in bankMandates)
                {
                    var bankMandate = unitOfWork.BankMandateRepository.Get().First(x => x.Id == bankMandateId);
                    Assert.AreEqual(tenant.Id, bankMandate.User_ID);
                    Assert.AreEqual(tenant.BankAccountID, bankMandate.BankAccount_ID);

                    var gcMandate = await goCardlessApiClientReal.GetMandateAsync(bankMandateId);
                    Assert.AreEqual(gcMandate.id, bankMandate.Id);
                    Assert.AreEqual(gcMandate.created_at, bankMandate.created_at);
                    Assert.AreEqual(gcMandate.next_possible_charge_date, bankMandate.next_possible_charge_date);
                    Assert.AreEqual(gcMandate.status, bankMandate.status);
                    Assert.AreEqual(gcMandate.reference, bankMandate.reference);
                    Assert.AreEqual(gcMandate.scheme, bankMandate.scheme);
                }
            }

            unitOfWork.Save();
        }
    }
}