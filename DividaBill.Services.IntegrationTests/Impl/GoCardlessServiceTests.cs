using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Library;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.Unity;
using DividaBill.ViewModels;
using GoCardless_API.api;
using Microsoft.Practices.Unity;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;

namespace DividaBill.Services.IntegrationTests.Impl
{
    [Timeout(0)]
    [TestFixture]
    public class GoCardlessServiceTests
    {
        [TestCase("26207729", "56-00-29")]
        [TestCase("26207729", "560029")]
        public async Task GivenProperAccountDetails_WhenValidateBankAccountDetailsInvoked_ThenNoErrorReturned(string accountNumber, string accountSortCode)
        {
            var goCardlessApiClient = new GoCardlessApiClient();

            var appConfigurationReader = new AppConfigurationReader();
            var testDataService = new TestDataService(appConfigurationReader);
            var classUnderTest = new GoCardlessBankAccountDetailsService(testDataService, goCardlessApiClient);

            var bankAccountDetails = new BankAccountDetails
            {
                AccountSortCode = accountSortCode,
                AccountNumber = accountNumber
            };
            var result = await classUnderTest.ValidateBankAccountDetails(bankAccountDetails);

            Assert.IsNull(result);
        }

        [TestCase("I'm not a account number", "I'm not a Sort Code")]
        [TestCase("84958438290574823957483957042395704839", "57438957482935743902574930574839")]
        public async Task GivenWrongAccountDetails_WhenValidateBankAccountDetailsInvoked_ThenListOfErrorsReturned(string accountNumber, string accountSortCode)
        {
            var goCardlessApiClient = new GoCardlessApiClient();
            var appConfigurationReader = new AppConfigurationReader();
            var testDataService = new TestDataService(appConfigurationReader);

            var classUnderTest = new GoCardlessBankAccountDetailsService(testDataService, goCardlessApiClient);

            var bankAccountDetails = new BankAccountDetails
            {
                AccountSortCode = accountSortCode,
                AccountNumber = accountNumber
            };

            var result = await classUnderTest.ValidateBankAccountDetails(bankAccountDetails);

            Assert.IsNotNull(result);
            Assert.Greater(result.Count, 0);
        }

        [Test]
        public void GivenLast10UsersWithGoCardlessCustomerId_WhenAddToGoCardlessAsyncInvoked_ThenBankMandateInformationUpdatedInDb()
        {
            var container = new UnityContainer();
            container.RegisterDAL();
            container.RegisterServices();

            var newGcMandate = new GCMandate
            {
                id = "integration test mandate id",
                scheme = "integration test mandate scheme"
            };
            var newCustomer = new Customer
            {
                id = "integration test GoCardlessId id"
            };
            var newGcBankAccount = new GCBankAccount
            {
                id = "integration test bankAccount id",
                country_code = "integration test bankAccount country_code",
                account_holder_name = "integration test bankAccount account_holder_name"
            };

            var goCardlessApiClientReal = new GoCardlessApiClient();

            var mocker = new AutoMocker();
            var goCardlessApiClientMocked = mocker.GetMock<IGoCardlessApiClient>();
            goCardlessApiClientMocked.Setup(x => x.CreateMandateAsync(It.IsAny<NewMandate>())).Returns(Task.FromResult(newGcMandate));
            goCardlessApiClientMocked.Setup(x => x.CreateCustomerAsync(It.IsAny<NewCustomerInfo>())).Returns(Task.FromResult(newCustomer));
            goCardlessApiClientMocked.Setup(x => x.CreateBankAccountAsync(It.IsAny<NewBankAccount>())).Returns(Task.FromResult(newGcBankAccount));

            goCardlessApiClientMocked.Setup(x => x.GetMandateAsync(It.IsAny<string>())).Returns((string mandateId) =>
                mandateId == newGcMandate.id ? Task.FromResult(newGcMandate) : goCardlessApiClientReal.GetMandateAsync(mandateId));
            goCardlessApiClientMocked.Setup(x => x.GetBankAccountAsync(It.IsAny<string>())).Returns((string accountId) =>
                accountId == newGcBankAccount.id ? Task.FromResult(newGcBankAccount) : goCardlessApiClientReal.GetBankAccountAsync(accountId));
            goCardlessApiClientMocked.Setup(x => x.GetBankAccountsAsync(It.IsAny<string>())).Returns((string customerId) =>
                newCustomer.id == customerId ? Task.FromResult(new List<GCExistingBankAccount>
                {
                    new GCExistingBankAccount
                    {
                        id = newGcBankAccount.id,
                        country_code = newGcBankAccount.country_code,
                        account_holder_name = newGcBankAccount.account_holder_name
                    }
                }
                    ) : goCardlessApiClientReal.GetBankAccountsAsync(customerId));
            goCardlessApiClientMocked.Setup(x => x.GetMandatesForBankAccount(It.IsAny<string>())).Returns((string bankAccountId) =>
                goCardlessApiClientReal.GetMandatesForBankAccount(bankAccountId));

            container.RegisterType<IGoCardlessApiClient, GoCardlessApiClient>(new InjectionFactory(uc => goCardlessApiClientMocked.Object));

            var unitOfWork = container.Resolve<IUnitOfWork>();
            var classUnderTest = container.Resolve<IGoCardlessService>();
            var bankMandateService = container.Resolve<IBankMandateService>();
            var testDataService = container.Resolve<ITestDataService>();
            var tenantEncryptionService = container.Resolve<ITenantEncryptionService>();

            var tenantsList = unitOfWork.UserRepository.Get()
                .Where(x => x.GoCardlessCustomerID != null)
                .OrderByDescending(user => user.SignupDate).Take(10)
                .ToList()
                ;

            foreach (var tenant in tenantsList)
            {
                var result = classUnderTest.AddToGoCardlessAsync(tenant).Result;

                var bankMandates = bankMandateService.GetAllMandatesIDsForUser(tenant).Select(x => x.Id);

                Assert.NotNull(bankMandates);

                var tenantDecrypted = tenantEncryptionService.DecryptUser(tenant);
                if (!testDataService.ValidateBankAccountDetailsForTestData(tenantDecrypted))
                {
                    Assert.Greater(bankMandates.Count(), 0);
                }

                if (!bankMandates.IsEmpty())
                {
                    foreach (var bankMandateId in bankMandates)
                    {
                        //Check BankMandate
                        var bankMandate = unitOfWork.BankMandateRepository.Get().First(x => x.Id == bankMandateId);
                        Assert.AreEqual(tenant.Id, bankMandate.User_ID);

                        var gcMandateForCompare = newGcMandate;

                        if (bankMandate.Id != gcMandateForCompare.id)
                        {
                            gcMandateForCompare = goCardlessApiClientReal.GetMandateAsync(bankMandate.Id).Result;
                        }

                        Assert.AreEqual(gcMandateForCompare.id, bankMandate.Id);
                        Assert.AreEqual(gcMandateForCompare.created_at, bankMandate.created_at);
                        Assert.AreEqual(gcMandateForCompare.next_possible_charge_date, bankMandate.next_possible_charge_date);
                        Assert.AreEqual(gcMandateForCompare.status, bankMandate.status);
                        Assert.AreEqual(gcMandateForCompare.reference, bankMandate.reference);
                        Assert.AreEqual(gcMandateForCompare.scheme, bankMandate.scheme);

                        //Check BankAccount
                        var bankAccount = unitOfWork.BankAccountRepository.Get().First(x => x.Id == bankMandate.BankAccount_ID);
                        Assert.AreEqual(tenant.Id, bankAccount.Owner_ID);

                        var gcBankAccountForCompare = newGcBankAccount;

                        if (bankAccount.Id != gcBankAccountForCompare.id)
                        {
                            gcBankAccountForCompare = goCardlessApiClientReal.GetBankAccountAsync(bankAccount.Id).Result;
                        }

                        Assert.AreEqual(gcBankAccountForCompare.id, bankAccount.Id);
                        Assert.AreEqual(gcBankAccountForCompare.created_at, bankAccount.created_at);
                        Assert.AreEqual(gcBankAccountForCompare.enabled, bankAccount.enabled);
                        Assert.AreEqual(gcBankAccountForCompare.account_holder_name, bankAccount.account_holder_name);
                        Assert.AreEqual(gcBankAccountForCompare.account_number, bankAccount.account_number);
                        Assert.AreEqual(gcBankAccountForCompare.branch_code, bankAccount.sort_code);
                        Assert.AreEqual(gcBankAccountForCompare.country_code, bankAccount.country_code);
                        Assert.AreEqual(gcBankAccountForCompare.currency, bankAccount.currency);
                    }
                }

                var testBankMandatesForDelete = unitOfWork.BankMandateRepository.Get().Where(x => x.Id == newGcMandate.id);
                foreach (var testBankMandate in testBankMandatesForDelete)
                {
                    unitOfWork.BankMandateRepository.Delete(testBankMandate);
                }
            }
            unitOfWork.Save();
        }

        [Test]
        public async Task GivenTestAccount_WhenValidateBankAccountDetailsInvoked_ThenNullReturned()
        {
            var goCardlessApiClient = new GoCardlessApiClient();
            var appConfigurationReader = new AppConfigurationReader();
            var testDataService = new TestDataService(appConfigurationReader);

            var classUnderTest = new GoCardlessBankAccountDetailsService(testDataService, goCardlessApiClient);

            var bankAccountDetails = new BankAccountDetails
            {
                AccountNumber = "55779911",
                AccountSortCode = "20-00-00"
            };

            var result = await classUnderTest.ValidateBankAccountDetails(bankAccountDetails);

            Assert.IsNull(result);
        }
    }
}