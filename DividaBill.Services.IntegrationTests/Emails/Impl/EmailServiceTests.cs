﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Threading.Tasks;
using DividaBill.DAL.Unity;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Emails;
using DividaBill.Models.Errors;
using DividaBill.Services.AppConfiguration;
using DividaBill.Services.AppConfiguration.Impl;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Impl;
using DividaBill.Services.Emails.Models;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.ThirdPartyServices.Impl;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using SendGrid.API.api;

namespace DividaBill.Services.IntegrationTests.Emails.Impl
{
    [TestFixture]
    public class EmailServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _emailSendTo = "55779912@mailinator.com";
            var mocker = new AutoMocker();
            _tenantService = mocker.GetMock<ITenantService>();
            _houseService = mocker.GetMock<IHouseService>();
            var householdBillService = mocker.GetMock<IHouseholdBillService>();
            _emailTemplateService = mocker.GetMock<IEmailTemplateService>();
            var emailSentMessageService = mocker.GetMock<IEmailSentMessageService>();

            var appConfigurationReader = new AppConfigurationReader();
            _emailConfigurationReader = new EmailConfigurationReader(appConfigurationReader);

            var emailThirdPartyService = new EmailThirdPartyService(new SendGridApiClient(appConfigurationReader), _emailTemplateService.Object,
                new SendGridTemplateToEmailTemplateMappingFactory(), emailSentMessageService.Object, _tenantService.Object, appConfigurationReader);
            ClassUnderTest = new EmailService(_emailConfigurationReader, _tenantService.Object, _houseService.Object, householdBillService.Object,
                emailThirdPartyService, appConfigurationReader);
        }

        private Mock<ITenantService> _tenantService;
        private string _emailSendTo;
        private Mock<IHouseService> _houseService;
        private Mock<IEmailTemplateService> _emailTemplateService;
        private IEmailConfigurationReader _emailConfigurationReader;

        private IEmailService ClassUnderTest { get; set; }

        [Test]
        public async Task GivenFalseIsNewHouse_WhenSendUserRegisteredEmail_ThenEmailSent()
        {
            var houseModel = new HouseModel
            {
                ID = 12
            };
            var user = new User
            {
                Email = _emailSendTo,
                House_ID = houseModel.ID,
                FirstName = "Test user name"
            };

            var emailTemplate = new EmailTemplate();

            _tenantService.Setup(x => x.GetTenantDecrypted(It.IsAny<string>())).Returns(user);
            _houseService.Setup(x => x.GetHouse(It.IsAny<int>())).Returns(houseModel);

            _emailTemplateService.Setup(x => x.GetByTemplateId(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns((EmailTemplate) null);
            _emailTemplateService.Setup(x => x.InsertAndSave(It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);
            _emailTemplateService.Setup(x => x.Renew(It.IsAny<int>(), It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);

            await ClassUnderTest.SendUserRegisteredEmail(user, false);
        }

        [Test]
        public async Task GivenTrueIsNewHouse_WhenSendUserRegisteredEmail_ThenEmailSent()
        {
            var houseModel = new HouseModel
            {
                ID = 12
            };
            var user = new User
            {
                Email = _emailSendTo,
                House_ID = houseModel.ID,
                FirstName = "Test user name"
            };

            var emailTemplate = new EmailTemplate();

            _tenantService.Setup(x => x.GetTenantDecrypted(It.IsAny<string>())).Returns(user);
            _houseService.Setup(x => x.GetHouse(It.IsAny<int>())).Returns(houseModel);

            _emailTemplateService.Setup(x => x.GetByTemplateId(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns((EmailTemplate) null);
            _emailTemplateService.Setup(x => x.InsertAndSave(It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);
            _emailTemplateService.Setup(x => x.Renew(It.IsAny<int>(), It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);

            await ClassUnderTest.SendUserRegisteredEmail(user, true);
        }

        [Test]
        public async Task SendBillMessage()
        {
            const int houseId = 12;
            var tenant = new User
            {
                House_ID = houseId,
                FirstName = "House mate 1"
            };
            var tenant2 = new User
            {
                House_ID = houseId,
                FirstName = "House mate 2"
            };
            var houseModel = new HouseModel
            {
                ID = houseId,
                Tenants = new List<User> {tenant, tenant2},
                Housemates = 2,
                Address = new Address
                {
                    Line1 = "35 KINGSLAND ROAD",
                    County = "SHOREDITCH",
                    City = "London",
                    Postcode = "E2 8AA"
                }
            };
            var tenantServiceAmounts = new Dictionary<int, decimal>
            {
                {ServiceType.Electricity.Id, 1.0m},
                {ServiceType.Gas.Id, 2.0m},
                {ServiceType.Water.Id, 3.0m},
                {ServiceType.Broadband.Id, 4.0m},
                {ServiceType.SkyTV.Id, 5.0m},
                {ServiceType.TVLicense.Id, 6.0m},
                {ServiceType.LandlinePhone.Id, 7.0m},
                {ServiceType.LineRental.Id, 8.0m}
            };
            var householdBill = new HouseholdBill(houseModel, DateTime.Now.Year, DateTime.Now.Month)
            {
                UtilityBills = new List<UtilityBill>()
            };
            var billMessageSubtitution = new BillMessageSubtitution
            {
                House = houseModel,
                Bill = householdBill,
                Tenant = tenant,
                TenantServiceAmounts = tenantServiceAmounts,
                Tenants = $"{tenant.FirstName}, {tenant2.FirstName}"
            };
            var emailConfiguration = new EmailConfiguration
            {
                SendFromEmail = "billing@dividabill.co.uk",
                Subject = $"Your {householdBill.GetMonth()} {householdBill.Year} DividaBill",
                SendFromEmailDisplayName = "DividaBill Billing",
                TemplateId = "e9fc4b83-ab0d-43d2-979a-fe9d0c0eef52"
            };
            await ClassUnderTest.SendBillMessage(emailConfiguration, billMessageSubtitution, _emailSendTo);
        }

        [Test]
        public void GivenNoInformation_WhenSendMessageForException_ThenEmailSent()
        {
            var exceptionInfo = new ExceptionInfo(new Exception("Test exception"), typeof (EmailServiceTests).Assembly);

            ClassUnderTest.SendEmailForException(exceptionInfo);
        }

        [Test]
        public void GivenFullInformation_WhenSendMessageForException_ThenEmailSent()
        {
            var testHttpmethod = new NameValueCollection
            {
                {"key1", "value1"},
                {"key2", "value2"}
            };
            var exceptionInfo = new ExceptionInfo(new Exception("Test exception"), typeof(EmailServiceTests).Assembly)
            {
                Url = "Test URL",
                UserAgent = "Test UserAgent",
                UserHostName = "Test UserHostName",
                UserHostAddress = "Test UserHostAddress",
                HttpMethod = "Test HttpMethod",
                Headers = testHttpmethod,
                Form = @"{
                formParameter : 'any data'
                                       }"
            };

            ClassUnderTest.SendEmailForException(exceptionInfo);
        }

        [Test]
        public async Task SendSignUpConfirmationEmail()
        {
            var houseModel = new HouseModel
            {
                ID = 12,
                StartDate = DateTime.Now
            };
            var user = new User
            {
                Email = _emailSendTo,
                House_ID = houseModel.ID,
                FirstName = "Test user name"
            };

            var emailTemplate = new EmailTemplate();

            _tenantService.Setup(x => x.GetTenantDecrypted(It.IsAny<string>())).Returns(user);
            _houseService.Setup(x => x.GetHouse(It.IsAny<int>())).Returns(houseModel);

            _emailTemplateService.Setup(x => x.GetByTemplateId(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns((EmailTemplate) null);
            _emailTemplateService.Setup(x => x.InsertAndSave(It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);
            _emailTemplateService.Setup(x => x.Renew(It.IsAny<int>(), It.IsAny<EmailTemplate>(), It.IsAny<AsyncExecutionParam>())).Returns(emailTemplate);

            await ClassUnderTest.SendSignUpConfirmationEmail(user, "http://google.co.uk/");
        }
    }
}