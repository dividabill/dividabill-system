﻿using System;
using AutoMapper;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Library;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Security.Abstractions;
using DividaBill.Security.Utilities;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Factories.Impl.Api;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.Services.Unity;
using EncryptDB.Abstractions;
using EncryptDB.EncryptInfo;
using GoCardless_API.api;
using Microsoft.Practices.Unity;

namespace EncryptDB
{
    internal class Program
    {
        static readonly string[] encryptionChoice = {
            "encrypt",
            "decrypt"
            };

        private static void Main(string[] args)
        {
            var container = new UnityContainer();

            container.RegisterType<IMappingEngine>(new InjectionFactory(_ => Mapper.Engine));
            container.RegisterDAL();
            container.RegisterServices();

            var unitOfWork = container.Resolve<IUnitOfWork>();
            var encryptionService = container.Resolve<IEncryptionService>();

            var dtNow = DateTime.Now;
            var logFileName = "encryptor-log-{0}-{1}-{2}_{3}-{4}.log".F(dtNow.Year, dtNow.Month, dtNow.Day, dtNow.Hour,
                dtNow.Minute);
            ILoggingEncryptionService loggingEncryptionService = new LoggingEncryptionService(logFileName);

            var encryptAddressService = new AddressEncryptionService(encryptionService);
            ITenantEncryptionService tenantEncryptionService = new TenantEncryptionService(encryptionService,
                encryptAddressService);

            ITableEncryptor userEncryptor = new UserInfoEncryptor(unitOfWork, tenantEncryptionService,
                loggingEncryptionService);

            var bankAccountEncryptor = new BankAccountInfoEncryptor(unitOfWork
                , container.Resolve<IBankAccountEncryptionService>()
                , container.Resolve<ILoggingEncryptionService>()
                , container.Resolve<IAccountService>()
                );

            IScriptRunner scriptRunner = new ScriptRunner();

            int i = scriptRunner.PickListItem(encryptionChoice);

            if (encryptionChoice[i] == "encrypt")
            {
                userEncryptor.EncryptTableRows();
                bankAccountEncryptor.EncryptTableRows();
                Console.ReadLine();
            }
            else
            {
                userEncryptor.DecryptTableRows();
                bankAccountEncryptor.DecryptTableRows();
                Console.ReadLine();
            }
    }

    }
}