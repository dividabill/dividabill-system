using System;
using System.Web.Http.Results;
using AutoMapper;
using Dividabill.Tests.Core;
using DividaBill.API.Controllers.Public;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using Moq;
using NUnit.Framework;

namespace DividaBill.API.Tests.Controllers.Public
{
    [TestFixture]
    public class BuildingControllerTests : BaseAutoMock<BuildingController>
    {
        [Test]
        public void GivenAnyPremise_WhenGetInvoked_ThenOkResultResponseAndGetBuildingsForPremiseInvokedOnceAndMappingInvokedOnce()
        {
            var premise = new Random().Next(999);

            var premiseService = GetMock<IPremiseService>();
            premiseService.Setup(x => x.GetBuildingsForPremise(It.IsAny<double>())).Returns(new Building());
            var mappingEngine = GetMock<IMappingEngine>();
            mappingEngine.Setup(x => x.Map<Building, BuildingViewModel>(It.IsAny<Building>())).Returns(new BuildingViewModel());

            var result = ClassUnderTest.Get(premise);

            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<BuildingViewModel>>());
            mappingEngine.Verify(x => x.Map<Building, BuildingViewModel>(It.IsAny<Building>()), Times.Once);
            premiseService.Verify(x => x.GetBuildingsForPremise(It.IsAny<double>()), Times.Once);
        }
    }
}