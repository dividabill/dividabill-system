﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.App_Start
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            ConfigureCouponMapping();
        }

        private static void ConfigureCouponMapping()
        {
            Mapper.CreateMap<GetQuoteViewModel, CouponSearchViewModel>().ForMember(
    coup => coup.tenants,
    map => map.MapFrom(q => q.Housemates))
    .ForMember(
    coup => coup.broadband,
    map => map.MapFrom(q => q.BroadbandType))
    .ForMember(
    coup => coup.phoneline,
    map => map.MapFrom(q => q.LandlinePhoneType));
        }
    }
}