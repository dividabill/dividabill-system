﻿using Seterlund.CodeGuard;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace DividaBill
{
    public class BundleConfig
    {

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Styles.DefaultTagFormat = "<link href='https://az754941.vo.msecnd.net{0}' rel='stylesheet'/>";

            bundles.Add(new ScriptBundle("~/bundles/autocomplete").Include(
                        "~/Scripts/jquery.autocomplete.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
        "~/Content/themes/base/all.css"));

            bundles.Add(new StyleBundle("~/Content/metronic-login").Include(
                "~/Content/metronic/pages/login-soft.css",
                "~/Content/select2.css"));


            bundles.Add(new ScriptBundle("~/bundles/metronic-login").Include(
                "~/Scripts/login.js",
                "~/Scripts/jquery.backstretch.js",
                "~/Scripts/select2.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));



            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate*"
                        
            ));
            

            //Using JS
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js",
                        "~/Scripts/bootstrap-select.js",
                        "~/Scripts/bootstrap-datepicker.js",
                        "~/Scripts/jquery.inputmask/jquery.inputmask.js",
                        "~/Scripts/jquery.inputmask/inputmask.extensions.js",
                        "~/Scripts/jquery.inputmask/inputmask.phone.extensions.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/bootstrap2-toggle.min.js",
                        "~/Scripts/jquery.simplyscroll.min.js"
                       ));

            //Using CSS
            bundles.Add(new StyleBundle("~/Content/css2").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/Site.css",
                        "~/Content/font-awesome.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/bootstrap-select.css",
                        "~/Content/css/FABSCheckbox.css",
                        "~/Content/bootstrap-datepicker.css",
                        "~/Content/simple-sidebar.css",
                        "~/Content/bootstrap2-toggle.min.css",
                        "~/Content/jquery.simplyscroll.css"
                        ));

            //Using JS
            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                        "~/Scripts/site.js",

                        "~/Scripts/underscore.min.js",
                        "~/Scripts/backbone.js",
                        "~/Scripts/backbone.marionette.min.js",
                        "~/Scripts/q.js",

                         "~/Scripts/app/app.js",
                         "~/Scripts/app/Views/AboutUs.js",
                         "~/Scripts/app/Views/ContactUs.js",
                         "~/Scripts/app/Views/Footer.js",
                         "~/Scripts/app/Views/Header.js",
                         "~/Scripts/app/Views/HouseDetails.js",
                         "~/Scripts/app/Views/Index.js",
                         "~/Scripts/app/Views/MainWindow.js",
                         "~/Scripts/app/Views/PersonalDetails.js",
                         "~/Scripts/app/Views/Services.js",
                         "~/Scripts/app/Views/SignUp.js",
                         "~/Scripts/app/Views/BankDetails.js",
                         "~/Scripts/app/Views/ConfirmationSignup.js",
                         "~/Scripts/app/Views/ServicesDetails.js",
                         "~/Scripts/app/Models/SignupDetails.js",
                         "~/Scripts/app/Models/House.js",
                         "~/Scripts/app/Models/Postcode.js",
                         "~/Scripts/app/Models/PostcodeCollection.js",
                         "~/Scripts/app/Models/Pricing.js",
                         "~/Scripts/app/Models/Coupon.js",
                         "~/Scripts/app/Views/SelectBuilding.js",
                         "~/Scripts/app/Router.js",
                         "~/Scripts/app/bootstrap.js"

                ));


            bundles.Add(new ScriptBundle("~/bundles/postcodefinder").Include(
                        "~/Scripts/postcodefinder-{version}.js",
                        "~/Scripts/postcodeanywhere.js",
                        "~/Scripts/bootstrap-select.js",
                        "~/Scripts/bootstrap-datepicker.js",
                        "~/Scripts/jquery.inputmask/jquery.inputmask.js",
                        "~/Scripts/jquery.inputmask/inputmask.extensions.js",
                        "~/Scripts/jquery.inputmask/inputmask.phone.extensions.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/signup.js"));

            bundles.Add(new ScriptBundle("~/bundles/zxcvbn").Include(
                        "~/Scripts/zxcvbn/zxcvbn.js"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }

    public class ReplaceContentsBundleBuilder : IBundleBuilder
    {
        private readonly string _find;

        private readonly string _replaceWith;

        private readonly IBundleBuilder _builder;

        public ReplaceContentsBundleBuilder(string find, string replaceWith)
            : this(find, replaceWith, new DefaultBundleBuilder())
        {
        }

        public ReplaceContentsBundleBuilder(string find, string replaceWith, IBundleBuilder builder)
        {
            Guard.That(() => find).IsNotNullOrEmpty();
            Guard.That(() => replaceWith).IsNotNullOrEmpty();
            Guard.That(() => builder).IsNotNull();

            _find = find;
            _replaceWith = replaceWith;
            _builder = builder;
        }

        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<System.Web.Optimization.BundleFile> files)
        {
            string contents = _builder.BuildBundleContent(bundle, context, files);

            return contents.Replace(_find, _replaceWith);
        }
    }
}
