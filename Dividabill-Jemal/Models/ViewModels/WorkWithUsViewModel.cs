﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models.ViewModels
{
    public class WorkWithUsViewModel
    {

        [Required]
        [Display(Name = "Your Name (required)")]
        public String Name { get; set; }

        [Required]
        [Display(Name = "Your Email (required)")]
        [EmailAddress]
        public String Email { get; set; }

        [Display(Name = "Subject")]
        public String Subject { get; set; }

        [Display(Name = "Your Message")]
        public String MessageText { get; set; }
    }
}