﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DividaBill.Startup))]
namespace DividaBill
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
