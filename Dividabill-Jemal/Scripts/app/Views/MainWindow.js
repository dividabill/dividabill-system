﻿(function (App) {
    'use strict';

    var that;

    var MainWindow = Marionette.LayoutView.extend({
        template: "#main-window-tpl",
        regions: {
            Content: "#content"
        },
        initialize: function () {
             that = this;

            App.vent.on('show:Index', this.showIndex);
            App.vent.on("show:aboutUs", this.showAboutus);
            App.vent.on("show:whyItWorks", this.showWhyItWorks);
            App.vent.on("show:contactUs", this.showContactUs);
            App.vent.on("show:Services", this.showServices);
            App.vent.on("show:SignUp", this.showSignUp);
            App.vent.on("show:SignupServices", this.showSignupServices);
            App.vent.on("show:GetQuote", this.GetQuote);
        },
        showIndex: function () {
            that.Content.empty();
            that.Content.show(new App.View.Index());
        },
        showAboutus: function () {
            that.Content.empty();
            that.Content.show(new App.View.AboutUs());
        },
        showWhyItWorks: function () {
            that.Content.empty();
            that.Content.show(new App.View.WhyitWorks());
        },
        showContactUs: function () {
            that.Content.empty();
            that.Content.show(new App.View.ContactUs());
        },
        showServices: function () {
            that.Content.empty();
            that.Content.show(new App.View.Services());
        },
        showSignUp: function () {
            that.Content.empty();
            that.Content.show(new App.View.SignUp());
        },
        GetQuote: function (query) {
            that.Content.empty();
            var view = new App.View.SignUp();
            view.getQueryString(query)
            that.Content.show(view);
        }




    });

    App.View.MainWindow = MainWindow;

})(window.App);
