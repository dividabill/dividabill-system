﻿(function (App) {
    'use strict';

    var ContactUs = Marionette.LayoutView.extend({
        template: "#contactus-tpl",
        initialize: function () {
            console.log("Init Contact Us")
        }
    });

    App.View.ContactUs = ContactUs;

})(window.App);