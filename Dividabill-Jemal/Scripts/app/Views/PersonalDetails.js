﻿(function (App) {
    'use strict';
    var click1 = 0, click2 = 0, click3 = 0, click4 = 0, click5 = 0, click6 = 0, click7 = 0, click8 = 0;
    var progress = 60;
    var PersonalDetails = Marionette.LayoutView.extend({
        template: "#personaldetails-tpl",
        events: {
            'keyup #Password': "checkPassword",
            'change #FirstName': "changeFirstName",
            'change #LastName': 'changeLastName',
            'change #Email': 'changeEmail',
            'change #ConfirmEmail' : "changeConfirmEmail",
            'change #Phone': 'changePhone',
            'change #ConfirmPassword': 'changeConfirmPassword',
            'change #DoB' : "changeDoB"
        },
        initialize: function () {
            console.log("Init Personal Details")
        },
        onShow: function () {
            $('.selectpicker').selectpicker();
            $("#DoB").datepicker();
            this.accessedFromAnotherStep();
        },
        changeFirstName: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'FirstName': value });
        },
        changeLastName: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'LastName': value });
        },
        changeEmail: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'Email': value });
        },
        changeConfirmEmail: function (ev) {
            var value = $(ev.target).val();
            this.model.set({"ConfirmEmail": value});
        },
        changeConfirmPassword: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'ConfirmPassword': value });
        },
        checkPassword: function (ev) {
            var pass = $(ev.target).val();
            this.model.set({ 'Password': pass });
            var pass_score = 0;
            if (pass != "") {
               // var res = zxcvbn.zxcvbn(pass);
               // pass_score = res.score + 1;
            }

            var passStrengthBar = $('#PassMeter');
            passStrengthBar
                .css('width', (100 * pass_score / 5) + '%')
                .attr('aria-valuenow', pass_score)
                .attr('title', 'Password Strength')
                .removeClass('progress-bar-success progress-bar-warning progress-bar-danger');

            if (pass_score < 2)
                passStrengthBar.addClass('progress-bar-danger')
            else if (pass_score < 4)
                passStrengthBar.addClass('progress-bar-warning')
            else
                passStrengthBar.addClass('progress-bar-success')
        },
        changePhone: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'Phone': value });
        },
        changeDoB: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ "DateOfBirth": value });
        },
        accessedFromAnotherStep: function () {
            $("#FirstName").val(this.model.get("FirstName"));
            $("#LastName").val(this.model.get("LastName"));
            $("#Email").val(this.model.get("Email"));
            $("#ConfirmEmail").val(this.model.get("ConfirmEmail"));
            $("#Phone").val(this.model.get("Phone"));
            $("#DoB").val(this.model.get("DateOfBirth"));
            $("#Password").val(this.model.get("Password"));
            $("#ConfirmPassword").val(this.model.get("ConfirmPassword"));
        }
    });



    App.View.PersonalDetails = PersonalDetails;

})(window.App);