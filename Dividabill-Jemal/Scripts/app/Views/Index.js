﻿(function (App) {
    'use strict';

    var Index = Marionette.LayoutView.extend({
        template: "#index-tpl",
        initialize: function () {
            console.log("Init Index");
        },
        events: {
            'submit form': 'getQuote'
        },
        onShow: function () {
            $(".drop_cart").hide();
            $(".signup-nav").show();
            $(".scroller").simplyScroll();

        },
        getQuote: function (e) {
            e.preventDefault();
            var query = $(e.currentTarget).serialize();
            console.log("The query target: " + query)
            Backbone.history.navigate("Account/SignUp/" + query, { trigger: true });
        }
     });

    App.View.Index = Index;

})(window.App);
