﻿(function (App) {
    'use strict';
    var basket_popover = false;
    var total_person = 0;
    var total_house = 0;
    var ServicesDetails = Marionette.LayoutView.extend({
        template: "#servicesdetails-tpl",

        initialize: function () {
            console.log("Init Services Details");
        },
        events: {
            'click .third-single-img': "checkedService",
            'click #checkTelevision': 'checkService',
            'click #checkInternet': 'checkService',
            'click #checkTelephone': 'checkService',
            'click #cancelTelevision': 'cancelService',
            'click #cancelInternet': 'cancelService',
            'click #cancelTelephone': 'cancelService',
            'change .service_check': 'switchSelection',
            'change .service_radio': "getServiceRadio",
            'change .tv_checks': 'checkTvRadios',
            'click .close-popover': 'closePopover'
        },
        onShow: function () {
            this.checkServiceFromModel();

            var that = this;
            console.log(this.model);

        },
        switchSelection: function (ev) {        
            console.log(ev)
            var target = ev.target.id;
            var info = $('.' + target + '-info');
            if ($("#Electricity").prop("checked") == false && target == "Gas") {
                $('.Gas').popover({html: true});
                $('.Gas').popover('toggle');
                $("#Gas").prop("checked", false);
            } 
            else {
                $(ev.target).parent().toggleClass(target + "-active");
                info.find(".front-content").toggleClass("front-transform");
                info.find(".back-content").toggleClass("back-transform");
                $('.Gas').popover('hide');             
            }
        },
        
        checkedService: function (ev) {
            var target = $(ev.target).closest(".third-single-img").data("target");
            var checkbox = $("#" + target);

            if (target == "Electricity" && $("#Electricity").prop("checked") == true && $("#Gas").prop("checked") == true) {
                $("#Gas").prop("checked", false).trigger('change');
                $("#Electricity").prop("checked", false).trigger('change');
                this.model.set({ "Gas": false });
                this.model.set({ "Electricity": false });
                this.removeFromBasket("Electricity");
                this.removeFromBasket("Gas");
            }
                else if (target == "Internet" || target == "Television" || target == "Telephone") {
                $('#' + target + "Modal").modal('toggle');
            }

            else if (checkbox.prop("checked") == false) {
                //console.log(target + " was changed!");
                checkbox.prop("checked", true).trigger('change');
                if (target == "Electricity") {
                    this.model.set({ "Electricity": true });
                    this.createServiceForBasket("Electricity", "#E74B3C", "bolt");
                }
                if (target == "Water") {
                    this.model.set({ "Water": true });
                    this.createServiceForBasket("Water", "#3596DA", "tint");
                }
            }
            else {
                checkbox.prop("checked", false).trigger('change');
                this.removeFromBasket(target);
                if (target == "Electricity") {
                    this.model.set({ "Electricity": false });
                }
                if (target == "Gas") {
                    this.model.set({ "Gas": false });
                }
                if (target == "Water") {
                    this.model.set({ "Water": false });
                }

            }
            if (target == 'Gas' && $("#Gas").prop("checked") == true) {
                this.model.set({ "Gas": true })
                this.createServiceForBasket("Gas", "#F29B11", "fire");
            }

            this.checkCoupon();
        },
        checkService: function (ev) {
            var inputs = $(ev.target).parent().parent().find("input");
            var checked_inputs = $(ev.target).parent().parent().find("input:checked").attr("id");
            var target = $(ev.target).data("target");
            var info = $("." + target).parent().parent().find("." + target + "-info");

            if (inputs.valid())
            {
                $('#' + target + 'Modal').modal('toggle');
                $("." + target).addClass(target + "-active");
                info.find(".front-content").addClass("front-transform");
                info.find(".back-content").addClass("back-transform");

                $("#" + target).prop("checked", true);
            }
               
        },
        cancelService: function (ev) {
            var target = $(ev.target).data("target");
            var info = $("." + target).parent().parent().find("." + target + "-info");
            $("." + target).removeClass(target + "-active");
            info.find(".front-content").removeClass("front-transform");
            info.find(".back-content").removeClass("back-transform");

            $("#" + target).prop("checked", false);

            if (target == "Television" && $("#TVLicense").is(":checked") && $("#SkyTV").is(":checked")) {
                this.model.set({ "TVLicense": false })
                this.removeFromBasket("TVLicense");
                this.model.set({ "SkyTV": false });
                this.removeFromBasket("SkyTV");

            }
            else if (target == "Television" && $("#SkyTV").is(":checked"))
            {
                this.removeFromBasket("SkyTV");
                this.model.set({ "SkyTV": false });
            }
            else if (target == "Television" && $("#TVLicense").is(":checked"))
            {
                this.removeFromBasket("TVLicense");
                this.model.set({ "TVLicense": false })
            }
            else if (target == "Internet" && $("#BroadbandTypeADSL20").is(":checked")) {
                this.removeFromBasket("ADSL20");
                this.model.set({ "BroadbandType": 0 })
                this.model.set({ "Broadband": false })

            } else if (target == "Internet" && $("#BroadbandTypeFibre40").is(":checked")) {
                this.removeFromBasket("Fibre40");
                this.model.set({ "BroadbandType": 0 })
                this.model.set({ "Broadband": false })

            } else if (target == "Telephone" && $("#LandlineTypeBasic").is(":checked")) {
                this.removeFromBasket("Basic");
                this.model.set({ "LandlinePhoneType": 0 })
                this.model.set({ "LandlinePhone": false})

            } else if (target == "Telephone" && $("#LandlineTypeMedium").is(":checked")) {
                this.removeFromBasket("Medium");
                this.model.set({ "LandlinePhoneType": 0 })
                this.model.set({ "LandlinePhone": false })
            }
            $(ev.target).parent().parent().find("input").prop("checked", false);
            this.checkCoupon();
        },
        getServiceRadio: function (ev) {
            if ($(ev.target).val() == "ADSL20") {
                this.model.set({ "Broadband": true });
                this.model.set({ "BroadbandType": 1 });
                this.createServiceForBasket("ADSL20", "#19937B", "wifi");
                this.removeFromBasket("Fibre40");
                $(".internet-service").text("ADSL Broadband")
            }else if($(ev.target).val() == "Fibre40")
            {
                this.model.set({ "Broadband": true });
                this.model.set({ "BroadbandType": 2 });
                this.createServiceForBasket("Fibre40", "#19937B", "wifi");
                this.removeFromBasket("ADSL20");
                $(".internet-service").text("Fibre Optic Broadband")

            }
            else if ($(ev.target).val() == "Medium") {
                this.model.set({ "LandlinePhone": true });
                this.model.set({ "LandlinePhoneType": 2 });
                this.createServiceForBasket("Medium", "#19937B", "phone");
                this.removeFromBasket("Basic");
                $(".telephone-service").text("Medium Package")

            } else {
                this.model.set({ "LandlinePhone": true });
                this.model.set({ "LandlinePhoneType": 1 });
                this.createServiceForBasket("Basic", "#19937B", "phone");
                this.removeFromBasket("Medium");
                $(".telephone-service").text("Basic Package")
            }
            this.checkCoupon();
        },
        checkTvRadios: function (ev) {
            var checkbox_name = $(ev.target).attr("name");
            var full_text = "";
            var name1 = "", name2= "";
            console.log("Target: " + ev.target);

            if ($(ev.target).is(":checked")) {
                console.log("Checkbox: " +checkbox_name);             
                if (checkbox_name == "SkyTV") {
                    this.model.set({ "SkyTV": true });
                    name1 = "Sky TV";
                }
                else {
                    this.model.set({ "TVLicense": true });
                    name2 = "TV License";
                }
                this.createServiceForBasket(checkbox_name, "#7863E0", "desktop");
               
            }
            else
            {
                this.removeFromBasket(checkbox_name);
                if (checkbox_name == "SkyTV") {
                    this.model.set({ "SkyTV": false });
             
                    name1 = "";
                }
                else {
                    this.model.set({ "TVLicense": false });
                    name2 = "";
                }
            }
            if (this.model.get("SkyTV") == true && this.model.get("TVLicense") == true) {
                $(".television-service").text("Sky TV & TV License");
            }
            else if (this.model.get("SkyTV") == true) {
                $(".television-service").text("Sky TV");
            }
            else if (this.model.get("TVLicense") == true){
                $(".television-service").text("TV License");
            }
            
            this.checkCoupon();
        },
        checkCoupon: function () {
            console.log("Check pricing...");
            $(".totalServices").text($(".basket_services").find('tr').length);
            $(".number_tenants").text(this.model.get("Housemates"));

            var that = this;
            var Electricity = this.model.get("Electricity");
            var Gas = this.model.get("Gas");
            var Water = this.model.get("Water");
            var Broadband = this.model.get("Broadband");
            var BroadbandType = this.model.get("BroadbandType");
            var SkyTV = this.model.get("SkyTV");
            var TVLicense = this.model.get("TVLicense");
            var LandlinePhone = this.model.get("LandlinePhone");
            var LandlinePhoneType = this.model.get("LandlinePhoneType");
            var broadband = "", landline = "";

            if (this.model.get("Broadband") == false || this.model.get("BroadbandType") == 0) {
                broadband = "None";
            }
            else if (this.model.get("BroadbandType") == 1) {
                broadband = "ADSL20";
            }
            else {
                broadband = "Fibre40"
            }
            if (this.model.get("LandlinePhone") == false || this.model.get("LandlinePhoneType") == 0) {
                landline = "None";
            }
            else if (this.model.get("LandlinePhoneType") == 1) {
                landline = "Basic"
            }
            else {
                landline = "Medium"
            }

            var el_st_charge = 0;
            var el_urate = 0;
            var el_tcr = 0;
            var gas_st_charge = 0;
            var gas_urate =0;
            var gas_tcr = 0;
            var broadbandPrice = 0;
            var landlinePrice = 0;
            var totalbill = 0;
            var personbill = 0;
            var tvprice = 0;
            var gasprice = 0;
            var waterprice = 0;
            var elprice = 0;
            var skyprice = 0;
            var tvlicenseprice = 0;

            /*this.Prices = new App.Model.Pricing({ 'id': this.model.get('Postcode') });
            this.Prices.fetch().done(function() {
                el_st_charge = that.Prices.get("ElectricityStandingCharge");
                el_urate = that.Prices.get("ElectrcityUnitRate");
                el_tcr = that.Prices.get("ElectrcityTCR");
                gas_st_charge = that.Prices.get("GasStandingCharge");
                gas_urate = that.Prices.get("GasUnitRate");
                gas_tcr = that.Prices.get("GasTCR");

                $(".El-tcr").text(el_tcr);
                $(".El-price-unit").text(el_urate);
                $(".El-charge").text(el_st_charge);
                $(".Gas-tcr").text(gas_tcr);
                $(".Gas-price-unit").text(gas_urate);
                $(".Gas-charge").text(gas_st_charge);


            });  */

            if (Electricity == true || Gas == true || Water == true || Broadband == true || SkyTV == true || TVLicense == true || LandlinePhone == true) {
                if (basket_popover == false) {
                    $('body').on('click', '.close-popover', function () {
                        var target = $(this).data("target");
                        console.log("World");
                        $('.' + target).popover('hide');
                    });

                    $('.dropdown').popover({
                        content: "You can view a service by service by clicking 'Your Quote' above. <i class='fa fa-times-circle close-popover' data-dismiss='popover' data-target='dropdown'></i>",
                        template: '<div class="blue-popover popover" role="popover"><div class="arrow" style="border-bottom-color: #29AAE1;"></div><div class="popover-content"></div></div>',
                        html: true
                    });
                    $('.dropdown').popover('show');
                    basket_popover = true;
                }
            }

            this.Coupon = new App.Model.Coupon();
            this.Coupon.fetch({
                data: {
                    'coupon': this.model.get("Coupon"),
                    'tenants': this.model.get("Housemates"),
                    'postcode': this.model.get("Postcode"),
                    'water': this.model.get("Water"),
                    'gas': this.model.get("Gas"),
                    'electricity': this.model.get("Electricity"),
                    'broadband': broadband,
                    'skytv': this.model.get("SkyTV"),
                    'tvlicense': this.model.get("TVLicense"),
                    'phoneline': landline

                }
            }).done(function () {
           
            totalbill =  (that.Coupon.get("TotalBill") * that.model.get("Housemates")).toFixed(2);
            personbill = (that.Coupon.get("TotalBill")).toFixed(2);
            if (totalbill != 0) {
                $(".total_prices_services").show();
                $(".pricePerson").text("£" + personbill);
                $(".priceHouse").text("£" + totalbill);
                $(".total_price_person").text("£" + personbill);
                $(".total_price_house").text("£" + totalbill);

                that.model.set({ "TotalBill": that.Coupon.get("TotalBill") });

            }
            else {
                $(".total_prices_services").hide();
                $(".pricePerson").text("£0");
                $(".priceHouse").text("£0");
            }

            if (Electricity == true) {
                elprice = that.Coupon.get("Electricity").toFixed(2);
                $('#fee-Electricity').find(".price-person").html(elprice);

                $(".price-person-Electricity").text(elprice);
                $(".price-house-Electricity").text((elprice * that.model.get("Housemates")).toFixed(2));
            }
            if (Gas == true) {
                gasprice = that.Coupon.get("Gas").toFixed(2);
                $('#fee-Gas').find(".price-person").html(gasprice);

                $(".price-person-Gas").text(gasprice);
                $(".price-house-Gas").text((gasprice * that.model.get("Housemates")).toFixed(2));
            }
            if (Water == true) {
                waterprice = that.Coupon.get("Water").toFixed(2);
                $('#fee-Water').find(".price-person").html(waterprice);

                $(".price-person-Water").text(waterprice)
                $(".price-house-Water").text((waterprice * that.model.get("Housemates")).toFixed(2))
            }          
            if (Broadband == true) {
                if (BroadbandType == 1) {
                    broadbandPrice += that.Coupon.get("BroadbandADSL") + that.Coupon.get("LineRental");

                    $(".price-person-ADSL20").text(broadbandPrice.toFixed(2));
                    $(".price-house-ADSL20").text((broadbandPrice * that.model.get("Housemates")).toFixed(2));
                }
                else if (BroadbandType == 2) {
                    broadbandPrice += that.Coupon.get("BroadbandFibre40") + that.Coupon.get("LineRental");

                    $(".price-person-Fibre40").text(broadbandPrice.toFixed(2));
                    $(".price-house-Fibre40").text((broadbandPrice * that.model.get("Housemates")).toFixed(2));
                }
                $('#fee-Internet').find(".price-person").html(broadbandPrice.toFixed(2));
            }
            if (SkyTV == true) {
                skyprice = that.Coupon.get("SkyTV").toFixed(2);

                $(".price-person-SkyTV").text(skyprice);
                $(".price-house-SkyTV").text((skyprice * that.model.get("Housemates")).toFixed(2));
            }
            if (TVLicense == true) {
                tvlicenseprice = that.Coupon.get("TVLicense").toFixed(2);

                $(".price-person-TVLicense").text(tvlicenseprice);
                $(".price-house-TVLicense").text((tvlicenseprice * that.model.get("Housemates")).toFixed(2));
            }
            if (skyprice != 0 || tvlicenseprice != 0) {
                tvprice += that.model.get("SkyTV") ? that.Coupon.get("SkyTV") : 0;
                tvprice += that.model.get("TVLicense") ? that.Coupon.get("TVLicense") : 0;
                $('#fee-tv').find(".price-person").html(tvprice.toFixed(2));

            }
            if (LandlinePhone == true) {
                if (Broadband == false) {
                    var lineRental = that.Coupon.get("LineRental");
                    landlinePrice += lineRental;
                }
                if (LandlinePhoneType == 1) {
                    landlinePrice += that.Coupon.get("LandlinePhoneBasic");
                    var totalLandlineBasic = landlinePrice * that.model.get("Housemates");
                    $(".price-person-Basic").text(landlinePrice.toFixed(2));
                    $(".price-house-Basic").text(totalLandlineBasic.toFixed(2));
                }
                else if (LandlinePhoneType == 2) {
                    landlinePrice += that.Coupon.get("LandlinePhoneMedium");
                    var totalLandlineMedium = landlinePrice * that.model.get("Housemates");
                    $(".price-person-Medium").text(landlinePrice.toFixed(2));
                    $(".price-house-Medium").text(totalLandlineMedium.toFixed(2));
                }
                $('#fee-phone').find(".price-person").html(landlinePrice.toFixed(2));
            }

            });
        },
        createServiceForBasket: function (service, color, icon) {
            if(service == "SkyTV")
            {
                $("<tr class='added-service-" + service + "'>").append("<td class='text-center'>SKY TV<br/><div class='basket-service' style='background-color:#374F6E;fill: white'> <svg xmlns:svg='http://www.w3.org/2000/svg' xmlns='http://www.w3.org/2000/svg' version='1.0' width='30px' style='margin-top: 2px;' viewBox='-19.11 -19.11 1101.22 675.22' id='svg2521'><defs id='defs2523' /><path class='sky' d='M 394.90627,0 C 374.03247,0 359.15627,10.3904 359.15627,35.9375 L 359.15627,486.09375 C 359.15627,496.11256 365.79197,500.375 374.18747,500.375 L 426.59377,500.375 C 447.54947,500.37499 462.37497,489.9385 462.37497,464.375 L 462.37497,14.28125 C 462.37497,4.21325 455.69007,0 447.34377,0 L 394.90627,0 z M 121.75,153.46875 C 41.9112,153.46875 0,199.7226 0,258.6875 C 0,311.09351 32.2622,343.0109 96.6875,357.375 L 199.5312,380.375 C 220.4379,385.08109 226.9375,390.9667 226.9375,401.625 C 226.9375,409.80729 220.4359,416.34375 201.5625,416.34375 L 42.0937,416.34375 C 16.5302,416.34374 6.25,431.159 6.25,452 L 6.25,485.3125 C 6.25,493.65869 10.3874,500.375 20.4062,500.375 L 195.3437,500.375 C 289.8253,500.37499 327.96873,454.0469 327.96873,399.3125 C 327.96873,342.693 295.2489,310.80005 231.25,296.46875 L 128.375,273.40625 C 107.4684,268.73366 101,262.84735 101,252.15625 C 101,244.00745 107.4852,237.5 126.375,237.5 L 279.4062,237.5 C 304.90414,237.5 315.21873,222.6403 315.21873,201.75 L 315.21873,168.53125 C 315.21874,160.16855 311.06488,153.46875 301.06248,153.46875 L 121.75,153.46875 z M 635.74997,153.46875 C 619.99207,153.46875 611.59957,157.682 602.18747,167.75 L 468.68747,311.09375 L 588.43747,485.59375 C 595.35717,495.66176 601.13217,500.375 611.59377,500.375 L 682.93747,500.375 C 693.61217,500.37499 699.81247,496.1107 699.81247,487.125 C 699.81247,481.61539 697.13557,478.03555 693.59377,472.96875 L 588.43747,317.3125 L 668.28127,231.3125 L 825.90627,464.375 L 756.21877,611.6875 C 754.30027,616.0164 752.06247,620.19367 752.06247,624.375 C 752.06247,632.88523 758.30227,637 766.81247,637 L 830.03127,637 C 842.91957,637.00001 848.47827,632.55064 853.24997,622.1875 L 1058.8125,178.875 C 1060.8458,174.5133 1063,170.38685 1063,166.15625 C 1063,157.66245 1056.8243,153.46875 1048.2813,153.46875 L 984.56247,153.46875 C 971.80517,153.46875 966.01537,157.96485 961.37497,168.34375 L 871.46877,367.9375 L 746.06247,170.40625 C 738.32287,158.15805 730.15707,153.46875 711.56247,153.46875 L 635.74997,153.46875 z' /></svg></div></td><td style='vertical-align: middle; width: 112px;' class='text-center'><span class='par personp'>£<span class='price-person-" + service + "'></span> </span><span class='par housep' style='display:none'>£<span class='price-house-" + service + "'></span> </span></td>").appendTo(".basket_services");
            }
            else
            {
                $("<tr class='added-service-" + service + "'>").append("<td class='text-center'>"+service+"<Br/><div class='basket-service' style='background-color:" + color + ";'><i class='fa fa-" + icon + "'></i></div></td><td style='vertical-align: middle; width: 112px;' class='text-center'><span class='par personp'>£<span class='price-person-" + service + "'></span> </span><span class='par housep' style='display:none'>£<span class='price-house-" + service + "'></span> </span></td>").appendTo(".basket_services");
            }

            if ($(".get_price").prop("checked") == false) {
                $(".housep").show();
                $(".personp").hide();
            }
            else {
                $(".housep").hide();
                $(".personp").show();
            }
            $(".dropdown-toggle").addClass("blinker");
            setTimeout(function () {
                $(".dropdown-toggle").removeClass("blinker");
            }, 2000);

        },
        removeFromBasket: function(target) {
            total_person -= $(".price-person-" + target).text();
            total_house -= $(".price-house-" + target).text();
            $(".pricePerson").text("$" + total_person);
            $(".priceHouse").text("$" + total_house);
            $(".added-service-" + target).remove();
        },
        checkServiceFromModel: function () {
            $(".basket_services").empty();
            if (this.model.get("Electricity") == true) {
                $("#Electricity").prop("checked", true).trigger('change');
                this.createServiceForBasket("Electricity", "#E74B3C", "bolt");
            }
            if (this.model.get("Gas") == true) {
                $("#Gas").prop("checked", true).trigger('change');
                this.createServiceForBasket("Gas", "#F29B11", "fire");
            }
            if (this.model.get("Water") == true) {
                $("#Water").prop("checked", true).trigger('change');
                this.createServiceForBasket("Water", "#3596DA", "tint");
            }
            if (this.model.get("Broadband") == true) {
                console.log(this.model.get("Broadband"));
                if (this.model.get("BroadbandType") == 1) {
                    console.log(this.model.get("BroadbandType"));
                    $("#BroadbandTypeADSL20").prop("checked", true).trigger('change');
                    if (!$("#BroadbandTypeADSL20").attr('value') == true) {
                        this.createServiceForBasket("ADSL20", "#19937B", "wifi");
                    }
                }
                if (this.model.get("BroadbandType") == 2) {
                    $("#BroadbandTypeFibre40").prop("checked", true).trigger('change');
                    if (!$("#BroadbandTypeFibre40").attr('value') == true) {
                        this.createServiceForBasket("Fibre40", "#19937B", "wifi");
                    }
                }
                $("#Internet").prop("checked", true).trigger('change');
                $(".Internet").addClass("Internet-active");
                $(".Internet-info").find(".front-content").addClass("front-transform");
                $(".Internet-info").find(".back-content").addClass("back-transform");
            }
            if (this.model.get("LandlinePhone") == true) {
                if (this.model.get("LandlinePhoneType") == 1) {
                    $("#LandlineTypeBasic").prop("checked", true).trigger('change');
                    if (!$("#LandlineTypeBasic").attr('value') == true) {
                        this.createServiceForBasket("Basic", "#19937B", "phone");
                    }
                }
                if (this.model.get("LandlinePhoneType") == 2) {
                    $("#LandlineTypeMedium").prop("checked", true).trigger('change');
                    if (!$("#LandlineTypeMedium").attr('value') == true) {
                        this.createServiceForBasket("Medium", "#19937B", "phone");
                    }
                }
                $("#Telephone").prop("checked", true).trigger('change');
                $(".Telephone").addClass("Telephone-active");
                $(".Telephone-info").find(".front-content").addClass("front-transform");
                $(".Telephone-info").find(".back-content").addClass("back-transform");

            }
            if (this.model.get("SkyTV") == true) {
                $("#Television").prop("checked", true).trigger('change');
                $("#SkyTV").prop("checked", true).trigger('change');
                $(".Television").addClass("Television-active");
                $(".Television-info").find(".front-content").addClass("front-transform");
                $(".Television-info").find(".back-content").addClass("back-transform");
                if (!$("#SkyTV").attr('value') == true) {
                    this.createServiceForBasket("SkyTV", "#7863E0", "desktop");
                }
            }
            if (this.model.get("TVLicense") == true) {
                $("#TVLicense").prop("checked", true).trigger('change');
                $("#Television").prop("checked", true).trigger('change');
                $(".Television").addClass("Television-active");
                $(".Television-info").find(".front-content").addClass("front-transform");
                $(".Television-info").find(".back-content").addClass("back-transform");
                if (!$("#TVLicense").attr('value') == true) {
                    this.createServiceForBasket("TVLicense", "#7863E0", "desktop");
                }
            }
            this.checkCoupon();
        }


    });
    App.View.ServicesDetails = ServicesDetails;


})(window.App);