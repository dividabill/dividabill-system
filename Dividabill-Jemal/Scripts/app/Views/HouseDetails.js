﻿(function (App) {
    'use strict';

    var HouseDetails = Marionette.LayoutView.extend({
        template: "#housedetails-tpl",
        regions: {
            Select: "#select_building"
        },
        events: {
            'click .quick-find': "FindPostcode",
            'change #building' : "SendPostcode",
            'change #Ongoing'  : "ChangeOngoing",
            'change #StartTime': "ChangeStartTime",
            'change #Period'   : "ChangePeriod",
            'change #Housemates': "ChangeHousemates",
            'keyup #PostCode': 'changePostcode',
            'change #Coupon': 'changeCoupon'
        },

        initialize: function (options) {
            console.log("Init House Details");
            if (this.model.get("Postcode") != "") {
                this.FindPostcode();
            }
        },
        onShow: function () {
            var that = this;
            $("#StartTime").datepicker();

            $('.selectpicker').selectpicker();
            $('[data-toggle="popover"]').popover();

            this.HouseModel = new App.Model.House();
            if (this.model.get("Postcode") != "") {
                $("#PostCode").val(this.model.get("Postcode"));
            }
            if (this.model.get("Coupon") != "") {
                $("#Coupon").val(this.model.get("Coupon"));
                $("#Coupon").attr("readonly", true)
            }
        },
        FindPostcode: function () {
            if ($("#PostCode").val() == "")
                return;
            var that = this;
            this.Postcode = new App.Model.Postcode();
            this.postcodeCollection = new App.Model.PostcodeCollection([], { id: this.model.get('Postcode') });
            if (!$("#quick-find").hasClass("fa-spinner")) {
                $("#quick-find").addClass("fa-spinner fa-spin");
                this.postcodeCollection.fetch().done(function () {
                    that.Select.show(new App.View.SelectBuilding({ collection: that.postcodeCollection }));
                    $("#quick-find").removeClass("fa-spinner fa-spin");
                    $("#building").selectpicker('refresh');
                    $(".choose-building").show();
                    if (that.model.get("Udprn") != "") {
                        $("#building").val(that.model.get("Udprn")).change();
                        $("#PostCode").val(that.model.get("Postcode"));
                        $("#filled-signup-info").removeClass("hidden");
                        $("#StartTime").val(that.model.get("StartTime"));
                        $("#Period").val(that.model.get("Period")).change();
                        $("#Housemates").val(that.model.get("Housemates")).change();
                        $("#Ongoing").val(that.model.get("Ongoing")).change();
                    }
                }).fail(function () {
                    $("#quick-find").removeClass("fa-spinner fa-spin");
                    $('#postcodeError').modal('toggle');                    
                });
            }
        },
        SendPostcode: function () {
            var that = this;
            this.model.set({ 'Udprn': $('#building').val() });

            this.HouseModel.set({ 'id': this.model.get("Udprn") });
            this.HouseModel.fetch().done(function () {
                $("#StartTime").val(that.HouseModel.get("StartDate"));
                $("#Housemates").val(that.HouseModel.get("Housemates")).change();
                
                that.model.set({ "Broadband": that.HouseModel.get("Broadband") });
                that.model.set({ "BroadbandType": that.HouseModel.get("BroadbandType") });
                that.model.set({ "SkyTV": that.HouseModel.get("SkyTV") });
                that.model.set({ "TVLicense": that.HouseModel.get("TVLicense") });
                that.model.set({ "LandlinePhone": that.HouseModel.get("LandlinePhone") });
                that.model.set({ "LandlinePhoneType": that.HouseModel.get("LandlinePhoneType") });
                that.model.set({ "Electricity": that.HouseModel.get("Electricity") });
                that.model.set({ "Water": that.HouseModel.get("Water") });
                that.model.set({ "Gas": that.HouseModel.get("Gas") });
                that.model.set({ "StartTime": that.HouseModel.get("StartDate") });
                $("#Period").val(that.HouseModel.get("Period")).change();

                that.model.set({ 'Changed': true });
            }).fail(function () {
                that.model.set({ 'Postcode': $("#PostCode").val() });
                that.model.set({ 'Udprn': $("#building").val() });
            });
            if (this.model.get("Ongoing") == true) {
                $("#Ongoing").prop("checked", true);
            }
            else {
                $("#Ongoing").prop("checked", false);
            }
            that.model.on("change:Udprn", function (model, Udprn) {
                if (that.model.previous("Udprn") != Udprn) {
                    $("#StartTime").datepicker("clearDates")
                    $("#Period").val("").change();
                    $("#Housemates").val("").change();
                    $("#Ongoing").prop("checked", false);
                }

            });
            $("#filled-signup-info").removeClass("hidden");
            $("#continue_step2").show();
        },
        ChangeOngoing: function () {
            if ($('#Ongoing').is(':checked')) {
                $('#Period').prop('disabled', true);
                this.model.set({ 'Ongoing': true });
                this.model.unset("Period", { silent: true });
                $("#Period").val("").change();
            }
            else
            {
                $('#Period').prop('disabled', false);
                this.model.unset("Ongoing", { silent: true });
                this.model.set({ 'Period': $("#Period").val() });
            }
            $('#Period').selectpicker('refresh');   
        },
        changePostcode: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'Postcode': value })
            if (ev.keyCode == 13) {
                this.FindPostcode();
            }
        },
        ChangeStartTime: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'StartTime': value });

            //console.log("StartTime: " + this.model.get('StartTime'));
        },
        ChangePeriod: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'Period': value });
            //console.log("Period: " + this.model.get('Period'));
        },
        changeCoupon: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ "Coupon": value });
            //console.log("Coupon" + value);
        },
        ChangeHousemates: function(ev){
            var value = $(ev.target).val();
            this.model.set({ "Housemates": value });
            //console.log("Housemates: " + this.model.get('Housemates'));
        }
    });

    App.View.HouseDetails = HouseDetails;

})(window.App);