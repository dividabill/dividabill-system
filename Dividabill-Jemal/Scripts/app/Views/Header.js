﻿(function (App) {
    'use strict';

    var HeaderBar = Marionette.LayoutView.extend({
        template: "#header-tpl",

        events: {
            'click .nav-button': "showContent",
            'click .basket': "dropdownStayOpen",
            'change .get_price': 'changePrice',
            'click .drop_cart': 'openBasket'

        },
        initialize: function () {
            console.log("Init Content")
        },
        onShow: function () {
        },
         showContent: function (e) {
            e.preventDefault();
            var link = $(e.currentTarget).data("target");
            App.Router.navigate(link, { trigger: true });
         },
         changePrice: function (e) {
             if ($(e.target).prop("checked") == false) {
                 $(".housep").show();
                 $(".personp").hide();
             }
             else {
                 $(".housep").hide();
                 $(".personp").show();
             }
         },
         openBasket: function (e) {
             $(e.target).parent().toggleClass("open");
             console.log(e.target);
            
         }
    });
    App.View.HeaderBar = HeaderBar;

})(window.App);