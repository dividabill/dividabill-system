﻿(function (App) {
    'use strict';

    var AboutUs = Marionette.LayoutView.extend({
        template: "#aboutus-tpl",
        initialize: function () {
            console.log("Init About Us")
        }
    });

    App.View.AboutUs = AboutUs;

})(window.App);
