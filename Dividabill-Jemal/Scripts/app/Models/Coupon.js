﻿(function (App) {
    
    var Coupon = Backbone.Model.extend({

        url: function() {
            return 'https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Coupon';
        }
    });

    App.Model.Coupon = Coupon;
})(window.App);