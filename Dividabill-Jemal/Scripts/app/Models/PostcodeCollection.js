﻿(function (App) {
    'use strict';
    var PostcodeCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            this.id = options.id;
        },   
        url: function(){
            return "https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Address/Get/" + this.id
        },
        model: App.Model.Postcode,

    });

    App.Model.PostcodeCollection = PostcodeCollection;
})(window.App);