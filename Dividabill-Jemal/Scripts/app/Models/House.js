﻿(function (App) {
    
    var House = Backbone.Model.extend({
        urlRoot: 'https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/House/GetHouse/'
    });

    App.Model.House = House;
})(window.App);