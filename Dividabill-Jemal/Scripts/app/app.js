﻿// Create our Application
var App = new Backbone.Marionette.Application();
_.extend(App, {
    Controller: {},
    View: {},
    Model: {},
    Page: {},
    Localization: {},
});

App.addRegions({
    Header: ".header-section",
    Window: ".main-region",
    Footer: "#footer"
});

//Keeps a list of stacked views
App.ViewStack = [];

var initTemplates = function () {
    // Load in external templates
    var ts = [];

    _.each(document.querySelectorAll('[type="text/x-template"]'), function (el) {
        var d = Q.defer();
        $.get(el.src, function (res) {
            el.innerHTML = res;
            d.resolve(true);
        });
        ts.push(d.promise);
    });

    return Q.all(ts);
};

var addRouter = function () {
    new App.Router();
    //Backbone.history.start();
}

App.on('start', function () {
    Backbone.history.start({ pushState: true })


    App.Header.show(new App.View.HeaderBar());
    App.Footer.show(new App.View.FooterView());

});

App.addInitializer(function (options) {
    initTemplates().then(addRouter);
});