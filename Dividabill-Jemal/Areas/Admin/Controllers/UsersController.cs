﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dividabill.Models;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using Dividabill.DAL;
using System.Text;
using System.Reflection;
using System.Data.SqlTypes;

namespace Dividabill.Areas.Admin.Controllers
{
    public class UsersController : AdminController
    {
        // GET: Admin/Users
        public ActionResult Index()
        {
            return All();
        }

        // GET: Admin/Users/Active
        public ActionResult Active()
        {
            return View("Index", unitOfWork.UserRepository.GetActiveClients().ToList());
        }
        
        // GET: Admin/Users/All
        public ActionResult All()
        {
            return View("Index", unitOfWork.UserRepository.Get().ToList());
        }

        // GET: Admin/Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = unitOfWork.UserRepository.GetByID(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Admin/Users/Create
        public ActionResult Create()
        {

            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.GetHouses().ToList(), "ID", "Address.Line1");

            return View();
        }

        // POST: Admin/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RefTitle,FirstName,LastName,AccountNumber,AccountSortCode,AccountHolder,AccountSoleHolder,DOB,HomeAddress,Note,NewsletterSubscription,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] User user)
        {
            user.UserName = user.Email;
            if (ModelState.IsValid)
            {
                unitOfWork.UserRepository.Insert(user);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Admin/Users/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = unitOfWork.UserRepository.GetByID(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.GetHouses().ToList(), "ID", "Address.Line1");
            return View(user);
        }

        // POST: Admin/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RefTitle,FirstName,LastName,AccountNumber,AccountSortCode,AccountHolder,AccountSoleHolder,DOB,HomeAddress,Note,NewsletterSubscription,Email,PhoneNumber,House_ID,DDRef")] User user)
        {
            if (ModelState.IsValid)
            {
                if (user.SignupDate == DateTime.MinValue)
                {
                    user.SignupDate = SqlDateTime.MinValue.Value;
                }
                user.UserName = user.Email;
                unitOfWork.UserRepository.Update(user);
                unitOfWork.Save();

                return RedirectToAction("Index");
            } 
            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.GetHouses().ToList(), "ID", "Address.Line1");

            return View(user);
        }

        // GET: Admin/Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = unitOfWork.UserRepository.GetByID(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Admin/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            unitOfWork.UserRepository.Delete(id);
            unitOfWork.Save();

            return RedirectToAction("Index");
        }


        public ActionResult Export(string FileType = "Excel", string Type = "Clients", int days = 100, int houseID = 0)
        {

            var clients = new System.Data.DataTable("Clients");

            clients.Columns.Add("Id", typeof(string));
            clients.Columns.Add("Email", typeof(string));
            clients.Columns.Add("Title", typeof(string));
            clients.Columns.Add("FirstName", typeof(string));
            clients.Columns.Add("LastName", typeof(string));
            clients.Columns.Add("AccountNumber", typeof(string));
            clients.Columns.Add("AccountSortCode", typeof(string));
            clients.Columns.Add("AccountHolder", typeof(string));
            clients.Columns.Add("AccountSoleHolder", typeof(bool));
            clients.Columns.Add("DOB", typeof(DateTime));
            clients.Columns.Add("HomeAddress", typeof(string));
            clients.Columns.Add("PhoneNumber", typeof(string));

            //House addin
            if (houseID != 0)
            {

                clients.Columns.Add("Housemates", typeof(int));
                clients.Columns.Add("Address", typeof(string));
                clients.Columns.Add("Electricity", typeof(bool));
                clients.Columns.Add("Gas", typeof(bool));
                clients.Columns.Add("Water", typeof(bool));
                clients.Columns.Add("Media", typeof(bool));
                clients.Columns.Add("StartDate", typeof(DateTime));
                clients.Columns.Add("EndDate", typeof(DateTime));
                clients.Columns.Add("RegisteredDate", typeof(DateTime));
                clients.Columns.Add("Coupon", typeof(string));
            }

            List<User> dbClients;
            switch (Type)
            {
                default:
                    dbClients = unitOfWork.UserRepository.GetActiveClients().ToList();
                    break;
            }
            if (houseID != 0)
            {
                dbClients = dbClients.Where(u => u.House_ID == houseID).ToList();
            }
            foreach (User client in dbClients)
            {
                if (houseID != 0)
                {

                    clients.Rows.Add(client.Id, client.Email, client.RefTitle, client.FirstName, client.LastName, client.AccountNumber,
                        client.AccountSortCode, client.AccountHolder, client.AccountSoleHolder, client.DOB, client.HomeAddress,
                        client.PhoneNumber, client.House.Housemates, client.House.Address, client.House.Electricity, client.House.Gas,
                        client.House.Water, client.House.Media, client.House.StartDate.Date, client.House.EndDate.Date, client.House.RegisteredDate.Date,
                        (client.House.Coupon != null) ? client.House.Coupon.Code : "");
                }
                else
                {
                    clients.Rows.Add(client.House.ID, client.Email, client.RefTitle, client.FirstName, client.LastName, client.AccountNumber,
                        client.AccountSortCode, client.AccountHolder, client.AccountSoleHolder, client.DOB, client.HomeAddress, client.PhoneNumber);
                }
            }


            var grid = new GridView();
            grid.DataSource = clients;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Clients.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }
        public ActionResult Export2(string FileType = "Excel", string Type = "Clients", int days = 100, int houseID = 0)
        {


            List<User> dbClients = unitOfWork.UserRepository.GetActiveClients().Where(u => u.DDRef == null || u.DDRef.StartsWith("DBILL") == false).ToList();
            List<EazyCollectPerson> eazycollect = new List<EazyCollectPerson>();
            int i=0;
            foreach (User client in dbClients)
            {
                i++;
                client.DDRef = i.ToString();
                //unitOfWork.Save();
                eazycollect.Add(
                    new EazyCollectPerson
                    {
                        contract = new Contract
                        {
                            IsGiftAid = false,
                            PaymentDayInMonth = "1",
                            Start = "01/11/2014",
                            Every = "1"
                        },
                        customer = new Customer
                        {
                            FirstName = client.FirstName,
                            Surname = client.LastName,
                            DateOfBirth = client.DOB.ToShortDateString(),
                            Email = client.Email,
                            AccountHolderName = client.AccountHolder,
                            AccountNumber = client.AccountNumber,
                            BankSortCode = client.AccountSortCode.Replace(" ", "").Replace("-",""),
                            Line1 = client.HomeAddress.Line1,
                            Line2 = client.HomeAddress.Line2,
                            Line3 = client.HomeAddress.Line3,
                            Line4 = client.HomeAddress.Line4,
                            MobilePhoneNumber = client.PhoneNumber,
                            PostCode = client.HomeAddress.Postcode,
                            Title = client.RefTitle,
                            CompanyName = "",
                            Initials = client.FirstName.Substring(0,1) + client.LastName.Substring(0,1),
                            HomePhoneNumber= "",
                            CustomerRef = "",

                        },
                        schedule = new Schedule {
                            Name = "DD Dates 1/15 - Adhoc"
                        }
                    }
                    );
            }

            string csv = GetCSV(eazycollect);

            //string csv = String.Join(",", eazycollect.Select(x => x.ToString()).ToArray());

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Clients.csv");
            //Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }


        private static string GetCSV(List<EazyCollectPerson> list)
        {
            StringBuilder csvData = null;
            try
            {
                csvData = new StringBuilder();

                //Get the properties for type T for the headers
                PropertyInfo[] propInfos = typeof(Customer).GetProperties();
                PropertyInfo[] propInfos2 = typeof(Schedule).GetProperties();
                PropertyInfo[] propInfos3 = typeof(Contract).GetProperties();
                for (int i = 0; i <= propInfos.Length - 1; i++)
                {
                    csvData.Append(propInfos[i].Name);
                        csvData.Append(",");
                }

                for (int i = 0; i <= propInfos2.Length - 1; i++)
                {
                    csvData.Append(propInfos2[i].Name);
                    if ((i < (propInfos2.Length - 1)) || (propInfos2.Length == 1))
                    {
                        csvData.Append(",");
                    }
                }
                for (int i = 0; i <= propInfos3.Length - 1; i++)
                {
                    csvData.Append(propInfos3[i].Name);
                    if (i < (propInfos3.Length - 1))
                    {
                        csvData.Append(",");
                    }
                }
                csvData.AppendLine();

                //Loop through the collection, then the properties and add the values
                for (int i = 0; i <= list.Count - 1; i++)
                {
                    Customer item = list[i].customer;
                    for (int j = 0; j <= propInfos.Length - 1; j++)
                    {
                        object csvProperty = item.GetType().GetProperty(propInfos[j].Name).GetValue(item, null);
                        if (csvProperty != null)
                        {
                            string value = csvProperty.ToString();
                            //Check if the value contans a comma and place it in quotes if so
                            if (value.Contains("+44"))
                            {
                                value = value.Replace("+44", "0");
                            }
                            if (value.Contains("'"))
                            {
                                value = value.Replace("'", "");
                            }
                            if (value.StartsWith("44"))
                            {
                                value = value.Replace("44", "0");
                            }
                            if (value.Contains(","))
                            {
                                value = string.Concat("\"", value, "\"");
                            }
                            //Replace any \r or \n special characters from a new line with a space
                            if (value.Contains("\r"))
                            {
                                value = value.Replace("\r", " ");
                            }
                            if (value.Contains("\n"))
                            {
                                value = value.Replace("\n", " ");
                            }
                            csvData.Append(value);
                        }
                            csvData.Append(",");
                    }


                    Schedule item2 = list[i].schedule;
                    for (int j = 0; j <= propInfos2.Length - 1; j++)
                    {
                        object csvProperty = item2.GetType().GetProperty(propInfos2[j].Name).GetValue(item2, null);
                        if (csvProperty != null)
                        {
                            string value = csvProperty.ToString();
                            //Check if the value contans a comma and place it in quotes if so
                            if (value.Contains(","))
                            {
                                value = string.Concat("\"", value, "\"");
                            }
                            //Replace any \r or \n special characters from a new line with a space
                            if (value.Contains("\r"))
                            {
                                value = value.Replace("\r", " ");
                            }
                            if (value.Contains("\n"))
                            {
                                value = value.Replace("\n", " ");
                            }
                            csvData.Append(value);
                        }
                            csvData.Append(",");
                    }


                    Contract item3 = list[i].contract;
                    for (int j = 0; j <= propInfos3.Length - 1; j++)
                    {
                        object csvProperty = item3.GetType().GetProperty(propInfos3[j].Name).GetValue(item3, null);
                        if (csvProperty != null)
                        {
                            string value = csvProperty.ToString();
                            //Check if the value contans a comma and place it in quotes if so
                            if (value.Contains(","))
                            {
                                value = string.Concat("\"", value, "\"");
                            }
                            //Replace any \r or \n special characters from a new line with a space
                            if (value.Contains("\r"))
                            {
                                value = value.Replace("\r", " ");
                            }
                            if (value.Contains("\n"))
                            {
                                value = value.Replace("\n", " ");
                            }
                            csvData.Append(value);
                        }
                        if (j < propInfos3.Length - 1)
                        {
                            csvData.Append(",");
                        }
                    }
                    csvData.AppendLine();
                }
            }
            catch (Exception exGeneral)
            {
                //Exception
            }
            return csvData.ToString();
        }


    }

    public class Customer {
        public String Title{get;set;} public String FirstName{get;set;} public String Surname{get;set;} public String CompanyName{get;set;} public String Initials{get;set;} public String DateOfBirth{get;set;} public String PostCode{get;set;} public String Line1{get;set;} public String Line2{get;set;} public String Line3{get;set;} public String Line4{get;set;} public String HomePhoneNumber{get;set;} public String MobilePhoneNumber{get;set;} public String Email{get;set;} public String CustomerRef{get;set;} public String AccountHolderName{get;set;} public String AccountNumber{get;set;} public String BankSortCode{get;set;}
    }

    public class Contract
    {
        public String PaymentMonthInYear { get; set; }
        public String PaymentDayInMonth { get; set; }
        public String PaymentDayInWeek { get; set; }
        public String Start { get; set; }
        public String TerminationDate { get; set; }
        public String TerminationType { get; set; }
        public String AtTheEnd { get; set; }
        public String NumberOfDebits { get; set; }
        public String InitialAmount { get; set; }
        public String ExtraInitialAmounts { get; set; }
        public String Amount { get; set; }
        public String FinalAmount { get; set; }
        public String Every { get; set; }
        public String DirectDebitRefSuffix { get; set; }
        public String CustomDirectDebitRef { get; set; }
        public String AdditionalReference { get; set; }
        public Boolean IsGiftAid { get; set; }

    }

    public class Schedule
    {
        public String Name { get; set; } 

    }

    public class EazyCollectPerson
    {
        public Customer customer {get;set;}
        public Schedule schedule {get;set;}
        public Contract contract {get;set;}
    }


}
