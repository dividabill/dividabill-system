﻿using Dividabill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dividabill.Helpers;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class BillsViewModel
    {
        public int BillID { get; set; }
        public HouseModel House { get; set; }
        public decimal GasBill { get; set; }
        public decimal ElectricBill { get; set; }
        public decimal WaterBill { get; set; }
        public decimal ServiceCharge { get; set; }
        public decimal GrossTotalBill { get { return ElectricBill + GasBill + WaterBill + ServiceCharge; } }
        public decimal NetTotalBill { get { return Dividabill.Helpers.Functions.grossPrice(ElectricBill, GasBill, WaterBill, House.Housemates); } }
        public decimal? Adjustment { get; set; }
        public String Note { get; set; }
    }
}