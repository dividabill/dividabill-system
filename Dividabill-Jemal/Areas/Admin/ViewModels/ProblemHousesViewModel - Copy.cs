﻿using Dividabill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dividabill.Helpers;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class ProblemHousesViewModel2
    {
        public int ID { get; set; }
        public Address HouseAddress { get; set; }
        public int RegisteredTenants { get; set;}
        public int TotalTenants { get; set; }

        public DateTime RegisteredDate { get; set; }
    }
}