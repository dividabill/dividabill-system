﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class INotificationViewModel
    {
        public String Title { get; set; }
        public String Type { get; set; }
        public String Created { get; set; }
        public String Link { get; set; }
        public String Action { get;set;}
    }
    public class NotificationViewModel: INotificationViewModel
    {
    }

    //public enum NotificationType {
    //    Danger = "label-danger", Success = "label-success", Default = "label-default", Primary = "label-primary", Info = "label-info", Warning = "label-warning"
    //}
}