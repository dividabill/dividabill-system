﻿using DividaBill.Models;
using DividaBill.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SendGrid;
using DividaBill.DAL;

namespace DividaBill.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {
        [OutputCache(Location=System.Web.UI.OutputCacheLocation.Any, Duration=36000)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Services()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult Partners()
        {
            return RedirectToActionPermanent("Business");
        }

        public ActionResult Business()
        {
            return View();
        }

        public ActionResult WhyItWorks()
        {
            return View();
        }

        public ActionResult Pricing()
        {
            return RedirectToActionPermanent("Services");
        }

        public ActionResult Questions()
        {
            return View();
        }

        public ActionResult TermsOfUse()
        {
            return View();
        }
        public ActionResult Customers()
        {
            return View();
        }
        public ActionResult WhosItFor()
        {
            return RedirectToActionPermanent("Customers");
        }

        public ActionResult Blog()
        {
            return View();
        }
        
        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WorkWithUs(WorkWithUsViewModel msg)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Message newMsg = new Message { Email = msg.Email, MessageText = msg.MessageText, Name = msg.Name, Subject = msg.Subject, Created = DateTime.Now };
                db.Messages.Add(newMsg);
                db.SaveChanges();
            }
            return View("Success");
        }

        public ActionResult Media()
        {
            return View();
        }

        public ActionResult Terms()
        {
            //Response.AppendHeader("Content-Disposition", "inline; filename=something.pdf");
            //ActionResultreturn File("something.pdf", "application/pdf",);

            return File("~/Content/Documents/TermsAndConditions.pdf", System.Net.Mime.MediaTypeNames.Application.Pdf);
        }

        public ActionResult About()
        {
            return RedirectToActionPermanent("AboutUs");
        }

        public ActionResult FAQ()
        {
            return RedirectToActionPermanent("Questions");
        }
    }
}