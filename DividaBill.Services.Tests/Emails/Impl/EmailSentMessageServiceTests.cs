﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails.Impl;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Emails.Impl
{
    [TestFixture]
    public class EmailSentMessageServiceTests : BaseAutoMock<EmailSentMessageService>
    {
        [Test]
        public void GivenUserEmail_WhenInsertInvoked_ThenSendToEmailSetToThisEmail()
        {
            var email = Guid.NewGuid().ToString();
            var user = new User
            {
                Email = email,
                Id = Guid.NewGuid().ToString()
            };
            var emailSentMessage = new EmailSentMessage
            {
                UserId = user.Id
            };

            var userRepository = new Mock<UserRepository>(GetMock<IApplicationContext>().Object);
            userRepository.Setup(x => x.Get()).Returns(new List<User> {user}.AsQueryable());

            var emailSentMessageRepository = GetMock<GenericRepository<EmailSentMessage, User, IApplicationContext>>();
            emailSentMessageRepository.Setup(x => x.Insert(It.IsAny<EmailSentMessage>())).Returns(emailSentMessage);

            GetMock<IUnitOfWork>().Setup(x => x.EmailSentMessageRepository).Returns(emailSentMessageRepository.Object);
            GetMock<IUnitOfWork>().Setup(x => x.UserRepository).Returns(userRepository.Object);

            var result = ClassUnderTest.Insert(emailSentMessage);

            Assert.IsNull(result.SendToEmail);
        }

        [Test]
        public void WhenInsertInvoked_ThenCreatedAtAndLastModifiedNotNull()
        {
            var emailSentMessage = new EmailSentMessage
            {
                SendToEmail = Guid.NewGuid().ToString()
            };

            var emailSentMessageRepository = GetMock<GenericRepository<EmailSentMessage, User, IApplicationContext>>();
            emailSentMessageRepository.Setup(x => x.Insert(It.IsAny<EmailSentMessage>())).Returns(emailSentMessage);

            GetMock<IUnitOfWork>().Setup(x => x.EmailSentMessageRepository).Returns(emailSentMessageRepository.Object);

            var result = ClassUnderTest.Insert(emailSentMessage);

            Assert.NotNull(result.CreatedAt);
            Assert.NotNull(result.LastModified);
        }
    }
}