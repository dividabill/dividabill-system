﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dividabill.Tests.Core;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.ViewModels;
using GoCardless_API.api;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.ThirdPartyServices.Impl
{
    [TestFixture]
    public class GoCardlessThirdPartyServiceTests : BaseAutoMock<GoCardlessThirdPartyService>
    {
        [Test]
        public async Task GivenAnyBankAccountDetails_WhenValidateBankAccountDetailsInvoked_ThenGoCardlessServiceValidateBankAccountDetailsCalledOnce()
        {
            var bankAccountDetails = new BankAccountDetails();
            await ClassUnderTest.ValidateBankAccountDetails(bankAccountDetails);

            GetMock<IGoCardlessBankAccountDetailsService>().Verify(x => x.ValidateBankAccountDetails(bankAccountDetails, It.IsAny<IGoCardlessApiClient>()), Times.Once);
        }

        [Test]
        public async Task GivenAnyBankAccountDetails_WhenValidateBankAccountDetailsInvoked_ThenGoCardlessServiceValidateBankAccountDetailsResultReturned()
        {
            var goCardlessService = GetMock<IGoCardlessBankAccountDetailsService>();

            var uiErrors = new List<UIError>();
            goCardlessService.Setup(x => x.ValidateBankAccountDetails(It.IsAny<BankAccountDetails>(), It.IsAny<IGoCardlessApiClient>())).Returns(Task.FromResult(uiErrors));

            var bankAccountDetails = new BankAccountDetails();
            var result = await ClassUnderTest.ValidateBankAccountDetails(bankAccountDetails);

            Assert.AreSame(uiErrors, result);
        }

        [Test]
        public void GivenAnyPremise_WhenGetBuildingsByPremiseInvoked_ThenNullReturned()
        {
            //GoCardless doesn't provide this service
            var result = ClassUnderTest.GetBuildingsByPremise(123);

            Assert.IsNull(result);
        }

        [Test]
        public void GivenAnyPremise_WhenGetPremisesByPostcodeInvoked_ThenNullReturned()
        {
            //GoCardless doesn't provide this service
            var result = ClassUnderTest.GetPremisesByPostcode(Guid.NewGuid().ToString());

            Assert.IsNull(result);
        }
    }
}