﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using Dividabill.Tests.Core;
using DividaBill.AppConstants;
using DividaBill.DAL.Unity;
using DividaBill.Library.AppConfiguration;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Models;
using DividaBill.Services.ThirdPartyServices.Impl;
using Moq;
using NUnit.Framework;
using SendGrid.API.api;
using SendGrid.API.Models;

namespace DividaBill.Services.Tests.ThirdPartyServices.Impl
{
    [TestFixture]
    public class EmailThirdPartyServiceTests : BaseAutoMock<EmailThirdPartyService>
    {
        [Test]
        public async Task GivenRealEmails_WhenSaveUserMessageInvoked_ThenSavedIntoDB()
        {
            GetMock<IEmailTemplateService>().Setup(x => x.GetByTemplateId(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(new EmailTemplate());
            var sendGridTemplate = new SendGridTemplate
            {
                versions = new List<SendGridTemplateVersion>
                {
                    new SendGridTemplateVersion
                    {
                        active = 1
                    }
                }
            };
            GetMock<ISendGridApiClient>().Setup(x => x.GetSendGridTemplateById(It.IsAny<string>())).Returns(Task.FromResult(sendGridTemplate));

            var emailMessageToSend = new EmailMessageToSend(new EmailConfiguration(), EmailType.SignUpMessages, string.Empty, new Dictionary<string, string>());
            await ClassUnderTest.SaveUserMessage(string.Empty, emailMessageToSend);

            GetMock<IEmailSentMessageService>().Verify(x => x.InsertAndSave(It.IsAny<EmailSentMessage>(), It.IsAny<AsyncExecutionParam>()), Times.Once);
        }

        [Test]
        public async Task GivenRealEmails_WhenSendMessageInvoked_ThenEmailSent()
        {
            var emailConfiguration = new EmailConfiguration
            {
                SendFromEmail = "info@dividabill.co.uk"
            };
            var emailMessageToSend = new EmailMessageToSend(emailConfiguration, EmailType.SignUpMessages, string.Empty, new Dictionary<string, string>());
            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(AppSettingsEnum.SendGridEnabled)).Returns(true.ToString());

            await ClassUnderTest.SendMessage(emailMessageToSend);

            GetMock<ISendGridApiClient>().Verify(x => x.SendEmailFromTemplate(emailMessageToSend.Configuration.TemplateId, emailMessageToSend.Configuration.Subject, It.IsAny<MailAddress>(), emailMessageToSend.SendTo, emailMessageToSend.SubstitutionsList), Times.Once);
        }

        [Test]
        public async Task GivenTestEmails_WhenSaveAndSendEmailInvoked_ThenDoesntSavedIntoDB()
        {
            var emailConfiguration = new EmailConfiguration
            {
                SendFromEmail = "info@dividabill.co.uk"
            };
            var emailMessageToSend = new EmailMessageToSend(emailConfiguration, null, string.Empty, null);
            emailMessageToSend.SetTestSending(true);

            await ClassUnderTest.SaveAndSendEmail(string.Empty, emailMessageToSend);

            var emailTemplateService = GetMock<IEmailSentMessageService>();
            emailTemplateService.Verify(x => x.Insert(It.IsAny<EmailSentMessage>(), It.IsAny<AsyncExecutionParam>()), Times.Never);
            emailTemplateService.Verify(x => x.InsertAndSave(It.IsAny<EmailSentMessage>(), It.IsAny<AsyncExecutionParam>()), Times.Never);
            emailTemplateService.Verify(x => x.Save(It.IsAny<AsyncExecutionParam>()), Times.Never);
        }

        [Test]
        public async Task GivenTestEmails_WhenSaveAndSendEmailInvoked_ThenEmailSent()
        {
            var emailConfiguration = new EmailConfiguration
            {
                SendFromEmail = "info@dividabill.co.uk"
            };
            var emailMessageToSend = new EmailMessageToSend(emailConfiguration, null, string.Empty, null);
            emailMessageToSend.SetTestSending(true);
            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(AppSettingsEnum.SendGridEnabled)).Returns(true.ToString());

            await ClassUnderTest.SaveAndSendEmail(string.Empty, emailMessageToSend);

            GetMock<ISendGridApiClient>().Verify(x => x.SendEmailFromTemplate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<MailAddress>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()), Times.Once);
        }
    }
}