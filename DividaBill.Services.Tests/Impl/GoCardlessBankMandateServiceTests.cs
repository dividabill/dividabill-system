﻿using System;
using System.Threading.Tasks;
using Dividabill.Tests.Core;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using GoCardless_API.api;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class GoCardlessBankMandateServiceTests : BaseAutoMock<GoCardlessBankMandateService>
    {
        [TestCase("failed")]
        [TestCase("expired")]
        public void GivenOldGcMandate_WhenCreateNewGcMandateIfAlreadyOldInvoked_ThenCreateGcMandateInvoked(string gcMandateStatus)
        {
            var gcMandate = new GCMandate
            {
                status = gcMandateStatus
            };
            var tenant = new User
            {
                BankAccountID = Guid.NewGuid().ToString()
            };

            var newInsertedBankMandate = new BankMandate();

            var goCardlessBankMandateService = new Mock<GoCardlessBankMandateService>(
                GetMock<IUnitOfWork>().Object,
                GetMock<IGoCardlessApiClient>().Object,
                GetMock<IBankMandateService>().Object,
                GetMock<IAccountService>().Object,
                GetMock<ITestDataService>().Object
                ) {CallBase = true};
            goCardlessBankMandateService.Setup(x => x.CreateAndInsertBankMandateFromGc(It.IsAny<User>(), It.IsAny<IGoCardlessApiClient>(), It.IsAny<AsyncExecutionParam>())).Returns(Task.FromResult(newInsertedBankMandate));

            var result = goCardlessBankMandateService.Object.CreateNewGcMandateIfAlreadyOld(tenant, gcMandate).Result;

            goCardlessBankMandateService.Verify(x => x.CreateAndInsertBankMandateFromGc(tenant, It.IsAny<IGoCardlessApiClient>(), It.IsAny<AsyncExecutionParam>()), Times.Once);

            Assert.AreSame(result, newInsertedBankMandate);
        }

        [Test]
        public void GivenNotOldGcMandate_WhenCreateNewGcMandateIfAlreadyOldInvoked_ThenNullReturned()
        {
            var gcMandate = new GCMandate
            {
                status = "any valid status"
            };
            var tenant = new User();

            var result = ClassUnderTest.CreateNewGcMandateIfAlreadyOld(tenant, gcMandate).Result;

            var goCardlessBankMandateService = GetMock<IGoCardlessBankMandateService>();
            goCardlessBankMandateService.Verify(x => x.CreateGcMandate(It.IsAny<string>(), It.IsAny<IGoCardlessApiClient>()), Times.Never);
            goCardlessBankMandateService.Verify(x => x.UpdateOrInsertBankMandateFromGcMandate(It.IsAny<User>(), It.IsAny<GCMandate>(), It.IsAny<AsyncExecutionParam>()), Times.Never);

            Assert.IsNull(result);
        }

        [Test]
        public void GivenExistingInDbBankMandate_WhenUpdateOrInsertBankMandateFromGcMandateInvoked_ThenUpdateInvoked()
        {
            var bankMandate = new BankMandate
            {
                Id = Guid.NewGuid().ToString()
            };
            var gcMandate = new GCMandate();

            GetMock<IAccountService>().Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(new BankAccount());

            var bankMandateService = GetMock<IBankMandateService>();
            bankMandateService.Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(bankMandate);

            ClassUnderTest.UpdateOrInsertBankMandateFromGcMandate(new User(), gcMandate);

            bankMandateService.Verify(x => x.UpdateBankMandateFromGcMandate(bankMandate.Id, gcMandate, It.IsAny<AsyncExecutionParam>()), Times.Once);
        }

        [Test]
        public void GivenExistingInDbBankMandate_WhenUpdateOrInsertBankMandateFromGcMandateInvoked_ThenUpdateResultReturned()
        {
            var bankMandate = new BankMandate
            {
                Id = Guid.NewGuid().ToString()
            };

            GetMock<IAccountService>().Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(new BankAccount());

            var bankMandateService = GetMock<IBankMandateService>();
            bankMandateService.Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(bankMandate);
            bankMandateService.Setup(x => x.UpdateBankMandateFromGcMandate(It.IsAny<string>(), It.IsAny<GCMandate>(), It.IsAny<AsyncExecutionParam>())).Returns(bankMandate);

            var result = ClassUnderTest.UpdateOrInsertBankMandateFromGcMandate(new User(), new GCMandate());

            Assert.AreEqual(bankMandate, result);
        }

        [Test]
        public void GivenNotExistedInDbBankMandate_WhenUpdateOrInsertBankMandateFromGcMandateInvoked_ThenInsertAndSaveInvoked()
        {
            var tenant = new User();
            var gcMandate = new GCMandate();

            GetMock<IAccountService>().Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(new BankAccount());

            var bankMandateService = GetMock<IBankMandateService>();
            bankMandateService.Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Throws(new BankMandateNotFound());

            ClassUnderTest.UpdateOrInsertBankMandateFromGcMandate(tenant, gcMandate);

            bankMandateService.Verify(x => x.InsertAndSaveBankMandateFromGcMandate(tenant, gcMandate, It.IsAny<AsyncExecutionParam>()), Times.Once);
        }

        [Test]
        public void GivenNotExistedInDbBankMandate_WhenUpdateOrInsertBankMandateFromGcMandateInvoked_ThenUpdateResultReturned()
        {
            var bankMandate = new BankMandate
            {
                Id = Guid.NewGuid().ToString()
            };

            GetMock<IAccountService>().Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns(new BankAccount());

            var bankMandateService = GetMock<IBankMandateService>();
            bankMandateService.Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Throws(new BankMandateNotFound());
            bankMandateService.Setup(x => x.InsertAndSaveBankMandateFromGcMandate(It.IsAny<User>(), It.IsAny<GCMandate>(), It.IsAny<AsyncExecutionParam>())).Returns(bankMandate);

            var result = ClassUnderTest.UpdateOrInsertBankMandateFromGcMandate(new User(), new GCMandate());

            Assert.AreEqual(bankMandate, result);
        }

        [Test]
        public void GivenNotExistedBankAccount_WhenUpdateOrInsertBankMandateFromGcMandateInvoked_ThenNullReturned()
        {
            GetMock<IAccountService>().Setup(x => x.GetById(It.IsAny<string>(), It.IsAny<AsyncExecutionParam>())).Returns((BankAccount) null);

            var result = ClassUnderTest.UpdateOrInsertBankMandateFromGcMandate(new User(), new GCMandate());

            Assert.IsNull(result);
        }
    }
}