﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class PremiseServiceTests : BaseAutoMock<PremiseService>
    {
        public interface IThirdPartyServiceOne : IThirdPartyService
        {
        }

        public interface IThirdPartyServiceTwo : IThirdPartyService
        {
        }

        public interface IThirdPartyServiceThree : IThirdPartyService
        {
        }

        [Test]
        public void GetPremisesForPostcodeFromThirdParty()
        {
            var postCode = Guid.NewGuid().ToString();
            var premise = new Premise
            {
                Postcode = postCode
            };
            var thirdPartyResult = new List<Premise>
            {
                premise
            };

            var iThirdPartyService1 = GetMock<IThirdPartyServiceOne>();
            iThirdPartyService1.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder
            {
                PremisesByPostCode = 2
            });
            iThirdPartyService1.Setup(x => x.GetPremisesByPostcode(It.IsAny<string>())).Returns(thirdPartyResult);

            var iThirdPartyService2 = GetMock<IThirdPartyServiceTwo>();
            iThirdPartyService2.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder
            {
                PremisesByPostCode = 3
            });
            iThirdPartyService2.Setup(x => x.GetPremisesByPostcode(It.IsAny<string>())).Returns(new List<Premise>());

            var iThirdPartyService3 = GetMock<IThirdPartyServiceThree>();
            iThirdPartyService3.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder
            {
                PremisesByPostCode = 1
            });
            iThirdPartyService3.Setup(x => x.GetPremisesByPostcode(It.IsAny<string>())).Returns((List<Premise>) null);

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService1.Object,
                iThirdPartyService2.Object,
                iThirdPartyService3.Object
            });

            var result = ClassUnderTest.GetPremisesForPostcodeFromThirdParty(postCode);

            Assert.AreSame(thirdPartyResult, result);
        }

        [Test]
        public void GivenRandomPostCode_WhenGetPremisesForPostcodeInvoked_ThenInsertPremisesIntoDBAndInvokedExactTimes()
        {
            var postCode = Guid.NewGuid().ToString();
            var thirdPartyResult = new List<Premise>
            {
                new Premise
                {
                    Postcode = postCode
                }
            };

            var iThirdPartyService = GetMock<IThirdPartyService>();
            iThirdPartyService.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder());
            iThirdPartyService.Setup(x => x.GetPremisesByPostcode(It.IsAny<string>())).Returns(thirdPartyResult);

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService.Object
            });

            var premiseRepository = GetMock<GenericRepository<Premise, User, IApplicationContext>>();
            premiseRepository.Setup(x => x.Get()).Returns(new List<Premise>().AsQueryable());
            premiseRepository.Setup(x => x.Insert(It.IsAny<Premise>()));

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.PremiseRepository).Returns(premiseRepository.Object);
            unitOfWork.Setup(x => x.Save());

            var result = ClassUnderTest.GetPremisesForPostcode(postCode).ToList();

            Assert.NotNull(result);
            Assert.AreEqual(thirdPartyResult.Count, result.Count);
            for (var i = 0; i < thirdPartyResult.Count; i++)
            {
                Assert.AreEqual(thirdPartyResult[i].Postcode, result[i].Postcode);
            }
            premiseRepository.Verify(x => x.Insert(It.IsAny<Premise>()), Times.Exactly(thirdPartyResult.Count));
            premiseRepository.Verify(x => x.Get(), Times.Once);
            unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void GivenRandomUDPRN_WhenGetBuildingsForPremiseInvoked_ThenInsertOneBuildingIntoDBAndInvokedOnce()
        {
            var premise = new Random().NextDouble();

            var thirdPartyResult = new List<Building>
            {
                new Building
                {
                    Udprn = premise
                }
            };

            var iThirdPartyService = GetMock<IThirdPartyService>();
            iThirdPartyService.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder());
            iThirdPartyService.Setup(x => x.GetBuildingsByPremise(It.IsAny<double>())).Returns(thirdPartyResult);

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService.Object
            });

            var buildingRepository = GetMock<GenericRepository<Building, User, IApplicationContext>>();
            buildingRepository.Setup(x => x.Get()).Returns(new List<Building>().AsQueryable());
            buildingRepository.Setup(x => x.Insert(It.IsAny<Building>()));

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.BuildingRepository).Returns(buildingRepository.Object);
            unitOfWork.Setup(x => x.Save());

            var result = ClassUnderTest.GetBuildingsForPremise(premise);

            Assert.NotNull(result);
            Assert.AreEqual(premise, result.Udprn);
            buildingRepository.Verify(x => x.Insert(It.IsAny<Building>()), Times.Once);
            buildingRepository.Verify(x => x.Get(), Times.Once);
            unitOfWork.Verify(x => x.Save(), Times.Once);
        }
    }
}