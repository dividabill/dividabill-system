﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class BankAccountServiceTests : BaseAutoMock<BankAccountService>
    {
        [Test]
        public void GivenAnyBankAccount_WhenInsertAndSaveInvoked_ThenInsertAndSaveFunctionsInvoked()
        {
            var entityToInsert = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.InsertAndSave(entityToInsert);

            bankAccountRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
            bankAccountRepository.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Test]
        public void GivenAnyBankAccount_WhenUpdateAndSaveInvoked_ThenUpdateAndSaveFunctionsInvoked()
        {
            var entityToUpdate = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.UpdateAndSave(entityToUpdate);

            bankAccountRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
            bankAccountRepository.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Test]
        public void GivenAnyBankAccountForInsert_WhenInsertInvoked_ThenDefaultPropertiesSet()
        {
            var entityToInsert = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);
            bankAccountRepository.Setup(x => x.Insert(entityToInsert)).Returns(entityToInsert);

            var result = ClassUnderTest.Insert(entityToInsert);

            Assert.NotNull(result.CreatedAt);
            Assert.NotNull(result.LastModified);
        }

        [Test]
        public void GivenAnyBankAccountForUpdate_WhenUpdateInvoked_ThenDefaultPropertiesSet()
        {
            var entityToUpdate = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);
            bankAccountRepository.Setup(x => x.Update(entityToUpdate)).Returns(entityToUpdate);

            var result = ClassUnderTest.Update(entityToUpdate);

            Assert.NotNull(result.LastModified);
        }

        [Test]
        public void GivenAnyBankAccountForInsert_WhenInsertInvoked_ThenInsertInvoked()
        {
            var entityToInsert = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.Insert(entityToInsert);

            bankAccountRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
        }

        [Test]
        public void GivenAnyBankAccountForUpdate_WhenUpdateInvoked_ThenUpdateInvoked()
        {
            var entityToUpdate = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.Update(entityToUpdate);

            bankAccountRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
        }

        [Test]
        public void GivenAnyBankAccountForUpdate_WhenGetActiveInvoked_ThenUpdateInvoked()
        {
            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.GetActive();

            bankAccountRepository.Verify(x => x.Get(), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenInsertInvoked_ThenInsertOfAsyncUnitOfWorkInvoked()
        {
            var entityToInsert = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.Insert(entityToInsert, new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankAccountRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenUpdateInvoked_ThenUpdateOfAsyncUnitOfWorkInvoked()
        {
            var entityToUpdate = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.Update(entityToUpdate, new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankAccountRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenGetActiveInvoked_ThenGetOfAsyncUnitOfWorkInvoked()
        {
            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.GetActive(new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankAccountRepository.Verify(x => x.Get(), Times.Once);
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenInsertInvoked_ThenFailed()
        {
            var entityToInsert = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.Insert(entityToInsert, new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        [ExpectedException(typeof(NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenUpdateInvoked_ThenFailed()
        {
            var entityToUpdate = new BankAccount();

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.Update(entityToUpdate, new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        [ExpectedException(typeof(NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenGetActiveInvoked_ThenFailed()
        {
            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.GetActive(new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        public void GivenAnyUserForInsert_WhenInsertBankAccountFromGcAccountInvoked_ThenMapInsertAndSaveInvoked()
        {
            var tenant = new User();
            var gcBankAccount = new GCBankAccount();
            var bankAccount = new BankAccount();

            var gcBankAccountToBankAccountMappingFactory = GetMock<IGcBankAccountToBankAccountMappingFactory>();
            gcBankAccountToBankAccountMappingFactory.Setup(x => x.Map(It.IsAny<User>(), It.IsAny<GCBankAccount>())).Returns(bankAccount);

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            ClassUnderTest.InsertBankAccountFromGcAccount(tenant, gcBankAccount);

            gcBankAccountToBankAccountMappingFactory.Verify(x => x.Map(tenant, gcBankAccount), Times.Once);
            bankAccountRepository.Verify(x => x.Insert(bankAccount), Times.Once);
            bankAccountRepository.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Test]
        public void GivenNotNullBankAccount_WhenLinkOrUnlinkBankAccountToUserInvoked_ThenLinkedToTenant()
        {
            var tenant = new User();
            var bankAccount = new BankAccount
            {
                Id = Guid.NewGuid().ToString()
            };
            ClassUnderTest.LinkOrUnlinkBankAccountToUser(tenant, bankAccount);

            Assert.NotNull(tenant.BankAccountID);
        }

        [Test]
        public void GivenNullBankAccount_WhenLinkOrUnlinkBankAccountToUserInvoked_ThenUnLinkedFromTenant()
        {
            var tenant = new User();
            ClassUnderTest.LinkOrUnlinkBankAccountToUser(tenant, null);

            Assert.Null(tenant.BankAccountID);
        }

        [Test]
        public void GivenNotNullBankAccount_WhenLinkOrUnlinkBankAccountToUserInvoked_ThenTenantUpdateInvoked()
        {
            var tenant = new User();
            ClassUnderTest.LinkOrUnlinkBankAccountToUser(tenant, new BankAccount());

            GetMock<ITenantService>().Verify(x => x.UpdateAndSave(tenant, It.IsAny<AsyncExecutionParam>()), Times.Once);
        }

        [Test]
        public void GivenNotNullBankAccount_WhenLinkOrUnlinkBankAccountToUserInvoked_ThenUpdatedTenantReturned()
        {
            var tenant = new User();

            var tenantService = GetMock<ITenantService>();
            tenantService.Setup(x => x.UpdateAndSave(It.IsAny<User>(), It.IsAny<AsyncExecutionParam>())).Returns(tenant);

            var result = ClassUnderTest.LinkOrUnlinkBankAccountToUser(tenant, new BankAccount());

            Assert.AreSame(tenant, result);
        }

        [Test]
        public void GivenExistedBankAccount_WhenUpdateBankAccountFromGcMandateInvoked_ThenUpdateInvokedButNotSaved()
        {
            var bankAccountId = Guid.NewGuid().ToString();
            var bankAccount = new BankAccount
            {
                Id = bankAccountId
            };

            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            bankAccountRepository.Setup(x => x.Get()).Returns(
                new List<BankAccount>
                {
                    bankAccount
                }.AsQueryable()
                );

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            GetMock<IGcBankAccountToBankAccountMappingFactory>().Setup(x => x.Map(It.IsAny<GCBankAccount>(), It.IsAny<BankAccount>())).Returns(bankAccount);

            var gcMandate = new GCBankAccount();
            var result = ClassUnderTest.UpdateBankAccountFromGcAccount(bankAccountId, gcMandate);

            GetMock<IGcBankAccountToBankAccountMappingFactory>().Verify(x => x.Map(gcMandate, It.IsAny<BankAccount>()), Times.Once);
            bankAccountRepository.Verify(x => x.Update(It.IsAny<BankAccount>()), Times.Once);
            bankAccountRepository.Verify(x => x.Insert(It.IsAny<BankAccount>()), Times.Never);
            bankAccountRepository.Verify(x => x.SaveChanges(), Times.Never);
        }

        [Test]
        public void GivenNotExistBankAccount_WhenInsertBankAccountFromGcMandateInvoked_ThenInsertInvokedAndSaved()
        {
            var bankAccountRepository = GetMock<GenericRepository<BankAccount, User, IApplicationContext>>();
            bankAccountRepository.Setup(x => x.Get()).Returns(new List<BankAccount>().AsQueryable());

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.BankAccountRepository).Returns(bankAccountRepository.Object);

            GetMock<IGcBankAccountToBankAccountMappingFactory>().Setup(x => x.Map(It.IsAny<User>(), It.IsAny<GCBankAccount>())).Returns(new BankAccount());

            var gcBankAccount = new GCBankAccount();
            var result = ClassUnderTest.InsertBankAccountFromGcAccount(new User(), gcBankAccount);

            GetMock<IGcBankAccountToBankAccountMappingFactory>().Verify(x => x.Map(It.IsAny<User>(), gcBankAccount), Times.Once);
            bankAccountRepository.Verify(x => x.Insert(It.IsAny<BankAccount>()), Times.Once);
            bankAccountRepository.Verify(x => x.SaveChanges(), Times.Once);
            bankAccountRepository.Verify(x => x.Update(It.IsAny<BankAccount>()), Times.Never);
        }
    }
}