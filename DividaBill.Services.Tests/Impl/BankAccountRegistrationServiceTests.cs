﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dividabill.Tests.Core;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.ViewModels;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class BankAccountRegistrationServiceTests : BaseAutoMock<BankAccountRegistrationService>
    {
        private Task<List<UIError>> ExceptionTestFunctionAsync()
        {
            throw new Exception("Exception for unit test");
        }

        [Test]
        public async Task GivenExceptionThrown_WhenValidateBankAccountDetailsInvoked_ThenNullReturned()
        {
            var iThirdPartyService = GetMock<IThirdPartyService>();
            iThirdPartyService.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder());
            iThirdPartyService.Setup(x => x.ValidateBankAccountDetails(It.IsAny<BankAccountDetails>())).Returns(ExceptionTestFunctionAsync);

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService.Object
            });

            var result = await ClassUnderTest.ValidateBankAccountDetails(new BankAccountDetails());

            Assert.IsNull(result);
        }


        [Test]
        public async Task GivenNoError_WhenValidateBankAccountDetailsInvoked_ThenNullReturned()
        {
            var iThirdPartyService = GetMock<IThirdPartyService>();
            iThirdPartyService.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder());
            iThirdPartyService.Setup(x => x.ValidateBankAccountDetails(It.IsAny<BankAccountDetails>())).Returns((Func<Task<List<UIError>>>) null);

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService.Object
            });

            var result = await ClassUnderTest.ValidateBankAccountDetails(new BankAccountDetails());

            Assert.IsNull(result);
        }

        [Test]
        public async Task GivenSomeError_WhenValidateBankAccountDetailsInvoked_ThenSameErrorListReturned()
        {
            var errors = new List<UIError> {new UIError("some error key", "some error message")};

            var iThirdPartyService = GetMock<IThirdPartyService>();
            iThirdPartyService.Setup(x => x.SortOrder).Returns(new ThirdPartyExecutionSortOrder());

            iThirdPartyService.Setup(x => x.ValidateBankAccountDetails(It.IsAny<BankAccountDetails>())).Returns(Task.FromResult(errors));

            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                iThirdPartyService.Object
            });

            var result = await ClassUnderTest.ValidateBankAccountDetails(new BankAccountDetails());

            Assert.AreEqual(errors, result);
        }
    }
}