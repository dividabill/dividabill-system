﻿using System;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Services.NewBillingSystem.Impl;
using NUnit.Framework;

namespace DividaBill.Services.Tests.NewBillingSystem.Impl
{
    [TestFixture]
    public class HouseholdBillExtensionsTests
    {
        [TestCase(1, "January")]
        [TestCase(2, "February")]
        [TestCase(3, "March")]
        [TestCase(4, "April")]
        [TestCase(5, "May")]
        [TestCase(6, "June")]
        [TestCase(7, "July")]
        [TestCase(8, "August")]
        [TestCase(9, "September")]
        [TestCase(10, "October")]
        [TestCase(11, "November")]
        [TestCase(12, "December")]
        public void GivenMonthNumberAndName_WhenGetMonthExtenstionInvoked_ThenConvertNumberIntoName(int monthNumber, string monthName)
        {
            var bill = new HouseholdBill(new HouseModel(), DateTime.Now.Year, monthNumber);

            var result = bill.GetMonth();

            Assert.AreEqual(monthName, result);
        }
    }
}