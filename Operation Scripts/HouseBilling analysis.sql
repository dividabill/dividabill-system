
--- Total Active Houses and non Active Houses ---
DECLARE @dummyHouse INT = 1270


SELECT 'ActiveHouseIds' as Status, ID 'HouseID' 
INTO #activehouses
FROM housemodels WHERE startdate <= getdate() AND enddate > getdate() and id != @dummyHouse

SELECT 'Total Houses >>>', COUNT(*) FROM housemodels where id != @dummyHouse
UNION ALL
SELECT 'Total Active Houses >>>', COUNT(*) FROM housemodels WHERE startdate <= getdate() AND enddate > getdate() and id != @dummyHouse
UNION ALL
SELECT 'Total Non Active Houses >>>', COUNT(*) FROM housemodels WHERE ID NOT IN (SELECT HouseID FROM #activehouses) and id != @dummyHouse
UNION ALL
SELECT * FROM #activehouses
UNION ALL
SELECT 'NonActiveHouses', ID 'HouseID' FROM housemodels WHERE ID NOT IN (SELECT HouseID FROM #activehouses) and id != @dummyHouse


DROP TABLE #activehouses



--- Total Amount billed paid ---
SELECT 
[Year] 
, [Month]
, Count(DISTINCT hb.HOUSE_ID) as HouseCount
, CONVERT(MONEY, SUM(bb.ammount)) as 'TotalAmountBilled_FromTableBillbases'
, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
FROM HouseHoldbills hb
INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id
INNER JOIN billpayments bp ON bp.bill_id = bb.id
WHERE hb.HOUSE_ID != 1270 and bp.isPaid = 1
GROUP BY [Month], [Year]
ORDER BY [Year] DESC, [Month] DESC

--- Total Amount billed unpaid ---
SELECT 
[Year] 
, [Month]
, Count(DISTINCT hb.HOUSE_ID) as HouseCount
, CONVERT(MONEY, SUM(bb.ammount)) as 'TotalAmountBilled_FromTableBillbases'
, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
FROM HouseHoldbills hb
INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id
INNER JOIN billpayments bp ON bp.bill_id = bb.id
WHERE hb.HOUSE_ID != 1270 and bp.isPaid = 0
GROUP BY [Month], [Year]
ORDER BY [Year] DESC, [Month] DESC


--- Total house count ---


--select  * from billpayments
--select  * from Billbases
--select top 2 * FROM householdbills