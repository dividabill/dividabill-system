DECLARE @dummyHouse INT = 1270


/*
3. a list of the houses that were biled for each month for the last 6 months including house id, address, number of tenants, services signed up to, amount that was billed by service, amount that was paid or unpaid if available, date it was billed 
*/
SELECT *
INTO #ServicesList
FROM (SELECT Id, Name FROM ServiceTypes) as t



CREATE TABLE #Data (
[Report #3] VARCHAR(20)
, House_Id INT
, [Year] INT
, [Month] INT
, [Month_Name] VARCHAR(20)
, Service VARCHAR(20)
, Nb_HouseMates INT
, CreatedDate DATETIME
, [TotalAmountBilled_FromTableBillbases] MONEY
, TotalAmountBilled_FromTablebillpayments MONEY
, [IsPaid?] VARCHAR(5)
, AddressLine_1 VARCHAR(50)
, AddressCity VARCHAR(50)
, County VARCHAR(50)
)


DECLARE @N INT = (SELECT MAX(ID) FROM #ServicesList)

DECLARE @T INT = 1
DECLARE @ServiceType VARCHAR(20)


WHILE @T <= @N 

BEGIN 

	SELECT @ServiceType = Name FROM #ServicesList WHERE ID = @T

	INSERT INTO #Data

	SELECT 'Billing by Service' as 'Report #3'
	, hb.House_ID
	, hb.[Year] 
	, hb.[Month]
	, CASE hb.[Month] 
		WHEN 1 THEN 'January'
		WHEN 2 THEN 'February'
		WHEN 3 THEN 'Mars'
		WHEN 4 THEN 'April'
		WHEN 5 THEN 'May'
		WHEN 6 THEN 'June'
		WHEN 7 THEN 'July'
		WHEN 8 THEN 'August'
		WHEN 9 THEN 'September'
		WHEN 10 THEN 'October'
		WHEN 11 THEN 'November'
		WHEN 12 THEN 'December'
	  END AS 'Month Name'
	, @ServiceType as 'Service'
	, h.HouseMates
	, hb.CreatedDate
	, bb.ammount as 'TotalAmountBilled_FromTableBillbases'
	, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
	, CASE bp.isPaid WHEN 1 THEN 'Yes' ELSE 'No' END as 'IsPaid?'
	, h.Address_Line1
	, h.Address_City
	, h.Address_County
	FROM HouseHoldbills hb
		INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id AND hb.House_ID != @dummyHouse AND hb.CreatedDate >= '2015-05-01'
		INNER JOIN billpayments bp ON bp.bill_id = bb.id
		INNER JOIN HouseModels h ON hb.House_ID = h.ID 
		INNER JOIN Contracts c on bb.Contract_ID = c.ID AND ServiceNew_ID = @T -- ServiceID = Electricity
	GROUP BY hb.[Year] 
	, hb.[Month]
	, hb.House_ID
	, h.HouseMates
	, h.Address_Line1
	, h.Address_City
	, h.Address_County
	, hb.CreatedDate
	, bb.ammount
	, bp.isPaid
	ORDER BY hb.House_ID, [Year] DESC, [Month] DESC,  bp.isPaid


	SET @T = @T + 1
END


SELECT * FROM #Data
ORDER BY House_Id, [Year] DESC, [Month] Desc, [Service]


DROP TABLE #ServicesList, #Data
