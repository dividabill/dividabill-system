﻿namespace DividaBill.ViewModels.API
{
    public class ContractApiViewModel : ContractViewModel
    {
        public string ServiceName { get; set; }
        public string LengthName { get; set; }
        public string SubmissionStatusName { get; set; }
    }
}