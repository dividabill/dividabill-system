﻿using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.API
{
    public class ProviderApiOutputViewModel: IApiOutputViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactPerson { get; set; }
        public int AverageInstallationDelay { get; set; }
    }
}