﻿using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.API
{
    public class IpOutputViewModel : IApiOutputViewModel
    {
        public string Ip { get; set; }
    }
}