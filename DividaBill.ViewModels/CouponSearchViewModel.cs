﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ViewModels
{
    public class CouponSearchViewModel
    {

        public String coupon { get; set; }
        public String postcode { get; set; }
        public int tenants { get; set; }
        public bool water { get; set; }
        public bool gas { get; set; }
        public bool electricity { get; set; }
        public BroadbandType broadband { get; set; }
        public bool skytv { get; set; }
        public bool tvlicense { get; set; }
        public LandlineType phoneline { get; set; }
        public bool netflixtv { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}