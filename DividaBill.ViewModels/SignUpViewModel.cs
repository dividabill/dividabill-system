﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DividaBill.Models;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    [Serializable]
    public class SignUpViewModel : IViewModel
    {
        public SignUpViewModel()
        {
            TVItems = new[]
            {
                new TvItemsViewModel {Name = "SkyTV"},
                new TvItemsViewModel {Name = "NetflixTV"},
                new TvItemsViewModel {Name = "TVLicense"}
            }.ToList();
        }

        [Required]
        [Display(Name = "Postcode")]
        public string PostCode { get; set; }

        [Required]
        [Display(Name = "Selected Address")]
        public string AddressTT1 { get; set; }

        public string AddressTT2 { get; set; }
        public string AddressTT3 { get; set; }
        public string AddressTT4 { get; set; }
        public string Udprn { get; set; }

        [Required]
        [Display(Name = "Town/City")]
        public string CityTT { get; set; }

        [Display(Name = "County")]
        public string CountyTT { get; set; }

        public BroadbandType BroadbandType { get; set; }
        public LandlineType LandlinePhoneType { get; set; }

        //Step 2

        [Required]
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartTime { get; set; }

        //[Required]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]

        //public DateTime EndTime { get; set; }

        [Display(Name = "End Date")]
        public int Period { get; set; }


        [Required]
        [Display(Name = "Number of Tenants")]
        public ushort Housemates { get; set; }

        [Display(Name = "Discount Code")]
        public string Coupon { get; set; }

        public bool Gas { get; set; }

        public bool Water { get; set; }
        public bool Electricity { get; set; }
        public bool Media { get; set; }

        //Step 3

        [Required]
        [Display(Name = "Title")]
        public string RefTitle { get; set; }


        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }


        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        //TODO: make a required field
        //[Required]
        [Display(Name = "Confirm Email")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string ConfirmEmail { get; set; }


        [Required]
        [Display(Name = "Contact Number")]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }


        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DoB { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        //[Required]
        [Display(Name = "Postal code:")]
        public string PostCodeOT { get; set; }

        //[Required]
        [Display(Name = "Select Address:")]
        public string AddressOT1 { get; set; }

        public string AddressOT2 { get; set; }
        public string AddressOT3 { get; set; }
        public string AddressOT4 { get; set; }

        //[Required]
        [Display(Name = "Town/City")]
        public string CityOT { get; set; }

        [Display(Name = "County")]
        public string CountyOT { get; set; }

        //Step 4
        [Required]
        public BankAccountDetails BankAccountDetails { get; set; }

        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(8, ErrorMessage = "Invalid account number code.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account Sort Code")]
        [StringLength(8, ErrorMessage = "Invalid sort code.")]
        public string AccountSortCode { get; set; }


        [Required]
        public bool AccountStandalone { get; set; }

        //[BooleanRequired(ErrorMessage = "You must accept the terms and conditions.")]
        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must accept the terms and conditions.")]
        //[Required]
        //public bool AcceptTermsAndConditions { get; set; }
        public bool NewsletterSubscription { get; set; }

        public bool AllowPayments { get; set; }

        public bool Broadband { get; set; }

        public bool SkyTV { get; set; }

        public bool NetflixTV { get; set; }

        public bool LandlinePhone { get; set; }

        public bool TVLicense { get; set; }

        public bool Ongoing { get; set; }

        public bool TV { get; set; }

        public IList<TvItemsViewModel> TVItems { get; set; }

        public Dictionary<string, string> ValidationErrors { get; set; }

        /// <summary>
        ///     Removes dashes from the sort code and checks the period.
        /// </summary>
        public void Validate()
        {
            if (Period < 9 && !Ongoing)
                throw new ArgumentException("The Period must be more than 9 months.", "Period");
        }

        /// <summary>
        ///     Creates a new <see cref="HouseDetailsViewModel" /> from the underlying <see cref="SignUpViewModel" />.
        /// </summary>
        /// <returns></returns>
        public HouseDetailsViewModel CreateHouseViewModel()
        {
            DateTime EndDate;
            if (Ongoing)
                EndDate = DateTime.MaxValue;
            else
                EndDate = StartTime.AddMonths(Period);

            return new HouseDetailsViewModel
            {
                Address = new Address
                {
                    City = CityTT,
                    County = CountyTT,
                    Line1 = AddressTT1,
                    Line2 = AddressTT2,
                    Line3 = AddressTT3,
                    Line4 = AddressTT4,
                    Postcode = PostCode,
                    UDPRN = Udprn
                },
                Electricity = Electricity,
                Gas = Gas,
                Water = Water,
                Media = Media,
                Broadband = Broadband,
                BroadbandType = BroadbandType,
                LandlinePhoneType = LandlinePhoneType,
                LandlinePhone = LandlinePhone,
                NetflixTV = NetflixTV,
                SkyTV = SkyTV,
                TVLicense = TVLicense,
                Housemates = Housemates,
                StartDate = StartTime.ToShortDateString(),
                EndDate = EndDate.ToShortDateString(),
                Coupon = Coupon,
                Period = Period
            };
        }

        public int HouseId { get; set; }
    }
}