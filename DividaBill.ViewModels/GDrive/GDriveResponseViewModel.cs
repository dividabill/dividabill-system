﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DividaBill.ViewModels.GDrive
{
    public class GDriveResponseViewModel
    {
        /// <summary>
        /// The last X entries of the log. 
        /// </summary>
        public string[] LogLines { get; set; }

        
        public GDriveFileViewModel[] Files { get; set; }

    }
}