﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ViewModels
{
    public class EstimatedPriceViewModel
    {
        public decimal Water { get; set; }
        public decimal Gas { get; set; }
        public decimal Electricity { get; set; }
        public decimal ServiceCharge { get; set; }
        public decimal TotalBill { get; set; }
        public decimal LineRental { get; set; }
        public decimal SkyTV { get; set; }
        public decimal NetflixTV { get; set; }
        public decimal TVLicense { get; set; }
        public decimal BroadbandADSL { get; set; }
        public decimal BroadbandFibre40 { get; set; }
        public decimal LandlinePhoneMedium { get; set; }
        public decimal LandlinePhoneBasic { get; set; }
    }
}