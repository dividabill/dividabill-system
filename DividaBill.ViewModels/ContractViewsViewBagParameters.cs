using System.Web.Mvc;

namespace DividaBill.ViewModels
{
    public class ContractViewsViewBagParameters
    {
        public SelectList Services { get; set; }
        public SelectList Providers { get; set; }
        public SelectList ContractLengths { get; set; }
        public SelectList Houses { get; set; }
        public SelectList Packages { get; set; }
        public SelectList HouseTenants { get; set; }
    }
}