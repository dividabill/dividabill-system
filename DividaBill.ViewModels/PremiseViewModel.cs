﻿using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    public class PremiseViewModel : IViewModel
    {
        public int Id { get; set; }
        public double PAId { get; set; }
        public string StreetAddress { get; set; }
        public string Place { get; set; }
        public string Postcode { get; set; }
    }
}