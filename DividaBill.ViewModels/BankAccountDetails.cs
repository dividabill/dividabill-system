using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    [Serializable]
    public class BankAccountDetails : IViewModel
    {
        [Required]
        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        [StringLength(8, ErrorMessage = "Invalid account number code.")]
        public string AccountNumber { get; set; }

        [Required]
        [Display(Name = "Account Sort Code")]
        [StringLength(8, ErrorMessage = "Invalid sort code.")]
        public string AccountSortCode { get; set; }
    }
}