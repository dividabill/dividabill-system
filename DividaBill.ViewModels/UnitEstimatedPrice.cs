﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ViewModels
{
    public class UnitEstimatedPrice
    {
        public decimal ElectricityStandingCharge { get; set; }
        public decimal ElectrcityUnitRate { get; set; }
        public decimal ElectrcityTCR { get; set; }
        public decimal GasStandingCharge { get; set; }
        public decimal GasUnitRate { get; set; }
        public decimal GasTCR { get; set; }
        public decimal WaterStandingCharge { get; set; }
        public decimal WaterUnitRate { get; set; }
        public decimal WaterTCR { get; set; }
    }
}
