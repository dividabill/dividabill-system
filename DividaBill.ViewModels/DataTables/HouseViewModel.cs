﻿using System;
using DividaBill.Models;
using DividaBill.ViewModels.Abstractions;
using Mvc.JQuery.DataTables;

namespace DividaBill.ViewModels.DataTables
{
    public class HouseViewModel : IViewModel
    {
        public int ID { get; set; }

        public int Housemates { get; set; }

        public string Address { get; set; }

        public string Coupon { get; set; }

        [DataTablesFilter(DataTablesFilterType.NumberRange)]
        public int RegisteredTenants { get; set; }

        public bool Electricity { get; set; }
        public bool Gas { get; set; }
        public bool Water { get; set; }
        public bool Broadband { get; set; }

        [DataTables(DisplayName = "Package")]
        public BroadbandType BroadbandType { get; set; }

        [DataTables(DisplayName = "Phone")]
        public bool LandlinePhone { get; set; }

        [DataTables(DisplayName = "Package")]
        public LandlineType LandlineType { get; set; }

        [DataTables(DisplayName = "Sky")]
        public bool SkyTV { get; set; }

        public bool TVLicense { get; set; }


        [DataTablesFilter(DataTablesFilterType.DateRange)]
        public DateTime RegisteredDate { get; set; }

        [DataTablesFilter(DataTablesFilterType.DateRange)]
        public DateTime StartDate { get; set; }

        [DataTablesFilter(DataTablesFilterType.DateRange)]
        public DateTime EndDate { get; set; }

        [DataTablesFilter(DataTablesFilterType.DateRange)]
        public DateTimeOffset? ActualStartDate { get; set; }

        [DataTables(DisplayName = "Options", Searchable = false, Sortable = false)]
        public string Options { get; set; }
    }
}