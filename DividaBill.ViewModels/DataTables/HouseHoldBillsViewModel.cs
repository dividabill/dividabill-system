﻿using System;
using System.ComponentModel.DataAnnotations;
using Mvc.JQuery.DataTables;
using DividaBill.Models.Common;

namespace DividaBill.ViewModels.DataTables
{
    public class HouseHoldBillsViewModel
    {
        [Display(Name = "Bill ID")]
        public string ID { get; set; }

        [Display(Name = "Created date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Payment date")]
        public DateTime PaymentDate { get; set; }

        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Total bill")]
        public decimal TotalBill { get; set; }

        [DataTables(Visible = false)]
        [Display(Name = "House ID")]
        public int HouseID { get; set; }

        [Display(Name = "House Address")]
        public SimpleAddress Address { get; set; }
    }
}