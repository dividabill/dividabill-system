﻿using Mvc.JQuery.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ViewModels.DataTables
{
    public class BankPaymentsViewModel
    {
        public string ID { get; set; }
        [DataTables(DisplayName = "Created Date")]
        public string CreatedDate { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }
        [DataTables(DisplayName = "Charge Date")]
        public string ChargeDate { get; set; }
        public string Client { get; set; }
        [DataTables(Visible = false)]
        public string ClientID { get; set; }
    }
}