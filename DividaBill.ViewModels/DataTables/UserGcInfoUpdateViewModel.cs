using System;
using DividaBill.ViewModels.Abstractions;
using Mvc.JQuery.DataTables;

namespace DividaBill.ViewModels.DataTables
{
    public class UserGcInfoUpdateViewModel : IViewModel
    {
        public string UserId { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string UserRefTitle { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string UserLastName { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string UserFirstName { get; set; }

        [DataTables(Searchable = false)]
        public string UserFullName { get; set; }

        public string MandateId { get; set; }

        [DataTables(Searchable = false)]
        public string MandateStatus { get; set; }

        [DataTables(Searchable = false)]
        public DateTime? MandateLastUpdatedTime { get; set; }

        public string BankAccountId { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string BankAccountHolder { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string BankAccountNumber { get; set; }

        [DataTables(Searchable = false, Visible = false)]
        public string BankAccountSortCode { get; set; }

        [DataTables(Searchable = false)]
        public string BankAccountDetails { get; set; }

        public string GoCardlessCustomerId { get; set; }

        [DataTables(DisplayName = "Options", Searchable = false, Sortable = false)]
        public string Options { get; set; }
    }
}