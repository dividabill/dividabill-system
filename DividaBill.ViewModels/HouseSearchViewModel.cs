﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ViewModels
{
    public class HouseSearchViewModel
    {

        public String PostCode { get; set; }

        public String AddressTT1 { get; set; }
        public String AddressTT2 { get; set; }
        public String AddressTT3 { get; set; }
        public String AddressTT4 { get; set; }

        public String CityTT { get; set; }
        public String CountyTT { get; set; }
    }
}