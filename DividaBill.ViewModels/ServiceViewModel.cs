﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ViewModels
{
    public class ServiceViewModel
    {
        public String ServiceTitle { get; set; }
        public decimal ServiceLastEstimateBill { get; set; }
        public String ServiceStatus { get; set; }
        public String ServiceOption { get; set; }
    }
}
