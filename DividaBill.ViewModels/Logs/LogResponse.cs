﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ViewModels
{
    public class LogResponseViewModel
    {
        public static readonly LogResponseViewModel Empty = new LogResponseViewModel(new string[0], -1, -1);

        public readonly string[] Lines;

        public readonly long FirstLineId;

        public readonly long LastLineId;

        public int LinesCount {  get { return Lines.Length; } }

        public LogResponseViewModel(string[] lines, long firstId, long lastId)
        {
            Lines = lines;
            FirstLineId = firstId;
            LastLineId = lastId;
        }
    }
}
