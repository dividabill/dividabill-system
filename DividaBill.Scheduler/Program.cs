﻿using System.Diagnostics;
using DividaBill.DAL;
using DividaBill.DAL.Unity;
using DividaBill.Services.Unity;
using Microsoft.Practices.Unity;
using Quartz;
using Quartz.Unity;
using Scheduler.Jobs;

namespace DividaBill.Scheduler
{
    public static class Program
    {
        private static IScheduler scheduler;


        private static JobList getJobList()
        {
            var jobDefs = new JobList();

            jobDefs.Add<BillGenerationJob>("BillGeneration", "0 11 * * ? *");

            return jobDefs;
        }

        private static void Main(string[] args)
        {
            Log.Default.TraceEvent(TraceEventType.Verbose, 0, "Starting the scheduler...");

            //initialize the Unity resolver
            var container = new UnityContainer();
            container.RegisterDAL();
            container.RegisterServices();
            container.AddNewExtension<QuartzUnityExtension>();

            //generate the job list
            var jobDefs = getJobList();

            //obtain and setup the scheduler
            scheduler = container.Resolve<IScheduler>();
            scheduler.Clear();
            jobDefs.RegisterAllJobs(scheduler);

            //Start the scheduler
            scheduler.Start();
            Log.Default.TraceEvent(TraceEventType.Information, 0, "Scheduler started!");
        }
    }
}