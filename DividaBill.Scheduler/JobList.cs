﻿using Quartz;
using Scheduler.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Scheduler
{
    class JobList
    {
        struct JobInfo
        {
            public readonly ITrigger Trigger;
            public readonly IJobDetail Job;
            public readonly string Name;

            public JobInfo(string name, IJobDetail job, ITrigger trig)
            {
                Name = name;
                Job = job;
                Trigger = trig;
            }

            public void AddToScheduler(IScheduler scheduler)
            {
                scheduler.ScheduleJob(Job, Trigger);
            }
        }


        readonly Dictionary<string, JobInfo> jobs = new Dictionary<string, JobInfo>();


        /*
            Cron format is used throughout this wrapper's actual scheduler
            Check http://www.cronmaker.com/
        */

        /// <summary>
        /// Adds the given <see cref="IJob"/> to the scheduler on the specified schedule. 
        /// </summary>
        /// <typeparam name="TJob">The job to enqueue. </typeparam>
        /// <param name="name">The name of the job. </param>
        /// <param name="cronSchedule">The schedule at which de job will be running. </param>
        public void Add<TJob>(string name, string cronSchedule)
            where TJob : IJob
        {
            var job = JobBuilder.Create<TJob>()
                .WithIdentity(name + "Job", name)
                .Build();

            var trig = TriggerBuilder.Create()
                .WithIdentity(name + "Trigger", name)
                .WithCronSchedule(cronSchedule)
                .Build();

            var jobInfo = new JobInfo(name, job, trig);
            jobs.Add(name, jobInfo);
        }

        /// <summary>
        /// Registers all jobs in this list with the given scheduler. 
        /// </summary>
        /// <param name="sched">The scheduler to register the jobs with. </param>
        public void RegisterAllJobs(IScheduler sched)
        {
            foreach (var job in jobs.Values)
                job.AddToScheduler(sched);

            Log.Default.TraceInformation("Registered {0} jobs with the scheduler!", jobs.Count);
        }
    }
}
