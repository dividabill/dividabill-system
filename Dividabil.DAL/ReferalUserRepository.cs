﻿using System.Collections.Generic;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.DAL
{
    public class ReferalUserRepository<DBContext> : BasicUserRepository<ReferalUser, DBContext>
        where DBContext : IReferalContext
    {
        public ReferalUserRepository(DBContext context)
            : base(context)
        {
        }

        public IEnumerable<ReferalUser> GetAllClients()
        {
            IEnumerable<ReferalUser> users = Get();
            return users;
        }
    }
}