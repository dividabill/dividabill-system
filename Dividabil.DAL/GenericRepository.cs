﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DividaBill.DAL
{
    public class GenericRepository<TEntity, UserType, DBContext> : IGenericRepository<TEntity, UserType, DBContext>,
        IQueryable<TEntity>
        where TEntity : class
        where UserType : IdentityUser
        where DBContext : IDbContext<UserType>
    {
        internal DBContext context;

        internal IxLazy<DbSet<TEntity>> lazySet;

        public GenericRepository()
        {
        }

        public GenericRepository(DBContext context)
        {
            this.context = context;
            lazySet = new IxLazy<DbSet<TEntity>>(() => context.Set<TEntity>());
        }

        internal DbSet<TEntity> dbSet
        {
            get { return lazySet; }
        }

        public virtual int Count(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter != null)
            {
                return dbSet.Count(filter);
            }
            return dbSet.Count();
        }

        public virtual IQueryable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (var includeProperty in includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty);

            if (orderBy != null)
                return orderBy(query);

            return query;
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual async Task<TEntity> GetByIDAsync(object id)
        {
            return await dbSet.FindAsync(id);
        }

        public virtual TEntity Insert(TEntity entity)
        {
            return dbSet.Add(entity);
        }

        public virtual void SaveChanges()
        {
            context.SaveChanges();
        }

        public virtual async Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }

        public virtual void Delete(object id)
        {
            var entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual TEntity Update(TEntity entityToUpdate)
        {
            var inLocal = context.Set<TEntity>().Local.Contains(entityToUpdate);
            var asd = context.Entry(entityToUpdate).State;
            if (context.Entry(entityToUpdate).State == EntityState.Detached)
            {
                dbSet.Attach(entityToUpdate);
            }
            context.Entry(entityToUpdate).State = EntityState.Modified;
            return entityToUpdate;
        }

        //    }

        //        context.Set<T>().Attach(entity);
        //            context.Entry(existing).State = EntityState.Detached;


        public Expression Expression
        {
            get { return ((IQueryable<TEntity>) dbSet).Expression; }
        }

        public Type ElementType
        {
            get { return ((IQueryable<TEntity>) dbSet).ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return ((IQueryable<TEntity>) dbSet).Provider; }
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IQueryable<TEntity>) dbSet).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IQueryable<TEntity>) dbSet).GetEnumerator();
        }

        public virtual IQueryable<TEntity> Get()
        {
            return dbSet.AsQueryable();
        }

        public void SafeAttach(
            TEntity entity,
            Func<TEntity, object> keyFn)
        {
            var existing = context.Set<TEntity>().Local
                .FirstOrDefault(x => Equals(keyFn(x), keyFn(entity)));
            if (existing != null)
                context.Entry(existing).State = EntityState.Detached;

            context.Set<TEntity>().Attach(entity);
        }
    }
}