﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.DAL.Interfaces
{
    /// <summary>
    /// An imbalanced unit of work which allows the execution of SQL scripts. 
    /// </summary>
    public interface ImbaUnitOfWork
    {
        void ExecuteSqlScript<T>(string sql)
            where T : class;
    }
}
