﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DividaBill.DAL.Interfaces
{
    internal interface IGenericRepository<TEntity, UserType, DBContext>
        where TEntity : class
        where UserType : IdentityUser
        where DBContext : IDbContext<UserType>
    {
        void Delete(object id);
        void Delete(TEntity entityToDelete);
        int Count(Expression<Func<TEntity, bool>> filter = null);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");
        TEntity GetByID(object id);
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entityToUpdate);
        void SaveChanges();

        Task SaveChangesAsync();
        Task<TEntity> GetByIDAsync(object id);
    }
}