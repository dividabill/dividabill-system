﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.StoredProcs;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Emails;
using DividaBill.Models.Sheets;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ActiveHousesRepository<IApplicationContext> ActiveHouses { get; }
        ArchivedHousesRepository<IApplicationContext> ArchivedHouses { get; }
        GenericRepository<BankAccount, User, IApplicationContext> BankAccountRepository { get; }
        GenericRepository<BankMandateEvent, User, IApplicationContext> BankMandateEventRepository { get; }
        GenericRepository<BankMandate, User, IApplicationContext> BankMandateRepository { get; }
        GenericRepository<BankPaymentEvent, User, IApplicationContext> BankPaymentEventRepository { get; }
        GenericRepository<BankPayment, User, IApplicationContext> BankPaymentRepository { get; }
        GenericRepository<EmailSentMessage, User, IApplicationContext> EmailSentMessageRepository { get; }
        GenericRepository<EmailTemplate, User, IApplicationContext> EmailTemplateRepository { get; }

        GenericRepository<BillingQueue, User, IApplicationContext> BillingQueueRepository { get; }
        GenericRepository<BillingRun, User, IApplicationContext> BillingRunRepository { get; }
        GenericRepository<BillingQueueTransactionLog, User, IApplicationContext> BillingQueueTransactionLogRepository { get; }

        BillRepository<BroadbandBill, User, IApplicationContext> BroadbandBillRepository { get; }
        GenericRepository<BroadbandContract, User, IApplicationContext> BroadbandContractRepository { get; }
        GenericRepository<Building, User, IApplicationContext> BuildingRepository { get; }
        GenericRepository<Coupon, User, IApplicationContext> CouponRepository { get; }
        BillRepository<DeliveryBill, User, IApplicationContext> DeliveryBillRepository { get; }
        BillRepository<ElectricityBill, User, IApplicationContext> ElectricityBillRepository { get; }
        GenericRepository<ElectricityContract, User, IApplicationContext> ElectricityContractRepository { get; }
        BillRepository<GasBill, User, IApplicationContext> GasBillRepository { get; }
        GenericRepository<GasContract, User, IApplicationContext> GasContractRepository { get; }
        GenericRepository<HouseEstimatedPrice, User, IApplicationContext> HouseEstimatedPriceRepository { get; }
        GenericRepository<HouseholdBill, User, IApplicationContext> HouseholdBillRepository { get; }
        BillRepository<LandlinePhoneBill, User, IApplicationContext> LandlinePhoneBillRepository { get; }
        GenericRepository<LandlinePhoneContract, User, IApplicationContext> LandlinePhoneContractRepository { get; }
        BillRepository<LineRentalBill, User, IApplicationContext> LineRentalBillRepository { get; }
        GenericRepository<LineRentalContract, User, IApplicationContext> LineRentalContractRepository { get; }
        BillRepository<NetflixTVBill, User, IApplicationContext> NetflixTVBillRepository { get; }
        GenericRepository<NetflixTVContract, User, IApplicationContext> NetflixTVContractRepository { get; }
        GenericRepository<PostcodeEstimatedPrice, User, IApplicationContext> PostcodeEstimatedPriceRepository { get; }
        GenericRepository<Premise, User, IApplicationContext> PremiseRepository { get; }
        GenericRepository<Provider, User, IApplicationContext> ProviderRepository { get; }
        GenericRepository<ReferalCode, User, IApplicationContext> ReferalRepository { get; }
        ReferalUserRepository<IReferalContext> ReferalUserRepository { get; }

        BillRepository<UtilityBill, User, IApplicationContext> UtilityBillRepository { get; }

        GenericRepository<Tenancy, User, IApplicationContext> TenancyRepository { get; }

        //Client DB
        GenericRepository<ClientNotification, User, IClientContext> ClientNotifications { get; }

        GenericRepository<BillPayment, User, IApplicationContext> BillPaymentsRepository { get; }
        BillRepository<SkyTVBill, User, IApplicationContext> SkyTVBillRepository { get; }
        GenericRepository<SkyTVContract, User, IApplicationContext> SkyTVContractRepository { get; }
        BillRepository<TVLicenseBill, User, IApplicationContext> TVLicenseBillRepository { get; }
        GenericRepository<TVLicenseContract, User, IApplicationContext> TVLicenseContractRepository { get; }

        UserRepository UserRepository { get; }
        BillRepository<WaterBill, User, IApplicationContext> WaterBillRepository { get; }
        GenericRepository<WaterContract, User, IApplicationContext> WaterContractRepository { get; }
        GenericRepository<Contract, User, IApplicationContext> ContractRepository { get; }

        GenericRepository<File, User, IApplicationContext> GDriveFileRepository { get; }

        GenericRepository<AuditLog, User, IApplicationContext> AuditLogs { get; }
        void Save();

        void Dispose();

        void WrapInTransaction(Action act, bool commitOnSuccess = true);

        Task<T> WrapInTransactionAsync<T>(Func<Task<T>> functor, bool commitOnSuccess = true);
        Task WrapInTransactionAsync(Func<Task> functor, bool commitOnSuccess = true);

        IQueryable<AuditLog> GetLogs(string tableName);

        IQueryable<AuditLog> GetLogs<T>();
        Task SaveAsync();

        void ExecuteSqlScript<T>(string sql) where T : class;

        List<sp_Monthly_Billing_Report_Result> sp_Monthly_Billing_Report(DateTime? startDate, DateTime? endDate);
        List<sp_goCardlessExport_Result> sp_goCardlessExport(int billingRunId);
        void EnableBulkInsertOptimisations();
    }
}