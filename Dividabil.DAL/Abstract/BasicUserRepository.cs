﻿using DividaBill.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL
{
    public abstract class BasicUserRepository<UserType, DBContext> : GenericRepository<UserType, UserType, DBContext>
        where UserType : IdentityUser
        where DBContext : IDbContext<UserType>
    {

        public BasicUserRepository(DBContext context)
            : base(context)
        {

        }
    }

}