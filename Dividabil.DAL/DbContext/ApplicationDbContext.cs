﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.StoredProcs;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Emails;
using DividaBill.Models.Sheets;
using Microsoft.AspNet.Identity.EntityFramework;
using TrackerEnabledDbContext.Identity;

namespace DividaBill.DAL
{
    public class ApplicationDbContext : TrackerIdentityContext<User>, IApplicationContext
    {
        protected readonly bool ReadOnly;

        public ApplicationDbContext()
            : this("DefaultConnection", false)
        {
            Debug.WriteLine($" Db Connection created on Task:{Task.CurrentId}");
        }

        public ApplicationDbContext(string connectionString, bool readOnly = false)
            : base(connectionString)
        {
            ReadOnly = readOnly;

            //safe attach?!
            var local = Set<ServiceType>().ToArray();
            foreach (var service in ServiceType.All)
            {
                var existing = local.FirstOrDefault(x => x.Id == service.Id);
                if (existing != null)
                    Entry(existing).State = EntityState.Detached;

                try
                {
                    Set<ServiceType>().Attach(service);
                }
                catch (Exception)
                {
                }
            }
        }

        public DbSet<ServiceType> Services { get; set; }
        public DbSet<ReferalCode> ReferalCodes { get; set; }
        public DbSet<ElectricityBill> ElectricityBills { get; set; }
        public DbSet<GasBill> GasBills { get; set; }
        public DbSet<WaterBill> WaterBills { get; set; }
        public DbSet<DeliveryBill> DeliveryBills { get; set; }
        public DbSet<HouseholdBill> HouseholdBills { get; set; }
        public DbSet<EmailSentMessage> SentEmails { get; set; }
        public DbSet<BankMandate> BankMandates { get; set; }
        public DbSet<BankPayment> BankPayments { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<BankPaymentEvent> BankPaymentEvents { get; set; }
        public DbSet<BankMandateEvent> BankMandateEvents { get; set; }
        public DbSet<ElectricityContract> ElectricityContracts { get; set; }
        public DbSet<GasContract> GasContracts { get; set; }
        public DbSet<WaterContract> WaterContracts { get; set; }
        public DbSet<BroadbandContract> BroadbandContracts { get; set; }
        public DbSet<SkyTVContract> SkyTVContracts { get; set; }
        public DbSet<NetflixTVContract> NetflixTVContracts { get; set; }
        public DbSet<TVLicenseContract> TVLicenseContracts { get; set; }
        public DbSet<LandlinePhoneContract> LandlinePhoneContracts { get; set; }
        public DbSet<LineRentalContract> LineRentalContracts { get; set; }
        public DbSet<BillPayment> BillPayments { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<File> GFiles { get; set; }

        public DbSet<BillingRun> BillingRuns { get; set; }
        public DbSet<BillingQueue> BillingQueues { get; set; }
        public DbSet<BillingQueueTransactionLog> BillingQueueTransactionLogs { get; set; }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<HouseModel> Houses { get; set; }
        public DbSet<Premise> Premises { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<MediaRequest> MediaRequests { get; set; }
        public DbSet<HouseEstimatedPrice> HouseEstimatedPrices { get; set; }
        public DbSet<PostcodeEstimatedPrice> PostcodeEstimatedPrices { get; set; }
        public DbSet<PostCode> PostCodes { get; set; }
        public DbSet<Tenancy> Tenancies { get; set; }

        public void SetBulkInsertOptimisationOn()
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public DbContextTransaction BeginTransaction()
        {
            return Database.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public override int SaveChanges()
        {
            return SaveChanges(false);
        }

        public override async Task<int> SaveChangesAsync()
        {
            if (ReadOnly)
                throw new AccessViolationException("This database is marked as read-only!");
            return await base.SaveChangesAsync();
        }

        public List<sp_Monthly_Billing_Report_Result> sp_Monthly_Billing_Report(DateTime? startDate, DateTime? endDate)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("StartDate", startDate) :
                new SqlParameter("StartDate", typeof (DateTime));

            var endDateParameter = endDate.HasValue ?
                new SqlParameter("EndDate", endDate) :
                new SqlParameter("EndDate", typeof (DateTime));

            var results = Database.SqlQuery<sp_Monthly_Billing_Report_Result>("[dbo].[sp_Monthly_Billing_Report] @StartDate, @EndDate", startDateParameter, endDateParameter).ToList();
            return results;
        }

        public List<sp_goCardlessExport_Result> sp_goCardlessExport(int billingRunId)
        {
            var billingRunIdParameter = new SqlParameter("billingRunId", billingRunId);

            var results = Database.SqlQuery<sp_goCardlessExport_Result>("[dbo].[sp_goCardlessExport] @billingRunId", billingRunIdParameter).ToList();
            return results;
        }

        ~ApplicationDbContext()
        {
            Debug.WriteLine($" Db Connection closed on Task:{Task.CurrentId}");
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public int SaveChanges(bool updateServiceTypes = false)
        {
            if (ReadOnly)
                throw new AccessViolationException("This database is marked as read-only!");

            //ServiceTypes are always seeded instead of recreated or updated
            //if (!updateServiceTypes)
            //    foreach (var ty in ChangeTracker.Entries<ServiceType>())
            //        ty.State = EntityState.Unchanged;

            return base.SaveChanges();
        }
    }

    //public class WarehouseDbContext : IdentityDbContext<User>, IWarehouseContext
    //{
    //    public WarehouseDbContext()
    //        : base("WarehouseConnection", false)
    //    {
    //    }

    //    public DbSet<PostcodeEstimatedPrice> PostcodeEstimatedPrices { get; set; }

    //    public DbSet<Coupon> Coupons { get; set; }
    //    public DbSet<HouseModel> Houses { get; set; }

    //    public static WarehouseDbContext Create()
    //    {
    //        return new WarehouseDbContext();
    //    }
    //}
}