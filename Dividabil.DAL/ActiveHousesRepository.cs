﻿using System;
using System.Data.Entity;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.DAL
{
    public class ActiveHousesRepository<DBContext> : HouseRepository<DBContext>, IActiveHousesRepository<HouseModel, DBContext>
        where DBContext : IApplicationContext
    {
        public ActiveHousesRepository(DBContext context)
            : base(context)
        {
        }

        public void Archive(int houseId)
        {
            throw new NotImplementedException();

            //HouseModel house = this.GetByID(houseId);

            //house.Archived = true;
            //this.Update(house);
        }

        public virtual void Update(HouseModel entityToUpdate)
        {
            var inLocal = context.Set<HouseModel>().Local.Where(h => h.ID == entityToUpdate.ID);
            var existing = context.Set<HouseModel>().Local
                .FirstOrDefault(x => Equals(x.ID, entityToUpdate.ID));
            var asd = context.Entry(entityToUpdate).State;
            SafeAttach(entityToUpdate, e => e.ID);


            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}