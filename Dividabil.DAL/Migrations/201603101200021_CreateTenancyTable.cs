namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTenancyTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tenancies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        HouseId = c.Int(nullable: false),
                        JoiningDate = c.DateTime(),
                        LeavingDate = c.DateTime(),
                        RequestedDate = c.DateTime(nullable: false),
                        RequestedBy = c.String(),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.HouseId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.HouseId);

            Sql(@"BEGIN TRAN
                INSERT INTO dbo.Tenancies (UserId, HouseId, JoiningDate, LeavingDate, RequestedDate)
                SELECT
	                _user.Id
	                , _user.House_ID
	                , CAST(SWITCHOFFSET(CAST(z.StartDate AS DATETIMEOFFSET), '+00:00') AS DATETIME2)
	                , CAST(SWITCHOFFSET(CAST(t.EndDate AS DATETIMEOFFSET), '+00:00') AS DATETIME2)
                    , _user.SignupDate
                FROM AspNetUsers _user
                INNER JOIN HouseModels house ON _user.House_ID = house.ID
                INNER JOIN (
	                SELECT House_ID, MAX(EndDate) as EndDate
	                FROM Contracts 
	                GROUP BY House_ID
                ) t ON _user.House_ID = t.House_ID
                INNER JOIN (
	                SELECT House_ID, MIN(StartDate) as StartDate
	                FROM Contracts 
	                GROUP BY House_ID
                ) z ON _user.House_ID = z.House_ID
                COMMIT");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tenancies", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tenancies", "HouseId", "dbo.HouseModels");
            DropIndex("dbo.Tenancies", new[] { "HouseId" });
            DropIndex("dbo.Tenancies", new[] { "UserId" });
            DropTable("dbo.Tenancies");
        }
    }
}
