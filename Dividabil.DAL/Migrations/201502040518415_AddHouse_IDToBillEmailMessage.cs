namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHouse_IDToBillEmailMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillEmailMessages", "House_ID", c => c.Int());
            CreateIndex("dbo.BillEmailMessages", "House_ID");
            AddForeignKey("dbo.BillEmailMessages", "House_ID", "dbo.HouseModels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillEmailMessages", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.BillEmailMessages", new[] { "House_ID" });
            DropColumn("dbo.BillEmailMessages", "House_ID");
        }
    }
}
