namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTempPostcodeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostCode2",
                c => new
                    {
                        Postcode = c.String(nullable: false, maxLength: 15),
                        ClusteredID = c.Int(nullable: false, identity: true),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        Easting = c.String(),
                        Northing = c.String(),
                        GridRef = c.String(),
                        County = c.String(),
                        District = c.String(),
                        Ward = c.String(),
                        DistrictCode = c.String(),
                        WardCode = c.String(),
                        Country = c.String(),
                        CountyCode = c.String(),
                        Constituency = c.String(),
                        Introduced = c.String(),
                        Terminated = c.String(),
                        Parish = c.String(),
                        NationalPark = c.String(),
                        Population = c.String(),
                        Households = c.String(),
                        Built_up_area = c.String(),
                        Built_up_sub_division = c.String(),
                        Lower_layer_super_output_area = c.String(),
                        Added = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.Postcode, clustered: false)
                .Index(t => t.Postcode, unique: true)
                .Index(t => t.ClusteredID, clustered: true);

            //SQL script:
            /*
             SET IDENTITY_INSERT dbo.PostCode2 OFF
             GO
             INSERT INTO dbo.PostCode2 ([Postcode], [Latitude], [Longitude], [Easting], [Northing], [GridRef], [County], [District], [Ward], [DistrictCode], [WardCode], [Country], [CountyCode], [Constituency], [Introduced], [Terminated], [Parish], [NationalPark], [Population], [Households], [Built_up_area], [Built_up_sub_division], [Lower_layer_super_output_area], [Added]) SELECT [Postcode], [Latitude], [Longitude], [Easting], [Northing], [GridRef], [County], [District], [Ward], [DistrictCode], [WardCode], [Country], [CountyCode], [Constituency], [Introduced], [Terminated], [Parish], [NationalPark], [Population], [Households], [Built_up_area], [Built_up_sub_division], [Lower_layer_super_output_area], [Added] FROM dbo.PostCodes;
            */
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostCode2", new[] { "ClusteredID" });
            DropIndex("dbo.PostCode2", new[] { "Postcode" });
            DropTable("dbo.PostCode2");
        }
    }
}
