namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePostcodeEstimatedPrice2ToPostcodeEstimatedPrices : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PostcodeEstimatedPrice2", newName: "PostcodeEstimatedPrices");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.PostcodeEstimatedPrices", newName: "PostcodeEstimatedPrice2");
        }
    }
}
