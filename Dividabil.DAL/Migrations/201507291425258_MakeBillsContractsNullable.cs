namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeBillsContractsNullable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contracts", "SubmissionStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contracts", "SubmissionStatus");
        }
    }
}
