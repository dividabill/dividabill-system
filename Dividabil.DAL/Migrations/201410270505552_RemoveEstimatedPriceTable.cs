namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEstimatedPriceTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EstimatedPrices", newName: "HouseEstimatedPrices");
            DropColumn("dbo.HouseEstimatedPrices", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HouseEstimatedPrices", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropIndex("dbo.HouseEstimatedPrices", new[] { "HouseID" });
            AlterColumn("dbo.HouseEstimatedPrices", "HouseID", c => c.Int());
            CreateIndex("dbo.HouseEstimatedPrices", "HouseID");
            RenameTable(name: "dbo.HouseEstimatedPrices", newName: "EstimatedPrices");
        }
    }
}
