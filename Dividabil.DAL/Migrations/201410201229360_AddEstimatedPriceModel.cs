namespace Dividabil.DAL.Migrations
{
    using DividaBill.DAL;
    using DividaBill.Models;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEstimatedPriceModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EstimatedPrices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        type = c.Int(nullable: false),
                        internalPrice = c.String(),
                        HouseID = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HouseModels", t => t.HouseID, cascadeDelete: true)
                .Index(t => t.HouseID);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EstimatedPrices", "HouseID", "dbo.HouseModels");
            DropIndex("dbo.EstimatedPrices", new[] { "HouseID" });
            DropTable("dbo.EstimatedPrices");
        }
    }
}
