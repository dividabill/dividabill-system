namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePremisesPAIdToFloat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Premises", "PAId", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Premises", "PAId", c => c.Int(nullable: false));
        }
    }
}
