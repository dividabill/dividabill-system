namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRefToBillPayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillPayments", "PaymentRef", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BillPayments", "PaymentRef");
        }
    }
}
