namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedArchivedHousesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "Discriminator", c => c.String(nullable: false, maxLength: 128, defaultValue: "ActiveHouse"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "Discriminator");
        }
    }
}
