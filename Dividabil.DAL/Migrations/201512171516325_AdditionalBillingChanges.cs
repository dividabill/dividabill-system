namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalBillingChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillPayments", "CreatedAt", c => c.DateTime());
            AddColumn("dbo.BillPayments", "ChargeDate", c => c.DateTime());
            AddColumn("dbo.BillingRun", "SendGridEmailId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BillingRun", "SendGridEmailId");
            DropColumn("dbo.BillPayments", "ChargeDate");
            DropColumn("dbo.BillPayments", "CreatedAt");
        }
    }
}
