namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Warehouse : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.HouseModels", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HouseModels", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
