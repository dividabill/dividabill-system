namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTenancyIdToHouseModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "TenancyId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "TenancyId");
        }
    }
}
