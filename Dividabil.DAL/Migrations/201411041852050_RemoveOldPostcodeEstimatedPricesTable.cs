namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveOldPostcodeEstimatedPricesTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes");
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            DropTable("dbo.PostcodeEstimatedPrices");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PostcodeEstimatedPrices",
                c => new
                    {
                        PostCode_ID = c.String(nullable: false, maxLength: 15),
                        Water = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Gas = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Electricity = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PostCode_ID);
            
            CreateIndex("dbo.PostcodeEstimatedPrices", "PostCode_ID", unique: true);
            AddForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes", "Postcode");
        }
    }
}
