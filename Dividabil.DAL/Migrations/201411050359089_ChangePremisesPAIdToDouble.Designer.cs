// <auto-generated />
namespace DividaBill.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class ChangePremisesPAIdToDouble : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChangePremisesPAIdToDouble));
        
        string IMigrationMetadata.Id
        {
            get { return "201411050359089_ChangePremisesPAIdToDouble"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
