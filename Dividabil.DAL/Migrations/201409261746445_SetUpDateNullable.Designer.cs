// <auto-generated />
namespace DividaBill.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class SetUpDateNullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SetUpDateNullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201409261746445_SetUpDateNullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
