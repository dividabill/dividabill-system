namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoCardlessDetailsTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankAccounts",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        created_at = c.String(),
                        enabled = c.Boolean(nullable: false),
                        account_number = c.String(nullable: false),
                        sort_code = c.String(nullable: false),
                        country_code = c.String(nullable: false),
                        currency = c.String(),
                        account_holder_name = c.String(nullable: false),
                        Owner_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_ID)
                .Index(t => t.Owner_ID);
            
            CreateTable(
                "dbo.BankMandates",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        created_at = c.String(),
                        status = c.String(),
                        next_possible_charge_date = c.String(),
                        reference = c.String(),
                        scheme = c.String(nullable: false),
                        BankAccount_ID = c.String(maxLength: 128),
                        User_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BankAccounts", t => t.BankAccount_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_ID)
                .Index(t => t.BankAccount_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.BankPayments",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        created_at = c.String(),
                        status = c.String(),
                        amount_refunded = c.String(),
                        amount = c.Int(nullable: false),
                        currency = c.String(),
                        description = c.String(),
                        charge_date = c.String(),
                        reference = c.String(),
                        Mandate_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BankMandates", t => t.Mandate_ID)
                .Index(t => t.Mandate_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankPayments", "Mandate_ID", "dbo.BankMandates");
            DropForeignKey("dbo.BankAccounts", "Owner_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.BankMandates", "User_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.BankMandates", "BankAccount_ID", "dbo.BankAccounts");
            DropIndex("dbo.BankPayments", new[] { "Mandate_ID" });
            DropIndex("dbo.BankMandates", new[] { "User_ID" });
            DropIndex("dbo.BankMandates", new[] { "BankAccount_ID" });
            DropIndex("dbo.BankAccounts", new[] { "Owner_ID" });
            DropTable("dbo.BankPayments");
            DropTable("dbo.BankMandates");
            DropTable("dbo.BankAccounts");
        }
    }
}
