namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveBillEmailMessageFromBill : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HouseholdBills", "SentEmail_ID", "dbo.BillEmailMessages");
            DropIndex("dbo.HouseholdBills", new[] { "SentEmail_ID" });
            DropColumn("dbo.HouseholdBills", "SentEmail_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HouseholdBills", "SentEmail_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.UserEmailMessages", "HouseholdBill_ID1", "dbo.HouseholdBills");
            DropIndex("dbo.UserEmailMessages", new[] { "HouseholdBill_ID1" });
            AlterColumn("dbo.UserEmailMessages", "HouseholdBill_ID", c => c.Int());
            DropColumn("dbo.UserEmailMessages", "HouseholdBill_ID1");
            CreateIndex("dbo.UserEmailMessages", "HouseholdBill_ID");
            CreateIndex("dbo.HouseholdBills", "SentEmail_ID");
            AddForeignKey("dbo.UserEmailMessages", "HouseholdBill_ID", "dbo.HouseholdBills", "ID");
            AddForeignKey("dbo.HouseholdBills", "SentEmail_ID", "dbo.BillEmailMessages", "ID");
        }
    }
}
