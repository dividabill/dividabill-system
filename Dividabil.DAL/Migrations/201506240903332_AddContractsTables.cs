namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContractsTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        House_ID = c.Int(nullable: false),
                        Service = c.Int(nullable: false),
                        StartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        EndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Length = c.Int(nullable: false),
                        RequestedBy_ID = c.String(maxLength: 128),
                        Provider_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .ForeignKey("dbo.Providers", t => t.Provider_ID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.RequestedBy_ID)
                .Index(t => t.House_ID)
                .Index(t => t.RequestedBy_ID)
                .Index(t => t.Provider_ID);
            
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        ContactPerson = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BroadbandContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.ElectricityContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.GasContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.LandlinePhoneContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.LineRentalContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.NetflixTVContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.SkyTVContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.TVLicenseContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.WaterContracts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ID)
                .Index(t => t.ID);
            
            AddColumn("dbo.BroadbandBills", "BroadbandContract_ID", c => c.Int());
            AddColumn("dbo.ElectricityBills", "ElectricityContract_ID", c => c.Int());
            AddColumn("dbo.GasBills", "GasContract_ID", c => c.Int());
            AddColumn("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID", c => c.Int());
            AddColumn("dbo.LineRentalBills", "LineRentalContract_ID", c => c.Int());
            AddColumn("dbo.NetflixTVBills", "NetflixTVContract_ID", c => c.Int());
            AddColumn("dbo.SkyTVBills", "SkyTVContract_ID", c => c.Int());
            AddColumn("dbo.TVLicenseBills", "TVLicenseContract_ID", c => c.Int());
            AddColumn("dbo.WaterBills", "WaterContract_ID", c => c.Int());
            CreateIndex("dbo.BroadbandBills", "BroadbandContract_ID");
            CreateIndex("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID");
            CreateIndex("dbo.LineRentalBills", "LineRentalContract_ID");
            CreateIndex("dbo.ElectricityBills", "ElectricityContract_ID");
            CreateIndex("dbo.GasBills", "GasContract_ID");
            CreateIndex("dbo.NetflixTVBills", "NetflixTVContract_ID");
            CreateIndex("dbo.SkyTVBills", "SkyTVContract_ID");
            CreateIndex("dbo.TVLicenseBills", "TVLicenseContract_ID");
            CreateIndex("dbo.WaterBills", "WaterContract_ID");
            AddForeignKey("dbo.BroadbandBills", "BroadbandContract_ID", "dbo.BroadbandContracts", "ID");
            AddForeignKey("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID", "dbo.LandlinePhoneContracts", "ID");
            AddForeignKey("dbo.LineRentalBills", "LineRentalContract_ID", "dbo.LineRentalContracts", "ID");
            AddForeignKey("dbo.ElectricityBills", "ElectricityContract_ID", "dbo.ElectricityContracts", "ID");
            AddForeignKey("dbo.GasBills", "GasContract_ID", "dbo.GasContracts", "ID");
            AddForeignKey("dbo.NetflixTVBills", "NetflixTVContract_ID", "dbo.NetflixTVContracts", "ID");
            AddForeignKey("dbo.SkyTVBills", "SkyTVContract_ID", "dbo.SkyTVContracts", "ID");
            AddForeignKey("dbo.TVLicenseBills", "TVLicenseContract_ID", "dbo.TVLicenseContracts", "ID");
            AddForeignKey("dbo.WaterBills", "WaterContract_ID", "dbo.WaterContracts", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WaterContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.TVLicenseContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.SkyTVContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.NetflixTVContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.LineRentalContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.LandlinePhoneContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.GasContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.ElectricityContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.BroadbandContracts", "ID", "dbo.Contracts");
            DropForeignKey("dbo.WaterBills", "WaterContract_ID", "dbo.WaterContracts");
            DropForeignKey("dbo.TVLicenseBills", "TVLicenseContract_ID", "dbo.TVLicenseContracts");
            DropForeignKey("dbo.SkyTVBills", "SkyTVContract_ID", "dbo.SkyTVContracts");
            DropForeignKey("dbo.NetflixTVBills", "NetflixTVContract_ID", "dbo.NetflixTVContracts");
            DropForeignKey("dbo.GasBills", "GasContract_ID", "dbo.GasContracts");
            DropForeignKey("dbo.ElectricityBills", "ElectricityContract_ID", "dbo.ElectricityContracts");
            DropForeignKey("dbo.LineRentalBills", "LineRentalContract_ID", "dbo.LineRentalContracts");
            DropForeignKey("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID", "dbo.LandlinePhoneContracts");
            DropForeignKey("dbo.BroadbandBills", "BroadbandContract_ID", "dbo.BroadbandContracts");
            DropForeignKey("dbo.Contracts", "RequestedBy_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Contracts", "Provider_ID", "dbo.Providers");
            DropForeignKey("dbo.Contracts", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.WaterContracts", new[] { "ID" });
            DropIndex("dbo.TVLicenseContracts", new[] { "ID" });
            DropIndex("dbo.SkyTVContracts", new[] { "ID" });
            DropIndex("dbo.NetflixTVContracts", new[] { "ID" });
            DropIndex("dbo.LineRentalContracts", new[] { "ID" });
            DropIndex("dbo.LandlinePhoneContracts", new[] { "ID" });
            DropIndex("dbo.GasContracts", new[] { "ID" });
            DropIndex("dbo.ElectricityContracts", new[] { "ID" });
            DropIndex("dbo.BroadbandContracts", new[] { "ID" });
            DropIndex("dbo.WaterBills", new[] { "WaterContract_ID" });
            DropIndex("dbo.TVLicenseBills", new[] { "TVLicenseContract_ID" });
            DropIndex("dbo.SkyTVBills", new[] { "SkyTVContract_ID" });
            DropIndex("dbo.NetflixTVBills", new[] { "NetflixTVContract_ID" });
            DropIndex("dbo.GasBills", new[] { "GasContract_ID" });
            DropIndex("dbo.ElectricityBills", new[] { "ElectricityContract_ID" });
            DropIndex("dbo.LineRentalBills", new[] { "LineRentalContract_ID" });
            DropIndex("dbo.LandlinePhoneBills", new[] { "LandlinePhoneContract_ID" });
            DropIndex("dbo.BroadbandBills", new[] { "BroadbandContract_ID" });
            DropIndex("dbo.Contracts", new[] { "Provider_ID" });
            DropIndex("dbo.Contracts", new[] { "RequestedBy_ID" });
            DropIndex("dbo.Contracts", new[] { "House_ID" });
            DropColumn("dbo.WaterBills", "WaterContract_ID");
            DropColumn("dbo.TVLicenseBills", "TVLicenseContract_ID");
            DropColumn("dbo.SkyTVBills", "SkyTVContract_ID");
            DropColumn("dbo.NetflixTVBills", "NetflixTVContract_ID");
            DropColumn("dbo.LineRentalBills", "LineRentalContract_ID");
            DropColumn("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID");
            DropColumn("dbo.GasBills", "GasContract_ID");
            DropColumn("dbo.ElectricityBills", "ElectricityContract_ID");
            DropColumn("dbo.BroadbandBills", "BroadbandContract_ID");
            DropTable("dbo.WaterContracts");
            DropTable("dbo.TVLicenseContracts");
            DropTable("dbo.SkyTVContracts");
            DropTable("dbo.NetflixTVContracts");
            DropTable("dbo.LineRentalContracts");
            DropTable("dbo.LandlinePhoneContracts");
            DropTable("dbo.GasContracts");
            DropTable("dbo.ElectricityContracts");
            DropTable("dbo.BroadbandContracts");
            DropTable("dbo.Providers");
            DropTable("dbo.Contracts");
        }
    }
}
