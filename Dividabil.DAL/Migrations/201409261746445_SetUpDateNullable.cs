namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetUpDateNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HouseModels", "SetUpDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HouseModels", "SetUpDate", c => c.DateTime(nullable: false));
        }
    }
}
