namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SetDefaultBroadbandTypeForHouseModel : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.HouseModels SET BroadbandType = '0' "); 
            AlterColumn("dbo.HouseModels", "BroadbandType", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HouseModels", "BroadbandType", c => c.Int(nullable: true));
        }
    }
}
