namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBillingRunUserName : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillingRun", "InitiatedByUserId", "dbo.AspNetUsers");
            DropIndex("dbo.BillingRun", new[] { "InitiatedByUserId" });
            AddColumn("dbo.BillingRun", "InitiatedByUserName", c => c.String());
            AlterColumn("dbo.BillingRun", "InitiatedByUserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BillingRun", "InitiatedByUserId", c => c.String(maxLength: 128));
            DropColumn("dbo.BillingRun", "InitiatedByUserName");
            CreateIndex("dbo.BillingRun", "InitiatedByUserId");
            AddForeignKey("dbo.BillingRun", "InitiatedByUserId", "dbo.AspNetUsers", "Id");
        }
    }
}
