namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLandlineTypeToHouseModel : DbMigration
    {
        public override void Up()
        {
            //Sql("UPDATE dbo.HouseModels SET LandlineType = '0' "); 
            AddColumn("dbo.HouseModels", "LandlineType", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "LandlineType");
        }
    }
}
