namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveBillsIdentityColumns : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BroadbandBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.LandlinePhoneBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.LineRentalBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.ElectricityBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.GasBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.NetflixTVBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.SkyTVBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.TVLicenseBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.WaterBills", "Id", c => c.Int(nullable: false, identity: false));
            AlterColumn("dbo.DeliveryBills", "Id", c => c.Int(nullable: false, identity: false));
        }
        
        public override void Down()
        {
        }
    }
}
