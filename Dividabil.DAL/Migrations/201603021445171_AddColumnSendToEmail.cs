namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnSendToEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSentMessages", "SendToEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSentMessages", "SendToEmail");
        }
    }
}
