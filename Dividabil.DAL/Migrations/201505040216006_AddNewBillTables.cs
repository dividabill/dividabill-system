namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewBillTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BroadbandBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersBroadbandBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.BroadbandBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.LandlinePhoneBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersLandlinePhoneBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.LandlinePhoneBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.LineRentalBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersLineRentalBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                        LineRentalBill_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.LandlinePhoneBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.LineRentalBills", t => t.LineRentalBill_Id)
                .Index(t => t.UserId)
                .Index(t => t.BillId)
                .Index(t => t.LineRentalBill_Id);
            
            CreateTable(
                "dbo.NetflixTVBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersNetflixTVBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.NetflixTVBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.SkyTVBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersSkyTVBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.SkyTVBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.TVLicenseBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UsersTVLicenseBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.TVLicenseBills", t => t.BillId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            AddColumn("dbo.HouseholdBills", "BroadbandBill_Id", c => c.Int());
            AddColumn("dbo.HouseholdBills", "LandlinePhoneBill_Id", c => c.Int());
            AddColumn("dbo.HouseholdBills", "LineRentalBill_Id", c => c.Int());
            AddColumn("dbo.HouseholdBills", "NetflixTVBill_Id", c => c.Int());
            AddColumn("dbo.HouseholdBills", "SkyTVBill_Id", c => c.Int());
            AddColumn("dbo.HouseholdBills", "TVLicenseBill_Id", c => c.Int());
            CreateIndex("dbo.HouseholdBills", "BroadbandBill_Id");
            CreateIndex("dbo.HouseholdBills", "LandlinePhoneBill_Id");
            CreateIndex("dbo.HouseholdBills", "LineRentalBill_Id");
            CreateIndex("dbo.HouseholdBills", "NetflixTVBill_Id");
            CreateIndex("dbo.HouseholdBills", "SkyTVBill_Id");
            CreateIndex("dbo.HouseholdBills", "TVLicenseBill_Id");
            AddForeignKey("dbo.HouseholdBills", "BroadbandBill_Id", "dbo.BroadbandBills", "Id");
            AddForeignKey("dbo.HouseholdBills", "LandlinePhoneBill_Id", "dbo.LandlinePhoneBills", "Id");
            AddForeignKey("dbo.HouseholdBills", "LineRentalBill_Id", "dbo.LineRentalBills", "Id");
            AddForeignKey("dbo.HouseholdBills", "NetflixTVBill_Id", "dbo.NetflixTVBills", "Id");
            AddForeignKey("dbo.HouseholdBills", "SkyTVBill_Id", "dbo.SkyTVBills", "Id");
            AddForeignKey("dbo.HouseholdBills", "TVLicenseBill_Id", "dbo.TVLicenseBills", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HouseholdBills", "TVLicenseBill_Id", "dbo.TVLicenseBills");
            DropForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "BillId", "dbo.TVLicenseBills");
            DropForeignKey("dbo.TVLicenseBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "SkyTVBill_Id", "dbo.SkyTVBills");
            DropForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "BillId", "dbo.SkyTVBills");
            DropForeignKey("dbo.SkyTVBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "NetflixTVBill_Id", "dbo.NetflixTVBills");
            DropForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "BillId", "dbo.NetflixTVBills");
            DropForeignKey("dbo.NetflixTVBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "LineRentalBill_Id", "dbo.LineRentalBills");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "LineRentalBill_Id", "dbo.LineRentalBills");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills");
            DropForeignKey("dbo.LineRentalBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "LandlinePhoneBill_Id", "dbo.LandlinePhoneBills");
            DropForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills");
            DropForeignKey("dbo.LandlinePhoneBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "BroadbandBill_Id", "dbo.BroadbandBills");
            DropForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "BillId", "dbo.BroadbandBills");
            DropForeignKey("dbo.BroadbandBills", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.UsersTVLicenseBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersTVLicenseBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.TVLicenseBills", new[] { "House_ID" });
            DropIndex("dbo.UsersSkyTVBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersSkyTVBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.SkyTVBills", new[] { "House_ID" });
            DropIndex("dbo.UsersNetflixTVBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersNetflixTVBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.NetflixTVBills", new[] { "House_ID" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "LineRentalBill_Id" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.LineRentalBills", new[] { "House_ID" });
            DropIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.LandlinePhoneBills", new[] { "House_ID" });
            DropIndex("dbo.UsersBroadbandBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersBroadbandBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.BroadbandBills", new[] { "House_ID" });
            DropIndex("dbo.HouseholdBills", new[] { "TVLicenseBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "SkyTVBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "NetflixTVBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "LineRentalBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "LandlinePhoneBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "BroadbandBill_Id" });
            DropColumn("dbo.HouseholdBills", "TVLicenseBill_Id");
            DropColumn("dbo.HouseholdBills", "SkyTVBill_Id");
            DropColumn("dbo.HouseholdBills", "NetflixTVBill_Id");
            DropColumn("dbo.HouseholdBills", "LineRentalBill_Id");
            DropColumn("dbo.HouseholdBills", "LandlinePhoneBill_Id");
            DropColumn("dbo.HouseholdBills", "BroadbandBill_Id");
            DropTable("dbo.UsersTVLicenseBillsBillsPercentages");
            DropTable("dbo.TVLicenseBills");
            DropTable("dbo.UsersSkyTVBillsBillsPercentages");
            DropTable("dbo.SkyTVBills");
            DropTable("dbo.UsersNetflixTVBillsBillsPercentages");
            DropTable("dbo.NetflixTVBills");
            DropTable("dbo.UsersLineRentalBillsBillsPercentages");
            DropTable("dbo.LineRentalBills");
            DropTable("dbo.UsersLandlinePhoneBillsBillsPercentages");
            DropTable("dbo.LandlinePhoneBills");
            DropTable("dbo.UsersBroadbandBillsBillsPercentages");
            DropTable("dbo.BroadbandBills");
        }
    }
}
