namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQniquenessToPostCode : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PostCodes", "Postcode", c => c.String(maxLength: 15));
            CreateIndex("dbo.PostCodes", "Postcode", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostCodes", new[] { "Postcode" });
            AlterColumn("dbo.PostCodes", "Postcode", c => c.String());
        }
    }
}
