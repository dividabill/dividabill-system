namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LengthenUserFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "AccountNumber", c => c.String(maxLength: 100));
            AlterColumn("dbo.AspNetUsers", "AccountSortCode", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "AccountSortCode", c => c.String(maxLength: 100));
            AlterColumn("dbo.AspNetUsers", "AccountNumber", c => c.String(maxLength: 100));
        }
    }
}
