using DividaBill.Models.Emails;

namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SendGridEmailTemplateTables : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE TABLE [dbo].[BillingRunTemp](
	[ID] [int] NOT NULL,
	[Period] [datetime] NULL,
	[InitiatedByUserId] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[InitiatedByUserName] [nvarchar](max) NULL,
	[SendGridEmailId] [nvarchar](max) NULL,
);");

            Sql(@"INSERT INTO [dbo].[BillingRunTemp] (ID, Period, InitiatedByUserId, CreatedAt, LastModified, InitiatedByUserName, SendGridEmailId)
SELECT 
       br.*
FROM   BillingRun AS br");

            DropForeignKey("dbo.HouseEmailMessages", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.UserEmailMessages", "User_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.BillEmailMessages", "ID", "dbo.UserEmailMessages");
            DropForeignKey("dbo.BillEmailMessages", "HouseholdBill_ID", "dbo.HouseholdBills");
            DropForeignKey("dbo.BillEmailMessages", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.UserEmailMessages", new[] { "User_ID" });
            DropIndex("dbo.HouseEmailMessages", new[] { "House_ID" });
            DropIndex("dbo.BillEmailMessages", new[] { "ID" });
            DropIndex("dbo.BillEmailMessages", new[] { "HouseholdBill_ID" });
            DropIndex("dbo.BillEmailMessages", new[] { "House_ID" });
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TemplateId = c.String(maxLength: 36),
                        VersionId = c.String(maxLength: 36),
                        Name = c.String(maxLength: 100),
                        HtmlContent = c.String(unicode: false, storeType: "text"),
                        PlainContent = c.String(unicode: false, storeType: "text"),
                        Subject = c.String(maxLength: 500),
                        EmailProviderId = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        LastModified = c.DateTime(),
                        UsedFrom = c.DateTime(nullable: false),
                        UsedTo = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmailProviders", t => t.EmailProviderId)
                .Index(t => t.EmailProviderId);
            
            CreateTable(
                "dbo.EmailProviders",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(maxLength: 128),
                        Description = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailSentMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        EmailTemplateId = c.Int(nullable: false),
                        Subject = c.String(),
                        SentFromEmail = c.String(),
                        SentFromEmailDisplayName = c.String(),
                        EmailTypeId = c.Int(nullable: false),
                        TemplateSubstitutedValues = c.String(),
                        CreatedAt = c.DateTime(),
                        LastModified = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmailTemplates", t => t.EmailTemplateId)
                .ForeignKey("dbo.EmailTypes", t => t.EmailTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.EmailTemplateId)
                .Index(t => t.EmailTypeId);
            
            CreateTable(
                "dbo.EmailTypes",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(maxLength: 128),
                        Description = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.BillingRun", "EmailTemplateId", c => c.Int());
            AddColumn("dbo.BillingRun", "EmailSubject", c => c.String());
            AddColumn("dbo.BillingRun", "EmailSendFrom", c => c.String());
            AddColumn("dbo.BillingRun", "EmailSendFromDisplayName", c => c.String());
            AddColumn("dbo.BillingRun", "Name", c => c.String(maxLength: 128));
            AddColumn("dbo.BillingRun", "Description", c => c.String(maxLength: 256));
            CreateIndex("dbo.BillingRun", "EmailTemplateId");
            AddForeignKey("dbo.BillingRun", "EmailTemplateId", "dbo.EmailTemplates", "ID");
            DropColumn("dbo.BillingRun", "SendGridEmailId");
            DropTable("dbo.UserEmailMessages");
            DropTable("dbo.HouseEmailMessages");
            DropTable("dbo.BillEmailMessages");

            Sql($@"INSERT INTO dbo.EmailProviders (ID, NAME, [Description])
SELECT 
       *
FROM   (
  	        SELECT {EmailProvider.SendGridProvider.ID} id,'{EmailProvider.SendGridProvider.Name}' NAME,'{EmailProvider.SendGridProvider.Description}' DESCRIPTION
        )S");

            var emailTypesInsertQuery = $@"INSERT INTO EmailTypes (ID, NAME, Description)
SELECT 
       *
FROM   (";
            for (var index = 0; index < EmailType.All.Length; index++)
            {
                var item = EmailType.All[index];
                if (index != 0)
                {
                    emailTypesInsertQuery += Environment.NewLine + "UNION ALL ";
                }
                emailTypesInsertQuery += $"SELECT {item.ID} id,'{item.Name}' NAME,'{item.Description}' DESCRIPTION";
            }
            emailTypesInsertQuery += ")S";
            Sql(emailTypesInsertQuery);
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BillEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        HouseholdBill_ID = c.Int(),
                        House_ID = c.Int(),
                        SentToUser = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.HouseEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        House_ID = c.Int(nullable: false),
                        Sent = c.DateTime(nullable: false),
                        Message = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User_ID = c.String(maxLength: 128),
                        Sent = c.DateTime(nullable: false),
                        Message = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.BillingRun", "SendGridEmailId", c => c.String());
            DropForeignKey("dbo.EmailSentMessages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EmailSentMessages", "EmailTypeId", "dbo.EmailTypes");
            DropForeignKey("dbo.EmailSentMessages", "EmailTemplateId", "dbo.EmailTemplates");
            DropForeignKey("dbo.BillingRun", "EmailTemplateId", "dbo.EmailTemplates");
            DropForeignKey("dbo.EmailTemplates", "EmailProviderId", "dbo.EmailProviders");
            DropIndex("dbo.EmailSentMessages", new[] { "EmailTypeId" });
            DropIndex("dbo.EmailSentMessages", new[] { "EmailTemplateId" });
            DropIndex("dbo.EmailSentMessages", new[] { "UserId" });
            DropIndex("dbo.EmailTemplates", new[] { "EmailProviderId" });
            DropIndex("dbo.BillingRun", new[] { "EmailTemplateId" });
            DropColumn("dbo.BillingRun", "Description");
            DropColumn("dbo.BillingRun", "Name");
            DropColumn("dbo.BillingRun", "EmailSendFromDisplayName");
            DropColumn("dbo.BillingRun", "EmailSendFrom");
            DropColumn("dbo.BillingRun", "EmailSubject");
            DropColumn("dbo.BillingRun", "EmailTemplateId");
            DropTable("dbo.EmailTypes");
            DropTable("dbo.EmailSentMessages");
            DropTable("dbo.EmailProviders");
            DropTable("dbo.EmailTemplates");
            CreateIndex("dbo.BillEmailMessages", "House_ID");
            CreateIndex("dbo.BillEmailMessages", "HouseholdBill_ID");
            CreateIndex("dbo.BillEmailMessages", "ID");
            CreateIndex("dbo.HouseEmailMessages", "House_ID");
            CreateIndex("dbo.UserEmailMessages", "User_ID");
            AddForeignKey("dbo.BillEmailMessages", "House_ID", "dbo.HouseModels", "ID");
            AddForeignKey("dbo.BillEmailMessages", "HouseholdBill_ID", "dbo.HouseholdBills", "ID");
            AddForeignKey("dbo.BillEmailMessages", "ID", "dbo.UserEmailMessages", "ID");
            AddForeignKey("dbo.UserEmailMessages", "User_ID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.HouseEmailMessages", "House_ID", "dbo.HouseModels", "ID");

            Sql(@"DROP TABLE dbo.BillingRunTemp");
        }
    }
}
