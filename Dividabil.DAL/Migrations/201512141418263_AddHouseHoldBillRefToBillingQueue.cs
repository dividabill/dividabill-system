namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHouseHoldBillRefToBillingQueue : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.BillingQueue", name: "HouseholdBill_ID", newName: "HouseholdBillId");
            RenameIndex(table: "dbo.BillingQueue", name: "IX_HouseholdBill_ID", newName: "IX_HouseholdBillId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BillingQueue", name: "IX_HouseholdBillId", newName: "IX_HouseholdBill_ID");
            RenameColumn(table: "dbo.BillingQueue", name: "HouseholdBillId", newName: "HouseholdBill_ID");
        }
    }
}
