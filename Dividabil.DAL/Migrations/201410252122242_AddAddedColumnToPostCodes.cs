namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddedColumnToPostCodes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostCodes", "Added", c => c.DateTimeOffset(precision: 7, defaultValueSql: "SYSDATETIMEOFFSET()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostCodes", "Added");
        }
    }
}
