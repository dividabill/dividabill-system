namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EstimatedPriceAddSeparateColumnsForTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseEstimatedPrices", "Water", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "Gas", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "Electricity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PostcodeEstimatedPrices", "Water", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PostcodeEstimatedPrices", "Gas", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PostcodeEstimatedPrices", "Electricity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.HouseEstimatedPrices", "type");
            DropColumn("dbo.HouseEstimatedPrices", "internalPrice");
            DropColumn("dbo.PostcodeEstimatedPrices", "type");
            DropColumn("dbo.PostcodeEstimatedPrices", "internalPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostcodeEstimatedPrices", "internalPrice", c => c.String());
            AddColumn("dbo.PostcodeEstimatedPrices", "type", c => c.Int(nullable: false));
            AddColumn("dbo.HouseEstimatedPrices", "internalPrice", c => c.String());
            AddColumn("dbo.HouseEstimatedPrices", "type", c => c.Int(nullable: false));
            DropColumn("dbo.PostcodeEstimatedPrices", "Electricity");
            DropColumn("dbo.PostcodeEstimatedPrices", "Gas");
            DropColumn("dbo.PostcodeEstimatedPrices", "Water");
            DropColumn("dbo.HouseEstimatedPrices", "Electricity");
            DropColumn("dbo.HouseEstimatedPrices", "Gas");
            DropColumn("dbo.HouseEstimatedPrices", "Water");
        }
    }
}
