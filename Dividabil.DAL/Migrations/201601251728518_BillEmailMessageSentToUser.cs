namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BillEmailMessageSentToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillEmailMessages", "SentToUser", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BillEmailMessages", "SentToUser");
        }
    }
}
