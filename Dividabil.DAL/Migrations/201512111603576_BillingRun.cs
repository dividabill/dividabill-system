namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BillingRun : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillingQueue",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HouseId = c.Int(nullable: false),
                        DistributedProcessorId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        LastModified = c.DateTime(),
                        BillingRunId = c.Int(nullable: false),
                        BillingQueue_ID = c.Int(),
                        HouseholdBill_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BillingQueue", t => t.BillingQueue_ID)
                .ForeignKey("dbo.BillingRun", t => t.BillingRunId, cascadeDelete: true)
                .ForeignKey("dbo.HouseModels", t => t.HouseId, cascadeDelete: true)
                .ForeignKey("dbo.HouseholdBills", t => t.HouseholdBill_ID)
                .Index(t => t.HouseId)
                .Index(t => t.BillingRunId)
                .Index(t => t.BillingQueue_ID)
                .Index(t => t.HouseholdBill_ID);
            
            CreateTable(
                "dbo.BillingRun",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Period = c.DateTime(),
                        InitiatedByUserId = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(),
                        LastModified = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.InitiatedByUserId)
                .Index(t => t.InitiatedByUserId);
            
            CreateTable(
                "dbo.BillingQueueTransactionLogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BillingQueueId = c.Int(nullable: false),
                        BillingQueueStage = c.Int(nullable: false),
                        Changes = c.String(),
                        CreatedAt = c.DateTime(),
                        LastModified = c.DateTime(),
                        TransactionLogStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BillingQueue", t => t.BillingQueueId, cascadeDelete: true)
                .Index(t => t.BillingQueueId);
            
            AddColumn("dbo.HouseModels", "BillingRun_ID", c => c.Int());
            CreateIndex("dbo.HouseModels", "BillingRun_ID");
            AddForeignKey("dbo.HouseModels", "BillingRun_ID", "dbo.BillingRun", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillingQueueTransactionLogs", "BillingQueueId", "dbo.BillingQueue");
            DropForeignKey("dbo.BillingQueue", "HouseholdBill_ID", "dbo.HouseholdBills");
            DropForeignKey("dbo.BillingQueue", "HouseId", "dbo.HouseModels");
            DropForeignKey("dbo.BillingQueue", "BillingRunId", "dbo.BillingRun");
            DropForeignKey("dbo.BillingRun", "InitiatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.HouseModels", "BillingRun_ID", "dbo.BillingRun");
            DropForeignKey("dbo.BillingQueue", "BillingQueue_ID", "dbo.BillingQueue");
            DropIndex("dbo.BillingQueueTransactionLogs", new[] { "BillingQueueId" });
            DropIndex("dbo.BillingRun", new[] { "InitiatedByUserId" });
            DropIndex("dbo.BillingQueue", new[] { "HouseholdBill_ID" });
            DropIndex("dbo.BillingQueue", new[] { "BillingQueue_ID" });
            DropIndex("dbo.BillingQueue", new[] { "BillingRunId" });
            DropIndex("dbo.BillingQueue", new[] { "HouseId" });
            DropIndex("dbo.HouseModels", new[] { "BillingRun_ID" });
            DropColumn("dbo.HouseModels", "BillingRun_ID");
            DropTable("dbo.BillingQueueTransactionLogs");
            DropTable("dbo.BillingRun");
            DropTable("dbo.BillingQueue");
        }
    }
}
