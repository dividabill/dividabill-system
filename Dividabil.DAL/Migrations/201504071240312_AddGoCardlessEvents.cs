namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoCardlessEvents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankMandateEvents",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        Mandate_ID = c.String(maxLength: 128),
                        created_at = c.String(),
                        action = c.String(),
                        resource_type = c.String(),
                        details = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BankMandates", t => t.Mandate_ID)
                .Index(t => t.Mandate_ID);
            
            CreateTable(
                "dbo.BankPaymentEvents",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        Payment_ID = c.String(maxLength: 128),
                        created_at = c.String(),
                        action = c.String(),
                        resource_type = c.String(),
                        details = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BankPayments", t => t.Payment_ID)
                .Index(t => t.Payment_ID);
            
            DropTable("dbo.GoCardlessEvents");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GoCardlessEvents",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        created_at = c.String(),
                        eventMessage = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            DropForeignKey("dbo.BankPaymentEvents", "Payment_ID", "dbo.BankPayments");
            DropForeignKey("dbo.BankMandateEvents", "Mandate_ID", "dbo.BankMandates");
            DropIndex("dbo.BankPaymentEvents", new[] { "Payment_ID" });
            DropIndex("dbo.BankMandateEvents", new[] { "Mandate_ID" });
            DropTable("dbo.BankPaymentEvents");
            DropTable("dbo.BankMandateEvents");
        }
    }
}
