namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewBillSystem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ElectricityBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.GasBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.WaterBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WaterBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.GasBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.ElectricityBills", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.WaterBills", new[] { "House_ID" });
            DropIndex("dbo.GasBills", new[] { "House_ID" });
            DropIndex("dbo.ElectricityBills", new[] { "House_ID" });
            DropTable("dbo.WaterBills");
            DropTable("dbo.GasBills");
            DropTable("dbo.ElectricityBills");
        }
    }
}
