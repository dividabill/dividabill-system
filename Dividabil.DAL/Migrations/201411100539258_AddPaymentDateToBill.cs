namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentDateToBill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "PaymentDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bills", "PaymentDate");
        }
    }
}
