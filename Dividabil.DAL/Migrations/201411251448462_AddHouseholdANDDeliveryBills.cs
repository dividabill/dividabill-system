namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHouseholdANDDeliveryBills : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.HouseholdBills",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        House_ID = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        DeliveryBill_Id = c.Int(),
                        ElectricityBill_Id = c.Int(),
                        GasBill_Id = c.Int(),
                        WaterBill_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryBills", t => t.DeliveryBill_Id)
                .ForeignKey("dbo.ElectricityBills", t => t.ElectricityBill_Id)
                .ForeignKey("dbo.GasBills", t => t.GasBill_Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .ForeignKey("dbo.WaterBills", t => t.WaterBill_Id)
                .Index(t => t.House_ID)
                .Index(t => t.DeliveryBill_Id)
                .Index(t => t.ElectricityBill_Id)
                .Index(t => t.GasBill_Id)
                .Index(t => t.WaterBill_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HouseholdBills", "WaterBill_Id", "dbo.WaterBills");
            DropForeignKey("dbo.HouseholdBills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseholdBills", "GasBill_Id", "dbo.GasBills");
            DropForeignKey("dbo.HouseholdBills", "ElectricityBill_Id", "dbo.ElectricityBills");
            DropForeignKey("dbo.HouseholdBills", "DeliveryBill_Id", "dbo.DeliveryBills");
            DropForeignKey("dbo.DeliveryBills", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.HouseholdBills", new[] { "WaterBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "GasBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "ElectricityBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "DeliveryBill_Id" });
            DropIndex("dbo.HouseholdBills", new[] { "House_ID" });
            DropIndex("dbo.DeliveryBills", new[] { "House_ID" });
            DropTable("dbo.HouseholdBills");
            DropTable("dbo.DeliveryBills");
        }
    }
}
