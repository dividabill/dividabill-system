namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTEMPPostcodeEstimatedPrices2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostcodeEstimatedPrice2",
                c => new
                    {
                        PostCode_ID = c.String(nullable: false, maxLength: 15),
                        Water = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Gas = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Electricity = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PostCode_ID)
                .ForeignKey("dbo.PostCode2", t => t.PostCode_ID)
                .Index(t => t.PostCode_ID, unique: true);

            /*
             * 
INSERT INTO dbo.PostcodeEstimatedPrice2 ([PostCode_ID], [Water], [Gas], [Electricity]) SELECT [Postcode], [Water], [Gas], [Electricity] FROM dbo.PostcodeEstimatedPrices JOIN dbo.PostCodes ON dbo.PostcodeEstimatedPrices.PostCode_ID = dbo.PostCodes.ID ORDER BY dbo.PostcodeEstimatedPrices.ID OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY;
INSERT INTO dbo.PostcodeEstimatedPrice2 ([PostCode_ID], [Water], [Gas], [Electricity]) SELECT [Postcode], [Water], [Gas], [Electricity] FROM dbo.PostcodeEstimatedPrices JOIN dbo.PostCodes ON dbo.PostcodeEstimatedPrices.PostCode_ID = dbo.PostCodes.ID ORDER BY dbo.PostcodeEstimatedPrices.ID OFFSET 500000 ROWS FETCH NEXT 500000 ROWS ONLY;
INSERT INTO dbo.PostcodeEstimatedPrice2 ([PostCode_ID], [Water], [Gas], [Electricity]) SELECT [Postcode], [Water], [Gas], [Electricity] FROM dbo.PostcodeEstimatedPrices JOIN dbo.PostCodes ON dbo.PostcodeEstimatedPrices.PostCode_ID = dbo.PostCodes.ID ORDER BY dbo.PostcodeEstimatedPrices.ID OFFSET 1000000 ROWS FETCH NEXT 500000 ROWS ONLY;
INSERT INTO dbo.PostcodeEstimatedPrice2 ([PostCode_ID], [Water], [Gas], [Electricity]) SELECT [Postcode], [Water], [Gas], [Electricity] FROM dbo.PostcodeEstimatedPrices JOIN dbo.PostCodes ON dbo.PostcodeEstimatedPrices.PostCode_ID = dbo.PostCodes.ID ORDER BY dbo.PostcodeEstimatedPrices.ID OFFSET 1500000 ROWS FETCH NEXT 500000 ROWS ONLY;
             * 
             */

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostcodeEstimatedPrice2", "PostCode_ID", "dbo.PostCode2");
            DropIndex("dbo.PostcodeEstimatedPrice2", new[] { "PostCode_ID" });
            DropTable("dbo.PostcodeEstimatedPrice2");
        }
    }
}
