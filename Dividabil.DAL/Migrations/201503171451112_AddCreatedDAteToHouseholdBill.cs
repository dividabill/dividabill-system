namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedDAteToHouseholdBill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseholdBills", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseholdBills", "CreatedDate");
        }
    }
}
