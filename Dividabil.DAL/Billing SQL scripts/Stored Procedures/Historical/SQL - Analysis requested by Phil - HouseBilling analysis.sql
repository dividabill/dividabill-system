
--- Total Active Houses and non Active Houses ---
DECLARE @dummyHouse INT = 1270


SELECT 'ActiveHouseIds' as Status, ID 'HouseID' 
INTO #activehouses
FROM housemodels WHERE startdate <= getdate() AND enddate > getdate() and id != @dummyHouse

SELECT 'Total Houses >>>', COUNT(*) FROM housemodels where id != @dummyHouse
UNION ALL
SELECT 'Total Active Houses >>>', COUNT(*) FROM housemodels WHERE startdate <= getdate() AND enddate > getdate() and id != @dummyHouse
UNION ALL
SELECT 'Total Non Active Houses >>>', COUNT(*) FROM housemodels WHERE ID NOT IN (SELECT HouseID FROM #activehouses) and id != @dummyHouse
UNION ALL
SELECT * FROM #activehouses
UNION ALL
SELECT 'NonActiveHouses', ID 'HouseID' FROM housemodels WHERE ID NOT IN (SELECT HouseID FROM #activehouses) and id != @dummyHouse


DROP TABLE #activehouses



--- Total Amount billed paid ---
SELECT 'Paid bills',
[Year] 
, [Month]
, hb.House_ID
, CONVERT(MONEY, SUM(bb.ammount)) as 'TotalAmountBilled_FromTableBillbases'
, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
, bp.isPaid
FROM HouseHoldbills hb
INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id
INNER JOIN billpayments bp ON bp.bill_id = bb.id
WHERE hb.HOUSE_ID != @dummyHouse 
and bp.isPaid = 1
GROUP BY hb.House_ID, [Year], [Month],  bp.isPaid
ORDER BY hb.House_ID, [Year] DESC, [Month] DESC,  bp.isPaid


--- Total Amount billed unpaid ---
SELECT 'Unpaid bills',
[Year] 
, [Month]
, hb.House_ID
, bb.ammount as 'TotalAmountBilled_FromTableBillbases'
, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
, bp.isPaid
FROM HouseHoldbills hb
INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id
INNER JOIN billpayments bp ON bp.bill_id = bb.id
WHERE hb.HOUSE_ID != @dummyHouse 
AND bp.isPaid = 0
GROUP BY hb.House_ID, [Year], [Month], bb.ammount, bp.isPaid
ORDER BY hb.House_ID, [Year] DESC, [Month] DESC,  bp.isPaid

/*
1. a report showing the number of active houses for October and detail the  number of tenants, total amount billed
2. the number of houses using each service (ie water, gas and elec etc), and the total billed for each of the services.  If you could produce this report for each month back to the start of 2014.  Can you also included on this report the total number of non active houses in the monthly summary
*/


--SELECT 'ActiveHouseIds' as Status, ID 'HouseID', HouseMates
--INTO #activehouses
--FROM housemodels 
--WHERE StartDate <= '2015-10-31' and EndDate >= '2015-10-01'

--select * from #activehouses

SELECT * FROM ServiceTypes


/*
3. a list of the houses that were biled for each month for the last 6 months including house id, address, number of tenants, services signed up to, amount that was billed by service, amount that was paid or unpaid if available, date it was billed 
*/
SELECT *
INTO #ServicesList
FROM (SELECT Id, Name FROM ServiceTypes) as t


DECLARE @N INT = (SELECT MAX(ID) FROM #ServicesList)

DECLARE @T INT = 1
DECLARE @ServiceType VARCHAR(20)


WHILE @T <= @N 

BEGIN 

	SELECT @ServiceType = Name FROM #ServicesList WHERE ID = @T

	SELECT 'Billing by Service' as 'Report #3',
	@ServiceType as 'Service'
	, hb.[Year] 
	, hb.[Month]
	, CASE hb.[Month] 
		WHEN 1 THEN 'January'
		WHEN 2 THEN 'February'
		WHEN 3 THEN 'Mars'
		WHEN 4 THEN 'April'
		WHEN 5 THEN 'May'
		WHEN 6 THEN 'June'
		WHEN 7 THEN 'July'
		WHEN 8 THEN 'August'
		WHEN 9 THEN 'September'
		WHEN 10 THEN 'October'
		WHEN 11 THEN 'November'
		WHEN 12 THEN 'December'
	  END AS 'Month Name'
	, hb.House_ID
	, h.HouseMates
	, h.Address_Line1
	, h.Address_City
	, h.Address_County
	, hb.CreatedDate
	, bb.ammount as 'TotalAmountBilled_FromTableBillbases'
	, CONVERT(MONEY, SUM(bp.amount)) as 'TotalAmountBilled_FromTablebillpayments'
	, CASE bp.isPaid WHEN 1 THEN 'Yes' ELSE 'No' END as 'IsPaid?'
	FROM HouseHoldbills hb
		INNER JOIN Billbases bb ON hb.ID = bb.HouseHoldbill_id AND hb.House_ID != @dummyHouse AND hb.CreatedDate >= '2015-05-01'
		INNER JOIN billpayments bp ON bp.bill_id = bb.id
		INNER JOIN HouseModels h ON hb.House_ID = h.ID 
		INNER JOIN Contracts c on bb.Contract_ID = c.ID AND ServiceNew_ID = @T -- ServiceID = Electricity
	GROUP BY hb.[Year] 
	, hb.[Month]
	, hb.House_ID
	, h.HouseMates
	, h.Address_Line1
	, h.Address_City
	, h.Address_County
	, hb.CreatedDate
	, bb.ammount
	, bp.isPaid
	ORDER BY hb.House_ID, [Year] DESC, [Month] DESC,  bp.isPaid


	SET @T = @T + 1
END


DROP TABLE #Service


--exec sp_help housemodels
--select * from housemodels


--select TOP 5 'billpayments', * from billpayments
--select TOP 5 'Billbases', * from Billbases

--select * from contracts where id in (1049,1050,1051,1054,1055)
--SELECT 'Contracts', c.* FROM Contracts c 
--INNER JOIN (select TOP 5 * from Billbases) bb on bb.Contract_ID = c.ID
--select TOP 5 'householdbills', * FROM householdbills