/*2. a list of the houses that haven't been billed month by month since the system started back in 2014.  I need detail on house id, address, number of tenants, services signed up to, amount that should have been billed by service if it available
*/


SELECT ID AS House_ID
, Housemates
, ISNULL(Address_Line1, 'n/a') as Address_Line1
, ISNULL(Address_City, 'n/a') as Address_City
, ISNULL(Address_County, 'n/a') as Address_County
, CASE WHEN Electricity = 1 THEN 'Yes' ELSE 'No' END AS Electricity
, CASE WHEN Gas = 1 THEN 'Yes' ELSE 'No' END AS Gas
, CASE WHEN Water = 1 THEN 'Yes' ELSE 'No' END AS Water
, CASE WHEN Broadband = 1 THEN 'Yes' ELSE 'No' END AS Broadband
, CASE WHEN LandlinePhone = 1 THEN 'Yes' ELSE 'No' END AS LandlinePhone
, CASE WHEN SkyTV = 1 THEN 'Yes' ELSE 'No' END AS SkyTV
, CASE WHEN TVLicense = 1 THEN 'Yes' ELSE 'No' END AS TVLicense
, CASE WHEN NetflixTV = 1 THEN 'Yes' ELSE 'No' END AS NetflixTV
, StartDate
, EndDate
, RegisteredDate
INTO #allhouses
FROM housemodels


--select * from housemodels
--exec sp_help housemodels


select '#allhouses', * from #allhouses


SELECT A.*
FROM #allhouses a
LEFT JOIN householdbills hb  on a.House_ID = hb.house_ID
WHERE hb.House_ID IS NULL
ORDER BY A.House_ID

drop table #allhouses