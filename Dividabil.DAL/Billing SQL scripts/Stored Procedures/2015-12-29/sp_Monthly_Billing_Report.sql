USE [dividabill-live_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_Monthly_Billing_Report]    Script Date: 29/12/2015 18:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jeff Pinkham
-- Create date: 29/12/2015
-- Description:	Report billing via billpayments not active houses or contracts because that changes over time
--				BillPayments however are whatever was determined at the time to be owed so we work backwards from that 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Monthly_Billing_Report]
	-- Add the parameters for the stored procedure here
	@StartDate DateTime, 
	@EndDate DateTime 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/*
1. a report showing the number of active houses for October and detail the  number of tenants, total amount billed, the number of houses using each service (ie water, gas and elec etc), and the total billed for each of the services.  If you could produce this report for each month back to the start of 2014.  Can you also included on this report the total number of non active houses in the monthly summary
*/


IF (SELECT object_id('TempDB..#DateRange')) IS NOT NULL
BEGIN
    DROP TABLE #DateRange
END

IF (SELECT object_id('TempDB..#HouseServiceAnalysis')) IS NOT NULL
BEGIN
    DROP TABLE #HouseServiceAnalysis
END

IF (SELECT object_id('TempDB..#ACTIVECONTRACTS')) IS NOT NULL
BEGIN
    DROP TABLE #ACTIVECONTRACTS
END

IF (SELECT object_id('TempDB..#BillPayments')) IS NOT NULL
BEGIN
    DROP TABLE #BillPayments
END

IF (SELECT object_id('TempDB..#activehouses')) IS NOT NULL
BEGIN
    DROP TABLE #activehouses
END

IF (SELECT object_id('TempDB..#householdbills')) IS NOT NULL
BEGIN
    DROP TABLE #householdbills
END

--DROP TABLE #ACTIVECONTRACTS, #activehouses, #householdbills, #BillPayments


DECLARE @dummyHouse INT = 1270

CREATE TABLE #DateRange (ID INT IDENTITY(1,1), Firstdate DATETIME, LastDate DATETIME, [year] int, [month] int)

DECLARE @N DATETIME = @Enddate
DECLARE @T DATETIME = @StartDate
DECLARE @L DATETIME

WHILE @T < @N 
BEGIN

	SET @T = (SELECT DATEADD(month, DATEDIFF(month, 0, @T), 0))

	SET @L = (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@T)+1,0)))

	INSERT INTO #DateRange (FirstDate, LastDate)
	SELECT @T, @L

	SET @T = DATEADD(d, 32, @T)
END
UPDATE #DateRange SET [Year] = DATEPART(yy, LastDate), [Month] = DATEPART(mm, LastDate)

--SELECT *, Datename(mm, FirstDate), Datename(mm, LastDate), Datepart(YY, FirstDate), Datepart(YY, LastDate) from #DateRange

--select '#DateRange', * from #DateRange

DECLARE @j INT = (SELECT MAX(ID) FROM #DateRange)
DECLARE @i INT = 1

DECLARE @Firstdate DATETIME
DECLARE @LastDate DATETIME
DECLARE @Month INT, @year INT
DECLARE @ActiveHouseCount INT

CREATE TABLE #HouseServiceAnalysis (
  [Month] INT
, [Year] INT
, [BilledHouseCount] INT
, [Housemates] INT
, [TotalAmountBilled] MONEY
, [Nb of Houses with Electricity contract] INT default 0
, [Total Amount billed for Electricity] MONEY default 0
, [Nb of Houses with Gas contract] INT default 0
, [Total Amount billed for Gas] MONEY default 0
, [Nb of Houses with Water contract] INT default 0
, [Total Amount billed for Water] MONEY default 0
, [Nb of Houses with Broadband contract] INT default 0
, [Total Amount billed for Broadband] MONEY default 0
, [Nb of Houses with NetflixTV contract] INT default 0
, [Total Amount billed for NetflixTV] MONEY default 0
, [Nb of Houses with SkyTV contract] INT default 0
, [Total Amount billed for SkyTV] MONEY default 0
, [Nb of Houses with TVLicense contract] INT default 0
, [Total Amount billed for TVLicense] MONEY default 0
, [Nb of Houses with LandlinePhone contract] INT default 0
, [Total Amount billed for LandlinePhone] MONEY default 0
, [Nb of Houses with LineRental contract] INT default 0
, [Total Amount billed for LineRental] MONEY default 0
)

WHILE @i <= @j
BEGIN

	SELECT @Firstdate = Firstdate, @LastDate = LastDate, @Month = [month], @Year = [year]  FROM #DateRange WHERE ID = @i

	SELECT House_ID, ID AS HouseHoldbill_id
	INTO #householdbills
	FROM HouseHoldbills 
	WHERE month = @month and year = @year

    SELECT distinct 
	   c.[ID]
      ,c.[House_ID]
      ,c.[Service]
      ,c.[StartDate]
      ,c.[EndDate]
      ,c.[Length]
      ,c.[RequestedBy_ID]
      ,c.[Provider_ID]
      ,c.[SubmissionStatus]
      ,c.[ServiceNew_ID]
      ,c.[PackageRaw]
      ,c.[RequestedStartDate]
	INTO #ACTIVECONTRACTS
	FROM BillPayments bp
	INNER JOIN Billbases bb ON bp.Bill_Id = bb.ID --AND hb.HouseHoldbill_id = bb.HouseHoldbill_id
	INNER JOIN #householdbills hb ON bb.HouseholdBill_Id = hb.HouseHoldbill_id
	left JOIN Contracts C on bb.Contract_Id = c.ID

	SELECT distinct 'ActiveHouseIds' as Status
	, H.ID-- AS 'ID' 
	INTO #activehouses
	FROM #ACTIVECONTRACTS C 
	INNER JOIN HouseModels H ON C.House_ID = H.ID
	
	CREATE TABLE #BillPayments (
	ServiceNew_ID INT default 0,
	[Amount] MONEY default 0,
	[PaymentRequestedDate] DATETIME default null
)

	
IF @Month = 12 AND @year = 2015 or @year > 2015 
	BEGIN 

		Insert into #BillPayments(ServiceNew_ID, Amount, PaymentRequestedDate)
		select 
		c.ServiceNew_ID as ServiceNew_ID,
		bp.Amount as Amount,
		bp.PaymentRequestedDate asPaymentRequestedDate
		FROM BillPayments bp
		INNER JOIN Billbases bb ON bp.Bill_Id = bb.ID --AND hb.HouseHoldbill_id = bb.HouseHoldbill_id
		INNER JOIN #householdbills hb ON bb.HouseholdBill_Id = hb.HouseHoldbill_id
		left JOIN Contracts c on bb.Contract_Id = c.ID
		WHERE bp.PaymentRequestedDate is not null
	END
ELSE
	BEGIN

		Insert into #BillPayments(ServiceNew_ID, Amount, PaymentRequestedDate)
		select 
		c.ServiceNew_ID as ServiceNew_ID,
		bp.Amount as Amount,
		bp.PaymentRequestedDate asPaymentRequestedDate
		FROM BillPayments bp
		INNER JOIN Billbases bb ON bp.Bill_Id = bb.ID --AND hb.HouseHoldbill_id = bb.HouseHoldbill_id
		INNER JOIN #householdbills hb ON bb.HouseholdBill_Id = hb.HouseHoldbill_id
		left JOIN CONTRACTS c on bb.Contract_Id = c.ID
		
	END 

	SELECT @ActiveHouseCount = COUNT(*) FROM #activehouses

	INSERT INTO #HouseServiceAnalysis
	SELECT 
		  @Month
		, @Year
		, @ActiveHouseCount AS [BilledHouseCount]

		, ISNULL((SELECT COUNT(u.Id) FROM #activehouses ah INNER JOIN AspNetUsers u on u.House_ID = ah.ID), 0)	AS 'TotalHousemates'
		, ISNULL((SELECT SUM(bp.Amount) FROM #BillPayments bp ), 0)												AS 'TotalAmountBilled'
		 
		, ISNULL((SELECT COUNT(ce.House_ID) FROM #ACTIVECONTRACTS ce WHERE ce.ServiceNew_ID = 1), 0)			AS 'Nb of Houses with Electricity contract'
		, ISNULL((SELECT SUM(be.Amount) FROM #BillPayments be WHERE be.ServiceNew_ID = 1), 0)					AS 'Total Amount billed for Electricty'
																												
		, ISNULL((SELECT COUNT(cg.House_ID) FROM #ACTIVECONTRACTS cg WHERE cg.ServiceNew_ID = 2 ), 0)			AS 'Nb of Houses with Gas contract'
		, ISNULL((SELECT SUM(bg.Amount) FROM #BillPayments bg WHERE bg.ServiceNew_ID = 2), 0)					AS 'Total Amount billed for Gas'
																												
		, ISNULL((SELECT COUNT(cw.House_ID) FROM #ACTIVECONTRACTS cw WHERE cw.ServiceNew_ID = 3), 0)			AS 'Nb of Houses with Water contract'
		, ISNULL((SELECT SUM(bw.Amount) FROM #BillPayments bw WHERE bw.ServiceNew_ID = 3), 0)					AS 'Total Amount billed for Water'
																												
		, ISNULL((SELECT COUNT(cb.House_ID) FROM #ACTIVECONTRACTS cb WHERE cb.ServiceNew_ID = 4), 0)			AS 'Nb of Houses with Broadband contract'
		, ISNULL((SELECT SUM(bb.Amount) FROM #BillPayments bb WHERE bb.ServiceNew_ID = 4), 0)					AS 'Total Amount billed for Broadband'

		, ISNULL((SELECT COUNT(cn.House_ID) FROM #ACTIVECONTRACTS cn WHERE cn.ServiceNew_ID = 5), 0)			AS 'Nb of Houses with NetflixTV contract'
		, ISNULL((SELECT SUM(bn.Amount) FROM #BillPayments bn WHERE bn.ServiceNew_ID = 5), 0)					AS 'Total Amount billed for NetflixTV'

		, ISNULL((SELECT COUNT(cs.House_ID) FROM #ACTIVECONTRACTS cs WHERE cs.ServiceNew_ID = 6), 0)			AS 'Nb of Houses with SkyTV contract'
		, ISNULL((SELECT SUM(bs.Amount) FROM #BillPayments bs WHERE bs.ServiceNew_ID = 6), 0)					AS 'Total Amount billed for SkyTV'
																												
		, ISNULL((SELECT COUNT(ct.House_ID) FROM #ACTIVECONTRACTS ct WHERE ct.ServiceNew_ID = 7), 0)			AS 'Nb of Houses with TVLicense contract'
		, ISNULL((SELECT SUM(bt.Amount) FROM #BillPayments bt WHERE bt.ServiceNew_ID = 7), 0)					AS 'Total Amount billed for TVLicense'
																												
		, ISNULL((SELECT COUNT(cla.House_ID)FROM #ACTIVECONTRACTS cla WHERE cla.ServiceNew_ID = 8), 0)			AS 'Nb of Houses with LandlinePhone contract'
		, ISNULL((SELECT SUM(bla.Amount) FROM #BillPayments bla WHERE bla.ServiceNew_ID = 8), 0)				AS 'Total Amount billed for LandlinePhone'

		, ISNULL((SELECT COUNT(cli.House_ID) FROM #ACTIVECONTRACTS cli WHERE cli.ServiceNew_ID = 9), 0)			AS 'Nb of Houses with LineRental contract'
		, ISNULL((SELECT SUM(bli.Amount) FROM #BillPayments bli WHERE bli.ServiceNew_ID = 9), 0)				AS 'Total Amount billed for LineRental'

	SET @i = @i +1

	DROP TABLE #ACTIVECONTRACTS, #activehouses, #householdbills, #BillPayments
END

SELECT 
  [Month]
, [Year] 
, [BilledHouseCount] 
, [Housemates] 
, [TotalAmountBilled] 
, [Nb of Houses with Electricity contract] 
, [Total Amount billed for Electricity] 
, [Nb of Houses with Gas contract]  
, [Total Amount billed for Gas] 
, [Nb of Houses with Water contract] 
, [Total Amount billed for Water]
, [Nb of Houses with Broadband contract]
, [Total Amount billed for Broadband]
, [Nb of Houses with NetflixTV contract]
, [Total Amount billed for NetflixTV]
, [Nb of Houses with SkyTV contract]
, [Total Amount billed for SkyTV]
, [Nb of Houses with TVLicense contract]
, [Total Amount billed for TVLicense]
, [Nb of Houses with LandlinePhone contract]
, [Total Amount billed for LandlinePhone] 
, [Nb of Houses with LineRental contract] 
, [Total Amount billed for LineRental] 
 FROM #HouseServiceAnalysis

DROP TABLE #DateRange, #HouseServiceAnalysis




END
