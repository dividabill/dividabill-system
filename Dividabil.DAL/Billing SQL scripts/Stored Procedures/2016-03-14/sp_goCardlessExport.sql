-- ==========================================================
-- Create Stored Procedure Template for Windows Azure SQL Database
-- ==========================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jeff Pinkham
-- Create date: 2016-03-14
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_goCardlessExport]																											   
	-- Add the parameters for the stored procedure here																											   
	@billingRunId int																																	 																																	   
AS																																							   
BEGIN																																						   
	-- SET NOCOUNT ON added to prevent extra result sets from																									   
	-- interfering with SELECT statements.																														   
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
	 u.MandateID as 'mandate_id'
	, SUM(bp.Amount) as 'amount'
	, Convert(date, DateAdd(day, 7, GETDate())) as 'charge_date'
	, hb.House_ID as 'house_id'
	, bp.User_Id as 'user_id'
	FROM billingqueue bq
	INNER JOIN HouseholdBills hb on bq.householdbillid = hb.id
	INNER JOIN BillBases bb ON hb.Id = bb.HouseholdBill_Id
	INNER JOIN BillPayments bp ON bb.ID = bp.Bill_Id
	LEFT JOIN AspNetUsers u ON bp.User_Id = u.id
	where bq.billingrunid = @billingRunId
	group by bp.user_id, hb.house_id, u.mandateid 
	having sum(bp.amount)> 0
	ORDER BY hb.House_ID
END
GO
