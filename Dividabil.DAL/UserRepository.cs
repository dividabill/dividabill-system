﻿using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.DAL
{
    public class UserRepository : GenericRepository<User, User, IApplicationContext>, IUserRepository
    {
        public UserRepository(IApplicationContext context)
            : base(context)
        {
        }

        public void Archive(int userId)
        {
            var user = GetByID(userId);
            //user.Archived = true;
        }

        public void AddToHouse(User tenant, HouseModel house)
        {
            tenant.House_ID = house.ID;
        }


        public IQueryable<User> GetByHouseId(int houseId)
        {
            return Get().Where(u => u.House_ID == houseId);
        }

        public User GetByEmail(string email)
        {
            return Get().Where(u => u.Email == email).SingleOrDefault();
        }
    }
}