﻿using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DividaBill.DAL
{
    public class BillRepository<BillTypeModel, UserType, DBContext> : GenericRepository<BillTypeModel, UserType, DBContext>
        where BillTypeModel : UtilityBill, IUtilityBill
        where UserType : IdentityUser
        where DBContext : IDbContext<UserType>
    {
        public BillRepository(DBContext context)
            : base(context)
        {
        }
    }
}