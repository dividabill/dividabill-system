﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL
{
    public class ArchivedHousesRepository<DBContext> : HouseRepository<DBContext>, IArchivedHousesRepository<HouseModel, DBContext>
        where DBContext : IApplicationContext
    {
        public ArchivedHousesRepository(DBContext context)
            : base(context)
        {

        }

        public void Unarchive(int houseId)
        {
            throw new NotImplementedException();
            //HouseModel house = this.GetByID(houseId);
            //this.Update(house);
        }
    }
}