﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.DbContext;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.StoredProcs;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Emails;
using DividaBill.Models.Sheets;
using Microsoft.Practices.Unity;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.DAL
{
    public class UnitOfWorkBase : IUnitOfWork
    {
        public static IUnityContainer Container;
        protected IClientContext clientDB;

        protected IApplicationContext context;


        private bool disposed;
        protected IReferalContext referalDB;
        //protected IStoredProcsContext storedProcsContext;


        protected IUnityContainer container
        {
            get { return Container; }
        }

        public ActiveHousesRepository<IApplicationContext> ActiveHouses { get; private set; }
        public ArchivedHousesRepository<IApplicationContext> ArchivedHouses { get; private set; }
        public UserRepository UserRepository { get; private set; }
        public ReferalUserRepository<IReferalContext> ReferalUserRepository { get; private set; }
        public GenericRepository<Coupon, User, IApplicationContext> CouponRepository { get; private set; }
        public GenericRepository<BillPayment, User, IApplicationContext> BillPaymentsRepository { get; private set; }
        public GenericRepository<Premise, User, IApplicationContext> PremiseRepository { get; private set; }
        public GenericRepository<Building, User, IApplicationContext> BuildingRepository { get; private set; }
        public GenericRepository<PostcodeEstimatedPrice, User, IApplicationContext> PostcodeEstimatedPriceRepository { get; private set; }
        public GenericRepository<HouseEstimatedPrice, User, IApplicationContext> HouseEstimatedPriceRepository { get; private set; }
        public GenericRepository<EmailSentMessage, User, IApplicationContext> EmailSentMessageRepository { get; private set; }
        public GenericRepository<EmailTemplate, User, IApplicationContext> EmailTemplateRepository { get; private set; }

        public GenericRepository<BillingQueue, User, IApplicationContext> BillingQueueRepository { get; private set; }
        public GenericRepository<BillingQueueTransactionLog, User, IApplicationContext> BillingQueueTransactionLogRepository { get; private set; }
        public GenericRepository<BillingRun, User, IApplicationContext> BillingRunRepository { get; private set; }

        public BillRepository<ElectricityBill, User, IApplicationContext> ElectricityBillRepository { get; private set; }
        public BillRepository<WaterBill, User, IApplicationContext> WaterBillRepository { get; private set; }
        public BillRepository<GasBill, User, IApplicationContext> GasBillRepository { get; private set; }
        public BillRepository<DeliveryBill, User, IApplicationContext> DeliveryBillRepository { get; private set; }
        public GenericRepository<HouseholdBill, User, IApplicationContext> HouseholdBillRepository { get; private set; }
        public GenericRepository<ReferalCode, User, IApplicationContext> ReferalRepository { get; private set; }
        public GenericRepository<BankPayment, User, IApplicationContext> BankPaymentRepository { get; private set; }
        public GenericRepository<BankMandate, User, IApplicationContext> BankMandateRepository { get; private set; }
        public GenericRepository<BankAccount, User, IApplicationContext> BankAccountRepository { get; private set; }
        public GenericRepository<BankPaymentEvent, User, IApplicationContext> BankPaymentEventRepository { get; private set; }
        public GenericRepository<BankMandateEvent, User, IApplicationContext> BankMandateEventRepository { get; private set; }
        public GenericRepository<Tenancy, User, IApplicationContext> TenancyRepository { get; private set; }
        public BillRepository<NetflixTVBill, User, IApplicationContext> NetflixTVBillRepository { get; private set; }
        public BillRepository<SkyTVBill, User, IApplicationContext> SkyTVBillRepository { get; private set; }
        public BillRepository<TVLicenseBill, User, IApplicationContext> TVLicenseBillRepository { get; private set; }
        public BillRepository<BroadbandBill, User, IApplicationContext> BroadbandBillRepository { get; private set; }
        public BillRepository<LandlinePhoneBill, User, IApplicationContext> LandlinePhoneBillRepository { get; private set; }
        public BillRepository<LineRentalBill, User, IApplicationContext> LineRentalBillRepository { get; private set; }
        public BillRepository<UtilityBill, User, IApplicationContext> UtilityBillRepository { get; private set; }
        public GenericRepository<ElectricityContract, User, IApplicationContext> ElectricityContractRepository { get; private set; }
        public GenericRepository<GasContract, User, IApplicationContext> GasContractRepository { get; private set; }
        public GenericRepository<WaterContract, User, IApplicationContext> WaterContractRepository { get; private set; }
        public GenericRepository<BroadbandContract, User, IApplicationContext> BroadbandContractRepository { get; private set; }
        public GenericRepository<SkyTVContract, User, IApplicationContext> SkyTVContractRepository { get; private set; }
        public GenericRepository<NetflixTVContract, User, IApplicationContext> NetflixTVContractRepository { get; private set; }
        public GenericRepository<TVLicenseContract, User, IApplicationContext> TVLicenseContractRepository { get; private set; }
        public GenericRepository<LandlinePhoneContract, User, IApplicationContext> LandlinePhoneContractRepository { get; private set; }
        public GenericRepository<LineRentalContract, User, IApplicationContext> LineRentalContractRepository { get; private set; }
        public GenericRepository<Provider, User, IApplicationContext> ProviderRepository { get; private set; }
        public GenericRepository<ClientNotification, User, IClientContext> ClientNotifications { get; private set; }
        public GenericRepository<Contract, User, IApplicationContext> ContractRepository { get; private set; }
        public GenericRepository<File, User, IApplicationContext> GDriveFileRepository { get; private set; }
        public GenericRepository<AuditLog, User, IApplicationContext> AuditLogs { get; private set; }

        public void Save()
        {
            context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Wraps the given action in a single transaction.
        ///     If an exception is thrown during the execution of the action,
        ///     all changes are rolled back and the exception is re-thrown.
        ///     <para />
        ///     You can optionally specify not to save the changes,
        ///     even if the method executes successfully.
        /// </summary>
        /// <param name="act">The actions to wrap in a transaction. </param>
        /// <param name="commitOnSuccess">Whether to commit successful transactions to the database. True by default. </param>
        public void WrapInTransaction(Action act, bool commitOnSuccess = true)
        {
            using (var trans = BeginTransaction())
            {
                try
                {
                    act();

                    if (commitOnSuccess)
                        trans.Commit();
                    else
                        trans.Rollback();
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        ///     Wraps the given method in a single transaction.
        ///     If an exception is thrown during the execution of the method,
        ///     all changes are rolled back and the exception is re-thrown.
        ///     <para />
        ///     You can optionally specify not to save the changes,
        ///     even if the method executes successfully.
        /// </summary>
        /// <param name="act">The actions to wrap in a transaction. </param>
        /// <param name="commitOnSuccess">Whether to commit successful transactions to the database. True by default. </param>
        public async Task<T> WrapInTransactionAsync<T>(Func<Task<T>> act, bool commitOnSuccess = true)
        {
            using (var trans = BeginTransaction())
            {
                try
                {
                    var ans = await act();

                    if (commitOnSuccess)
                        trans.Commit();
                    else
                        trans.Rollback();

                    return ans;
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public async Task WrapInTransactionAsync(Func<Task> functor, bool commitOnSuccess = true)
        {
            using (var trans = BeginTransaction())
            {
                try
                {
                    await functor();

                    if (commitOnSuccess)
                        trans.Commit();
                    else
                        trans.Rollback();
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public IQueryable<AuditLog> GetLogs(string tableName)
        {
            return context.GetLogs(tableName);
        }

        public IQueryable<AuditLog> GetLogs<T>()
        {
            return context.GetLogs<T>();
        }

        public void ExecuteSqlScript<T>(string sql) where T : class
        {
            ((ApplicationDbContext) context).Database.ExecuteSqlCommand(sql);
        }

        public List<sp_Monthly_Billing_Report_Result> sp_Monthly_Billing_Report(DateTime? startDate, DateTime? endDate)
        {
            return context.sp_Monthly_Billing_Report(startDate, endDate);
        }

        public List<sp_goCardlessExport_Result> sp_goCardlessExport(int billingRunId)
        {
            return context.sp_goCardlessExport(billingRunId);
        }

        public virtual void EnableBulkInsertOptimisations()
        {
            context.SetBulkInsertOptimisationOn();
        }

        public void Initialize()
        {
            ActiveHouses = new ActiveHousesRepository<IApplicationContext>(context);
            ArchivedHouses = new ArchivedHousesRepository<IApplicationContext>(context);
            UserRepository = new UserRepository(context);
            CouponRepository = new GenericRepository<Coupon, User, IApplicationContext>(context);
            BillPaymentsRepository = new GenericRepository<BillPayment, User, IApplicationContext>(context);
            PremiseRepository = new GenericRepository<Premise, User, IApplicationContext>(context);
            BuildingRepository = new GenericRepository<Building, User, IApplicationContext>(context);
            PostcodeEstimatedPriceRepository = new GenericRepository<PostcodeEstimatedPrice, User, IApplicationContext>(context);
            HouseEstimatedPriceRepository = new GenericRepository<HouseEstimatedPrice, User, IApplicationContext>(context);
            EmailSentMessageRepository = new GenericRepository<EmailSentMessage, User, IApplicationContext>(context);
            EmailTemplateRepository = new GenericRepository<EmailTemplate, User, IApplicationContext>(context);
            ElectricityBillRepository = new BillRepository<ElectricityBill, User, IApplicationContext>(context);
            WaterBillRepository = new BillRepository<WaterBill, User, IApplicationContext>(context);
            GasBillRepository = new BillRepository<GasBill, User, IApplicationContext>(context);
            DeliveryBillRepository = new BillRepository<DeliveryBill, User, IApplicationContext>(context);
            HouseholdBillRepository = new GenericRepository<HouseholdBill, User, IApplicationContext>(context);
            ReferalRepository = new GenericRepository<ReferalCode, User, IApplicationContext>(context);
            BankPaymentRepository = new GenericRepository<BankPayment, User, IApplicationContext>(context);
            BankMandateRepository = new GenericRepository<BankMandate, User, IApplicationContext>(context);
            TenancyRepository = new GenericRepository<Tenancy, User, IApplicationContext>(context);
        BankAccountRepository = new GenericRepository<BankAccount, User, IApplicationContext>(context);
            BankPaymentEventRepository = new GenericRepository<BankPaymentEvent, User, IApplicationContext>(context);
            BankMandateEventRepository = new GenericRepository<BankMandateEvent, User, IApplicationContext>(context);
            NetflixTVBillRepository = new BillRepository<NetflixTVBill, User, IApplicationContext>(context);
            SkyTVBillRepository = new BillRepository<SkyTVBill, User, IApplicationContext>(context);
            TVLicenseBillRepository = new BillRepository<TVLicenseBill, User, IApplicationContext>(context);
            BroadbandBillRepository = new BillRepository<BroadbandBill, User, IApplicationContext>(context);
            LandlinePhoneBillRepository = new BillRepository<LandlinePhoneBill, User, IApplicationContext>(context);
            LineRentalBillRepository = new BillRepository<LineRentalBill, User, IApplicationContext>(context);
            UtilityBillRepository = new BillRepository<UtilityBill, User, IApplicationContext>(context);
            ElectricityContractRepository = new GenericRepository<ElectricityContract, User, IApplicationContext>(context);
            GasContractRepository = new GenericRepository<GasContract, User, IApplicationContext>(context);
            WaterContractRepository = new GenericRepository<WaterContract, User, IApplicationContext>(context);
            BroadbandContractRepository = new GenericRepository<BroadbandContract, User, IApplicationContext>(context);
            SkyTVContractRepository = new GenericRepository<SkyTVContract, User, IApplicationContext>(context);
            NetflixTVContractRepository = new GenericRepository<NetflixTVContract, User, IApplicationContext>(context);
            TVLicenseContractRepository = new GenericRepository<TVLicenseContract, User, IApplicationContext>(context);
            LandlinePhoneContractRepository = new GenericRepository<LandlinePhoneContract, User, IApplicationContext>(context);
            LineRentalContractRepository = new GenericRepository<LineRentalContract, User, IApplicationContext>(context);
            ProviderRepository = new GenericRepository<Provider, User, IApplicationContext>(context);
            ContractRepository = new GenericRepository<Contract, User, IApplicationContext>(context);
            GDriveFileRepository = new GenericRepository<File, User, IApplicationContext>(context);
            AuditLogs = new GenericRepository<AuditLog, User, IApplicationContext>(context);

            ReferalUserRepository = new ReferalUserRepository<IReferalContext>(referalDB);

            ClientNotifications = new GenericRepository<ClientNotification, User, IClientContext>(clientDB);

            BillingQueueRepository = new GenericRepository<BillingQueue, User, IApplicationContext>(context);
            BillingQueueTransactionLogRepository = new GenericRepository<BillingQueueTransactionLog, User, IApplicationContext>(context);
            BillingRunRepository = new GenericRepository<BillingRun, User, IApplicationContext>(context);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public DbContextTransaction BeginTransaction()
        {
            return context.BeginTransaction();
        }
    }
}