﻿using DividaBill;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services;
using DividaBill_Library;
using GoCardless_API.api;
using GoCardless_API.helpers;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class GenerateNewBills : Script
    {
        public GenerateNewBills()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            var billService = new BillingService(unitOfWork);
            var dt = PickDateTime("Pick a month for the last bills to be generated: ");
            var houses = unitOfWork.ActiveHouses.Get().ToArray();
            foreach (var h in houses)
            {
                billService.GenerateMissingBills(h, dt.Month, dt.Year);

                //Mark past July 2015 bills as payed.
                foreach (var bill in h.HouseholdBills
                    .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
                    .Where(x => x.Year < dt.Year || (x.Month <= dt.Month - 3 && x.Year <= dt.Year))
                    .ToArray())
                    foreach (var uBill in bill.UtilityBills)
                        foreach (var payment in uBill.Payments)
                            payment.MarkAsPaid();

            }




            //foreach (var h in houses)
            //{
            //    var tenants = "";
            //    foreach (var t in h.Tenants)
            //        tenants += t.FullName + ", ";
            //    tenants = tenants.TrimEnd(',', ' ');


            //    //Mark past July 2015 bills as payed.
            //    foreach (var bill in h.HouseholdBills
            //        .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
            //        .Where(x => x.Year < dt.Year || (x.Month <= dt.Month - 3 && x.Year <= dt.Year))
            //        .ToArray())
            //        foreach (var uBill in bill.UtilityBills)
            //            foreach (var payment in uBill.Payments)
            //                payment.MarkAsPaid();


            //    var billsForHouse = h.HouseholdBills
            //        //.Where(x => x.Month == dt.Month && x.Year == dt.Year)
            //        .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
            //        .ToArray();
            //    foreach (var bill in billsForHouse)
            //    {

            //        var billTotalAmount = bill.TotalAmount;
            //        foreach (var tenant in h.Tenants)
            //        {
            //            var tenantTotalAmount = bill.GetTenantTotalAmount(tenant);
            //            var elecTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.Electricity.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var gasTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.Gas.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var waterTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.Water.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var bbTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.Broadband.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var skyTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.SkyTV.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var tvlicenseTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.TVLicense.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;
            //            var phoneTenantAmount = bill.UtilityBills.Where(x => x.Service.Id == ServiceType.LandlinePhone.Id).FirstOrDefault()?.GetTenantAmount(tenant) ?? 0.00m;



            //            var myMessage = new SendGridMessage();
            //            myMessage.AddTo("mike.austin@dividabill.co.uk");
            //            myMessage.From = new System.Net.Mail.MailAddress("billing@dividabill.co.uk", "DividaBill Billing");

            //            var month = "";
            //            if (bill.Month == 7)
            //                month = "July";
            //            else if (bill.Month == 8)
            //                month = "August";
            //            else if (bill.Month == 9)
            //                month = "September";


            //            myMessage.Subject = "Your " + month + " DividaBill";
            //            myMessage.Text = " ";
            //            myMessage.Html = " ";
            //            if (h.StartDate.Month == 9 && h.StartDate.Year == 2015)
            //                myMessage.EnableTemplateEngine("682bafb0-f89f-4ed2-82e6-86a9c50ce55e");
            //            if (h.ID < 160)
            //                myMessage.EnableTemplateEngine("719749bb-9d9b-4387-8f47-de48bd763add");
            //            else
            //                myMessage.EnableTemplateEngine("e9fc4b83-ab0d-43d2-979a-fe9d0c0eef52");




            //            myMessage.AddSubstitution("{{first_name}}", new List<string> { tenant.FirstName });
            //            myMessage.AddSubstitution("{{individual_payment}}", new List<string> { bill.GetTenantTotalAmount(tenant).ToString() });
            //            myMessage.AddSubstitution("{{house_payment}}", new List<string> { bill.TotalAmount.ToString() });
            //            myMessage.AddSubstitution("{{house_reference_number}}", new List<string> { tenant.House_ID.ToString() });
            //            myMessage.AddSubstitution("{{bill_start_date}}", new List<string> { bill.StartDate.ToString() });
            //            myMessage.AddSubstitution("{{bill_end_date}}", new List<string> { bill.EndDate.ToString() });
            //            myMessage.AddSubstitution("{{payment_collection_date}}", new List<string> { "07/09/2015" });
            //            myMessage.AddSubstitution("{{tenant_names}}", new List<string> { tenants });
            //            myMessage.AddSubstitution("{{individuals_total_electricity}}", new List<string> { elecTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_gas}}", new List<string> { gasTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_water}}", new List<string> { waterTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_broadband}}", new List<string> { bbTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_skytv}}", new List<string> { skyTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_tvlicense}}", new List<string> { tvlicenseTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{individuals_total_landlinephone}}", new List<string> { phoneTenantAmount.ToString() });
            //            myMessage.AddSubstitution("{{registered_tenants}}", new List<string> { h.Tenants.Count().ToString() });
            //            myMessage.AddSubstitution("{{total_tenants}}", new List<string> { h.Housemates.ToString() });




            //            var username = "azure_941522fcaccaaeee1ad9b6f9ca4a758a@azure.com";
            //            var pswd = "X6MGT2zz2Spv1JC";

            //            var credentials = new NetworkCredential(username, pswd);
            //            var transportWeb = new Web(credentials);
            //            try
            //            {
            //                transportWeb.DeliverAsync(myMessage);
            //            }
            //            catch (Exception e)
            //            {
            //                Console.WriteLine(e.Message);
            //            }
            //        }
            //    }

            //}


            //Dictionary<string, string> mandProblemOnes = new Dictionary<string, string>();
            //Dictionary<string, string> problemOnes = new Dictionary<string, string>();
            //GoCardless_API.GoCardless.Environment = GoCardless_API.GoCardless.Environments.Production;
            //ApiClient client = new ApiClient(
            //    "AC00000AWK3YF85V2TTXV7ERJACS0D89",
            //    "LTYhMXqv8cQX5oqcpiH6B52SG3_X4ELR-xKcFft7", "");

            //TenantService tenantService = new TenantService(unitOfWork);
            //foreach (var h in houses)
            //{


            //    var billsForHouse = h.HouseholdBills
            //        //.Where(x => x.Month == dt.Month && x.Year == dt.Year)
            //        .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
            //        .ToArray();
            //    foreach (var bill in billsForHouse)
            //    {

            //        foreach (var tenant in h.Tenants)
            //        {

            //            var amount = bill.GetTenantTotalAmount(tenant);



            //            var PayRef = "";

            //            DateTime paymentDate;

            //            paymentDate = DateTime.Today.AddBusinessDays(4);

            //            try
            //            {
            //                Mandate mand = Retry.Do<Mandate>(() => client.GetMandate(tenant.MandateID), TimeSpan.FromSeconds(5), 3);
            //                BankMandate mandate = unitOfWork.BankMandateRepository.GetByID(tenant.MandateID);
            //                mandate.next_possible_charge_date = mand.next_possible_charge_date;
            //                unitOfWork.BankMandateRepository.Update(mandate);



            //                var chargeDate = (Convert.ToDateTime(mandate.next_possible_charge_date) > paymentDate ? mand.next_possible_charge_date : paymentDate.ToString("yyyy-MM-dd"));
            //                var payment = new NewPayment
            //                {
            //                    amount = Convert.ToInt32(amount * 100),
            //                    charge_date = chargeDate,
            //                    currency = "GBP",
            //                    description = "September bill run. Payment for " + bill.Month + " " + bill.Year,
            //                    reference = ""
            //                };
            //                payment.links.Add("mandate", tenant.MandateID);

            //                Payment paym = Retry.Do<Payment>(() => client.CreatePayment(payment), TimeSpan.FromSeconds(5), 3);

            //                BankPayment domainBankPayment = new BankPayment
            //                {
            //                    amount = paym.amount,
            //                    amount_refunded = paym.amount_refunded,
            //                    charge_date = paym.charge_date,
            //                    created_at = paym.created_at,
            //                    currency = paym.currency,
            //                    description = paym.description,
            //                    id = paym.id,
            //                    Mandate_ID = tenant.MandateID,
            //                    reference = paym.reference,
            //                    status = paym.status
            //                };
            //                PayRef = paym.id;
            //                unitOfWork.BankPaymentRepository.Insert(domainBankPayment);
            //                unitOfWork.Save();

            //                foreach (var ub in bill.UtilityBills)
            //                {
            //                    foreach (var bp in ub.Payments)
            //                    {
            //                        bp.PaymentRef = PayRef;
            //                        unitOfWork.BillPaymentsRepository.Update(bp);
            //                        unitOfWork.Save();
            //                    }
            //                }
                            
            //            }
            //            catch (AggregateException ae)
            //            {
            //                ae.Handle((x) =>
            //                {
            //                    if (x is ApiException) // This we know how to handle.
            //                        {
            //                        if (tenant.MandateID == null)
            //                        {
            //                            if (!problemOnes.ContainsKey(tenant.Id))
            //                            {
            //                                problemOnes.Add(tenant.Id, ((ApiException)x).RawContent);
            //                            }
            //                        }
            //                        else if (!problemOnes.ContainsKey(tenant.MandateID))
            //                        {
            //                            problemOnes.Add(tenant.MandateID, ((ApiException)x).RawContent);
            //                        }
            //                    }
            //                    return true; //if you do something like this all exceptions are marked as handled  
            //                    });

            //            }



            //        }







                //}
            //}




            Console.WriteLine("done!");
        }
    }
}
