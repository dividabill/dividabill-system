﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill;

namespace DividaBill.Services.ViewModel
{
    class OriginRegistrationForm
    {
        [DisplayName("Internat Reference")]
        public string InternalReference { get { return "N/A"; } }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string FullAddress { get; set; }
        public string ProductOrdering { get; set; }
        public string ContractLength { get; set; }
        public string Situation { get { return "N/A"; } }
        public string DateOrdered { get { return "N/A"; } }
        public string CallPackage { get; set; }
        [DisplayName("P/L Live Date")]
        public string LiveDate { get { return "N/A"; } }

        [DisplayName("P/L Slot")]
        public string Slot { get { return "N/A"; } }

        public string NewPhoneNumber { get { return "N/A"; } }
        public string BBLiveDate { get { return "N/A"; } }
        public string BBSlot { get { return "N/A"; } }
        public string ConfirmedInstalledDate { get { return "N/A"; } }
        [DisplayName("Router Ordered?")]
        public string RouterOrdered { get { return "N/A"; } }
        [DisplayName("Additional Information")]
        public string InstallationDate { get; set; }
        public string HouseRegisteredDate { get; set; }

        public string HouseId { get; set; }

        public static OriginRegistrationForm Create(BroadbandContract broadbandContract, LandlinePhoneContract phoneContract)
        {
            var house =
                (broadbandContract?.House)
                ?? (phoneContract?.House);

            if (house == null)
                throw new ArgumentNullException("", "Neither a broadband nor a phone contract changes were supplied. ");
            if (!house.Tenants.Any())
                throw new Exception("House has no tenants!");

            var user = house.Tenants.First();

            var landlineString = "None";
            var landlineType = phoneContract?.Package;
            
            switch(landlineType)
            {
                case LandlineType.Basic:
                    landlineString = "Basic"; break;
                case LandlineType.Medium:
                    landlineString = "Advanced"; break;
                case LandlineType.None:
                    throw new Exception("Have a contract, dont have a landline!");
            }

            var installationDate =
                new[] { (broadbandContract?.RequestedStartDate.Date), (phoneContract?.RequestedStartDate.Date) }
                .Where(v => v.HasValue)
                .Min().Value;

            var contractLength =
                new[] { broadbandContract?.Length, phoneContract?.Length }
                .Where(v => v.HasValue)
                .Select(v => (int)(v.Value))
                .Max() + " months";

            if (broadbandContract?.Length == Models.ContractLength.Ongoing
                || phoneContract?.Length == Models.ContractLength.Ongoing)
                contractLength = "Ongoing";

            var productOrdering = "LINE";
            if (broadbandContract != null)
            {
                if (broadbandContract.Speed == BroadbandType.None)
                    throw new Exception("Internet with no speed!");

                productOrdering += "+" + (broadbandContract.Speed == BroadbandType.ADSL20 ? "ADSL" : "Fibre");
            }
            
            return new OriginRegistrationForm
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                FullAddress = house.Address.ToString(),
                ProductOrdering = productOrdering,
                ContractLength = contractLength,
                CallPackage = landlineString,
                InstallationDate = installationDate.ToShortDateString(),
                HouseRegisteredDate = house.RegisteredDate.ToShortDateString(),
                HouseId = house.ID.ToString(),
            };
        }
    }
}
