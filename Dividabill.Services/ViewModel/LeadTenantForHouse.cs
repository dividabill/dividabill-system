﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.ViewModel
{
    class LeadTenantForHouse
    {
        public int HouseID { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }
        public int Age { get; set; }
        public string HouseAddress { get; set; }


        public string Services { get; set; }
        public string NumberOfTenants { get; set; }
    }
}
