﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.ViewModel
{
    class TvLicensingOnboarding
    {
        public string LicenceRegisteredAddress { get; set; }
        public string LicenceStartDate { get; set; }


        public string BillingName { get; private set; }
        public string BillingAccountNumber { get; private set; }
        public string BillingSortCode { get; private set; }
        public string InvoiceSentToPreferred { get; private set; }
        public string InvoiceSentToSecondOption { get; private set; }


        private TvLicensingOnboarding()
        {
            BillingName = @"Soleus Limited (trading as DividaBill)";
            BillingAccountNumber = @"60496154";
            BillingSortCode = @"207929";
            InvoiceSentToPreferred = @"admin@dividabill.co.uk";
            InvoiceSentToSecondOption = @"DividaBill, 35 Kingsland Road, London, E2 8AA";
        }

        public static TvLicensingOnboarding Create(Contract c)
        {
            if (c.ServiceNew != ServiceType.TVLicense)
                return null;

            return new TvLicensingOnboarding
            {
                LicenceRegisteredAddress = c.House.Address.ToString(),
                LicenceStartDate = c.RequestedStartDate.Date.ToShortDateString(),
            };
        }
    }
}
