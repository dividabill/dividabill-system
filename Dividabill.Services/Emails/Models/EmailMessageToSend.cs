using System.Collections.Generic;
using DividaBill.Models;
using DividaBill.Models.Emails;

namespace DividaBill.Services.Emails.Models
{
    public class EmailMessageToSend
    {
        public EmailMessageToSend(EmailConfiguration configuration, EmailType type, User sendToUser, Dictionary<string, string> substitutionsList)
            : this(configuration, type, sendToUser.Email, substitutionsList)
        {
        }

        public EmailMessageToSend(EmailConfiguration configuration, EmailType type, string sendTo, Dictionary<string, string> substitutionsList)
        {
            Type = type;
            Configuration = configuration;
            SendTo = sendTo;
            SubstitutionsList = substitutionsList;
            IsTestSending = false;
        }

        public EmailMessageToSend(EmailConfiguration configuration, string sendTo, Dictionary<string, string> substitutionsList)
        {
            Type = EmailType.EmptyType;
            Configuration = configuration;
            SendTo = sendTo;
            SubstitutionsList = substitutionsList;
            IsTestSending = false;
        }

        public EmailConfiguration Configuration { get; }
        public EmailType Type { get; }
        public bool IsTestSending { get; private set; }
        public string SendTo { get; }
        public Dictionary<string, string> SubstitutionsList { get; }

        public void SetTestSending(bool isTestSending)
        {
            IsTestSending = isTestSending;
        }
    }
}