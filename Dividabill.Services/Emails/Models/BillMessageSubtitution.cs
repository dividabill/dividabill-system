using System.Collections.Generic;
using DividaBill.Models;
using DividaBill.Models.Bills;

namespace DividaBill.Services.Emails.Models
{
    public class BillMessageSubtitution
    {
        public Dictionary<int, decimal> TenantServiceAmounts { get; set; }
        public User Tenant { get; set; }
        public HouseholdBill Bill { get; set; }
        public string Tenants { get; set; }
        public HouseModel House { get; set; }
    }
}