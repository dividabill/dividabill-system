namespace DividaBill.Services.Emails.Models
{
    public class EmailConfiguration
    {
        public string TemplateId { get; set; }

        public string Subject { get; set; }

        public string SendFromEmail { get; set; }

        public string SendFromEmailDisplayName { get; set; }
    }
}