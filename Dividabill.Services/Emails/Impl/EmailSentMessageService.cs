using System;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models.Emails;

namespace DividaBill.Services.Emails.Impl
{
    public class EmailSentMessageService : IEmailSentMessageService
    {
        private readonly IUnitOfWork _unitOfWork;

        public EmailSentMessageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public EmailSentMessage InsertAndSave(EmailSentMessage emailSentMessage, AsyncExecutionParam asyncExecutionParam = null)
        {
            var newObject = Insert(emailSentMessage, asyncExecutionParam);
            Save(asyncExecutionParam);
            return newObject;
        }

        public EmailSentMessage Insert(EmailSentMessage emailSentMessage, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            if (string.IsNullOrEmpty(emailSentMessage.SendToEmail))
            {
                if (emailSentMessage.User == null)
                {
                    emailSentMessage.User = asyncExecutionParam.UnitOfWork.UserRepository.Get().First(x => x.Id == emailSentMessage.UserId);
                }
                //emailSentMessage.SendToEmail = emailSentMessage.User.Email;
            }
            emailSentMessage.CreatedAt = DateTime.UtcNow;
            emailSentMessage.LastModified = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.EmailSentMessageRepository.Insert(emailSentMessage);
        }

        public IQueryable<EmailSentMessage> GetActive(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return asyncExecutionParam.UnitOfWork.EmailSentMessageRepository.Get();
        }

        public void Save(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            asyncExecutionParam.UnitOfWork.EmailSentMessageRepository.SaveChanges();
        }

        public EmailSentMessage UpdateAndSave(EmailSentMessage entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var updatedEntity = Update(entityToUpdate, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.EmailSentMessageRepository.SaveChanges();
            return updatedEntity;
        }

        public EmailSentMessage Update(EmailSentMessage entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            entityToUpdate.LastModified = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.EmailSentMessageRepository.Update(entityToUpdate);
        }
    }
}