using System;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models.Emails;

namespace DividaBill.Services.Emails.Impl
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly IUnitOfWork _unitOfWork;

        public EmailTemplateService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public EmailTemplate GetByTemplateId(string templateId, AsyncExecutionParam asyncExecutionParam = null)
        {
            return GetActive(asyncExecutionParam).FirstOrDefault(x => x.TemplateId == templateId);
        }

        public IQueryable<EmailTemplate> GetActive(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var now = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.EmailTemplateRepository.Get().Where(x => x.Enabled && x.UsedFrom <= now && (!x.UsedTo.HasValue || x.UsedTo >= now));
        }

        public EmailTemplate InsertAndSave(EmailTemplate emailTemplate, AsyncExecutionParam asyncExecutionParam = null)
        {
            var newObject = Insert(emailTemplate, asyncExecutionParam);
            Save(asyncExecutionParam);
            return newObject;
        }

        public void Save(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            asyncExecutionParam.UnitOfWork.EmailTemplateRepository.SaveChanges();
        }

        public EmailTemplate Insert(EmailTemplate emailTemplate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            emailTemplate.CreatedAt = DateTime.UtcNow;
            emailTemplate.LastModified = DateTime.UtcNow;
            if (emailTemplate.UsedFrom == default(DateTime))
            {
                emailTemplate.UsedFrom = DateTime.UtcNow;
            }
            emailTemplate.UsedTo = null;
            emailTemplate.Enabled = true;
            return asyncExecutionParam.UnitOfWork.EmailTemplateRepository.Insert(emailTemplate);
        }

        public EmailTemplate UpdateAndSave(EmailTemplate entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            throw new NotImplementedException();
        }

        public EmailTemplate Update(EmailTemplate entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            entityToUpdate.LastModified = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.EmailTemplateRepository.Update(entityToUpdate);
        }

        public EmailTemplate Renew(int emailTemplateId, EmailTemplate newTemplate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var oldTemplate = asyncExecutionParam.UnitOfWork.EmailTemplateRepository.GetByID(emailTemplateId);

            var now = DateTime.UtcNow;
            oldTemplate.UsedTo = now;
            oldTemplate.Enabled = false;
            Update(oldTemplate, asyncExecutionParam);

            newTemplate.UsedFrom = now;
            return Insert(newTemplate, asyncExecutionParam);
        }

        public EmailTemplate GetById(int id, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            return asyncExecutionParam.UnitOfWork.EmailTemplateRepository.Get().FirstOrDefault(x => x.ID == id);
        }
    }
}