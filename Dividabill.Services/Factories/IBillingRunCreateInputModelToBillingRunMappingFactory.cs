using System.Security.Principal;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.ViewModels.SysOp.InputModels;

namespace DividaBill.Services.Factories
{
    public interface IBillingRunCreateInputModelToBillingRunMappingFactory : IViewModelToEntityMappingFactory<BillingRunCreateInputModel, BillingRun>
    {
        BillingRun Map(BillingRunCreateInputModel source, IPrincipal user, EmailTemplate emailTemplate);
    }
}