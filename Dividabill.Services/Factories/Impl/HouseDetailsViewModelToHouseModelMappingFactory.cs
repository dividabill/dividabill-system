using System;
using System.Collections.Generic;
using AutoMapper;
using DividaBill.Models;
using DividaBill.Models.Common;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class HouseDetailsViewModelToHouseModelMappingFactory : IViewModelToEntityMappingFactory<HouseDetailsViewModel, HouseModel>
    {
        private readonly ICouponService _couponService;

        public HouseDetailsViewModelToHouseModelMappingFactory(ICouponService couponService)
        {
            _couponService = couponService;
        }

        public HouseModel Map(HouseDetailsViewModel source)
        {
            var houseModelMapped = new HouseModel
            {
                //Building = source.,
                //ID = source.,
                Tenants = new List<User>(),
                //Coupon = source.,
                Housemates = source.Housemates,
                RegisteredDate = DateTime.Now,
                EndDate = Convert.ToDateTime(source.StartDate).AddContractLength((ContractLength) source.Period),
                Address = Mapper.Map<SimpleAddress, Address>(source.Address),
                LandlinePhone = source.LandlinePhone,
                TVLicense = source.TVLicense,
                Gas = source.Gas,
                StartDate = Convert.ToDateTime(source.StartDate),
                BroadbandType = source.BroadbandType,
                Water = source.Water,
                LandlineType = source.LandlinePhoneType,
                SkyTV = source.SkyTV,
                Electricity = source.Electricity,
                Broadband = source.Broadband,
                //Contracts = source.,
                //Coupon_Id = source.,
                //HouseholdBills = source.,
                Media = source.Media,
                NetflixTV = source.NetflixTV
                //Note = source.,
                //PaidHouseBills = { },
                //PaymentAllocations = source.,
                //ServiceEstimates = source.,
                //UnpaidHouseBills = { }
            };
            if (!string.IsNullOrEmpty(source.Coupon))
            {
                var coupon = _couponService.Get(source.Coupon);
                if (coupon != null)
                {
                    houseModelMapped.Coupon = coupon;
                    houseModelMapped.Coupon_Id = coupon.Id;
                }
            }
            return houseModelMapped;
        }

        public HouseModel Map(HouseDetailsViewModel source, HouseModel destination)
        {
            throw new NotImplementedException();
        }
    }
}