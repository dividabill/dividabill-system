using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl.ViewModelToEntity
{
    public class TenantViewModelToUserMappingFactory : IViewModelToEntityMappingFactory<TenantViewModel, User>
    {
        public User Map(TenantViewModel source)
        {
            return Map(source, new User());
        }

        public User Map(TenantViewModel source, User destination)
        {
            var newDestination = destination;
            newDestination.AccountHolder = source.AccountHolder;
            newDestination.AccountNumber = source.AccountNumber;
            newDestination.AccountSoleHolder = source.AccountSoleHolder;
            newDestination.AccountSortCode = source.AccountSortCode;
            newDestination.DOB = source.DOB;
            newDestination.Email = source.Email;
            newDestination.FirstName = source.FirstName;
            newDestination.House_ID = source.House_ID;
            newDestination.Id = source.Id;
            newDestination.LastName = source.LastName;
            newDestination.NewsletterSubscription = source.NewsletterSubscription;
            newDestination.Note = source.Note;
            newDestination.PhoneNumber = source.PhoneNumber;
            newDestination.RefTitle = source.RefTitle;
            newDestination.Email = source.Email;
            newDestination.UserName = source.Email;
            return newDestination;
        }
    }
}