using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class SignUpViewModelToUserMappingFactory : IViewModelToEntityMappingFactory<SignUpViewModel, User>
    {
        public User Map(SignUpViewModel source)
        {
            var nowDate = DateTime.Now;

            return new User
            {
                UserName = source.Email,
                Email = source.Email,
                HomeAddress = new Address
                {
                    City = source.CityOT,
                    Postcode = source.PostCodeOT,
                    Line1 = source.AddressOT1,
                    Line2 = source.AddressOT2,
                    Line3 = source.AddressOT3,
                    Line4 = source.AddressOT4,
                    County = source.CountyOT
                },
                FirstName = source.FirstName,
                LastName = source.LastName,
                PhoneNumber = source.Phone,
                RefTitle = source.RefTitle,
                DOB = source.DoB,
                AccountNumber = source.BankAccountDetails.AccountNumber,
                AccountHolder = source.BankAccountDetails.AccountName,
                AccountSortCode = source.BankAccountDetails.AccountSortCode.Replace("-", string.Empty),
                AccountSoleHolder = source.AccountStandalone,
                NewsletterSubscription = source.NewsletterSubscription,
                LastActivityDate = nowDate,
                SignupDate = nowDate
            };
        }

        public User Map(SignUpViewModel source, User destination)
        {
            throw new NotImplementedException();
        }
    }
}