using System;
using System.Linq;
using DividaBill.Models;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl
{
    public class HouseModelToHouseViewModelMappingFactory : IEntityToViewModelMappingFactory<HouseModel, HouseViewModel>
    {
        public HouseViewModel Map(HouseModel source)
        {
            return new HouseViewModel
            {
                ID = source.ID,
                Electricity = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.Electricity.Id),
                ActualStartDate = source.Contracts
                    .Where(c => c.StartDate.HasValue)
                    .OrderBy(c => c.StartDate)
                    .Select(c => c.StartDate)
                    .FirstOrDefault(),
                Address = source.Address.Line1 + ", " + source.Address.Postcode,
                Broadband = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.Broadband.Id),
                BroadbandType = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.Broadband.Id) ?
                    (BroadbandType) source.Contracts.FirstOrDefault(e => e.ServiceNew_ID == ServiceType.Broadband.Id).PackageRaw :
                    BroadbandType.None,
                Coupon = source.Coupon?.Code,
                EndDate = source.EndDate,
                Gas = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.Gas.Id),
                Housemates = source.Housemates,
                LandlinePhone = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id),
                LandlineType = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id) ?
                    (LandlineType) source.Contracts.FirstOrDefault(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id).PackageRaw :
                    LandlineType.None,
                Options = null,
                RegisteredDate = source.RegisteredDate,
                RegisteredTenants = source.Tenants.Count,
                SkyTV = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.SkyTV.Id),
                StartDate = source.StartDate,
                TVLicense = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.TVLicense.Id),
                Water = source.Contracts.Any(e => e.ServiceNew_ID == ServiceType.Water.Id)
            };
        }

        public HouseViewModel Map(HouseModel source, HouseViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}