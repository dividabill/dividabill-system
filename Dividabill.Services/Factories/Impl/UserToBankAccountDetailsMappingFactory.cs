using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class UserToBankAccountDetailsMappingFactory : IEntityToViewModelMappingFactory<User, BankAccountDetails>
    {
        public BankAccountDetails Map(User source)
        {
            return new BankAccountDetails
            {
                AccountSortCode = source.AccountSortCode,
                AccountName = source.AccountHolder,
                AccountNumber = source.AccountNumber
            };
        }

        public BankAccountDetails Map(User source, BankAccountDetails destination)
        {
            throw new NotImplementedException();
        }
    }
}