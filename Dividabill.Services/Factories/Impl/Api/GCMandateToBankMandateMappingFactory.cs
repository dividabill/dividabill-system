using DividaBill.Models;
using DividaBill.Models.GoCardless;

namespace DividaBill.Services.Factories.Impl.Api
{
    public class GcMandateToBankMandateMappingFactory : IGcMandateToBankMandateMappingFactory
    {
        public BankMandate Map(User tenant, GCMandate mand, BankMandate destination)
        {
            destination = Map(tenant, destination);
            destination = Map(mand, destination);
            return destination;
        }

        public BankMandate Map(GCMandate mand, BankMandate destination)
        {
            destination.created_at = mand.created_at;
            destination.Id = mand.id;
            destination.next_possible_charge_date = mand.next_possible_charge_date;
            destination.reference = mand.reference;
            destination.scheme = mand.scheme;
            destination.status = mand.status;
            return destination;
        }

        public BankMandate Map(User tenant, GCMandate mand)
        {
            var destination = new BankMandate();
            destination = Map(tenant, destination);
            destination = Map(mand, destination);
            return destination;
        }

        private BankMandate Map(User tenant, BankMandate destination)
        {
            destination.User_ID = tenant.Id;
            destination.BankAccount_ID = tenant.BankAccountID;
            return destination;
        }
    }
}