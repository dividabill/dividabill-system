using System;
using DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere;
using DividaBill.ViewModels.API;

namespace DividaBill.Services.Factories.Impl.Api
{
    public class GeoLocationToGeoLocationOutputApiModelMappingFactory : IThirdPartyModelToApiOutputViewModelMappingFactory<GeoLocation, GeoLocationApiOutputViewModel>
    {
        public GeoLocationApiOutputViewModel Map(GeoLocation source)
        {
            return new GeoLocationApiOutputViewModel
            {
                PostCode = source.Postcode
            };
        }

        public GeoLocationApiOutputViewModel Map(GeoLocation source, GeoLocationApiOutputViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}