using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class TenancyViewModelToTenancyMappingFactory : IViewModelToEntityMappingFactory<TenancyViewModel, Tenancy>
    {
        public Tenancy Map(TenancyViewModel source)
        {
            var today = DateTime.UtcNow;
            return new Tenancy
            {
                Id = source.TenancyId,
                HouseId = source.HouseId,
                JoiningDate = source.JoiningDate,
                LeavingDate = source.LeavingDate,
                RequestedDate = today,
                Notes = source.Notes,
                UserId = source.UserId,
                User = source.User
            };
        }

        public Tenancy Map(TenancyViewModel source, Tenancy destination)
        {
            throw new NotImplementedException();
        }
    }
}