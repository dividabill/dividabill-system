using System.Collections.Generic;
using DividaBill.Models.Abstractions;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.Factories
{
    public interface IEntityToApiOutputViewModelMappingFactory<in TEm, TVm> : IMappingFactory<TEm, TVm> where TEm : IEntityModel where TVm : IApiOutputViewModel
    {
        IEnumerable<TVm> Map(IEnumerable<TEm> entities);
    }
}