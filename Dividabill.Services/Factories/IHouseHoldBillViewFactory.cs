using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories
{
    public interface IHouseHoldBillViewFactory
    {
        List<HouseHoldBillsViewModel> Create(IQueryable<HouseholdBill> bills);
    }
}