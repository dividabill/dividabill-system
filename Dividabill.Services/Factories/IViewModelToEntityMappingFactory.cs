using DividaBill.Models.Abstractions;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.Factories
{
    public interface IViewModelToEntityMappingFactory<in TEm, TVm> : IMappingFactory<TEm, TVm> where TEm : IViewModel where TVm : IEntityModel
    {
    }
}