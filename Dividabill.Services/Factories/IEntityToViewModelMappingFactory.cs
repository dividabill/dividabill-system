using DividaBill.Models.Abstractions;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.Factories
{
    public interface IEntityToViewModelMappingFactory<in TEm, TVm> : IMappingFactory<TEm, TVm> where TEm : IEntityModel where TVm : IViewModel
    {
    }
}