using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class TemplateNotFound : Exception
    {
        public TemplateNotFound()
        {
        }

        public TemplateNotFound(string message) : base(message)
        {
        }

        public TemplateNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected TemplateNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}