using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class CouponNotFound : Exception
    {
        public CouponNotFound()
        {
        }

        public CouponNotFound(string message) : base(message)
        {
        }

        public CouponNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected CouponNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}