﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Exceptions
{
    [global::System.Serializable]
    public class MissingServiceException : Exception
    {
        public int houseId {get;set;}
        public string service { get; set; }
        public MissingServiceException() : base() { }
        public MissingServiceException(string message) : base(message) { }
        public MissingServiceException(string message, Exception inner) : base(message, inner) { }
        protected MissingServiceException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        public String GetPrettyExplanation()
        {
            return String.Format("The service {0} is not activated on the house account {1}.", service,  houseId);
        }
    }
}
