using System;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class TenancyNotFoundException : Exception
    {
        public TenancyNotFoundException() { }
        public TenancyNotFoundException(string message) : base(message) { }
        public TenancyNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected TenancyNotFoundException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}