﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Exceptions
{
    [global::System.Serializable]
    public class UserNotConnectedToHouse : Exception
    {
        public string user_ID { get; set; }
        public int house_ID { get; set; }

        public UserNotConnectedToHouse() { }
        public UserNotConnectedToHouse(string message) : base(message) { }
        public UserNotConnectedToHouse(string message, Exception inner) : base(message, inner) { }
        protected UserNotConnectedToHouse(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        public String GetPrettyExplanation()
        {
            return String.Format("User={0} not connected to house={1}", user_ID, house_ID);
        }
    }
}
