﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Exceptions
{

    [Serializable]
    public class HouseNotFoundException : Exception
    {
        public HouseNotFoundException() { }
        public HouseNotFoundException(string message) : base(message) { }
        public HouseNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected HouseNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}


