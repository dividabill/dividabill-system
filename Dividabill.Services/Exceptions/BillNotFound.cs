﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Exceptions
{

    public enum BillType
    {
        HouseholdBill,
        ContractBill

    }
    [global::System.Serializable]
    public class BillNotFound : Exception
    {
        public BillType type { get; set; }

        public BillNotFound() { }
        public BillNotFound(string message) : base(message) { }
        public BillNotFound(string message, Exception inner) : base(message, inner) { }
        protected BillNotFound(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        public String GetPrettyExplanation()
        {
            return String.Format("Bill type {0} not found", type);
        }
    }
}
