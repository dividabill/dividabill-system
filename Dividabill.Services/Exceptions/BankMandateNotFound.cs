using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class BankMandateNotFound : Exception
    {
        public BankMandateNotFound()
        {
        }

        public BankMandateNotFound(string message) : base(message)
        {
        }

        public BankMandateNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected BankMandateNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}