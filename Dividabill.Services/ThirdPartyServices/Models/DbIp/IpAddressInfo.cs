namespace DividaBill.Services.ThirdPartyServices.Models.DbIp
{
    public class IpAddressInfo
    {
        public string address { get; set; }
        public string country { get; set; }
        public string stateprov { get; set; }
        public string city { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string tz_offset { get; set; }
        public string tz_name { get; set; }
        public string isp { get; set; }
        public string ctype { get; set; }
        public string organization { get; set; }
    }
}