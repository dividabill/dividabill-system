using Newtonsoft.Json.Linq;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IThirdPartyReader
    {
        JObject ReadAndParseSafety(string url);
        string Read(string url);
    }
}