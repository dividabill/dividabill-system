using DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IPostCodeAnyWhereService
    {
        GeoLocation GetLocationByCoords(float latitude, float longitude);
    }
}