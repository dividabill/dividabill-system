using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.ThirdPartyServices.Configurations
{
    public class DbIpConfig : IThirdPartyConfig
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}