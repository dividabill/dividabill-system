using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.ThirdPartyServices.Configurations
{
    public class PostCodeAnyWhereConfig : IThirdPartyConfig, IThirdPartyMethodsSortOrderConfig
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
        public ThirdPartyExecutionSortOrder SortOrder { get; set; }
    }
}