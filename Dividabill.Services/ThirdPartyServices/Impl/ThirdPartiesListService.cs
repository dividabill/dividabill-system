using System.Collections.Generic;
using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class ThirdPartiesListService : IThirdPartiesListService
    {
        public ThirdPartiesListService(List<IThirdPartyService> services)
        {
            Services = services;
        }

        public List<IThirdPartyService> Services { get; }
    }
}