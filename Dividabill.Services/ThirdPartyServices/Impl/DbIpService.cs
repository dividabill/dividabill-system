using System.Web;
using System.Web.Script.Serialization;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.Services.ThirdPartyServices.Models.DbIp;
using Newtonsoft.Json.Linq;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class DbIpService : IDbIpService
    {
        private readonly DbIpConfig _config;
        private readonly IThirdPartyReader _thirdPartyReader;

        public DbIpService(DbIpConfig config, IThirdPartyReader thirdPartyReader)
        {
            _thirdPartyReader = thirdPartyReader;
            _config = config;
        }

        private string GetUrl(string subUrl)
        {
            return $"{_config.BaseUrl}/{subUrl}&api_key={_config.ApiKey}";
        }

        public IpAddressInfo GetIpAddressInfo(string ip)
        {
            var url = GetUrl($"addrinfo?addr={HttpUtility.UrlEncode(ip)}");

            var rawData = _thirdPartyReader.Read(url);
            return !ValidateForErrorResult(rawData) ? null : new JavaScriptSerializer().Deserialize<IpAddressInfo>(rawData);
        }

        private static bool ValidateForErrorResult(string rawData)
        {
            var data = JObject.Parse(rawData);
            return data != null && data["error"] == null;
        }
    }
}