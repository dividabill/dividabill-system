using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Library.Helpers;
using DividaBill.AppConstants;
using DividaBill.DAL.Unity;
using DividaBill.Library.AppConfiguration;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using Newtonsoft.Json;
using SendGrid.API.api;
using SendGrid.API.Models;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class EmailThirdPartyService : IEmailThirdPartyService
    {
        private readonly IAppConfigurationReader _appConfigurationReader;
        private readonly IEmailSentMessageService _emailSentMessageService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly ISendGridApiClient _sendGridApiClient;
        private readonly IMappingFactory<SendGridTemplate, EmailTemplate> _sendGridTemplateToEmailTemplateMappingFactory;
        private readonly ITenantService _tenantService;

        public EmailThirdPartyService(ISendGridApiClient sendGridApiClient, IEmailTemplateService emailTemplateService, IMappingFactory<SendGridTemplate, EmailTemplate> sendGridTemplateToEmailTemplateMappingFactory
            , IEmailSentMessageService emailSentMessageService, ITenantService tenantService, IAppConfigurationReader appConfigurationReader)
        {
            _appConfigurationReader = appConfigurationReader;
            _tenantService = tenantService;
            _sendGridTemplateToEmailTemplateMappingFactory = sendGridTemplateToEmailTemplateMappingFactory;
            _emailTemplateService = emailTemplateService;
            _sendGridApiClient = sendGridApiClient;
            _emailSentMessageService = emailSentMessageService;
        }

        public async Task<SendGridTemplate> GetTemplateFromThirdParty(string templateId)
        {
            SendGridTemplate templateFromThirdParty;
            try
            {
                templateFromThirdParty = await _sendGridApiClient.GetSendGridTemplateById(templateId);
            }
            catch (ApiException apiException)
            {
                if (apiException.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new TemplateNotFound($"Wrong template id [{templateId}]");
                }
                throw;
            }
            var versionFromThirdParty = templateFromThirdParty?.GetActiveVersion();
            if (versionFromThirdParty == null)
                throw new TemplateNotFound($"Wrong template id [{templateId}]");
            return templateFromThirdParty;
        }

        public async Task SaveAndSendEmail(string userId, EmailMessageToSend emailMessageToSend, AsyncExecutionParam asyncExecutionParam = null)
        {
            EmailSentMessage sentMessage = null;
            if (!emailMessageToSend.IsTestSending)
            {
                sentMessage = await SaveUserMessage(userId, emailMessageToSend, asyncExecutionParam);
            }
            await SendAndUpdate(emailMessageToSend, sentMessage, asyncExecutionParam);
        }

        public async Task SendAndUpdate(EmailMessageToSend emailMessageToSend, EmailSentMessage sentMessage, AsyncExecutionParam asyncExecutionParam = null)
        {
            var isSent = await SendMessage(emailMessageToSend);
            if (isSent
                && !emailMessageToSend.IsTestSending
                && sentMessage != null)
            {
                UpdateSendToEmail(sentMessage, asyncExecutionParam);
            }
        }

        public EmailTemplate CheckEmailTemplate(SendGridTemplate templateFromThirdParty, AsyncExecutionParam asyncExecutionParam = null)
        {
            var templateFromDb = InsertInDbIfNotExist(templateFromThirdParty, asyncExecutionParam);
            templateFromDb = UpdateIfExpired(templateFromDb, templateFromThirdParty, asyncExecutionParam);

            return templateFromDb;
        }

        public async Task<bool> SendMessage(EmailMessageToSend emailMessageToSend)
        {
            var sendGridEnabled = Convert.ToBoolean(_appConfigurationReader.GetFromAppConfig(AppSettingsEnum.SendGridEnabled));
            if (sendGridEnabled)
            {
                var sendFromAddress = new MailAddress(emailMessageToSend.Configuration.SendFromEmail, emailMessageToSend.Configuration.SendFromEmailDisplayName);
                var sendToAddress = emailMessageToSend.SendTo;
                await _sendGridApiClient.SendEmailFromTemplate(emailMessageToSend.Configuration.TemplateId, emailMessageToSend.Configuration.Subject, sendFromAddress, sendToAddress, emailMessageToSend.SubstitutionsList);
                return true;
            }
            return false;
        }

        private EmailTemplate InsertInDbIfNotExist(SendGridTemplate sendGridTemplate, AsyncExecutionParam asyncExecutionParam = null)
        {
            var templateFromDb = _emailTemplateService.GetByTemplateId(sendGridTemplate.id, asyncExecutionParam);
            if (templateFromDb == null)
            {
                templateFromDb = _sendGridTemplateToEmailTemplateMappingFactory.Map(sendGridTemplate);
                templateFromDb = SetProvider(templateFromDb);
                templateFromDb = _emailTemplateService.InsertAndSave(templateFromDb, asyncExecutionParam);
            }
            return templateFromDb;
        }

        public EmailTemplate UpdateIfExpired(EmailTemplate emailTemplate, SendGridTemplate sendGridTemplate, AsyncExecutionParam asyncExecutionParam = null)
        {
            var versionFromThirdParty = sendGridTemplate.GetActiveVersion();
            if (versionFromThirdParty != null &&
                (emailTemplate.VersionId != versionFromThirdParty.id
                 || emailTemplate.UsedFrom < versionFromThirdParty.updated_at))
            {
                var newTemplate = _sendGridTemplateToEmailTemplateMappingFactory.Map(sendGridTemplate);
                newTemplate = SetProvider(newTemplate);
                emailTemplate = _emailTemplateService.Renew(emailTemplate.ID, newTemplate, asyncExecutionParam);
                _emailTemplateService.Save();
            }
            return emailTemplate;
        }

        private EmailTemplate SetProvider(EmailTemplate emailTemplate)
        {
            emailTemplate.EmailProviderId = EmailProvider.SendGridProvider;
            return emailTemplate;
        }

        public async Task<EmailSentMessage> SaveUserMessage(string userId, EmailMessageToSend emailMessageToSend, AsyncExecutionParam asyncExecutionParam = null)
        {
            EmailTemplate emailTemplate = null;
            var templateId = emailMessageToSend.Configuration.TemplateId;
            try
            {
                var sendGridTemplate = await GetTemplateFromThirdParty(templateId);
                emailTemplate = CheckEmailTemplate(sendGridTemplate);
            }
            catch (ApiException)
            {
                emailTemplate = _emailTemplateService.GetByTemplateId(templateId, asyncExecutionParam);
            }

            if (emailTemplate == null)
            {
                throw new TemplateNotFound($"Wrong template id [{templateId}]");
            }

            var message = new EmailSentMessage
            {
                UserId = userId,
                //SendToEmail = emailMessageToSend.SendTo,
                EmailTemplateId = emailTemplate.ID,
                EmailTypeId = emailMessageToSend.Type,
                TemplateSubstitutedValues = JsonConvert.SerializeObject(emailMessageToSend.SubstitutionsList),
                SentFromEmail = emailMessageToSend.Configuration.SendFromEmail,
                SentFromEmailDisplayName = emailMessageToSend.Configuration.SendFromEmailDisplayName,
                Subject = emailMessageToSend.Configuration.Subject
            };

            return _emailSentMessageService.InsertAndSave(message, asyncExecutionParam);
        }

        private EmailSentMessage UpdateSendToEmail(EmailSentMessage emailMessageToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            var emailSentMessage = _emailSentMessageService.GetActive(asyncExecutionParam).FirstOrDefault(x => x.ID == emailMessageToUpdate.ID);
            if (emailSentMessage != null)
            {
                var tenant = _tenantService.GetTenant(emailMessageToUpdate.UserId);
                emailSentMessage.SendToEmail = tenant.Email;
                return _emailSentMessageService.UpdateAndSave(emailSentMessage, asyncExecutionParam);
            }
            return null;
        }
    }
}