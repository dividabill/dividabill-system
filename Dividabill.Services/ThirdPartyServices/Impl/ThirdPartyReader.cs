using System.IO;
using System.Net;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using Newtonsoft.Json.Linq;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class ThirdPartyReader : IThirdPartyReader
    {
        public JObject ReadAndParseSafety(string url)
        {
            try
            {
                var rowData = Read(url);
                return JObject.Parse(rowData);
            }
            catch (WebException)
            {
                return null;
            }
        }

        public string Read(string url)
        {
            var client = new WebClient();
            var stream = client.OpenRead(url);
            if (stream == null)
                return null;
            var reader = new StreamReader(stream);
            var rowData = reader.ReadLine();
            return rowData;
        }
    }
}