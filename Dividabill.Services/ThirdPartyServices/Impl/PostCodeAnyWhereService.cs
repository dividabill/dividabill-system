using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using DividaBill.AppConstants;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere;
using DividaBill.ViewModels;
using Newtonsoft.Json;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class PostCodeAnyWhereService : IThirdPartyService, IPostCodeAnyWhereService
    {
        private readonly PostCodeAnyWhereConfig _config;
        private readonly ITestDataService _testDataService;
        private readonly IThirdPartyReader _thirdPartyReader;

        public PostCodeAnyWhereService(PostCodeAnyWhereConfig config, IThirdPartyReader thirdPartyReader, ITestDataService testDataService)
        {
            _testDataService = testDataService;
            _thirdPartyReader = thirdPartyReader;
            _config = config;
        }

        public GeoLocation GetLocationByCoords(float latitude, float longitude)
        {
            var url = GetUrl("GovernmentData/Postzon/RetrieveByCoordinates/v1.20/dataset.ws?");
            url += "&" + GetApiKeyParam();
            url += "&CentrePoint=" + HttpUtility.UrlEncode($"{latitude},{longitude}");

            //Create the dataset
            var dataSet = new DataSet();
            dataSet.ReadXml(url);

            //Check for an error
            if (dataSet.Tables.Count == 1 && dataSet.Tables[0].Columns.Count == 4 && dataSet.Tables[0].Columns[0].ColumnName == "Error")
                throw new Exception(dataSet.Tables[0].Rows[0].ItemArray[1].ToString());

            var dataRow = dataSet.Tables[0].Rows[0];
            var location = new GeoLocation
            {
                Postcode = dataRow[0].ToString(),
                Easting = Convert.ToInt32(dataRow[1].ToString()),
                Northing = Convert.ToInt32(dataRow[2].ToString()),
                Latitude = float.Parse(dataRow[3].ToString()),
                Longitude = float.Parse(dataRow[4].ToString()),
                OsGrid = dataRow[5].ToString(),
                CountryCode = dataRow[6].ToString(),
                CountryName = dataRow[7].ToString(),
                CountyCode = dataRow[8].ToString(),
                CountyName = dataRow[9].ToString(),
                DistrictCode = dataRow[10].ToString(),
                DistrictName = dataRow[11].ToString(),
                WardCode = dataRow[12].ToString(),
                WardName = dataRow[13].ToString(),
                NhsShaCode = dataRow[14].ToString(),
                NhsShaName = dataRow[15].ToString(),
                NhsPctCode = dataRow[16].ToString(),
                NhsPctName = dataRow[17].ToString(),
                LeaCode = dataRow[18].ToString(),
                LeaName = dataRow[19].ToString(),
                GovernmentOfficeCode = dataRow[20].ToString(),
                GovernmentOfficeName = dataRow[21].ToString(),
                WestminsterConstituencyCode = dataRow[22].ToString(),
                WestminsterConstituencyName = dataRow[23].ToString(),
                WestminsterMP = dataRow[24].ToString(),
                WestminsterParty = dataRow[25].ToString(),
                Distance = Convert.ToInt32(dataRow[26].ToString()),
                WestminsterConstituencyCode2010 = dataRow[27].ToString(),
                WestminsterConstituencyName2010 = dataRow[28].ToString()
            };

            return location;
        }

        public List<Premise> GetPremisesByPostcode(string postCode)
        {
            //Build the url
            var url = GetUrl("PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json3.ws?");
            url += "&" + GetApiKeyParam();
            url += "&Postcode=" + HttpUtility.UrlEncode(postCode);
            url += "&PreferredLanguage=" + HttpUtility.UrlEncode("English");
            url += "&Filter=" + HttpUtility.UrlEncode("None");
            url += "&UserName=" + HttpUtility.UrlEncode("");

            var data = _thirdPartyReader.ReadAndParseSafety(url);
            if (data == null)
                return null;

            var resultList = new List<Premise>();
            foreach (var item in data["Items"])
            {
                if ((string) item["Error"] != null)
                {
                    return null;
                }
                resultList.Add(new Premise {PAId = (double) item["Id"], StreetAddress = (string) item["StreetAddress"], Place = (string) item["Place"], Postcode = postCode});
            }

            return resultList;
        }

        public List<Building> GetBuildingsByPremise(double premise)
        {
            //Build the URL
            var url = GetUrl("PostcodeAnywhere/Interactive/RetrieveById/v1.30/json3.ws?");
            url += "&Id=" + premise;
            url += "&PreferredLanguage=" + HttpUtility.UrlEncode("English");
            url += "&UserName=" + HttpUtility.UrlEncode("");
            url += "&" + GetApiKeyParam();

            var data = _thirdPartyReader.ReadAndParseSafety(url);
            if (data == null)
                return null;
            var buildings = new List<Building>();
            foreach (var item in data["Items"])
            {
                if ((string) item["Error"] != null)
                {
                    return null;
                }
                buildings.Add(JsonConvert.DeserializeObject<Building>(item.ToString()));
            }
            return buildings.Count <= 0 ? null : buildings;
        }

        public async Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails)
        {
            var sortCode = bankAccountDetails.AccountSortCode.Replace("-", string.Empty);
            if (_testDataService.ValidateBankAccountDetailsForTestData(bankAccountDetails))
                return null;

            var errorLookup = new[]
            {
                new KeyValuePair<string, UIError>("InvalidAccountNumber",
                    new UIError(BankAccountField.AccountNumber, "Account number is invalid.")),
                new KeyValuePair<string, UIError>("UnknownSortCode",
                    new UIError(BankAccountField.AccountSortCode, "Unrecognised Sort Code.")),
                new KeyValuePair<string, UIError>("AccountNumber Required",
                    new UIError(BankAccountField.AccountNumber, "Please supply an account number.")),
                new KeyValuePair<string, UIError>("AccountNumber Invalid",
                    new UIError(BankAccountField.AccountNumber,
                        "Account Number should contain only numbers. Account numbers must be between 6-10 digits long.")),
                new KeyValuePair<string, UIError>("SortCode Invalid",
                    new UIError(BankAccountField.AccountSortCode,
                        "The SortCode parameter should be 6 digits in the form 00-00-00. It should be prefixed with leading 0s if necessary."))
            };

            //Build the url
            var url = GetUrl("BankAccountValidation/Interactive/Validate/v2.00/dataset.ws?");
            url += GetApiKeyParam();
            url += $"&AccountNumber={WebUtility.UrlEncode(Convert.ToString(bankAccountDetails.AccountNumber))}";
            url += $"&SortCode={WebUtility.UrlEncode(sortCode)}";

            //Create the dataset
            var doc = new XmlDocument();
            doc.Load(url);

            string errorKey = null;
            if (doc.SelectSingleNode("/NewDataSet/Data/Data/Error") != null)
            {
                errorKey = doc.SelectSingleNode("/NewDataSet/Data/Description").InnerText;
            }
            else if (doc.SelectSingleNode("/NewDataSet/Data/IsCorrect").InnerText.ToLowerInvariant() == "false")
            {
                errorKey = doc.SelectSingleNode("/NewDataSet/Data/StatusInformation").InnerText;
            }
            var uiError = errorLookup.Where(x => x.Key == errorKey).Select(x => x.Value).SingleOrDefault();
            if (uiError != null)
            {
                return new List<UIError> {uiError};
            }
            return null;
        }

        public ThirdPartyExecutionSortOrder SortOrder => _config.SortOrder ?? new ThirdPartyExecutionSortOrder();

        private string GetUrl(string subUrl)
        {
            return $"{_config.BaseUrl}/{subUrl}";
        }

        private string GetApiKeyParam()
        {
            return "Key=" + HttpUtility.UrlEncode(_config.ApiKey);
        }
    }
}