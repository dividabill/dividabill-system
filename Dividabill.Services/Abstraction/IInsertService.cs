using DividaBill.DAL.Unity;
using DividaBill.Models.Abstractions;

namespace DividaBill.Services.Abstraction
{
    public interface IInsertService<T> where T : IEntityModel
    {
        T InsertAndSave(T entityToInsert, AsyncExecutionParam asyncExecutionParam = null);
        T Insert(T entityToInsert, AsyncExecutionParam asyncExecutionParam = null);
    }
}