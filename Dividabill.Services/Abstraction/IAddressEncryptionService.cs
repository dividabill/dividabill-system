using DividaBill.Models;

namespace DividaBill.Services.Abstraction
{
    public interface IAddressEncryptionService
    {
        void EncryptAddress(Address address);
        void DecryptAddress(Address address);
    }
}