using System.Linq;
using DividaBill.Models;

namespace DividaBill.Services.Abstraction
{
    public interface IProviderService
    {
        Provider GetProvider(int id);
        IQueryable<Provider> GetProvidersByServiceTypeId(int serviceTypeId);
        IQueryable<Provider> GetAllProviders();
    }
}