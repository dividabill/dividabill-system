using DividaBill.DAL.Unity;
using DividaBill.Models.Abstractions;

namespace DividaBill.Services.Abstraction
{
    public interface IUpdateService<T> where T : IEntityModel
    {
        T UpdateAndSave(T entityToUpdate, AsyncExecutionParam asyncExecutionParam = null);
        T Update(T entityToUpdate, AsyncExecutionParam asyncExecutionParam = null);
    }
}