using System.Linq;
using DividaBill.DAL.Unity;
using DividaBill.Models.Abstractions;

namespace DividaBill.Services.Abstraction
{
    public interface IGetService<out T> where T : IEntityModel
    {
        IQueryable<T> GetActive(AsyncExecutionParam asyncExecutionParam = null);
    }

    public interface IGetIdentityService<out T> : IGetService<T> where T : IIdentityEntity, IEntityModel
    {
        T GetById(int id, AsyncExecutionParam asyncExecutionParam = null);
    }

    public interface IGetIdentityService2<out T> : IGetService<T> where T : IIdentityEntity2, IEntityModel
    {
        T GetById(string id, AsyncExecutionParam asyncExecutionParam = null);
    }
}