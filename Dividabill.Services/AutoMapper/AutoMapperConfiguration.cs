﻿using System.Linq;
using AutoMapper;
using DividaBill.Models;
using DividaBill.ViewModels;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.AutoMapper
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            ConfigureViewModelsMappings();
        }

        private static void ConfigureViewModelsMappings()
        {
            Mapper.CreateMap<Premise, PremiseViewModel>();
            Mapper.CreateMap<Building, BuildingViewModel>();

            Mapper.CreateMap<HouseModel, HouseViewModel>()
                .ForMember(dto => dto.Electricity, conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Electricity.Id).Any()))
                .ForMember(dto => dto.Gas, conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Gas.Id).Any()))
                .ForMember(dto => dto.Water, conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Water.Id).Any()))
                .ForMember(dto => dto.Broadband, conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Broadband.Id).Any()))
                .ForMember(dto => dto.BroadbandType, conf => conf.MapFrom(dm =>
                    dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Broadband.Id).Any() ?
                        (BroadbandType) dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.Broadband.Id).FirstOrDefault().PackageRaw :
                        BroadbandType.None
                    ))
                .ForMember(
                    dto => dto.LandlinePhone,
                    conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id).Any()))
                .ForMember(dto => dto.LandlineType, conf => conf.MapFrom(dm =>
                    dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id).Any() ?
                        (LandlineType) dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.LandlinePhone.Id).FirstOrDefault().PackageRaw :
                        LandlineType.None
                    ))
                .ForMember(
                    dto => dto.SkyTV,
                    conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.SkyTV.Id).Any()))
                .ForMember(
                    dto => dto.TVLicense,
                    conf => conf.MapFrom(dm => dm.Contracts.Where(e => e.ServiceNew_ID == ServiceType.TVLicense.Id).Any()))
                .ForMember(
                    dto => dto.ActualStartDate,
                    conf => conf.MapFrom(dm => dm.Contracts
                        .Where(c => c.StartDate.HasValue)
                        .OrderBy(c => c.StartDate)
                        .Select(c => c.StartDate)
                        .FirstOrDefault()))
                .ForMember(
                    dto => dto.Coupon,
                    conf => conf.MapFrom(dm => dm.Coupon.Code))
                .ForMember(
                    dto => dto.Options,
                    conf => conf.Ignore())
                .ForMember(
                    dto => dto.RegisteredTenants,
                    conf => conf.MapFrom(dm => dm.Tenants.Count()))
                .ForMember(
                    dto => dto.Address,
                    conf => conf.MapFrom(dm => dm.Address.Line1 + ", " + dm.Address.Postcode));


            Mapper.CreateMap<User, TenantsViewModel>()
                .ForMember(dto => dto.Locked, conf => conf.MapFrom(ol => ol.LockoutEnabled))
                .ForMember(dto => dto.Options, conf => conf.Ignore());
        }
    }
}