﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Reflection;
using System.Web;
using DividaBill.AppConstants;
using DividaBill.DAL;
using DividaBill.Library.AppConfiguration;
using DividaBill.Library.Unity;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.Security.Unity;
using DividaBill.Services.Abstraction;
using DividaBill.Services.AppConfiguration;
using DividaBill.Services.AppConfiguration.Impl;
using DividaBill.Services.Auth;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Impl;
using DividaBill.Services.Factories;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Factories.Impl.Api;
using DividaBill.Services.Factories.Impl.EntityToViewModel;
using DividaBill.Services.Factories.Impl.ViewModelToEntity;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.Log;
using DividaBill.Services.Messaging;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.NewBillingSystem.Model;
using DividaBill.Services.Sheets;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere;
using DividaBill.ViewModels;
using DividaBill.ViewModels.API;
using DividaBill.ViewModels.DataTables;
using GoCardless_API.api;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Practices.Unity;
using SendGrid.API.api;
using SendGrid.API.Models;
using SendGrid.API.Unity;

namespace DividaBill.Services.Unity
{
    public static class Config
    {
        private const ThirdPartyServicesConfigEnum Baseurl = ThirdPartyServicesConfigEnum.BaseUrl;
        private const ThirdPartyServicesConfigEnum Apikey = ThirdPartyServicesConfigEnum.ApiKey;
        private const ThirdPartyServicesConfigEnum Version = ThirdPartyServicesConfigEnum.Version;

        /// <summary>
        ///     Adds all default dependencies needed to use the DividaBill.Services objects.
        ///     TODO: some of those should be moved to DAL
        /// </summary>
        /// <param name="container"></param>
        public static void RegisterServices(this IUnityContainer container)
        {
            container.RegisterSecurity();
            container.RegisterSendGridApi();
            container.RegisterLibrary();

            container.RegisterType<IDataProtectionProvider, MachineKeyProtectionProvider>();
            container.RegisterType<IIdentityMessageService, IdentityEmailService>();
            container.RegisterType<ITenancyService,TenancyService>();

            container.RegisterType<IUserStore<User>, UserStore<User>>();

            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());

            container.RegisterType<ApplicationDbContext>(
                new InjectionFactory(c => new ApplicationDbContext()));

            RegisterOldServices(container);


            RegisterNewBillingServices(container);

            RegisterFactories(container);
            RegisterMappingFactories(container);
            RegisterMappingFactoriesFromEntityToViewModel(container);
            RegisterMappingFactoriesFromViewModelToEntity(container);
            RegisterMappingFactoriesFromEntityToApiModel(container);
            RegisterMappingFactoriesFromThridPartyToApiModel(container);
            RegisterEncryptionServices(container);

            RegisterReportingServices(container);

            RegisterApiServices(container);
            RegisterThirdPartyServices(container);
            RegisterEmailServices(container);

            //Identity DI Config
            container.RegisterType<ApplicationSignInManager>();
            container.RegisterType<ApplicationUserManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            var dllLocation = Assembly.GetCallingAssembly().CodeBase;
            var dllUri = new Uri(dllLocation);
            var binFolder = Path.GetDirectoryName(dllUri.LocalPath);
            var gDriveCredentials = Path.Combine(binFolder, @"sheets\service-credentials.p12");

            if (!File.Exists(gDriveCredentials))
            {
                Console.WriteLine("Warning: GDrive service credentials not found! GDrive service will be unavailable!");
                return;
            }
            container.RegisterType<IGDriveService, GDriveService>();
            GDriveService.Initialize(gDriveCredentials);
        }

        private static void RegisterReportingServices(IUnityContainer container)
        {
            container.RegisterType<IReportingService, ReportingService>();
        }

        private static void RegisterNewBillingServices(IUnityContainer container)
        {
            container.RegisterType<IBillingQueueService, BillingQueueService>();
            container.RegisterType<IBillingQueueTransactionLogService, BillingQueueTransactionLogService>();
            container.RegisterType<IBillingRunService, BillingRunService>();
            container.RegisterType<IBillingService, BillingService>();
            container.RegisterType<IBillPaymentsService, BillPaymentsService>();
            container.RegisterType<ICalculateEstimatesPricesService, CalculateEstimatesPricesService>();
            container.RegisterType<ICancelPendingPaymentsGocardlessService, CancelPendingPaymentsGocardlessService>();
            container.RegisterType<IEstimatedUtilityPricesService, HardCodedEstimatedUtilityPricesService>();

            container.RegisterType<IHouseholdBillService, HouseholdBillService>();
            container.RegisterType<IHousesToBeBilledService, HousesToBeBilledService>();

            container.RegisterType<IHouseBlacklistService, HouseBlacklistService>();

            container.RegisterType<IHouseToBeBilledManager, HouseToBeBilledManager>();

            container.RegisterType<IPriceService, PriceService>();
            container.RegisterType<IRefreshPaymentStatusService, RefreshPaymentStatusService>();
            container.RegisterType<IRemoveAllPaymentsService, RemoveAllPaymentsService>();


            container.RegisterType<IGoCardlessService, GoCardlessService>();
            container.RegisterType<IGoCardlessBankMandateService, GoCardlessBankMandateService>();
            container.RegisterType<IGoCardlessBankAccountService, GoCardlessBankAccountService>();
            container.RegisterType<IGoCardlessBankAccountDetailsService, GoCardlessBankAccountDetailsService>();
            container.RegisterType<IGoCardlessCustomerService, GoCardlessCustomerService>();
            container.RegisterType<IBankAccountRegistrationService, BankAccountRegistrationService>();

            container.RegisterType<ISendBillEmailsService, SendBillEmailsService>();
            container.RegisterType<ISendBillsToGoCardlessService, SendBillsToGoCardlessService>();

            container.RegisterType<IUtilityBillService, UtilityBillService>();
            container.RegisterType<IDistributedBillingService, DistributedBillingService>();
            container.RegisterType<IMultiThreadingService, MultiThreadingService>();

            container.RegisterType<IProviderService, ProviderService>();

            //Identity DI Config
            container.RegisterType<ApplicationSignInManager>();
            container.RegisterType<ApplicationUserManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthenticationManager>(
                new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            var dllLocation = Assembly.GetCallingAssembly().CodeBase;
            var dllUri = new Uri(dllLocation);
            var binFolder = Path.GetDirectoryName(dllUri.LocalPath);
            var gDriveCredentials = Path.Combine(binFolder, @"sheets\service-credentials.p12");
        }

        private static void RegisterOldServices(IUnityContainer container)
        {
            container.RegisterType<IAccountService, BankAccountService>();
            container.RegisterType<ITestDataService, TestDataService>();
            container.RegisterType<IContractService, ContractService>();
            container.RegisterType<ICouponService, CouponService>();
            container.RegisterType<IHouseService, HouseService>();
            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IPremiseService, PremiseService>();
            container.RegisterType<IPriceService, PriceService>();
            container.RegisterType<IAuthService, AuthService>();
            container.RegisterType<ILoggingService, LoggingService>();
            container.RegisterType<IBankMandateService, BankMandateService>();
            container.RegisterType<ITenantService, TenantService>();
            container.RegisterType<IBillService, BillService>();
        }

        private static void RegisterApiServices(IUnityContainer container)
        {
            container.RegisterType<IGoCardlessApiClient, GoCardlessApiClient>();
            container.RegisterType<ISendGridApiClient, SendGridApiClient>();
        }

        private static void RegisterEmailServices(IUnityContainer container)
        {
            container.RegisterType<IEmailTemplateService, EmailTemplateService>();
            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IEmailSentMessageService, EmailSentMessageService>();
            container.RegisterType<IEmailConfigurationReader, EmailConfigurationReader>();
        }

        private static void RegisterThirdPartyServices(IUnityContainer container)
        {
            container.RegisterType<IThirdPartyReader, ThirdPartyReader>();

            var thirdPartyReader = container.Resolve<IThirdPartyReader>();
            var testDataService = container.Resolve<ITestDataService>();
            var goCardlessBankAccountDetailsService = container.Resolve<IGoCardlessBankAccountDetailsService>();

            var appConfigurationReader = container.Resolve<IAppConfigurationReader>();

            var thirdPartyServices = GetThirdPartyServicesByDefault(appConfigurationReader,thirdPartyReader, testDataService, goCardlessBankAccountDetailsService);
            container.RegisterType<IThirdPartiesListService, ThirdPartiesListService>(new InjectionConstructor(thirdPartyServices));

            const ThirdPartyServicesConfigEnum baseurl = ThirdPartyServicesConfigEnum.BaseUrl;
            const ThirdPartyServicesConfigEnum apikey = ThirdPartyServicesConfigEnum.ApiKey;

            var dbIpConfiguration = new DbIpConfig
            {
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.DbIp, baseurl),
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.DbIp, apikey)
            };

            container.RegisterType<IPostCodeAnyWhereService, PostCodeAnyWhereService>(new InjectionConstructor(GetPostCodeAnyWhereConfiguration(appConfigurationReader), thirdPartyReader, testDataService));
            container.RegisterType<IDbIpService, DbIpService>(new InjectionConstructor(dbIpConfiguration, thirdPartyReader));

            container.RegisterType<IEmailThirdPartyService, EmailThirdPartyService>();
        }

        private static List<IThirdPartyService> GetThirdPartyServicesByDefault(IAppConfigurationReader appConfigurationReader, IThirdPartyReader thirdPartyReader, ITestDataService testDataService
            , IGoCardlessBankAccountDetailsService goCardlessBankAccountDetailsService)
        {
            var postCodeAnyWhereConfiguration = GetPostCodeAnyWhereConfiguration(appConfigurationReader);

            var idealPostCodesConfiguration = new IdealPostCodesConfig
            {
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, Baseurl),
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, Apikey),
                Version = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, Version),
                SortOrder = new ThirdPartyExecutionSortOrder
                {
                    ValidateBankAccountDetails = 3,
                    BuildingByPremise = 2,
                    PremisesByPostCode = 2
                }
            };

            var goCardlessThirdPartyConfiguration = new GoCardlessThirdPartyConfig
            {
                SortOrder = new ThirdPartyExecutionSortOrder
                {
                    ValidateBankAccountDetails = 1,
                    BuildingByPremise = 3,
                    PremisesByPostCode = 3
                }
            };

            var thirdPartyServices = new List<IThirdPartyService>
            {
                new PostCodeAnyWhereService(postCodeAnyWhereConfiguration, thirdPartyReader, testDataService),
                new IdealPostCodesService(idealPostCodesConfiguration, thirdPartyReader),
                new GoCardlessThirdPartyService(goCardlessThirdPartyConfiguration, goCardlessBankAccountDetailsService)
            };
            return thirdPartyServices;
        }

        private static PostCodeAnyWhereConfig GetPostCodeAnyWhereConfiguration(IAppConfigurationReader appConfigurationReader)
        {
            var postCodeAnyWhereConfiguration = new PostCodeAnyWhereConfig
            {
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, Baseurl),
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, Apikey),
                SortOrder = new ThirdPartyExecutionSortOrder
                {
                    ValidateBankAccountDetails = 2,
                    BuildingByPremise = 1,
                    PremisesByPostCode = 1
                }
            };
            return postCodeAnyWhereConfiguration;
        }


        private static void RegisterEncryptionServices(IUnityContainer container)
        {
            container.RegisterType<IAddressEncryptionService, AddressEncryptionService>();
            container.RegisterType<ITenantEncryptionService, TenantEncryptionService>();
            container.RegisterType<IBankAccountEncryptionService, BankAccountEncryptionService>();

            container.RegisterType<IViewModelEncryptionService<TenantsViewModel>, TenantsViewModelEncryptionService>();
            container.RegisterType<IViewModelEncryptionService<TenantsDataTableViewModel>, TenantsDataTableViewModelEncryptionService>();
        }

        private static void RegisterFactories(IUnityContainer container)
        {
            container.RegisterType<INotificationFactory, NotificationFactory>();
        }

        private static void RegisterMappingFactories(IUnityContainer container)
        {
            container.RegisterType<IStoredProcFactory, StoredProcFactory>();
            container.RegisterType<IHouseHoldBillViewFactory, HouseHoldBillViewFactory>();
            container.RegisterType<IGcMandateToBankMandateMappingFactory, GcMandateToBankMandateMappingFactory>();
            container.RegisterType<IGcBankAccountToBankAccountMappingFactory, GcBankAccountToBankAccountMappingFactory>();
            container.RegisterType<IMappingFactory<HousesToBeBilledModel, HousesToBeBilledViewModel>, HousesToBeBilledModelToHousesToBeBilledViewModelMappingFactory>();
            container.RegisterType<IUserGcInfoUpdateViewModelMappingFactory, UserGcInfoUpdateViewModelMappingFactory>();
        }

        private static void RegisterMappingFactoriesFromEntityToViewModel(IUnityContainer container)
        {
            container.RegisterType<IEntityToViewModelMappingFactory<Contract, ContractRemoveViewModel>, ContractToContractRemoveOutputViewModelMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<User, BankAccountDetails>, UserToBankAccountDetailsMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<HouseModel, HouseViewModel>, HouseModelToHouseViewModelMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<User, TenantsViewModel>, UserToTenantsMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<User, TenantsDataTableViewModel>, UserToTenantsDataTableMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel>, TenancyToTenancyViewModelMappingFactory>();
            container.RegisterType<IEntityToViewModelMappingFactory<User, TenantViewModel>, UserToTenantViewModelMappingFactory>();
        }

        private static void RegisterMappingFactoriesFromViewModelToEntity(IUnityContainer container)
        {
            container.RegisterType<IViewModelToEntityMappingFactory<SignUpViewModel, User>, SignUpViewModelToUserMappingFactory>();
            container.RegisterType<IViewModelToEntityMappingFactory<TenantViewModel, User>, TenantViewModelToUserMappingFactory>();
            container.RegisterType<IViewModelToEntityMappingFactory<ContractRemoveViewModel, Contract>, ContractRemoveViewModelToContractMappingFactory>();
            container.RegisterType<IViewModelToEntityMappingFactory<HouseDetailsViewModel, HouseModel>, HouseDetailsViewModelToHouseModelMappingFactory>();
            container.RegisterType<IViewModelToEntityMappingFactory<TenancyViewModel, Tenancy>, TenancyViewModelToTenancyMappingFactory>();
            container.RegisterType<IBillingRunCreateInputModelToBillingRunMappingFactory, BillingRunCreateInputModelToBillingRunMappingFactory>();
            container.RegisterType<IMappingFactory<SendGridTemplate, EmailTemplate>, SendGridTemplateToEmailTemplateMappingFactory>();
        }

        private static void RegisterMappingFactoriesFromEntityToApiModel(IUnityContainer container)
        {
            container.RegisterType<IEntityToApiOutputViewModelMappingFactory<Provider, ProviderApiOutputViewModel>, ProviderOutputViewModelMappingViewOutputViewModelFactory>();
            container.RegisterType<IUserRegistrationService, UserRegistrationService>();
            container.RegisterType<IBankPaymentFactory, BankPaymentFactory>();
            //container.RegisterType<IFactory<BankPaymentFactory>, BankPaymentFactory>();
        }

        private static void RegisterMappingFactoriesFromThridPartyToApiModel(IUnityContainer container)
        {
            container.RegisterType<IThirdPartyModelToApiOutputViewModelMappingFactory<GeoLocation, GeoLocationApiOutputViewModel>, GeoLocationToGeoLocationOutputApiModelMappingFactory>();
        }
    }
}