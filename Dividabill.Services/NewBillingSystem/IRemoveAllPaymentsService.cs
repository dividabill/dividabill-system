using System;
using System.Threading.Tasks;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IRemoveAllPaymentsService
    {
        void RemovePayments(int billingRunId, DateTime? dt);
    }
}