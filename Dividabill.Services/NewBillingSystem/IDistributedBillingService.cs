using System;
using System.Threading.Tasks;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public interface IDistributedBillingService
    {
        Task DistributedGenerateBillsAsync(int billingRunId);
        Task<bool> DistributedGenerateBillForHouse(int billingRunId, int houseId, DateTime period);
    }
}