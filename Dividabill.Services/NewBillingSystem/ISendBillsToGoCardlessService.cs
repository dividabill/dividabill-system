using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.NewBillingSystem.Impl;
using GoCardless_API;

namespace DividaBill.Services.NewBillingSystem
{
    public interface ISendBillsToGoCardlessService
    {
        Task<BulkOperationResult<HouseModel>> SendBillsToGoCardlessAsync(int billingRunId, GoCardless.Environments goCardlessEnvironment);
    }
}