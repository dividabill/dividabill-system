﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.NewBillingSystem.Model;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IHousesToBeBilledService
    {
        IQueryable<HouseModel> GetHousesToBeBilled(int billingRunId);
        IQueryable<HouseModel> GetHousesThatHaventBeenBilled(int billingRunId);
        IQueryable<HouseModel> GetHousesThatHaveBeenBilled(int billingRunId);
        Task<List<int>> SetHousesToBeBilledInBillingRunAsync(int billingRunId, List<int> houseIds);
        Task<List<int>> DeleteHousesToBeBilledFromBillingRunAsync(int billingRunId, List<int> houseIds);
        IQueryable<HouseModel> GetHousesThatHaveBeenBilledAnAmount(int billingRunId);
        IQueryable<HousesToBeBilledModel> GetAllHousesToBeBilled(int billingRunId);
    }
}