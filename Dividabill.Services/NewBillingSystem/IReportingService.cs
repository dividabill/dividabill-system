﻿using System;
using System.Collections.Generic;
using DividaBill.Models.Business;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IReportingService
    {
        List<MonthlyBillingReport> GetTotalBilledForPeriod(DateTime start, DateTime end);
    }
}
