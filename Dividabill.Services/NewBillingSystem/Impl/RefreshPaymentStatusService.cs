﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Library.Helpers;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;
using DividaBill_Library;
using GoCardless_API;
using GoCardless_API.api;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public interface IBankPaymentFactory
    {
        BankPayment CreateOrUpdate(GCPayment pay, BankPayment existingBankPayment);
        BankPayment Create(GCPayment pay);
    }

    public interface IFactory<T>
    {
        T Create<I>(I source);
        T CreateOrUpdate<I>(I source, T existing);
    }

    public class BankPaymentFactory : IBankPaymentFactory
        //IBankPaymentFactory
    {
        public BankPayment CreateOrUpdate(GCPayment pay, BankPayment existingBankPayment)
        {
            var payment = existingBankPayment ?? new BankPayment();
            if (string.IsNullOrEmpty(payment.id))
                payment.id = pay.id;

            payment.created_at = pay.created_at;
            payment.charge_date = pay.charge_date;
            payment.amount = pay.amount;
            payment.description = pay.description;
            payment.currency = pay.currency;
            payment.status = pay.status;
            payment.amount_refunded = pay.amount_refunded;
            payment.Mandate_ID = pay.links["mandate"];
            payment.amount_refunded = pay.amount_refunded;

            return payment;
        }

        public BankPayment Create(GCPayment pay)
        {
            var payment = CreateOrUpdate(pay, null);

            return payment;
        }
    }

    //threadSafe

    public class RefreshPaymentStatusService : IRefreshPaymentStatusService
    {
        private readonly IBankPaymentFactory _bankPaymentFactory;
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly IUnitOfWork _unitOfWork;
        private IBankMandateService _bankMandateService;

        public RefreshPaymentStatusService(IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApiClient, IBankPaymentFactory bankPaymentFactory,IBankMandateService bankMandateService)
        {
            _bankMandateService = bankMandateService;
            _unitOfWork = unitOfWork;
            _goCardlessApiClient = goCardlessApiClient;
            _bankPaymentFactory = bankPaymentFactory;
        }

        public async Task<BulkOperationResult<GCPayment>> RefreshAllPaymentStatusAsync(GoCardless.Environments environment)
        {
            GoCardless.Environment = environment;

            var payments = await _goCardlessApiClient.GetPaymentsAsync();

            var multiThreadedService = new MultiThreadingService();

            var operationResult = await multiThreadedService.RunDatabaseTaskInParallelAsync(5, payments,
                async (payment, unitOfWork, bulkOperationResult) =>
                {
                    try
                    {
                        await UpdatePayment(payment, unitOfWork);
                    }
                    catch (Exception ex)
                    {
                        bulkOperationResult.Failed.Add(payment, ex);
                    }
                });

            return operationResult;
        }

        public async Task RefreshPaymentStatusAsync(GoCardless.Environments environment)
        {
            var mandProblemOnes = new Dictionary<string, string>();
            var problemOnes = new Dictionary<string, string>();
            GoCardless.Environment = environment;

            var payments = _unitOfWork.BankPaymentRepository.Get().Where(w => w.status != "paid_out").ToList();

            foreach (var pay in payments)
                await RefreshPayment(pay, problemOnes);


            var mandates = _bankMandateService.GetActive().ToList();
            foreach (var mand in mandates)
                await RefreshMandates(mand, mandProblemOnes);

            ExportCsv(problemOnes, mandProblemOnes);
            Console.WriteLine("Submited all payments to GoCardless. Check error log files for any problems.");
        }

        /// <summary>
        /// </summary>
        /// <param name="pay"></param>
        /// <param name="unitOfWork">Multithreading safe if you pass in an instance of unit of work</param>
        private async Task UpdatePayment(GCPayment pay, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var payment = await unitOfWork.BankPaymentRepository.GetByIDAsync(pay.id);

            payment = _bankPaymentFactory.CreateOrUpdate(pay, payment);

            if (payment.status == "paid_out")
            {
                var bp = unitOfWork.BillPaymentsRepository.Get().Where(x => x.PaymentRef == payment.id);

                foreach (var p in bp)
                {
                    if (!p.IsPaid)
                        p.MarkAsPaid();
                    unitOfWork.BillPaymentsRepository.Update(p);
                }
            }

            unitOfWork.Save();
        }

        private async Task RefreshMandates(BankMandate mand, Dictionary<string, string> mandProblemOnes)
        {
            try
            {
                var mand1 = await Retry.DoAsync(async () => await _goCardlessApiClient.GetMandateAsync(mand.Id), TimeSpan.FromSeconds(5), 1);
                BankMandate mandate = null;
                try
                {
                    mandate = _bankMandateService.GetById(mand.Id);
                }
                catch (BankMandateNotFound)
                {
                }
                if (mandate != null)
                {
                    mandate.status = mand1.status;

                    _bankMandateService.Update(mandate);
                    _unitOfWork.Save();
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(x =>
                {
                    var exception = x as ApiException;
                    if (exception != null) // This we know how to handle.
                        if (!mandProblemOnes.ContainsKey(mand.Id))
                            mandProblemOnes.Add(mand.Id, exception.RawContent);
                    return true; //if you do something like this all exceptions are marked as handled  
                });
            }
        }

        private async Task RefreshPayment(BankPayment pay, Dictionary<string, string> problemOnes)
        {
            try
            {
                var pay1 = await Retry.DoAsync(async () => await _goCardlessApiClient.GetPaymentAsync(pay.id), TimeSpan.FromSeconds(5), 1);
                var payment = _unitOfWork.BankPaymentRepository.GetByID(pay.id);
                if (payment != null)
                {
                    payment.status = pay1.status;
                    payment.amount_refunded = pay1.amount_refunded;


                    if (payment.status == "paid_out")
                    {
                        var bp = _unitOfWork.BillPaymentsRepository.Get().Where(x => x.PaymentRef == payment.id);

                        foreach (var p in bp)
                        {
                            if (!p.IsPaid)
                            {
                                p.MarkAsPaid();
                            }
                            _unitOfWork.BillPaymentsRepository.Update(p);
                        }
                    }

                    _unitOfWork.Save();
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(x =>
                {
                    var exception = x as ApiException;
                    if (exception != null) // This we know how to handle.
                        if (!problemOnes.ContainsKey(pay.id))
                            problemOnes.Add(pay.id, exception.RawContent);
                    return true; //if you do something like this all exceptions are marked as handled  
                });
            }
        }

        private void ExportCsv(Dictionary<string, string> problemOnes, Dictionary<string, string> mandProblemOnes)
        {
            var s = string.Join(Environment.NewLine, problemOnes.Select(x => x.Key + "," + x.Value));
            var s2 = string.Join(Environment.NewLine, mandProblemOnes.Select(x => x.Key + "," + x.Value));


            File.WriteAllText("problemUpdatesPaymentsGoCardless-" + DateTime.Now.Ticks + ".csv", s);
            File.WriteAllText("problemUpdatesMandatesGoCardless-" + DateTime.Now.Ticks + ".csv", s2);
        }
    }
}