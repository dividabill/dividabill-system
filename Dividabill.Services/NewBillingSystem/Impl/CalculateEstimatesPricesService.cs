using DividaBill.Models;
using DividaBill.Models.Abstractions;
using DividaBill.ViewModels;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class CalculateEstimatesPricesService : ICalculateEstimatesPricesService
    {
        private readonly IEstimatedUtilityPricesService _estimatedUtilityPricesService;

        public CalculateEstimatesPricesService(IEstimatedUtilityPricesService estimatedUtilityPricesService)
        {
            _estimatedUtilityPricesService = estimatedUtilityPricesService;
        }

        public decimal GetEstimatedLandlinePhoneMediumPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return (estimatedPrice.LandlinePhoneMedium + estimatedPrice.LandlinePhoneMediumMargin) * 1.2m / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedLandlinePhoneBasicPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return (estimatedPrice.LandlinePhoneBasic + estimatedPrice.LandlinePhoneBasicMargin) * 1.2m / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedTvLicensePrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return estimatedPrice.TVLicense / id.tenants / 52;
        }

        public decimal GetEstimatedSkyTvPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return ((estimatedPrice.SkyTV + estimatedPrice.SkyTVMargin + (estimatedPrice.SkyTVSatellite + estimatedPrice.SkyTVSTB + estimatedPrice.SkyTVInstallation) / 12 + estimatedPrice.SkyTVSatelliteMargin + estimatedPrice.SkyTVSTBMargin + estimatedPrice.SkyTVInstallationMargin) * 1.2m) / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedNetflixTvPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return estimatedPrice.NetflixTV / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedLineRental(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return ((estimatedPrice.LineRental + estimatedPrice.LineRentalMargin + estimatedPrice.LineRentalInstallation / 12 + estimatedPrice.LineRentalMargin) * 1.2m) / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedBroadbandFibre40Prices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return ((estimatedPrice.BroadbandFible38 + estimatedPrice.BroadbandFible38Margin + estimatedPrice.BroadbandFible38Router / 12 + estimatedPrice.BroadbandFibre40RouterMargin) * 1.2m) / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedBroadbandAdslPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return ((estimatedPrice.BroadbandADSL + estimatedPrice.BroadbandADSLMargin + estimatedPrice.BroadbandADSLRouter / 12 + estimatedPrice.BroadbandADSLRouterMargin) * 1.2m) / id.tenants * 12 / 52;
        }

        public decimal GetEstimatedGasPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return (estimatedPrice.GasStandingCharge / 100 * 7 + (estimatedPrice.Gas * _estimatedUtilityPricesService.GasUsage[((id.tenants >= 9) ? 9 : id.tenants) - 1] / 100)) / id.tenants;
        }

        public decimal GetEstimatedElectricityPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            return (estimatedPrice.ElectricityStandingCharge / 100 * 7 + (estimatedPrice.Electricity * _estimatedUtilityPricesService.ElectricityUsage[((id.tenants >= 9) ? 9 : id.tenants) - 1] / 100)) / id.tenants;
        }

        public decimal GetEstimatedWaterPrices(CouponSearchViewModel id)
        {
            return _estimatedUtilityPricesService.Water[((id.tenants >= 9) ? 9 : id.tenants) - 1];
        }
    }
}