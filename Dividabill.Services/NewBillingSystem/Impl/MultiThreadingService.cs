using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class MultiThreadingService : IMultiThreadingService
    {
        public int GetAvailableThreads(bool isIO = false)
        {
            var availableThreads = 0;
            var availableIoThreads = 0;
            ThreadPool.GetAvailableThreads(out availableThreads, out availableIoThreads);
            return !isIO ? availableThreads : availableIoThreads;
        }

        public async Task<BulkOperationResult<T>> RunTaskInParallelAsync<T,T2>(int maxParallelTasks, IList<T> collection, Action<T, IUnitOfWork, T2, BulkOperationResult<T>> action)
        {
            var maxThread = new SemaphoreSlim(maxParallelTasks);

            var bulkOperationResult = new BulkOperationResult<T>();

            for (var i = 0; i < collection.Count; i++)
            {
                await maxThread.WaitAsync();

                await Task.Run(() =>
                {
                    var item = collection[i];
                    try
                    {
                        //Your Works
                        var unitOfWork = new UnitOfWork();
                        unitOfWork.EnableBulkInsertOptimisations();
                        var other = default(T2);
                        

                        
                        action(item, unitOfWork, other,bulkOperationResult);
                        
                        bulkOperationResult.Successful.Add(item);
                    }
                    catch (Exception ex)
                    {
                        bulkOperationResult.Failed.Add(item, ex);
                    }
                    //unitOfWork = null;
                })
                //, TaskCreationOptions.LongRunning)
                .ContinueWith(task =>
                {
                    maxThread.Release();
                });
            }
            return bulkOperationResult;

        }

        public async Task<BulkOperationResult<T>> RunDatabaseTaskInParallelAsync<T>(int maxParallelTasks, IList<T> collection, Action<T, IUnitOfWork, BulkOperationResult<T>> action)
        {
            var maxThread = new SemaphoreSlim(maxParallelTasks);

            var bulkOperationResult = new BulkOperationResult<T>();

            for (var i = 0; i < collection.Count; i++)
            {
                await maxThread.WaitAsync();

                await Task.Run(() =>
                {
                    var item = collection[i];
                    try
                    {
                        //Your Works
                        var unitOfWork = new UnitOfWork();
                        //unitOfWork.EnableBulkInsertOptimisations();
                        
                        action(item, unitOfWork, bulkOperationResult);
                        //unitOfWork = null;
                        //if()
                        //unitOfWork.Dispose();
                        bulkOperationResult.Successful.Add(item);
                    }
                    catch (Exception ex)
                    {
                        bulkOperationResult.Failed.Add(item, ex);
                    }
                })
                //, TaskCreationOptions.LongRunning)
                .ContinueWith(task =>
                {
                    maxThread.Release();
                });
            }

            return bulkOperationResult;
        }
    }
}