using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem.Model;
using GoCardless_API;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class BillingRunService : IBillingRunService
    {
        private readonly IGoCardlessService _goCardlessService;
        private readonly IHouseholdBillService _householdBillService;
        private readonly IHouseService _houseService;
        private readonly IHousesToBeBilledService _housesToBeBilledService;
        private readonly ISendBillsToGoCardlessService _sendBillsToGoCardlessService;
        private readonly IUnitOfWork _unitOfWork;

        public BillingRunService(IUnitOfWork unitOfWork, IHousesToBeBilledService housesToBeBilledService, IHouseService houseService,
            ISendBillsToGoCardlessService sendBillsToGoCardlessService, IGoCardlessService goCardlessService, IHouseholdBillService householdBillService)
        {
            _unitOfWork = unitOfWork;
            _housesToBeBilledService = housesToBeBilledService;
            _houseService = houseService;
            _sendBillsToGoCardlessService = sendBillsToGoCardlessService;
            _goCardlessService = goCardlessService;
            _householdBillService = householdBillService;
        }

        public async Task<List<BillingRun>> GetAllBillingRunsAsync()
        {
            return await _unitOfWork.BillingRunRepository.Get().ToListAsync();
        }

        public async Task<BillingRun> GetBillingRunAsync(int billingRunId, IUnitOfWork unitOfWork = null)
        {
            var billingRun = await _unitOfWork.BillingRunRepository.GetByIDAsync(billingRunId);
            if (billingRun == null)
            {
                throw new BillingRunNotFound(billingRunId.ToString());
            }
            return billingRun;
        }

        public async Task<BillingRun> Insert(BillingRun billingRun)
        {
            billingRun.CreatedAt = DateTime.UtcNow;
            billingRun.LastModified = DateTime.UtcNow;
            billingRun = _unitOfWork.BillingRunRepository.Insert(billingRun);
            await _unitOfWork.SaveAsync();

            return billingRun.ID > 0 ? billingRun : null;
        }

        public async Task<List<int>> SetActiveHousesToBeBilledInBillingRunAsync(int billingRunId)
        {
            var allHouses = _houseService.GetAllHouses();
            var billingRun = _unitOfWork.BillingRunRepository.Get(w => w.ID == billingRunId).FirstOrDefault();
            var filteredHouses = _houseService.FilterActiveHouses(allHouses, billingRun.Period.Value);

            var activeHouseIds = filteredHouses.Select(s => s.ID).ToList();
            return await _housesToBeBilledService.SetHousesToBeBilledInBillingRunAsync(billingRunId, activeHouseIds);
        }

        public IQueryable<HouseModel> GetHousesThatHaventBeenBilled(int billingRunId)
        {
            return _housesToBeBilledService.GetHousesThatHaventBeenBilled(billingRunId);
        }

        public async Task<List<int>> SetHousesToBeBilledInBillingRunAsync(int billingRunId, List<int> houseIds)
        {
            return await _housesToBeBilledService.SetHousesToBeBilledInBillingRunAsync(billingRunId, houseIds);
        }

        public IQueryable<HouseModel> GetHousesToBeBilled(int billingRunId)
        {
            return _housesToBeBilledService.GetHousesToBeBilled(billingRunId);
        }

        public IQueryable<HousesToBeBilledModel> GetAllHousesToBeBilled(int billingRunId)
        {
            return _housesToBeBilledService.GetAllHousesToBeBilled(billingRunId);
        }

        public IQueryable<HouseModel> GetHousesThatHaveBeenBilled(int billingRunId)
        {
            return _housesToBeBilledService.GetHousesToBeBilled(billingRunId);
        }

        public IQueryable<HouseModel> GetHousesThatHaveBeenBilledAnAmount(int billingRunId)
        {
            return _housesToBeBilledService.GetHousesThatHaveBeenBilledAnAmount(billingRunId);
        }

        public async Task AddBillingRunTenantsToGoCardless(int billingRunId, GoCardless.Environments environment)
        {
            await _goCardlessService.AddBillingRunTenantsToGoCardlessAsync(billingRunId, environment);
        }

        public List<ManualMandateExportOutputModel> GetGoCardlessManualExport(int billingRunId)
        {
            var billingRun = _unitOfWork.BillingRunRepository.GetByID(billingRunId);

            var result = _unitOfWork.sp_goCardlessExport(billingRunId);

            var exports = result.Select(s => new ManualMandateExportOutputModel
            {
                mandate_id = s.mandate_id,
                HouseId = s.house_id,
                UserId = s.user_id,
                description = billingRun.Description,
                amount = s.amount.HasValue ? s.amount.Value : 0m,
                charge_date = s.charge_date.HasValue ? s.charge_date.Value : DateTime.UtcNow.AddBusinessDays(5)
            }).ToList();

            return exports;
        }

        public async Task<BulkOperationResult<HouseModel>> SendBillsToGoCardless(int billingRunId, GoCardless.Environments environment)
        {
            return await _sendBillsToGoCardlessService.SendBillsToGoCardlessAsync(billingRunId, environment);
        }
    }
}