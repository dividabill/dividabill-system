using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class DistributedBillingService : IDistributedBillingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHousesToBeBilledService _housesToBeBilledService;

        public DistributedBillingService(IUnitOfWork unitOfWork, IHousesToBeBilledService housesToBeBilledService)
        {
            _unitOfWork = unitOfWork;
            _housesToBeBilledService = housesToBeBilledService;
        }

        public async Task DistributedGenerateBillsAsync(int billingRunId)
        {
            var billingRun = await _unitOfWork.BillingRunRepository.GetByIDAsync(billingRunId);
            var housesToBill = _housesToBeBilledService.GetHousesThatHaventBeenBilled(billingRunId).Select(s => s.ID).ToList();
            await RunTasksInParallel(billingRunId, housesToBill, billingRun);

            //`await Task.WhenAll(tasks);
        }

        private async Task RunTasksInParallel(int billingRunId, List<int> housesToBill, BillingRun billingRun)
        {
            var maxThread = new SemaphoreSlim(10);

            for (var i = 0; i < housesToBill.Count; i++)
            {
                var houseId = housesToBill[i];

                maxThread.Wait();
                await Task.Factory.StartNew(async () =>
                {
                    //Your Works
                    await DistributedGenerateBillForHouse(billingRunId, houseId, billingRun.Period.Value);
                }
                    , TaskCreationOptions.LongRunning)
                    .ContinueWith((task) => maxThread.Release());
            }
        }

        public async Task<bool> DistributedGenerateBillForHouse(int billingRunId, int houseId, DateTime period)
        {
            var httpClient = new HttpClient { Timeout = new TimeSpan(1, 0, 0, 0) };
            var response = await httpClient.GetAsync($"http://localhost:4323/api/billing/GenerateBillForHouse/{billingRunId}/{houseId}/{period.Year}-{period.Month}-{period.Day}");
            return response.IsSuccessStatusCode;
        }
    }
}