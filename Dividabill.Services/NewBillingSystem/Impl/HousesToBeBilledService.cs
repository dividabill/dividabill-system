using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem.Model;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class HousesToBeBilledService : IHousesToBeBilledService
    {
        private readonly IHouseService _houseService;
        private readonly IMultiThreadingService _multiThreadingService;
        private readonly IUnitOfWork _unitOfWork;

        public HousesToBeBilledService(IUnitOfWork unitOfWork, IHouseService houseService, IMultiThreadingService multiThreadingService)
        {
            _unitOfWork = unitOfWork;
            _houseService = houseService;
            _multiThreadingService = multiThreadingService;
        }

        public IQueryable<HouseModel> GetHousesToBeBilled(int billingRunId)
        {
            var housesToBill = _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId).Select(s => s.House);
            //var billingRun = _unitOfWork.BillingRunRepository.FirstOrDefault(w => w.ID == billingRunId);
            //if (billingRun == null)
            //    throw new ArgumentNullException("BillingRun does not have period set");
            //housesToBill = _houseService.FilterActiveHouses(housesToBill, billingRun.Period.Value);
            return housesToBill;
        }

        public IQueryable<HousesToBeBilledModel> GetAllHousesToBeBilled(int billingRunId)
        {
            var housesToBill = _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId).Select(s => s.House);
            var billingRun = _unitOfWork.BillingRunRepository.FirstOrDefault(w => w.ID == billingRunId);
            if (billingRun == null)
                throw new ArgumentNullException("BillingRun does not have period set");
            housesToBill = _houseService.FilterActiveHouses(housesToBill, billingRun.Period.Value);

            var allHouses = _houseService.GetActiveHouses().Select(s =>
                new HousesToBeBilledModel
                {
                    ID = s.ID,
                    Address = s.Address,
                    Housemates = s.Housemates,
                    Tenants = s.Tenants,
                    IsAlreadyInQueue = housesToBill.Any(model => model.ID == s.ID)
                }
                );
            return allHouses;
        }

        public IQueryable<HouseModel> GetHousesThatHaventBeenBilled(int billingRunId)
        {
            var housesToBill =
                _unitOfWork.BillingQueueRepository.Where(
                    w => w.BillingRunId == billingRunId && !w.HouseholdBillId.HasValue).Select(s=> s.House);

            return housesToBill;
        }

        public IQueryable<HouseModel> GetHousesThatHaveBeenBilled(int billingRunId)
        {
            var billingRun = _unitOfWork.BillingRunRepository.GetByID(billingRunId);
            var housesToBeBilled = GetHousesToBeBilled(billingRunId);
            return housesToBeBilled.Where(w => w.HouseholdBills.Any(a => a.Year == billingRun.Period.Value.Year && a.Month == billingRun.Period.Value.Month));
        }

        public IQueryable<HouseModel> GetHousesThatHaveBeenBilledAnAmount(int billingRunId)
        {
            var thatHaveBeenBilledAnAmount = 
                                           from houseInBillingRun in _unitOfWork.BillingQueueRepository
                                           join house in _unitOfWork.ActiveHouses on houseInBillingRun.HouseId equals house.ID
                                           //join tenant in _unitOfWork.UserRepository on house.ID equals tenant.House_ID
                                           join houseHoldBill in _unitOfWork.HouseholdBillRepository on houseInBillingRun.HouseholdBillId equals houseHoldBill.ID
                                           join billBases in _unitOfWork.UtilityBillRepository on houseHoldBill.ID equals billBases.HouseholdBill_Id into utilityBills
                                           where 
                                           houseInBillingRun.BillingRunId == billingRunId &&
                                           houseInBillingRun.HouseholdBillId.HasValue &&
                                           utilityBills.Sum(s=> s.Ammount) > 0m
                                           select houseInBillingRun.House;

            return thatHaveBeenBilledAnAmount;
        }

        //public async Task<int?> GetNextHouseIdToBeBilled(int distributedProcessorId, int billingRunId)
        //{
        //    if (_houseToBeBilledManager.GetHousesToBeProcessed(billingRunId) == 0)
        //    {
        //        //todo: remove ones that are already being processed by distributed processors
        //        var ids = await GetHouseIdsToBeBilledAsync(null);
        //        var actualIds = await SetHousesToBeBilledInBillingRunAsync(billingRunId, ids);
        //        _houseToBeBilledManager.SetQueueOfHousesToBeBilled(billingRunId, actualIds);
        //    }

        //    if (_houseToBeBilledManager.GetHousesToBeProcessed(billingRunId) == 0)
        //        return null;

        //    var houseId =
        //        await
        //            _houseToBeBilledManager.GetNextHouseToBeBilledByDistributedProcessorAsync(billingRunId, distributedProcessorId);

        //    return houseId;
        //}

        public async Task<List<int>> SetHousesToBeBilledInBillingRunAsync(int billingRunId, List<int> houseIds)
        {
            var existingHouseIds =
                await
                    _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId)
                        .Select(s => s.HouseId)
                        .ToListAsync();

            var remainingHouseIds = houseIds.Where(p => !existingHouseIds.Any(p2 => p2 == p)).ToList();

            await
                _multiThreadingService.RunDatabaseTaskInParallelAsync(50, remainingHouseIds,
                    (houseId, unitOfWork, bulkdOperationResult) => { AddHouseToBillingQueue(billingRunId, houseId, unitOfWork); });

            return remainingHouseIds.Union(existingHouseIds).ToList();
        }

        public async Task<List<int>> DeleteHousesToBeBilledFromBillingRunAsync(int billingRunId, List<int> houseIds)
        {
            var existingHouseIds =
                await
                    _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId
                                                                  && houseIds.Contains(w.HouseId))
                        .Select(s =>
                            new
                            {
                                s.ID,
                                s.HouseId
                            }
                        )
                        .ToListAsync();

            await
                _multiThreadingService.RunDatabaseTaskInParallelAsync(50, existingHouseIds,
                    (billingQueueId, unitOfWork, bulkdOperationResult) => { DeleteHouseToBillingQueue(billingQueueId.ID, unitOfWork); });

            return existingHouseIds.Select(x => x.HouseId).ToList();
        }

        private void AddHouseToBillingQueue(int billingRunId, int houseId, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var billingQueue = new BillingQueue
            {
                BillingRunId = billingRunId,
                HouseId = houseId,
                CreatedAt = DateTime.UtcNow
            };
            unitOfWork.BillingQueueRepository.Insert(billingQueue);
            unitOfWork.Save();
        }

        private void DeleteHouseToBillingQueue(int billingQueueId, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var billingQueue = unitOfWork.BillingQueueRepository.GetByID(billingQueueId);
            if (billingQueue != null)
            {
                unitOfWork.BillingQueueRepository.Delete(billingQueue);
            }
            unitOfWork.Save();
        }
    }
}