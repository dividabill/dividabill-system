using DividaBill.DAL.Interfaces;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public interface IDatabaseMultithreadable
    {
        bool SetUnitOfWork(IUnitOfWork unitOfWork);
    }
}