using System;
using System.Collections.Generic;
using System.Threading;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class BulkOperationResult<T>
    {
        private readonly ReaderWriterLockSlim _successfulReaderWriterLockSlim = new ReaderWriterLockSlim();
        private List<T> _successful = new List<T>();
        public List<T> Successful
        {
            get
            {
                _successfulReaderWriterLockSlim.EnterReadLock();
                var value = _successful;
                _successfulReaderWriterLockSlim.ExitReadLock();
                return value;
            }
            set
            {
                _successfulReaderWriterLockSlim.EnterWriteLock();
                _successful = value;
                _successfulReaderWriterLockSlim.ExitWriteLock();
            }
        }

        private readonly ReaderWriterLockSlim _failedReaderWriterLockSlim = new ReaderWriterLockSlim();
        private Dictionary<T, Exception> _failed = new Dictionary<T, Exception>();
        public Dictionary<T, Exception> Failed
        {
            get
            {
                _failedReaderWriterLockSlim.EnterReadLock();
                var value = _failed;
                _failedReaderWriterLockSlim.ExitReadLock();
                return value;
            }
            set
            {
                _failedReaderWriterLockSlim.EnterWriteLock();
                _failed = value;
                _failedReaderWriterLockSlim.ExitWriteLock();
            }
        }
    }
}