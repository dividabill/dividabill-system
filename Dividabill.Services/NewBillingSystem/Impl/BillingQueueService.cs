using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class BillingQueueService : IBillingQueueService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBillingQueueTransactionLogService _billingQueueTransactionLogService;

        public BillingQueueService(IUnitOfWork unitOfWork, IBillingQueueTransactionLogService billingQueueTransactionLogService)
        {
            _unitOfWork = unitOfWork;
            _billingQueueTransactionLogService = billingQueueTransactionLogService;
        }

        public async Task<bool> InsertAsync(BillingQueue billingQueue)
        {
            _unitOfWork.BillingQueueRepository.Insert(billingQueue);
            await _unitOfWork.SaveAsync();

            if (billingQueue.ID == 0)
                return false;

            string changes = $"House Id: {billingQueue.HouseId} picked up by distributedProcessor {billingQueue.DistributedProcessorId}";
            var successfullyTrackedChanges = await _billingQueueTransactionLogService.TrackChangesAsync(billingQueue.ID, BillingQueueStage.AssigningHousesToBeBilled,  changes);
            return billingQueue.ID != 0 && successfullyTrackedChanges;
        }

        public async Task<bool> SetDistributedProcessorOfBillingQueueAsync(int billingRunId, int houseId, int distributedProcessorId)
        {
            var billingQueue = await _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId && w.HouseId == houseId).FirstOrDefaultAsync();

            if (billingQueue == null)
                return false;

            billingQueue.DistributedProcessorId = distributedProcessorId;
            _unitOfWork.BillingQueueRepository.Update(billingQueue);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}