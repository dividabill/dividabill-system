﻿using System;
using System.Globalization;
using DividaBill.Models.Bills;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public static class HouseholdBillExtensions
    {
        public static string GetMonth(this HouseholdBill bill)
        {
            return new DateTime(2000, bill.Month, 1).ToString("MMMM", CultureInfo.InvariantCulture);
        }
    }
}