using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class HouseToBeBilledManager : IHouseToBeBilledManager
    {
        private readonly IBillingQueueService _billingQueueService;
        private readonly ReaderWriterLockSlim _houseIdProcessorReaderWriterLockSlim = null;
        private readonly ReaderWriterLockSlim _houseQueueReaderWriterLockSlim = null;
        private readonly Dictionary<int, Dictionary<int, int>> _houseIdProcessor = null;
        private readonly Dictionary<int, Queue<int>> _billingRunHouseQueue = null;

        public HouseToBeBilledManager(IBillingQueueService billingQueueService)
        {
            _billingQueueService = billingQueueService;
            _houseIdProcessorReaderWriterLockSlim = new ReaderWriterLockSlim();
            _houseQueueReaderWriterLockSlim = new ReaderWriterLockSlim();
            _billingRunHouseQueue = new Dictionary<int, Queue<int>>();
            _houseIdProcessor = new Dictionary<int, Dictionary<int, int>>();
        }

        public int GetHousesToBeProcessed(int billingRunId)
        {
            _houseQueueReaderWriterLockSlim.EnterReadLock();
            var houseQueue = _billingRunHouseQueue[billingRunId];
            if (houseQueue == null)
            {
                _billingRunHouseQueue[billingRunId] = new Queue<int>();
                return 0;
            }
            var housesCount = houseQueue.Count; 
            _houseQueueReaderWriterLockSlim.ExitReadLock();
            return housesCount;
        }

        public bool SetQueueOfHousesToBeBilled(int billingRunId, List<int> houseIds)
        {
            if (houseIds == null || !houseIds.Any())
                return false;

            if (_billingRunHouseQueue[billingRunId] == null)
            {
                _billingRunHouseQueue[billingRunId] = new Queue<int>();
            }
            foreach (var houseId in houseIds)
            {
                _billingRunHouseQueue[billingRunId].Enqueue(houseId);
            }
            return _billingRunHouseQueue[billingRunId].ToList().All(houseIds.Contains);
        }

        public async Task<int?> GetNextHouseToBeBilledByDistributedProcessorAsync(int processorId, int billingRunId)
        {
            _houseQueueReaderWriterLockSlim.EnterWriteLock();
            var houseId = _billingRunHouseQueue[billingRunId].Peek();

            if (houseId == 0)
                return 0;

            var successfull = await AssignHouseToBillingProcessorAsync(houseId, processorId, billingRunId);
            if (!successfull)
                return 0;

            var actualHouseId = _billingRunHouseQueue[billingRunId].Dequeue();

            if (actualHouseId != houseId)
                throw new ArgumentOutOfRangeException($"houseId is {houseId} actual HouseId is {actualHouseId}");

            _houseQueueReaderWriterLockSlim.EnterWriteLock();

            return houseId;
        }

        private async Task<bool> AssignHouseToBillingProcessorAsync(int houseId, int processorId, int billingRunId)
        {
            _houseIdProcessorReaderWriterLockSlim.EnterWriteLock();

            var successful = await _billingQueueService.SetDistributedProcessorOfBillingQueueAsync(billingRunId, houseId, processorId);

            if (!successful)
                return false;

            _houseIdProcessor[billingRunId].Add(houseId, processorId);
            _houseIdProcessorReaderWriterLockSlim.ExitWriteLock();
            return _houseIdProcessor[billingRunId][houseId] == processorId;
        }
    }
}