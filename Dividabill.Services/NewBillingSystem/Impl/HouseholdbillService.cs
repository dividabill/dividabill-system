using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Bills;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class HouseholdBillService: IHouseholdBillService
    {
        private readonly IUtilityBillService _utilityBillService;
        private readonly IUnitOfWork _unitOfWork;

        //private readonly IUnitOfWork _unitOfWork;

        public HouseholdBillService(IUtilityBillService utilityBillService, IUnitOfWork unitOfWork)
        {
            _utilityBillService = utilityBillService;
            _unitOfWork = unitOfWork;
        }

        public List<HouseholdBill> GetBillsForHouse(HouseModel h)
        {
            var billsForHouse = h.HouseholdBills
                //.Where(x => x.Month == dt.Month && x.Year == dt.Year)
                .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
                .ToList();
            return billsForHouse;
        }

        public List<HouseholdBill> GetBillsForHouse(HouseModel h, DateTime date)
        {
            var billsForHouse = h.HouseholdBills
                .Where(x => x.Month == date.Month && x.Year == date.Year)
                .Where(p => p.UtilityBills.All(b => b.Payments.All(k => k.IsPaid == false)))
                .ToList();
            return billsForHouse;
        }


        public decimal GetTenantTotalAmount(User u, HouseholdBill bill)
        {
            return bill.Payments
                .Where(p => p.User_Id == u.Id)
                .Sum(p => p.Amount);
        }

        /// <summary>
        /// Outstanding amount
        /// </summary>
        /// <param name="u"></param>
        /// <param name="bill"></param>
        /// <returns></returns>
        public decimal GetTenantOutstandingAmount(User u, HouseholdBill bill)
        {
            return bill.Payments
                .Where(p => p.User_Id == u.Id && !p.IsPaid)
                .Sum(p => p.Amount);
        }

        /// <summary>
        /// Outstanding amount that hasn't been sent to a payment processor.
        /// </summary>
        /// <param name="u"></param>
        /// <param name="bill"></param>
        /// <returns></returns>
        public decimal GetTenantOutstandingAmountNotYetSentToBeDebited(User u, HouseholdBill bill)
        {
            return bill.Payments
                .Where(p => p.User_Id == u.Id && !p.IsPaid && !p.PaymentRequestedDate.HasValue)
                .Sum(p => p.Amount);
        }

        /// <summary>
        /// Outstanding amount that has been sent to payment processor but has failed to be collected
        /// </summary>
        /// <param name="u"></param>
        /// <param name="bill"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public decimal GetTenantOutstandingAmountThatHasFailedToBePaid(User u, HouseholdBill bill, DateTime now)
        {
            return bill.Payments
                .Where(p => p.User_Id == u.Id && !p.IsPaid && p.PaymentRequestedDate.HasValue && p.ChargeDate.HasValue && p.ChargeDate <= now)
                .Sum(p => p.Amount);
        }

       
        public bool IsPaidByTenant(User u, HouseholdBill bill)
        {
            return bill.Payments.All(p => p.IsPaid || u.Id != p.User_Id);
        }

        public IQueryable<HouseholdBill> GetHouseholdBillsForBillingRun(int billingRunId)
        {
            var billingQueues = _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId);

            var billsForHouse = billingQueues.Select(s => s.HouseholdBill);
            return billsForHouse;
        }

        public IQueryable<HouseholdBill> GetUnpaidHouseholdBills(int billingRunId)
        {
            var billingQueues = _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId);

            var billsForHouse = billingQueues.Where(w => w.HouseholdBill.UtilityBills.All(
                b => b.Payments.Any(k => k.IsPaid == false))).Select(s => s.HouseholdBill);
            return billsForHouse;
        }

        //public List<ManualMandateExport> GetGoCardlesMandateExports(int billingRunId)
        //{
        //    var billingQueues = _unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId);

        //    var mandates = billingQueues.Where(w => w.HouseholdBill.UtilityBills.All(
        //        b => b.Payments.Any(k => k.IsPaid == false))).Select(s => new
        //        {
        //            Bill = s.HouseholdBill,
        //            Payments = s.HouseholdBill.Payments
        //        });
        //    //mandates.Select(s=> new Man
        //    //{
                
        //    //})
        //    return null;
        //}
    }
}