﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Emails;
using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Models;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class SendBillEmailsService : ISendBillEmailsService
    {
        private readonly IBillingQueueTransactionLogService _billingQueueTransactionLogService;
        
        private readonly IEmailSentMessageService _emailSentMessageService;
        private readonly IEmailService _emailService;

        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IEncryptionService _encryptionService;
        private readonly IHouseholdBillService _householdBillService;
        private readonly IHousesToBeBilledService _housesToBeBilledService;
        private readonly IMultiThreadingService _multiThreadingService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUtilityBillService _utilityBillService;

        public SendBillEmailsService(IUnitOfWork unitOfWork,
            IHouseholdBillService householdBillService, IUtilityBillService utilityBillService, IBillingQueueTransactionLogService billingQueueTransactionLogService,
            IHousesToBeBilledService housesToBeBilledService,  IMultiThreadingService multiThreadingService,
            IEmailSentMessageService emailSentMessageService, IEmailService emailService, ITenantEncryptionService tenantEncryptionService, IEncryptionService encryptionService)
        {
            
            _tenantEncryptionService = tenantEncryptionService;
            _encryptionService = encryptionService;
            _emailSentMessageService = emailSentMessageService;
            _emailService = emailService;
            _unitOfWork = unitOfWork;
            _householdBillService = householdBillService;
            _utilityBillService = utilityBillService;
            _billingQueueTransactionLogService = billingQueueTransactionLogService;
            _housesToBeBilledService = housesToBeBilledService;
            _multiThreadingService = multiThreadingService;
        }

        public async Task SendBillEmailsAsync(int billingRunId, string testEmailAddress = null)
        {
            var goCardless = _unitOfWork.sp_goCardlessExport(billingRunId);

            var houseIds = goCardless.Select(s => s.house_id).Distinct().ToList();

            var houses = _unitOfWork.ActiveHouses.Where(w => houseIds.Contains(w.ID));

            await ProcessHousesToBeSentBillEmails(billingRunId, houses, testEmailAddress);
        }

        public async Task SendTestEmailAsync(int billingRunId, List<int> houseIds, string testEmailAddress = null)
        {
            var houses = _housesToBeBilledService.GetHousesThatHaveBeenBilledAnAmount(billingRunId).Where(w => houseIds.Contains(w.ID));

            await ProcessHousesToBeSentBillEmails(billingRunId, houses, testEmailAddress);
        }

        public async Task ProcessHousesToBeSentBillEmails(int billingRunId, IQueryable<HouseModel> houses, string testEmailAddress = null)
        {
            var houseList = houses.Select(s => s.ID).ToList();
            //await _multiThreadingService.RunDatabaseTaskInParallelAsync(5, houseList,
            //    async (houseId, unitOfWork, bulkOperationResult) =>
            //    {

            //    });
            foreach (var houseId in houseList)
            {
                try
                {
                    var house = _unitOfWork.ActiveHouses.Where(w => w.ID == houseId).Include("HouseholdBills").FirstOrDefault();
                    await Process(house, billingRunId, testEmailAddress, _unitOfWork);
                }
                catch (Exception ex)
                {
                    _billingQueueTransactionLogService.TrackException(billingRunId,houseId, ex, BillingQueueStage.SendBillEmails, _unitOfWork);
                    //bulkOperationResult.Failed.Add(houseId, ex);
                }
            }
        }

        private IUnitOfWork AssignThreadSafeUnitOfWork(IUnitOfWork unitOfWork)
        {
            return unitOfWork ?? _unitOfWork;
        }

        public async Task Process(HouseModel h, int billingRunId, string testEmailAddress, IUnitOfWork unitOfWork = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            //if (h.ID == 1890)
            //    return;

            var tenants = "";
            var sb = new StringBuilder();
            foreach (var t in h.Tenants)
                sb.Append($"{t.RefTitle} {_encryptionService.Decrypt(t.FirstName)} {_encryptionService.Decrypt(t.LastName)}, ");
            tenants = sb.ToString().TrimEnd(' ').TrimEnd(',').TrimEnd(' ');

            var billingRun = _unitOfWork.BillingRunRepository.Where(w => w.ID == billingRunId).FirstOrDefault();

            if (!billingRun.Period.HasValue)
                return;

            if (billingRun.EmailTemplate == null)
                throw new Exception($"BillingRun.EmailTemplate is null. BillingRun.ID=[{billingRun.ID}]");
            if (billingRun.EmailSubject == null)
                throw new Exception($"BillingRun.EmailSubject is null. BillingRun.ID=[{billingRun.ID}]");
            if (billingRun.EmailSendFrom == null)
                throw new Exception($"BillingRun.EmailSendFrom is null. BillingRun.ID=[{billingRun.ID}]");

            var billsForHouse = _householdBillService.GetBillsForHouse(h, billingRun.Period.Value);

            foreach (var bill in billsForHouse)
            {
                foreach (var tenant in h.Tenants)
                {
                    var decryptUser = _tenantEncryptionService.DecryptUser(tenant);
                    await SendBillEmailToTenant(h, billingRun, testEmailAddress, decryptUser, bill, tenants, unitOfWork);
                }
            }
        }

        private async Task SendBillEmailToTenant(HouseModel h, BillingRun billingRun, string testEmailAddress, User tenant, HouseholdBill bill, string tenants, IUnitOfWork unitOfWork = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            var asyncExecutionParam = new AsyncExecutionParam(unitOfWork);

            var alreadySent = _emailSentMessageService.GetActive(asyncExecutionParam).Where(x => x.UserId == tenant.Id
                                                                                                 && x.EmailTypeId == EmailType.BillRunMessages.ID
                                                                                                 && x.CreatedAt.HasValue
                                                                                                 && x.CreatedAt.Value.Month == billingRun.Period.Value.Month
                                                                                                 && x.CreatedAt.Value.Year == billingRun.Period.Value.Year);

            if (alreadySent.Any())
                return;

            var amountOutstanding = _householdBillService.GetTenantOutstandingAmount(tenant, bill);
            if (amountOutstanding <= 0)
                return;

            var tenantServiceAmounts = _utilityBillService.GetTenantUtilityBillAmountsForAllServiceTypes(tenant, bill);

            try
            {
                var billMessageSubtitution = new BillMessageSubtitution
                {
                    House = h,
                    Tenants = tenants,
                    Tenant = tenant,
                    TenantServiceAmounts = tenantServiceAmounts,
                    Bill = bill
                };

                var emailSubject = billingRun.EmailSubject;
                emailSubject = emailSubject.Replace("{Month}", bill.GetMonth());
                emailSubject = emailSubject.Replace("{Year}", bill.Year.ToString());
                var emailConfiguration = new EmailConfiguration
                {
                    TemplateId = billingRun.EmailTemplate.TemplateId,
                    Subject = emailSubject,
                    SendFromEmailDisplayName = billingRun.EmailSendFromDisplayName,
                    SendFromEmail = billingRun.EmailSendFrom
                };

                await _emailService.SendBillMessage(emailConfiguration, billMessageSubtitution, testEmailAddress, asyncExecutionParam);
            }
            catch (Exception ex)
            {
                _billingQueueTransactionLogService.TrackException(billingRun.ID, h.ID, ex, BillingQueueStage.SendBillEmails, unitOfWork);
            }
        }
    }
}