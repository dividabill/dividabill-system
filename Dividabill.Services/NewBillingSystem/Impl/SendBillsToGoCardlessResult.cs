using System.Collections.Generic;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class SendBillsToGoCardlessResult
    {
        public Dictionary<string, string> MandProblemOnes = new Dictionary<string, string>();
        public Dictionary<string, string> ProblemOnes = new Dictionary<string, string>();
        public Dictionary<string, string> ProblemTenants = new Dictionary<string, string>();
    }
}