using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Services.NewBillingSystem.Impl;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IMultiThreadingService
    {
        int GetAvailableThreads(bool isIO = false);
        Task<BulkOperationResult<T>> RunDatabaseTaskInParallelAsync<T>(int maxParallelTasks, IList<T> collection, Action<T, IUnitOfWork, BulkOperationResult<T>> action);
        Task<BulkOperationResult<T>> RunTaskInParallelAsync<T, T2>(int maxParallelTasks, IList<T> collection, Action<T, IUnitOfWork, T2, BulkOperationResult<T>> action);
    }
}