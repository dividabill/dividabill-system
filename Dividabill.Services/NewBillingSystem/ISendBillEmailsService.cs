﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem
{
    public interface ISendBillEmailsService
    {
        Task SendBillEmailsAsync(int billingRunId, string testEmailAddress = null);


        Task SendTestEmailAsync(int billingRunId, List<int> houseIds, string testEmailAddress = null);

        Task ProcessHousesToBeSentBillEmails(int billingRunId, IQueryable<HouseModel> houses, string testEmailAddress = null);
    }
}