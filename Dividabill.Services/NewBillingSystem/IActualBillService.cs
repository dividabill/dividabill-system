using System.Threading.Tasks;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IActualBillService
    {
        Task<bool> ProcessActualBillAsync(int billingQueueId);
    }
}