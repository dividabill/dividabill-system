using System;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IBillingQueueTransactionLogService
    {
        bool TrackChanges(int billingQueueId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null);
        bool TrackException(int billingQueueId, Exception ex, BillingQueueStage stage, IUnitOfWork unitOfWork = null);
        bool TrackException(int billingRunId, int houseId, Exception ex, BillingQueueStage stage, IUnitOfWork unitOfWork = null);
        Task<bool> TrackChangesAsync(int billingQueueId, BillingQueueStage stage, string changes, IUnitOfWork unitOfWork = null);
        Task<bool> TrackChangesAsync(int billingQueueId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null);
        Task<bool> TrackChangesAsync(int billingRunId, int houseId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null);
    }
}