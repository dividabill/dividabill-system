using System.Collections.Generic;
using System.Threading.Tasks;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IHouseToBeBilledManager
    {
        bool SetQueueOfHousesToBeBilled(int billingRunId, List<int> houseIds);
        Task<int?> GetNextHouseToBeBilledByDistributedProcessorAsync(int processorId, int billingRunId);
        int GetHousesToBeProcessed(int billingRunId);
    }
}