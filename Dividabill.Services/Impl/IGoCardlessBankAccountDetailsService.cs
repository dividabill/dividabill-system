using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.ViewModels;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public interface IGoCardlessBankAccountDetailsService
    {
        Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails, IGoCardlessApiClient goCardlessApi = null);
    }
}