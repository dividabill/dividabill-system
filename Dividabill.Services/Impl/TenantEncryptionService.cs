using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Impl
{
    public class TenantEncryptionService : ITenantEncryptionService
    {
        private readonly IAddressEncryptionService _addressEncryptionService;
        private readonly IEncryptionService _encryptionService;

        public TenantEncryptionService(IEncryptionService encryptionService, IAddressEncryptionService addressEncryptionService)
        {
            _encryptionService = encryptionService;
            _addressEncryptionService = addressEncryptionService;
        }

        public void EncryptUser(User user)
        {
            user.FirstName = _encryptionService.Encrypt(user.FirstName);
            user.LastName = _encryptionService.Encrypt(user.LastName);
            user.PhoneNumber = _encryptionService.Encrypt(user.PhoneNumber);
            user.AccountNumber = _encryptionService.Encrypt(user.AccountNumber);
            user.AccountSortCode = _encryptionService.Encrypt(user.AccountSortCode);
            user.AccountHolder = _encryptionService.Encrypt(user.AccountHolder);

            _addressEncryptionService.EncryptAddress(user.HomeAddress);
        }

        public User DecryptUser(User user)
        {
            user.FirstName = _encryptionService.Decrypt(user.FirstName);
            user.LastName = _encryptionService.Decrypt(user.LastName);
            user.PhoneNumber = _encryptionService.Decrypt(user.PhoneNumber);
            user.AccountNumber = _encryptionService.Decrypt(user.AccountNumber);
            user.AccountSortCode = _encryptionService.Decrypt(user.AccountSortCode);
            user.AccountHolder = _encryptionService.Decrypt(user.AccountHolder);

            _addressEncryptionService.DecryptAddress(user.HomeAddress);

            return user;
        }

        public User DecryptUserName(User user)
        {
            user.FirstName = _encryptionService.Decrypt(user.FirstName);
            user.LastName = _encryptionService.Decrypt(user.LastName);

            return user;
        }

        public IEnumerable<User> DecryptUsersList(IEnumerable<User> usersList)
        {
            return usersList.Select(DecryptUser);
        }
    }
}