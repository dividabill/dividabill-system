﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using CsvHelper;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.ViewModel;
using DividaBill.ViewModels;
using ExcelLibrary.SpreadSheet;

namespace DividaBill.Services.Impl
{
    public class HouseService : IHouseService
    {
        private static readonly Dictionary<ServiceType, Func<HouseModel, bool>> _houseServiceGetter = new Dictionary<ServiceType, Func<HouseModel, bool>>
        {
            {ServiceType.Broadband, m => m.Broadband},
            {ServiceType.Electricity, m => m.Electricity},
            {ServiceType.Gas, m => m.Gas},
            {ServiceType.LandlinePhone, m => m.LandlinePhone},
            {ServiceType.LineRental, m => m.LandlinePhone || m.Broadband},
            {ServiceType.NetflixTV, m => m.NetflixTV},
            {ServiceType.SkyTV, m => m.SkyTV},
            {ServiceType.TVLicense, m => m.TVLicense},
            {ServiceType.Water, m => m.Water}
        };

        private static readonly Dictionary<ServiceType, Func<HouseModel, int>> _contractPackageGetter = new Dictionary<ServiceType, Func<HouseModel, int>>
        {
            {ServiceType.Broadband, m => (int) m.BroadbandType},
            {ServiceType.Electricity, m => 0},
            {ServiceType.Gas, m => 0},
            {ServiceType.LandlinePhone, m => (int) m.LandlineType},
            {ServiceType.LineRental, m => 0},
            {ServiceType.NetflixTV, m => 0},
            {ServiceType.SkyTV, m => 0},
            {ServiceType.TVLicense, m => 0},
            {ServiceType.Water, m => 0}
        };

        private readonly IContractService _contractService;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViewModelToEntityMappingFactory<HouseDetailsViewModel, HouseModel> _houseDetailsViewModelToHouseModelMappingFactory;
        private readonly ITenancyService _tenancyService;

        public HouseService(IUnitOfWork unitOfWork, IContractService contractService, ITenantEncryptionService tenantEncryptionService, IViewModelToEntityMappingFactory<HouseDetailsViewModel, HouseModel> houseDetailsViewModelToHouseModelMappingFactory, ITenancyService tenancyService)
        {
            _tenancyService = tenancyService;
            _houseDetailsViewModelToHouseModelMappingFactory = houseDetailsViewModelToHouseModelMappingFactory;
            _tenantEncryptionService = tenantEncryptionService;
            _unitOfWork = unitOfWork;
            _contractService = contractService;
        }

        public HouseDetailsViewModel GetHouseDetails(int UDPRN)
        {
            try
            {
                var existingHouse = _unitOfWork.ActiveHouses.Get(h => h.Address.UDPRN == UDPRN.ToString(), null, "Coupon").SingleOrDefault();
                return HouseDetailsViewModel.Create(existingHouse);
            }
            catch
            {
                return null;
            }
        }

        public HouseDetailsViewModel GetHouseDetails(HouseSearchViewModel houseSearchParameters)
        {
            try
            {
                var existingHouse = _unitOfWork.ActiveHouses
                    .Get(h =>
                        h.Address.City == houseSearchParameters.CityTT &&
                        h.Address.Postcode == houseSearchParameters.PostCode &&
                        h.Address.Line1 == houseSearchParameters.AddressTT1 &&
                        h.Address.Line2 == houseSearchParameters.AddressTT2 &&
                        h.Address.Line3 == houseSearchParameters.AddressTT3 &&
                        h.Address.Line4 == houseSearchParameters.AddressTT4 &&
                        h.Address.County == houseSearchParameters.CountyTT, null, "Coupon")
                    .SingleOrDefault();

                return HouseDetailsViewModel.Create(existingHouse);
            }
            catch
            {
                return null;
            }
        }

        public HouseModel AddHouseIfNotExists(HouseDetailsViewModel wizard)
        {
            var existingHouse = _unitOfWork.ActiveHouses
                .Get(h => h.Address.UDPRN == wizard.Address.UDPRN, null, "Tenants")
                .SingleOrDefault();

            var isNewHouse = existingHouse == null;

            if (isNewHouse)
            {
                existingHouse = _houseDetailsViewModelToHouseModelMappingFactory.Map(wizard);

                _unitOfWork.ActiveHouses.Insert(existingHouse);
                _unitOfWork.Save();
            }
            return existingHouse;
        }

        public void AddTenantToHouse(HouseModel house, User tenant)
        {
            //Check if house is in active database
            if (_unitOfWork.ActiveHouses.GetByID(house.ID) == null)
            {
                throw new HouseNotFoundException(house.ID.ToString());
            }

            //Check if user exists
            if (_unitOfWork.UserRepository.GetByID(tenant.Id) == null)
            {
                throw new UserNotFound(tenant.Id);
            }
            _unitOfWork.UserRepository.AddToHouse(tenant, house);
            //tenant.House_ID = house.ID;

            _unitOfWork.Save();
        }

        public void MarkHouseActive(HouseModel house)
        {
            if (house.Housemates < house.Tenants.Count())
            {
                //house.SetUpDate = DateTime.Now;
                _unitOfWork.ActiveHouses.Update(house);
                _unitOfWork.Save();
                if (house.HasElectricity || house.HasGas)
                {
                    //this.SendBritishGasRegistrationForm(house.ID);
                }
                var billStartDate = house.StartDate;
                var daysInBetween = (billStartDate - DateTime.Now.AddDays(16)).Days;
                if (daysInBetween < 0)
                {
                    billStartDate = DateTime.Now.AddDays(16);
                }
                else if (daysInBetween < 15)
                {
                    billStartDate = house.StartDate.AddDays(daysInBetween);
                }
                else
                {
                    billStartDate = house.StartDate;
                }
                var _billService = new BillService(_unitOfWork);

                var firstOfNextNextMonth = new DateTime(billStartDate.Year, billStartDate.Month, 1).AddMonths(2);
                var lastOfNextMonth = firstOfNextNextMonth.AddDays(-1);

                //_billService.GenerateFirstMonthBill(billStartDate, lastOfNextMonth, house.ID, billStartDate);
            }

        }

        public void MergeHouses(int[] houses)
        {
            var houseObjects = _unitOfWork.ActiveHouses.GetHouses(q => houses.Contains(q.ID)).OrderBy(h => h.RegisteredDate).ToList();

            var originalHouse = houseObjects.First();
            if (!houseObjects.Remove(originalHouse))
            {
                throw new Exception("Cannot remove original house from housesList");
            }

            foreach (var house in houseObjects)
            {
                while (house.Tenants.Count() > 0)
                {
                    //Save client because we need a reference for adding later
                    var client = house.Tenants[0];
                    _unitOfWork.ActiveHouses.RemoveTenant(client, house.ID);
                    _unitOfWork.ActiveHouses.AddTenant(client, originalHouse);
                }
                _unitOfWork.ActiveHouses.Delete(house);
            }

            //Get the last signuped user by SignUpDate;
            var lastUser = _unitOfWork.UserRepository.Get().Where(p => p.House_ID == originalHouse.ID).OrderByDescending(s => s.SignupDate).First();

            //Set the SetUpDate date of the original house to the SignupDate of the lastUser
            //originalHouse.SetUpDate = lastUser.SignupDate;
            _unitOfWork.ActiveHouses.Update(originalHouse);
            _unitOfWork.Save();
        }

        public Workbook GenerateBritishGasRegistrationForm(int houseId)
        {
            var house = _unitOfWork.ActiveHouses.GetByID(houseId);
            var client = _unitOfWork.UserRepository.Get(u => u.House_ID == houseId).FirstOrDefault();
            _tenantEncryptionService.DecryptUser(client);
            var tenants = "";

            tenants = client.FullName;
            tenants = tenants.Substring(0, tenants.Length - 2);

            var workbook = new Workbook();
            var worksheet = new Worksheet("First Sheet");

            worksheet.Cells[0, 0] = new Cell("Gas");
            worksheet.Cells[0, 1] = new Cell("Electricity");
            worksheet.Cells[1, 0] = new Cell("FINAL READING");
            worksheet.Cells[1, 1] = new Cell("FINAL READING");
            worksheet.Cells[2, 0] = new Cell("N/A");
            worksheet.Cells[2, 1] = new Cell("N/A");

            //Address
            worksheet.Cells[4, 0] = new Cell("Address: ");
            worksheet.Cells[4, 1] = new Cell(house.Address.ToString());

            //Services
            worksheet.Cells[6, 0] = new Cell("Services Required");
            worksheet.Cells[7, 0] = new Cell("Gas");
            worksheet.Cells[7, 1] = new Cell("Electricity");
            var elec = house.HasElectricity ? "Yes" : "No";
            var gas = house.HasGas ? "Yes" : "No";
            worksheet.Cells[8, 0] = new Cell(elec);
            worksheet.Cells[8, 1] = new Cell(gas);

            //Tenants
            worksheet.Cells[10, 0] = new Cell("OUT GOING TENANT");
            worksheet.Cells[10, 1] = new Cell("N/A");


            worksheet.Cells[11, 0] = new Cell("FORWARDING ADDRESS");
            worksheet.Cells[11, 1] = new Cell("N/A");


            worksheet.Cells[12, 0] = new Cell("INCOMING TENANT");
            worksheet.Cells[12, 1] = new Cell(tenants);


            worksheet.Cells[13, 0] = new Cell("FUTURE BILLS TO BE SENT TO");
            worksheet.Cells[13, 1] = new Cell("energy@dividabill.co.uk");


            for (var i = 14; i <= 100; i++)
                worksheet.Cells[i, 0] = new Cell("");

            workbook.Worksheets.Add(worksheet);

            return workbook;
        }

        public List<User> GetTenantsForHouse(int id)
        {
            var users = _unitOfWork.UserRepository.Get(t => t.House_ID == id).ToList();
            if (users == null)
            {
                throw new HouseNotFoundException(id.ToString());
            }
            return users;
        }

        public void AddTenantsToGoCardless()
        {
            throw new NotImplementedException();
        }


        public string GenerateBTRegistrationForm(int houseId)
        {
            var sw = new StringWriter();
            var csv = new CsvWriter(sw);
            csv.WriteHeader<BTHouseRegistration>();

            csv = AddHouseToBTRegistrationForm(houseId, csv);
            return sw.ToString();
        }

        public string GenerateBTRegistrationFormForHouses(List<HouseModel> houses)
        {
            var sw = new StringWriter();
            var csv = new CsvWriter(sw);
            csv.WriteHeader<BTHouseRegistration>();
            foreach (var house in houses)
            {
                csv = AddHouseToBTRegistrationForm(house, csv);
            }
            return sw.ToString();
        }

        /// <summary>
        ///     Generates a GBEnergy registration form for all ActiveHouses starting in the given month.
        ///     Requires the date the form is to be sent as it is needed in the template.
        /// </summary>
        public string GenerateGBEnergyRegistrationsNew(IEnumerable<HouseModel> houses)
        {
            var regService = new GbEnergyRegistrationNewService();
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    csv.WriteHeader<GBEnergyRegistrationNew>();

                    foreach (var h in houses)
                    {
                        //get elec, gas contracts
                        var elecContract = h.GetActiveContract<ElectricityContract>();
                        var gasContract = h.GetActiveContract<GasContract>();

                        //unsubmitted only
                        if (elecContract.SubmissionStatus == ProviderSubmissionStatus.New)
                        {
                            //do the reg
                            csv.WriteRecord(regService.Create(elecContract, gasContract, elecContract.RequestedStartDate.Date));

                            //update the contracts
                            if (elecContract != null)
                            {
                                elecContract.MarkAsSubmitted();
                                _unitOfWork.ContractRepository.Update(elecContract);
                            }

                            if (gasContract != null)
                            {
                                gasContract.MarkAsSubmitted();
                                _unitOfWork.ContractRepository.Update(gasContract);
                            }
                        }
                    }
                }
                _unitOfWork.Save();
                return sw.ToString();
            }
        }


        public IEnumerable<Contract> GetContractsForHouse(int p)
        {
            var house = _unitOfWork.ActiveHouses.GetByID(p);

            if (house == null)
                throw new HouseNotFoundException();

            return house.Contracts;
        }

        public HouseModel GetHouse(int id)
        {
            var house = _unitOfWork.ActiveHouses.GetByID(id);
            if (house == null)
            {
                throw new HouseNotFoundException();
            }
            return house;
        }

        public HouseModel GetHouseIncluding(int id, string includeProperties)
        {
            try
            {
                var query = _unitOfWork.ActiveHouses.Get().Where(h => h.ID == id);
                foreach (var includeProperty in includeProperties.Split
                    (new[] {','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                var house = query.Single();
                var tenancies = _tenancyService.GetTenanciesbyHouseId(id);
                house.Tenancies = tenancies;

                return house;
            }
            catch
            {
                throw new HouseNotFoundException();
            }
        }



        public void UpdateHouse(HouseModel house)
        {
            _unitOfWork.ActiveHouses.Update(house);
            _unitOfWork.Save();
        }

        public void ArchiveHouse(int house_ID)
        {
            var house = GetHouse(house_ID);
            ArchiveHouse(house);
        }

        public void ArchiveHouse(HouseModel house)
        {
            throw new NotImplementedException();
            //house.Archived = true;
            //this.UpdateHouse(house);
        }

        public void UnarchiveHouse(int house_ID)
        {
            var house = GetHouse(house_ID);
            UnarchiveHouse(house);
        }

        public void UnarchiveHouse(HouseModel house)
        {
            throw new NotImplementedException();
            //house.Archived = false;
            //this.UpdateHouse(house);
        }

        public void RemoveTenantFromHouse(string userId, int house_ID)
        {
            var house = GetHouse(house_ID);
            var tenant = GetTenantsForHouse(house_ID).Where(u => u.Id == userId).SingleOrDefault();

            if (tenant == null)
            {
                throw new UserNotConnectedToHouse {user_ID = userId, house_ID = house_ID};
            }

            tenant.House_ID = null;

            _unitOfWork.UserRepository.Update(tenant);
            _unitOfWork.Save();
        }

        public List<HouseholdBill> GetHouseholdBills(HouseModel house)
        {
            return _unitOfWork.HouseholdBillRepository.Get().Where(b => b.House_ID == house.ID).ToList();
        }

        public IQueryable<HouseModel> GetTodayHouses()
        {
            return _unitOfWork.ActiveHouses.Get().Where(h => h.RegisteredDate.Day == DateTime.Today.Day && h.RegisteredDate.Month == DateTime.Today.Month && h.RegisteredDate.Year == DateTime.Today.Year);
        }

        public IQueryable<HouseModel> GetLastWeekHouses()
        {
            var mondayOfLastWeek = DateTime.Now.AddDays(-(int) DateTime.Now.DayOfWeek - 6).Date;
            var sundayOfLastWeek = mondayOfLastWeek.AddDays(7).AddTicks(-1);

            return _unitOfWork.ActiveHouses.Get().Where(h => h.RegisteredDate >= mondayOfLastWeek && h.RegisteredDate <= sundayOfLastWeek);
        }

        public IQueryable<HouseModel> GetYesterdayHouses()
        {
            var yesterday = DateTime.Now.AddDays(-1);

            return _unitOfWork.ActiveHouses.Get().Where(h => h.RegisteredDate.Day == yesterday.Day && h.RegisteredDate.Month == yesterday.Month && h.RegisteredDate.Year == yesterday.Year);
        }

        public IQueryable<HouseModel> GetActiveHouses(IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;
            var now = DateTime.UtcNow;
            return FilterActiveHouses(unitOfWork.ActiveHouses.Get(), now);
        }

        public IQueryable<HouseModel> FilterActiveHouses(IQueryable<HouseModel> houses, DateTime when)
        {
            //check if house has a confirmed start date
            var blackListedHouses = new HouseBlacklistService().GetBlackListedHouses();
            return houses.Where(w => !blackListedHouses.Contains(w.ID)).Where(w=> w.Contracts.Any(a => a.StartDate.HasValue && a.StartDate <= when && when <= a.EndDate));
        }

        public IQueryable<HouseModel> GetAllHouses()
        {
            return _unitOfWork.ActiveHouses.Get();
        }

        public IEnumerable<Coupon> GetAllCoupons()
        {
            throw new NotImplementedException();
        }

        private ContractRequest requestFromHouse(HouseModel h, ServiceType ty)
        {
            //was this service checked?
            if (!_houseServiceGetter[ty](h) || !h.Tenants.Any())
                return null;

            var user = h.Tenants.First();
            var provider = _contractService.GetDefaultProvider(user.House, ty);
            var conLength = h.EndDate.GetMonthsPassed() - h.StartDate.GetMonthsPassed();

            if (conLength < 9 && h.ID > 150)
                Console.WriteLine("House {0} has a duration of {1}!", h.ID, conLength);

            return new ContractRequest
            {
                Duration = (ContractLength) conLength,
                Provider = provider,
                TriggeringTenant = user,
                StartDate = h.StartDate,
                Service = ty,
                PackageRaw = _contractPackageGetter[ty](h)
            };
        }

        private CsvWriter AddHouseToBTRegistrationForm(int houseId, CsvWriter csv)
        {
            var house = _unitOfWork.ActiveHouses.GetByID(houseId);
            return AddHouseToBTRegistrationForm(house, csv);
        }

        private CsvWriter AddHouseToBTRegistrationForm(HouseModel house, CsvWriter csv)
        {
            //Check if house has signup for internet
            if (!house.HasBroadband)
            {
                var ex = new MissingServiceException();
                ex.service = "Broadband";
                ex.houseId = house.ID;
                throw ex;
            }

            var houseUsers = _unitOfWork.UserRepository.Get(u => u.House_ID == house.ID).OrderByDescending(k => k.SignupDate);
            var mainUser = houseUsers.First();
            var secondaryUser = houseUsers.Skip(1).FirstOrDefault();

            var termTime = (house.EndDate.Year - house.StartDate.Year)*12 + (house.EndDate.Month - house.StartDate.Month)
                           + (house.EndDate.Day - house.StartDate.Day == 0 ? 0 : 1);
            var record = new BTHouseRegistration
            {
                Answer = "Paris",
                BBJobNo = "",
                BBPackage = house.GetActiveContract<BroadbandContract>().Speed == BroadbandType.ADSL20 ? "BROADBAND UNLIMITED" : "INFINITY UNLIMITED",
                BBTerm = "12",
                ChallengeQuestion = "Name of First Pet",
                ExistingLineTelNo = "",
                FullAddress = house.Address.ToString(),
                IP = "Dynamic",
                IPTerm = "12",
                JobNo = "",
                LineRequired = "",
                OneBillRef = "",
                OrderContact = "Jon Akass",
                OrderEmail = "bt@dividabill.co.uk",
                OrderRef = "",
                OrderTelNo = "02380223596",
                Password = "DB" + house.ID + "MMz",
                PostCode = house.Address.Postcode,
                PropertyRefNo = house.ID.ToString(),
                Router = house.GetActiveContract<BroadbandContract>().Speed == BroadbandType.ADSL20 ? "3" : "5",
                SiteContact = mainUser.FullName,
                SiteMobile = mainUser.PhoneNumber,
                SiteTelNo = mainUser.PhoneNumber,
                Username = "house" + house.ID + "@bt.dividabill.co.uk",
                StartDate = house.StartDate.ToShortDateString(),
                BTConfirmedStartDate = ""
            };

            //add secondary contact information, if available
            if (secondaryUser != null)
            {
                record.SiteContact2 = secondaryUser.FullName;
                record.SiteMobile = secondaryUser.PhoneNumber;
                record.SiteTelNo = secondaryUser.PhoneNumber;
            }

            csv.WriteRecord(record);
            return csv;
        }


        /// <summary>
        ///     Generates a csv with all active tv licenses that are to expire before the specified date.
        /// </summary>
        public string GenerateTvLicensingOnboardingForm(IEnumerable<Contract> contracts)
        {
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    //header
                    csv.WriteHeader<TvLicensingOnboarding>();

                    foreach (var c in contracts)
                    {
                        var tv = TvLicensingOnboarding.Create(c);
                        if (tv != null)
                            csv.WriteRecord(tv);
                    }

                    return sw.ToString();
                }
            }
        }

        /// <summary>
        ///     Generates a csv form to be send to the sky installation team.
        ///     TODO: To be sent out on the same day as the order is sent.
        /// </summary>
        public string GenerateSkyInstallationOnboardingForm(IEnumerable<Contract> contracts)
        {
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    csv.WriteHeader<SkyInstallationOnboarding>();

                    foreach (var c in contracts)
                    {
                        var record = SkyInstallationOnboarding.Create(c);

                        if (record != null)
                            csv.WriteRecord(record);
                    }
                }
                return sw.ToString();
            }
        }

        public string GenerateWaterTemplate(IEnumerable<Contract> contracts)
        {
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    csv.WriteHeader<WaterServicesCotForm>();

                    foreach (var c in contracts)
                    {
                        var form = WaterServicesCotForm.Create(c);
                        if (form != null)
                            csv.WriteRecord(form);
                    }
                }
                return sw.ToString();
            }
        }

        public string GenerateOriginTemplate(IEnumerable<HouseModel> houses, DateTime from, DateTime to)
        {
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    csv.WriteHeader<OriginRegistrationForm>();

                    foreach (var h in houses)
                    {
                        var bbc = (BroadbandContract) h.GetContractStartingBetween(ServiceType.Broadband, from, to);
                        var lpc = (LandlinePhoneContract) h.GetContractStartingBetween(ServiceType.LandlinePhone, from, to);

                        if (bbc != null || lpc != null)
                        {
                            var form = OriginRegistrationForm.Create(bbc, lpc);

                            if (form != null)
                                csv.WriteRecord(form);
                        }
                    }
                }
                return sw.ToString();
            }
        }

        public string GenerateSparkTemplate(IEnumerable<HouseModel> houses, DateTime from, DateTime to)
        {
            using (var sw = new StringWriter())
            {
                using (var csv = new CsvWriter(sw))
                {
                    csv.WriteHeader<SparkOnboarding>();

                    foreach (var h in houses)
                    {
                        h.Building = _unitOfWork.BuildingRepository.Get()
                            .SingleOrDefault(b => ((int) b.Udprn).ToString() == h.Address.UDPRN);

                        if (h.Building == null)
                        {
                            Console.WriteLine("WARNING: Skipping house #{0} since it does not have a valid UDPRN!");
                            continue;
                        }

                        var elec = (ElectricityContract) h.GetContractStartingBetween(ServiceType.Electricity, from, to);
                        var gas = (GasContract) h.GetContractStartingBetween(ServiceType.Gas, from, to);

                        if (elec != null || gas != null)
                        {
                            var form = SparkOnboarding.Create(elec, gas);

                            if (form != null)
                                csv.WriteRecord(form);
                        }
                    }
                }
                return sw.ToString();
            }
        }

        public List<HouseholdBill> GetHouseholdBills(int houseId)
        {
            var house = _unitOfWork.ActiveHouses.GetByID(houseId);
            return GetHouseholdBills(house);
        }

        public IQueryable<HouseModel> GetHouseIfItIsActive(int houseId)
        {
            return GetActiveHouses().Where(w => w.ID == houseId).Include("Contract").Include("HouseholdBill");
        }
    }
}