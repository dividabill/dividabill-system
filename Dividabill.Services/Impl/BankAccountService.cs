﻿using System;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
    /// <summary>
    ///     Creates accounts.
    /// </summary>
    public class BankAccountService : IAccountService
    {
        private readonly IGcBankAccountToBankAccountMappingFactory _gcBankAccountToBankAccountMappingFactory;
        private readonly ITenantService _tenantService;
        private readonly IUnitOfWork _unitOfWork;

        public BankAccountService(IUnitOfWork unitOfWork,
            ITenantService tenantService, IGcBankAccountToBankAccountMappingFactory gcBankAccountToBankAccountMappingFactory)
        {
            _gcBankAccountToBankAccountMappingFactory = gcBankAccountToBankAccountMappingFactory;
            _tenantService = tenantService;
            _unitOfWork = unitOfWork;
        }

        public IQueryable<BankAccount> GetActive(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return asyncExecutionParam.UnitOfWork.BankAccountRepository.Get();
        }

        public BankAccount GetById(string id, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return asyncExecutionParam.UnitOfWork.BankAccountRepository.Get().FirstOrDefault(x => x.Id == id);
        }

        public User LinkOrUnlinkBankAccountToUser(User tenant, BankAccount bankAccount, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            tenant.BankAccountID = bankAccount?.Id;
            return _tenantService.UpdateAndSave(tenant, asyncExecutionParam);
        }

        public BankAccount InsertAndSave(BankAccount entityToInsert, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var newEntity = Insert(entityToInsert, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.BankAccountRepository.SaveChanges();
            return newEntity;
        }

        public BankAccount Insert(BankAccount entityToInsert, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            entityToInsert.LastModified = DateTime.UtcNow;
            entityToInsert.CreatedAt = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.BankAccountRepository.Insert(entityToInsert);
        }

        public BankAccount InsertBankAccountFromGcAccount(User tenant, GCBankAccount gcBankAccount, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var bankAccount = _gcBankAccountToBankAccountMappingFactory.Map(tenant, gcBankAccount);
            bankAccount = InsertAndSave(bankAccount, asyncExecutionParam);
            return bankAccount;
        }

        public BankAccount UpdateBankAccountFromGcAccount(string bankAccountId, GCBankAccount gcBankAccount, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var bankAccount = GetById(bankAccountId, asyncExecutionParam);
            bankAccount = _gcBankAccountToBankAccountMappingFactory.Map(gcBankAccount, bankAccount);
            bankAccount = Update(bankAccount, asyncExecutionParam);
            return bankAccount;
        }

        public BankAccount UpdateAndSave(BankAccount entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var updatedEntity = Update(entityToUpdate, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.BankAccountRepository.SaveChanges();
            return updatedEntity;
        }

        public BankAccount Update(BankAccount entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            entityToUpdate.LastModified = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.BankAccountRepository.Update(entityToUpdate);
        }
    }
}