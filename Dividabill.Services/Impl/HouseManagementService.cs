﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.BusinessLogic.Components;
using DividaBill.Models;

namespace DividaBill.Services
{
    class HouseManagementService
    {
        private EmailWorkflowComponent _emailService;
        private BillService _billService;
        private HouseWorkflowComponent _houseWorkflow;
        
        public HouseManagementService()
        {
            this._emailService = new EmailWorkflowComponent();
            this._houseWorkflow = new HouseWorkflowService();
            this._billService = new BillService();
        }

        public void ActivateHouse(int houseId)
        {
            HouseModel house = _houseWorkflow.MarkHouseActive(houseId);
            _emailService.SendWholeHouseSignedUpEmail(house.Tenants);

            //Generate and send BG registration form
            MemoryStream m = _houseWorkflow.GetBritishGasRegistrationForm(house);
            DateTime setupDate = house.SetUpDate.GetValueOrDefault(DateTime.Now);
            DateTime signupDate = (setupDate > house.StartDate) ? setupDate : house.StartDate;
            _emailService.SendBGSignUpEmail(house, m, signupDate);

            //Create first bill
            _billService.GenerateFirstMonthBill(DateTime.Now, DateTime.Now.AddMonths(1), house.ID);
        }
    }
}
