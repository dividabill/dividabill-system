﻿using System;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Impl
{
    public class ProviderService : IProviderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProviderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Provider GetProvider(int id)
        {
            return _unitOfWork.ProviderRepository.GetByID(id);
        }

        public IQueryable<Provider> GetProvidersByServiceTypeId(int serviceTypeId)
        {
            return _unitOfWork.ProviderRepository.Get().Where(p => p.Type.Any(t => t.Id == serviceTypeId));
        }

        public IQueryable<Provider> GetAllProviders()
        {
            return _unitOfWork.ProviderRepository.Get();
        }
    }
}