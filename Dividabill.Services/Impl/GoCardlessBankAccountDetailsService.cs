using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Library.Helpers;
using DividaBill.AppConstants;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public class GoCardlessBankAccountDetailsService : IGoCardlessBankAccountDetailsService
    {
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly ITestDataService _testDataService;

        public GoCardlessBankAccountDetailsService(ITestDataService testDataService, IGoCardlessApiClient goCardlessApiClient)
        {
            _goCardlessApiClient = goCardlessApiClient;
            _testDataService = testDataService;
        }

        public async Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            if (_testDataService.ValidateBankAccountDetailsForTestData(bankAccountDetails))
                return null;

            try
            {
                await CheckBankDetails(bankAccountDetails, goCardlessApi);
                return null;
            }
            catch (ApiException exception)
            {
                if ((int) exception.StatusCode == 422) //(Unprocessable Entity)
                {
                    return exception.Content.ToObject<GCValidationError>().error.errors.Select(item => new UIError(GetViewModelField(item.field), $"{GetFieldDisplayName(item.field)} {item.message}")).ToList();
                }
                throw;
            }
        }

        private async Task<BankAccountDetailsLookupsOutputModel> CheckBankDetails(BankAccountDetails bankAccountDetails, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var bankDetailsInputModel = new GCBankAccountDetailsInputModel
            {
                bank_details_lookups = new BankAccountDetailsInputModel
                {
                    account_number = bankAccountDetails.AccountNumber,
                    branch_code = bankAccountDetails.AccountSortCode,
                    country_code = "GB"
                }
            };

            var mand = await goCardlessApi.CheckBankAccountDetails(bankDetailsInputModel);
            return mand;
        }

        private string GetViewModelField(string gcField)
        {
            switch (gcField)
            {
                case "account_number":
                    return BankAccountField.AccountNumber;
                case "branch_code":
                    return BankAccountField.AccountSortCode;
            }
            return string.Empty;
        }

        private string GetFieldDisplayName(string gcField)
        {
            switch (gcField)
            {
                case "account_number":
                    return "Account Number";
                case "branch_code":
                    return "Account Sort Code";
            }
            return string.Empty;
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }
    }
}