using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.Services.Responses;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.Impl
{
    public class BankAccountRegistrationService : IBankAccountRegistrationService
    {
        private readonly IContractService _contractService;
        private readonly ICouponService _couponService;
        private readonly IHouseService _houseService;
        private readonly IViewModelToEntityMappingFactory<SignUpViewModel, User> _signUpViewModelToUserMappingFactory;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IThirdPartiesListService _thirdPartyServices;
        private readonly IUnitOfWork _unitOfWork;

        public BankAccountRegistrationService(IUnitOfWork unitOfWork, IContractService contractService, IHouseService houseService,
            ITenantEncryptionService tenantEncryptionService,
            IViewModelToEntityMappingFactory<SignUpViewModel, User> signUpViewModelToUserMappingFactory, ICouponService couponService,
            IThirdPartiesListService thirdPartyServices)
        {
            _unitOfWork = unitOfWork;
            _thirdPartyServices = thirdPartyServices;
            _couponService = couponService;
            _signUpViewModelToUserMappingFactory = signUpViewModelToUserMappingFactory;
            _tenantEncryptionService = tenantEncryptionService;
            _houseService = houseService;
            _contractService = contractService;
        }

        public async Task<RegisterResult> RegisterAccount(IUserManager userManager, SignUpViewModel signUpViewModel)
        {
            var vals = await RegisterAccountUnsafe(userManager, signUpViewModel);

            return vals;
        }

        private async Task<RegisterResult> RegisterAccountUnsafe(IUserManager userManager, SignUpViewModel signUpViewModel)
        {
            var validationResult = await RegisterAccountValidation(signUpViewModel);
            if (validationResult != null)
            {
                return validationResult;
            }
            var registerResult = new RegisterResult();

            // create a new user and encrypt the data
            registerResult.User = _signUpViewModelToUserMappingFactory.Map(signUpViewModel);
            _tenantEncryptionService.EncryptUser(registerResult.User);

            // and register the user
            var result = await userManager.CreateAsync(registerResult.User, signUpViewModel.Password);
            if (string.IsNullOrEmpty(registerResult.User?.Id))
                throw new Exception("New User id is empty");
            if (!(result?.Succeeded ?? false))
            {
                if (result?.Errors != null)
                    foreach (var error in result.Errors)
                    {
                        registerResult.Errors.Add(new UIError("Email", error));
                    }
            }
            else
            {
                // add user roles
                await userManager.AddToRoleAsync(registerResult.User.Id, "Client");

                // Create the house
                registerResult.HouseExisted = AccountHouseExists(signUpViewModel);
                var houseWizard = signUpViewModel.CreateHouseViewModel();
                var house = _houseService.AddHouseIfNotExists(houseWizard);

                //add the user to the house
                _houseService.AddTenantToHouse(house, registerResult.User);

                //if house was just created
                if (!registerResult.HouseExisted)
                {
                    //create base contracts
                    _contractService.CreateSignUpContracts(signUpViewModel);

                    //and mark it as active (?)
                    _houseService.MarkHouseActive(house);
                }
            }

            return registerResult;
        }

        private async Task<RegisterResult> RegisterAccountValidation(SignUpViewModel signUpViewModel)
        {
            var registerResult = new RegisterResult();

            // validate signUpViewModel
            if (signUpViewModel == null)
                throw new ArgumentNullException(nameof(signUpViewModel));
            signUpViewModel.Validate();

            // validate bank details. 
            var bankAccountValidationErrors = await ValidateBankAccountDetails(signUpViewModel.BankAccountDetails);
            if (bankAccountValidationErrors != null)
            {
                foreach (var item in bankAccountValidationErrors)
                {
                    registerResult.Errors.Add(item);
                }
                return registerResult;
            }

            //validate Coupon
            if (!string.IsNullOrEmpty(signUpViewModel.Coupon))
            {
                try
                {
                    _couponService.Get(signUpViewModel.Coupon);
                }
                catch (CouponNotFound)
                {
                    registerResult.Errors.Add(new UIError("Coupon", "Discount code isn't recognized"));
                    return registerResult;
                }
            }
            return null;
        }

        private bool AccountHouseExists(SignUpViewModel wizard)
        {
            var existingHouse = _unitOfWork.ActiveHouses
                .Get(h => h.Address.UDPRN == wizard.Udprn, null, "Tenants")
                .SingleOrDefault();

            return existingHouse != null;
        }

        public async Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails)
        {
            foreach (var thirdPartyService in _thirdPartyServices.Services.OrderBy(service => service.SortOrder.ValidateBankAccountDetails))
            {
                try
                {
                    var bankResponse = await thirdPartyService.ValidateBankAccountDetails(bankAccountDetails);
                    if (bankResponse != null && bankResponse.Any(x => x != null))
                    {
                        return bankResponse;
                    }
                    break;
                }
                catch (Exception)
                {
                }
            }
            return null;
        }
    }
}