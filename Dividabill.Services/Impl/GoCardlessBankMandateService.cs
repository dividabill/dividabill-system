using System;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.AppConstants;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public class GoCardlessBankMandateService : IGoCardlessBankMandateService
    {
        private readonly IAccountService _bankAccountService;
        private readonly IBankMandateService _bankMandateService;
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly ITestDataService _testDataService;
        private readonly IUnitOfWork _unitOfWork;

        public GoCardlessBankMandateService(IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApiClient, IBankMandateService bankMandateService, IAccountService bankAccountService,
            ITestDataService testDataService)
        {
            _bankAccountService = bankAccountService;
            _testDataService = testDataService;
            _unitOfWork = unitOfWork;
            _bankMandateService = bankMandateService;
            _goCardlessApiClient = goCardlessApiClient;
        }

        public virtual BankMandate UpdateOrInsertBankMandateFromGcMandate(User tenant, GCMandate mandate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            if (_bankAccountService.GetById(tenant.BankAccountID, asyncExecutionParam) == null)
            {
                return null;
            }

            BankMandate bankMandate = null;
            try
            {
                bankMandate = _bankMandateService.GetById(mandate.id, asyncExecutionParam);
            }
            catch (BankMandateNotFound)
            {
            }
            bankMandate = bankMandate == null
                ? _bankMandateService.InsertAndSaveBankMandateFromGcMandate(tenant, mandate, asyncExecutionParam)
                : _bankMandateService.UpdateBankMandateFromGcMandate(bankMandate.Id, mandate, asyncExecutionParam);

            return bankMandate;
        }

        public virtual async Task<GCMandate> GetExistingOrCreateGcMandate(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            if (await ValidateForTestDataAndCancelBankMandate(tenant, goCardlessApi))
                return null;

            var bankAccountId = tenant.BankAccountID;
            GCMandate gcMandate = null;
            if (string.IsNullOrEmpty(tenant.MandateID))
            {
                gcMandate = await GetActiveMandateFromGc(bankAccountId, goCardlessApi) ?? await CreateGcMandate(bankAccountId, goCardlessApi);
            }
            else
            {
                gcMandate = await goCardlessApi.GetMandateAsync(tenant.MandateID);
            }
            return gcMandate;
        }

        public virtual async Task<GCMandate> CreateGcMandate(string bankAccountId, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var mandate = new NewMandate();
            mandate.links.Add("customer_bank_account", bankAccountId);
            var mand = await goCardlessApi.CreateMandateAsync(mandate);
            return mand;
        }

        public virtual async Task<BankMandate> CreateAndInsertBankMandateFromGc(User tenant, IGoCardlessApiClient goCardlessApi = null, AsyncExecutionParam asyncExecutionParam = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            if (await ValidateForTestDataAndCancelBankMandate(tenant, goCardlessApi))
                return null;

            var mandateFromGc = await GetActiveMandateFromGc(tenant.BankAccountID, goCardlessApi) ?? await CreateGcMandate(tenant.BankAccountID, goCardlessApi);
            return UpdateOrInsertBankMandateFromGcMandate(tenant, mandateFromGc, asyncExecutionParam);
        }

        public virtual async Task UpdateAllExistingBankMandatesFromGc(User tenant, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null)
        {
            await UpdateAllMandatesFromDb(tenant, asyncExecutionParam, goCardlessApi);

            await UpdateAllMandatesFromGcByBankAccountId(tenant, asyncExecutionParam, goCardlessApi);
        }

        public virtual async Task<BankMandate> SyncBankMandateAsync(User tenant, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            var asyncExecutionParam = new AsyncExecutionParam(AssignThreadSafeUnitOfWork(unitOfWork));

            await UpdateAllExistingBankMandatesFromGc(tenant, asyncExecutionParam, goCardlessApi);

            var gcMandate = await GetExistingOrCreateGcMandate(tenant, goCardlessApi);
            if (gcMandate == null)
            {
                return null;
            }
            var bankMandate = UpdateOrInsertBankMandateFromGcMandate(tenant, gcMandate, asyncExecutionParam);
            bankMandate = await CreateNewGcMandateIfAlreadyOld(tenant, gcMandate, asyncExecutionParam, goCardlessApi) ?? bankMandate;
            return bankMandate;
        }

        public virtual async Task<BankMandate> CreateNewGcMandateIfAlreadyOld(User tenant, GCMandate gcMandate, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null)
        {
            if (gcMandate.status == GoCardlessMandateStatus.Failed
                || gcMandate.status == GoCardlessMandateStatus.Expired)
            {
                return await CreateAndInsertBankMandateFromGc(tenant, goCardlessApi, asyncExecutionParam);
            }
            return null;
        }

        private async Task<BankMandate> UpdateBankMandateFromGc(string bankMandateId, IGoCardlessApiClient goCardlessApi = null, AsyncExecutionParam asyncExecutionParam = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var mandateFromGc = await goCardlessApi.GetMandateAsync(bankMandateId);
            return _bankMandateService.UpdateBankMandateFromGcMandate(bankMandateId, mandateFromGc, asyncExecutionParam);
        }

        private async Task<bool> ValidateForTestDataAndCancelBankMandate(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            if (_testDataService.ValidateBankAccountDetailsForTestData(tenant)
                || string.IsNullOrEmpty(tenant.BankAccountID))
            {
                if (!string.IsNullOrEmpty(tenant.MandateID))
                {
                    await goCardlessApi.CancelMandateAsync(tenant.MandateID);
                }
                return true;
            }
            return false;
        }

        private async Task<GCMandate> GetActiveMandateFromGc(string bankAccountId, IGoCardlessApiClient goCardlessApi)
        {
            if (string.IsNullOrEmpty(bankAccountId))
                throw new Exception("BankAccountId is null");
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            var gcMandates = await goCardlessApi.GetMandatesForBankAccount(bankAccountId);

            var existingBankMandate = gcMandates?.FirstOrDefault(
                w => w.status == GoCardlessMandateStatus.Active
                     || w.status == GoCardlessMandateStatus.PendingSubmission
                );
            return existingBankMandate;
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }

        private async Task UpdateAllMandatesFromGcByBankAccountId(User tenant, AsyncExecutionParam asyncExecutionParam, IGoCardlessApiClient goCardlessApi)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            if (!string.IsNullOrEmpty(tenant.BankAccountID))
            {
                var gcMandates = await goCardlessApi.GetMandatesForBankAccount(tenant.BankAccountID);
                if (!gcMandates.IsEmpty())
                {
                    foreach (var gcMandate in gcMandates.ToList())
                    {
                        UpdateOrInsertBankMandateFromGcMandate(tenant, gcMandate, asyncExecutionParam);
                    }
                }
            }
        }

        private async Task UpdateAllMandatesFromDb(User tenant, AsyncExecutionParam asyncExecutionParam, IGoCardlessApiClient goCardlessApi)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var bankMandateIds = _bankMandateService.GetAllMandatesIDsForUser(tenant, asyncExecutionParam).Select(x => x.Id).ToList();
            if (!bankMandateIds.IsEmpty())
            {
                foreach (var bankMandateId in bankMandateIds)
                {
                    await UpdateBankMandateFromGc(bankMandateId, goCardlessApi, asyncExecutionParam);
                }
            }
        }

        private IUnitOfWork AssignThreadSafeUnitOfWork(IUnitOfWork unitOfWork)
        {
            return unitOfWork ?? _unitOfWork;
        }
    }
}