using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.ViewModels;
using GoCardless_API;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public class GoCardlessService : IGoCardlessService
    {
        private readonly IAccountService _bankAccountService;
        private readonly IBankMandateService _bankMandateService;
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly IGoCardlessBankAccountDetailsService _goCardlessBankAccountDetailsService;
        private readonly IGoCardlessBankAccountService _goCardlessBankAccountService;
        private readonly IGoCardlessBankMandateService _goCardlessBankMandateService;
        private readonly IMultiThreadingService _multiThreadingService;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityToViewModelMappingFactory<User, BankAccountDetails> _userToBankAccountDetailsMappingFactory;
        private readonly IGoCardlessCustomerService _cardlessCustomerService;

        public GoCardlessService(IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApiClient, IMultiThreadingService multiThreadingService,
            ITenantEncryptionService tenantEncryptionService,
            IBankMandateService bankMandateService, IGoCardlessBankMandateService goCardlessBankMandateService,
            IAccountService bankAccountService, IGoCardlessBankAccountService goCardlessBankAccountService,
            IGoCardlessCustomerService cardlessCustomerService,
            IEntityToViewModelMappingFactory<User, BankAccountDetails> userToBankAccountDetailsMappingFactory,
            IGoCardlessBankAccountDetailsService goCardlessBankAccountDetailsService)
        {
            _cardlessCustomerService = cardlessCustomerService;
            _goCardlessBankAccountDetailsService = goCardlessBankAccountDetailsService;
            _userToBankAccountDetailsMappingFactory = userToBankAccountDetailsMappingFactory;
            _goCardlessBankAccountService = goCardlessBankAccountService;
            _bankAccountService = bankAccountService;
            _goCardlessBankMandateService = goCardlessBankMandateService;
            _bankMandateService = bankMandateService;
            _goCardlessApiClient = goCardlessApiClient;
            _multiThreadingService = multiThreadingService;
            _tenantEncryptionService = tenantEncryptionService;
            _unitOfWork = unitOfWork;
        }

        public async Task<BulkOperationResult<string>> AddBillingRunTenantsToGoCardlessAsync(int billingRunId, GoCardless.Environments environment)
        {
            //var tenants = _housesToBeBilledService.GetHousesThatHaveBeenBilledAnAmount(billingRunId)
            //    .SelectMany(s => s.Tenants)
            //    .Where(w => string.IsNullOrEmpty(w.MandateID));
            var tenants = _unitOfWork.UserRepository.Where(w => string.IsNullOrEmpty(w.MandateID));
            return await AddTenantsToGoCardlessAsync(tenants, environment, false);
        }

        public async Task<List<UIError>> AddToGoCardlessAsync(User tenant, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            var asyncExecutionParam = new AsyncExecutionParam(unitOfWork);

            tenant = _tenantEncryptionService.DecryptUser(tenant);

            var bankAccountDetails = _userToBankAccountDetailsMappingFactory.Map(tenant);
            var validationResult = await _goCardlessBankAccountDetailsService.ValidateBankAccountDetails(bankAccountDetails);
            if (validationResult != null)
            {
                return validationResult;
            }

            //check if the customer has a GoCardless ID
            await _cardlessCustomerService.AddUserAsGoCardlessCustomerAsync(tenant, asyncExecutionParam, goCardlessApi);

            var bankAccount = await _goCardlessBankAccountService.SyncUserBankAccountAsync(tenant, asyncExecutionParam, goCardlessApi);
            _bankAccountService.LinkOrUnlinkBankAccountToUser(tenant, bankAccount, asyncExecutionParam);

            var domainBankMandate = await _goCardlessBankMandateService.SyncBankMandateAsync(tenant, unitOfWork, goCardlessApi);
            _bankMandateService.LinkOrUnlinkBankMandateToUser(tenant, domainBankMandate, asyncExecutionParam);

            unitOfWork.Save();
            return null;
        }

        private async Task<BulkOperationResult<string>> AddTenantsToGoCardlessAsync(IQueryable<User> tenants, GoCardless.Environments environment, bool multiThread = false)
        {
            var userIds = tenants.Select(s => s.Id).ToList();
            _goCardlessApiClient.SetGoCardlessEnvironment(environment);
            var result = multiThread ? await MultiThread(userIds) : await SingleThread(userIds);
            return result;
        }

        private async Task<BulkOperationResult<string>> MultiThread(List<string> userIds)
        {
            return await _multiThreadingService.RunTaskInParallelAsync<string, GoCardlessApiClient>(1, userIds,
                async (userId, unitOfWork, goCardlessApi, bulkOperationResult) =>
                {
                    await BulkAddTenantToGoCardless(unitOfWork, userId, bulkOperationResult);
                    unitOfWork.Dispose();
                });
        }

        private async Task<BulkOperationResult<string>> SingleThread(List<string> userIds)
        {
            var bulkOperationResult = new BulkOperationResult<string>();
            foreach (var userId in userIds)
            {
                await BulkAddTenantToGoCardless(_unitOfWork, userId, bulkOperationResult);
            }
            return bulkOperationResult;
        }

        private async Task BulkAddTenantToGoCardless(IUnitOfWork unitOfWork, string userId, BulkOperationResult<string> bulkOperationResult)
        {
            try
            {
                var user = unitOfWork.UserRepository.FirstOrDefault(w => w.Id == userId);
                if (user == null)
                    throw new ArgumentNullException("userId is null");

                var validationError = await AddToGoCardlessAsync(user);
                if (validationError == null)
                {
                    bulkOperationResult.Successful.Add(user.Id);
                }
                else
                {
                    foreach (var uiError in validationError)
                    {
                        bulkOperationResult.Failed.Add($"userId=[{userId}];Key=[{uiError.Key}];", new Exception($"ErrorMessage=[{uiError.ErrorMessage}]"));
                    }
                }
            }
            catch (Exception ex)
            {
                bulkOperationResult.Failed.Add(userId, ex);
                //_billingQueueTransactionLogService.TrackException()
            }
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }

        private IUnitOfWork AssignThreadSafeUnitOfWork(IUnitOfWork unitOfWork)
        {
            return unitOfWork ?? _unitOfWork;
        }
    }
}