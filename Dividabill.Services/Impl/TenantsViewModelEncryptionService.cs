using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Impl
{
    public class TenantsViewModelEncryptionService : IViewModelEncryptionService<TenantsViewModel>
    {
        private readonly IEncryptionService _encryptionService;

        public TenantsViewModelEncryptionService(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public TenantsViewModel DecryptViewModel(TenantsViewModel tenant)
        {
            tenant.FirstName = _encryptionService.Decrypt(tenant.FirstName);
            tenant.LastName = _encryptionService.Decrypt(tenant.LastName);

            return tenant;
        }
    }
}