using DividaBill.Models;

namespace DividaBill.Services.Interfaces
{
    public interface ICouponService
    {
        Coupon Get(string code);
    }
}