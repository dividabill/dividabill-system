﻿using System.Collections.Generic;
using DividaBill.Models;
using DividaBill.Models.Sheets;

namespace DividaBill.Services.Interfaces
{
    public interface IGDriveService
    {
        void Pull(File file, bool commitToDb);
        void Push(File file, bool commitToDb, IEnumerable<HouseModel> houses);
        void Sync(File file, bool commitToDb, IEnumerable<HouseModel> houses);
        void SyncAll(bool saveChanges);
    }
}