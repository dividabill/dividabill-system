﻿using DividaBill.ViewModels;

namespace DividaBill.Services.Interfaces
{
    public interface ILoggingService
    {
        string[] GetFormatters();
        LogResponseViewModel GetLastLines(int feedId, int nItems = 50, int upToRev = int.MaxValue);
        LogResponseViewModel GetNewestLines(int feedId, int lastSeenId);
    }
}