using System.Threading.Tasks;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using GoCardless_API.api;

namespace DividaBill.Services.Interfaces
{
    public interface IGoCardlessBankAccountService
    {
        Task<BankAccount> SyncUserBankAccountAsync(User tenant, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null);
    }
}