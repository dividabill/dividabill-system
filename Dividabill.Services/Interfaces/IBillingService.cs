using System.Threading.Tasks;

namespace DividaBill.Services.Interfaces
{

    public interface IBillingService
    {
        // Generates all bills for the house. 
        void GenerateBills(int billingRunId);
    }
}