﻿using System.Threading.Tasks;
using DividaBill.Models;

namespace DividaBill.Services.Interfaces
{
    public interface IAuthService
    {
        Task SignInAsync(User user, bool isPersistent);
    }
}