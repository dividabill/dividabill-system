﻿using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Interfaces
{
    public interface ITenantService : IUpdateService<User>
    {
        //User GetTenant(string id);
        //void UpdateTenant(User user);
        //bool DeleteTenant(string id);
        //bool DeleteTenant(User user);

        //void CreateTenant(SignUpViewModel wizard);

        //IQueryable<User> GetAllTenants();


        //List<User> GetTenantListDecrypted(IQueryable<User> usersList);

        //IQueryable<User> GetActiveTenants();

        //List<User> GetAllTenantsDecrypted();
        //IEnumerable<User> GetAllTenantsNameDecrypted();

        //User GetTenantDecrypted(string id);
        //List<User> GetActiveTenantsDecrypted();
        //List<User> GetActiveTenantNameDecrypted();

        //IQueryable<TenantsViewModel> GetAllTenantsForDataTable();
        //IQueryable<TenantsViewModel> GetAllTenantsForDataTableDecrypted();
        //IQueryable<TenantsViewModel> GetActiveTenantsForDataTableDecrypted();
        //IEnumerable<TenantsViewModel> GetAllTenantsForDataTableDecrypted();

        //IQueryable<TenantsViewModel> GetActiveTenantsForDataTableDecrypted();
        IQueryable<User> GetAllTenants();
        List<User> GetAllTenantsDecrypted();
        IEnumerable<User> GetAllTenantsNameDecrypted();
        IQueryable<User> GetActiveTenants();
        IEnumerable<User> GetActiveTenantsDecrypted();
        User GetTenant(string id);
        User GetTenantDecrypted(string id);
    }
}