﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Library;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.Services.Log.Formatters
{
    class Registrations : ILogFormatter
    {
        public int Id { get; } = 1;

        public string FriendlyName { get; } = "Tenants Joined";

        public bool CanParse(AuditLog log)
        {
            return log.TypeFullName == typeof(User).FullName
                && log.EventType == EventType.Added;
        }

        public string GetLine(AuditLog log)
        {
            return "[{0}] {1} joined house #{2} for a total of {3}/{4} users. ".F(
                log.EventDateUTC.ToString(), 1, 1, 1);
        }
    }
}
