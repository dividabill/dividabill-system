﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Library;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.Services.Log.Formatters
{
    /// <summary>
    /// Displays all log lines. 
    /// </summary>
    class Everything : ILogFormatter
    {

        public string FriendlyName { get; } = "Everything";

        public bool CanParse(AuditLog log)
        {
            return true;
        }

        public string GetLine(AuditLog log)
        {
            return "[{0}] {1} #{2} was {3}".F(
                log.EventDateUTC.DateTime.ToString(), 
                log.TypeFullName.Substring(log.TypeFullName.LastIndexOf('.')), 
                log.RecordId, 
                log.EventType.ToString());
        }
    }
}
