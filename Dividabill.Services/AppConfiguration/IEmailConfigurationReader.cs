using DividaBill.AppConstants;
using DividaBill.Services.AppConfiguration.Impl;
using DividaBill.Services.Emails.Models;

namespace DividaBill.Services.AppConfiguration
{
    public interface IEmailConfigurationReader
    {
        EmailConfiguration GetEmailConfiguration(EmailEnum emailEnum);
    }
}