A TenantService.md file contain comment about DividaBill.Services.TenantService

-------------------------------------------------------

AddToGoCardless(User tenant):
-------------

Mainly this function contain working with 3 GoCardless services:
1) Customer:
-------------------- CreateCustomer --------------------
https://developer.gocardless.com/pro/2015-07-06/#customers-create-a-customer

2) BankAccount:
-------------------- CreateBankAccount --------------------
https://developer.gocardless.com/pro/2015-07-06/#customer-bank-accounts-create-a-customer-bank-account

hardcoded creation of sending object(NewBankAccount) to GoCardless server (Might be exception here)
necessary for double checking for Is there necessary for checking to already existed row in BankAccounts by id column

When editing user from list of result this SQL
--- SQL ---
SELECT *
FROM   AspNetUsers AS anu
WHERE  anu.GoCardlessCustomerID IS NULL
       AND anu.BankAccountID IS NULL
       AND (
               anu.AccountNumber IS NULL
               OR anu.AccountNumber = '00000000'
           )
--- SQL ---
Then EXCEPTION throws.
GoCardless answer: {"error":{"message":"Validation failed","errors":[{"field":"account_number","message":"is invalid","request_pointer":"/customer_bank_accounts/account_number"}],"documentation_url":"https://developer.gocardless.com/pro#validation_failed","type":"validation_failed","request_id":"7655ba97-165b-4e25-8d05-764123920c0a","code":422}}

-------------------- GetBankAccount --------------------
https://developer.gocardless.com/pro/2015-07-06/#customer-bank-accounts-list-customer-bank-accounts
If (User.BankAccountID has value and this value not found in BankAccounts table (BankAccountRepository))
Then GoCardless.GetBankAccount server invoked
--- SQL ---
SELECT *
FROM   AspNetUsers AS anu
WHERE  anu.BankAccountID IS NOT NULL
       AND anu.BankAccountID NOT IN (SELECT ba.id
                                     FROM   BankAccounts AS ba)
--- SQL ---

If given User.BankAccountID value doesn't exist on GoCardless
Then EXCEPTION throws
GoCardless answer: {"error":{"message":"Resource not found","errors":[{"reason":"resource_not_found","message":"Resource not found"}],"documentation_url":"https://developer.gocardless.com/pro#resource_not_found","type":"invalid_api_usage","request_id":"b84fc704-019c-4273-bf18-ae0b4600e835","code":404}}


3) Mandate:
-------------------- CreateMandate --------------------
https://developer.gocardless.com/pro/2015-07-06/#mandates-create-a-mandate


-------------------- GetMandate --------------------
https://developer.gocardless.com/pro/2015-07-06/#mandates-list-mandates
If given User.MandateID value doesn't exist on GoCardless
Then EXCEPTION throws
GoCardless answer: {"error":{"message":"Resource not found","errors":[{"reason":"resource_not_found","message":"Resource not found"}],"documentation_url":"https://developer.gocardless.com/pro#resource_not_found","type":"invalid_api_usage","request_id":"e2396e9e-66ec-4418-a06f-357f0c03eef8","code":404}}

And also with same 3 Repositories:
1) UserRepository
2) BankAccountRepository
3) BankMandateRepository

