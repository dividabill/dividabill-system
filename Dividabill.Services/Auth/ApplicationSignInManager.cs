﻿using System.Security.Claims;
using System.Threading.Tasks;
using DividaBill.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace DividaBill.Services.Auth
{

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<User, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }
    }
}