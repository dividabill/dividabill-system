﻿using System.Web.Security;
using Microsoft.Owin.Security.DataProtection;

namespace DividaBill.Services.Auth
{
    // Allows Unity to create DataProtection guys
    // Taken from:
    // http://stackoverflow.com/questions/28606676/how-can-i-instantiate-owin-idataprotectionprovider-in-azure-web-jobs
    internal class MachineKeyProtectionProvider : IDataProtectionProvider
    {
        public IDataProtector Create(params string[] purposes)
        {
            return new MachineKeyDataProtector(purposes);
        }
    }

    internal class MachineKeyDataProtector : IDataProtector
    {
        private readonly string[] _purposes;

        public MachineKeyDataProtector(string[] purposes)
        {
            _purposes = purposes;
        }

        public byte[] Protect(byte[] userData)
        {
            return MachineKey.Protect(userData, _purposes);
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            return MachineKey.Unprotect(protectedData, _purposes);
        }
    }
}