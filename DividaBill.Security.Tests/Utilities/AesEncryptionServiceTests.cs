using System;
using Dividabill.Tests.Core;
using DividaBill.Security.Utilities;
using NUnit.Framework;

namespace DividaBill.Security.Tests.Utilities
{
    [TestFixture]
    public class AesEncryptionServiceTests : BaseAutoMock<AesEncryptionService>
    {
        [TestCase("test text", "bMbAV+grLRZDSPiFZPMX42AdOlmbmFgHCaxxv9iJ58E=")]
        public void GivenEncryptedAndDecrypted_WhenEncryptInvoked_ThenEncryptedValueReturned(string textToEncrypt,
            string encrypted)
        {
            // Arrange

            // Act
            var result = ClassUnderTest.Encrypt(textToEncrypt);

            // Assert
            Assert.AreEqual(result, Convert.ToChar(1).ToString() + Convert.ToChar(2) + encrypted);
        }

        [TestCase("bMbAV+grLRZDSPiFZPMX42AdOlmbmFgHCaxxv9iJ58E=", "test text")]
        public void GivenEncryptedAndDecrypted_WhenDecryptInvoked_ThenDecryptedValueReturned(string textToDecrypt,
            string decrypted)
        {
            // Arrange

            // Act
            var result = ClassUnderTest.Decrypt(Convert.ToChar(1).ToString() + Convert.ToChar(2) + textToDecrypt);

            // Assert
            Assert.AreEqual(result, decrypted);
        }

        [TestCase("test text")]
        public void GivenAnyText_WhenEncryptAndDecryptInvoked_ThenOriginalValueReturned(string textToTest)
        {
            // Arrange

            // Act
            var resultEncrypted = ClassUnderTest.Encrypt(textToTest);
            var resultDecrypted = ClassUnderTest.Decrypt(resultEncrypted);

            // Assert
            Assert.AreEqual(resultDecrypted, textToTest);
        }

        [TestCase(@"test text
    m")]
        public void GivenNonBase64String_WhenIsBase64Invoked_ThenReturnedFalse(string textToTest)
        {
            // Arrange

            // Act
            var result = ClassUnderTest.IsEncrypted(textToTest);

            // Assert
            Assert.AreEqual(result, false);
        }

        [TestCase("bMbAV+grLRZDSPiFZPMX42AdOlmbmFgHCaxxv9iJ58E=")]
        public void GivenBase64String_WhenIsBase64Invoked_ThenReturnedTrue(string textToTest)
        {
            // Arrange

            // Act
            var result = ClassUnderTest.IsEncrypted(Convert.ToChar(1).ToString() + Convert.ToChar(2) + textToTest);

            // Assert
            Assert.AreEqual(result, true);
        }
    }
}