using System;
using System.Collections.Generic;

namespace API.Library.Models
{
    public class ApiRestConfig
    {
        public ApiRestConfig(Uri baseUri, string userAgent, string apiKey, Dictionary<string, string> header = null)
        {
            BaseUrl = baseUri;
            UserAgent = userAgent;
            ApiKey = apiKey;
            Header = header;
        }

        public Uri BaseUrl { get; }
        public string UserAgent { get; }
        public string ApiKey { get; }

        public Dictionary<string, string> Header { get; }
    }
}