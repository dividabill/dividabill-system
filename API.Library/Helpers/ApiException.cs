﻿using System;
using System.Net;
using Newtonsoft.Json.Linq;

namespace API.Library.Helpers
{
    public class ApiException : Exception
    {
        public ApiException(string message)
            : base(message)
        {
        }

        public JObject Content { get; set; }

        public string RawContent { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}