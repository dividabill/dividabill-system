﻿using Newtonsoft.Json.Serialization;

namespace API.Library.Helpers
{
    public class UnderscoreToCamelCasePropertyResolver : DefaultContractResolver
    {
        public UnderscoreToCamelCasePropertyResolver()
            : base(true)
        {
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToUnderscoreCase();
        }
    }
}