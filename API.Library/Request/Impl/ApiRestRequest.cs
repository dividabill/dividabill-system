using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using API.Library.Helpers;
using API.Library.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace API.Library.Request.Impl
{
    public class ApiRestRequest : IDisposable
    {
        private readonly ApiRestConfig _apiRestConfig;
        private RestClient _client;

        private JsonSerializer _serializer;

        public ApiRestRequest(ApiRestConfig apiRestConfig)
        {
            _apiRestConfig = apiRestConfig;
            _serializer = new JsonSerializer
            {
                ContractResolver = new UnderscoreToCamelCasePropertyResolver()
                //DateFormatString = ""
            };

            _client = new RestClient(_apiRestConfig.BaseUrl)
            {
                UserAgent = _apiRestConfig.UserAgent
            };
            _client.AddHandler("application/json", new NewtonsoftJsonDeserializer(_serializer));
        }

        public void Dispose()
        {
            _serializer = null;
            _client = null;
        }

        public IRestRequest CreateRestRequest(string resource, Method method, object options = null)
        {
            var request = new RestRequest(resource, method)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new NewtonsoftJsonSerializer()
            }.AddBody(options);

            return request;
        }

        public async Task<T> ExecuteAsync<T>(IRestRequest request, HttpStatusCode expected = HttpStatusCode.OK) where T : new()
        {
            if (_apiRestConfig.Header != null && _apiRestConfig.Header.Any())
            {
                foreach (var item in _apiRestConfig.Header.Where(x => !string.IsNullOrEmpty(x.Key)))
                {
                    request.AddHeader(item.Key, item.Value);
                }
            }
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {_apiRestConfig.ApiKey}");

            var response = await _client.ExecuteTaskAsync<T>(request);

            Debug.WriteLine(response.Content);
            if (response.StatusCode != expected)
            {
                var ex = new ApiException("Expected response " + (int) expected + " " + expected + " but received " +
                                          (int) response.StatusCode + " " + response.StatusCode +
                                          ". See Content for more details");
                try
                {
                    ex.Content = JObject.Parse(response.Content);
                }
                catch (Exception)
                {
                }
                ex.RawContent = response.Content;
                ex.StatusCode = response.StatusCode;
                throw ex;
            }
            return response.Data;
        }
    }
}