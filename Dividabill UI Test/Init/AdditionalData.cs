﻿using System;
using System.IO;

namespace SeleniumTesting
{
    public class AdditionalData
    {
        public String header = "header";
        public String footer = "footer";
        public String sourceError = "Incorrect source selected";
        public String itemError = "Incorrect item selected";
        public String dividabillItem = "DividaBill";
        public String gasItem = "Gas & Electricity";
        public String waterItem = "Water";
        public String internetlItem = "Internet";
        public String phoneItem = "Landline Phone";
        public String skyItem = "Sky";
        public String netflixItem = "Netflix";
        public String tvItem = "TV License";
        public String generalItem = "General";
        public String paymentsItem = "Payments";
        public String electricityItem = "Electricity";
    }
}
