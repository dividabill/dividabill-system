﻿using System;
using OpenQA.Selenium;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class Common : Init
    {

        public void open(String url)
        {
            IWebDriver dd = driver;
            driver.Navigate().GoToUrl(baseUrl + url);
        }

        public String getRandomName(string name)
        {
            return name + DateTime.Now.ToString("HHmmssfff");
        }

        public String getCurrentDate()
        {
            return DateTime.Today.ToString("MM/dd/yyyy"); ;
        }

        public String getRandomID(string name)
        {
            return name + DateTime.Now.ToString("mmssfff");
        }

        public String getRandomValue(string name)
        {
            return name + DateTime.Now.ToString("ssfff");
        }
    }
}
