﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class ServicesPage : AbstractPage
    {
        /*The selectors list item*/
        private By divida_bill_item_locator = By.CssSelector("[href='#collapseDividabill']");
        private By gas_and_electricity_item_locator = By.CssSelector("[href='#collapseGas']");
        private By water_item_locator = By.CssSelector("[href='#collapseWater']");
        private By internet_item_locator = By.CssSelector("[href='#collapseInternet']");
        private By landline_phone_item_locator = By.CssSelector("[href='#collapsePhone']");
        private By sky_item_locator = By.CssSelector("[href='#collapseSky']");
        private By netflix_item_locator = By.CssSelector("[href='#collapseNetflix']");
        private By tvlicense_item_locator = By.CssSelector("[href='#collapseTV']");

        /*The selectors dividaBill item*/
        private By divida_bill_more_information_button_locator = By.CssSelector("[aria-labelledby='dividabill'] .small-button");
        private By divida_bill_more_information_popup_locator = By.CssSelector("[aria-labelledby='dividabill'] .modal-dialog");
        private By divida_bill_more_information_close_button_locator = By.CssSelector("[aria-labelledby='dividabill'] [data-dismiss='modal']");

        /*The selectors Gas and Electricity item*/
        private By gas_more_information_button_locator = By.CssSelector("[aria-labelledby='gas'] .small-button");
        private By gas_more_information_popup_locator = By.CssSelector("[aria-labelledby='gas'] .modal-dialog");
        private By gas_more_information_close_button_locator = By.CssSelector("[aria-labelledby='gas'] [data-dismiss='modal']");

        /*The selectors Water item*/
        private By water_more_information_button_locator = By.CssSelector("[aria-labelledby='water'] .small-button");
        private By water_more_information_popup_locator = By.CssSelector("[aria-labelledby='water'] .modal-dialog");
        private By water_more_information_close_button_locator = By.CssSelector("[aria-labelledby='water'] [data-dismiss='modal']");

        /*The selectors internet item*/
        private By internet_more_information_button_locator = By.CssSelector("[aria-labelledby='internet'] .small-button");
        private By internet_more_information_popup_locator = By.CssSelector("[aria-labelledby='internet'] .modal-dialog");
        private By internet_more_information_close_button_locator = By.CssSelector("[aria-labelledby='internet'] [data-dismiss='modal']");

        /*The selectors Landline Phone item*/
        private By phone_more_information_button_locator = By.CssSelector("[aria-labelledby='phone'] .small-button");
        private By phone_more_information_popup_locator = By.CssSelector("[aria-labelledby='phone'] .modal-dialog");
        private By phone_more_information_close_button_locator = By.CssSelector("[aria-labelledby='phone'] [data-dismiss='modal']");

        /*The selectors Sky item*/
        private By sky_more_information_button_locator = By.CssSelector("[aria-labelledby='sky'] .small-button");
        private By sky_more_information_popup_locator = By.CssSelector("[aria-labelledby='sky'] .modal-dialog");
        private By sky_more_information_close_button_locator = By.CssSelector("[aria-labelledby='sky'] [data-dismiss='modal']");

        /*The selectors netflix item*/
        private By netflix_more_information_button_locator = By.CssSelector("[aria-labelledby='netflix'] .small-button");
        private By netflix_more_information_popup_locator = By.CssSelector("[aria-labelledby='netflix'] .modal-dialog");
        private By netflix_more_information_close_button_locator = By.CssSelector("[aria-labelledby='netflix'] [data-dismiss='modal']");

        /*The selector TV Services item*/
        private By tv_more_information_button_locator = By.CssSelector("[aria-labelledby='tv'] .small-button");
        private By tv_more_information_popup_locator = By.CssSelector("[aria-labelledby='tv'] .modal-dialog");
        private By tv_more_information_close_button_locator = By.CssSelector("[aria-labelledby='tv'] [data-dismiss='modal']");

        /*Locators by serching form in Gas and electricity Item*/
        private By gas_post_code_form_locator = By.Id("postcode_prices");
        private By gas_post_code_submit_button_locator = By.Id("find-prices-btn");

        /*Locators table elements*/
        private By gas_table_gas_unit_rate_locator = By.Id("gas-ur");
        private By gas_table_gas_standing_change_locator = By.Id("gas-sc");
        private By gas_table_electricity_unit_rate_locator = By.Id("el-ur");
        private By gas_table_electricity_standing_change_locator = By.Id("el-sc");

        private By divida_bill_sign_up_link_locator = By.CssSelector("#service-dividabill [href='/Account/SignUp']");
        private By tv_sign_up_link_locator = By.CssSelector("#collapseTV [href='/Account/SignUp']");

        public ServicesPage(IWebDriver driver)
            : base(driver)
        {
        }

        public void selectItem(String item)
        {
            pause(1000);
            if (item == dividabillItem)
            {
                click(divida_bill_item_locator);
            }
            else if (item == gasItem)
            {
                click(gas_and_electricity_item_locator);
            }
            else if (item == waterItem)
            {
                click(water_item_locator);
            }
            else if (item == internetlItem)
            {
                click(internet_item_locator);
            }
            else if (item == phoneItem)
            {
                click(landline_phone_item_locator);
            }
            else if (item == skyItem)
            {
                click(sky_item_locator);
            }
            else if (item == netflixItem)
            {
                click(netflix_item_locator);
            }
            else if (item == tvItem)
            {
                click(tvlicense_item_locator);
            }

            else throw new System.ArgumentException(itemError);
        }

        public void clickMoreInformationButton(String item)
        {

            if (item == dividabillItem)
            {
                click(divida_bill_more_information_button_locator);
            }
            else if (item == gasItem)
            {
                click(gas_more_information_button_locator);
            }
            else if (item == waterItem)
            {
                click(water_more_information_button_locator);
            }
            else if (item == internetlItem)
            {
                click(internet_more_information_button_locator);
            }
            else if (item == phoneItem)
            {
                click(phone_more_information_button_locator);
            }
            else if (item == skyItem)
            {
                click(sky_more_information_button_locator);
            }
            else if (item == netflixItem)
            {
                click(netflix_more_information_button_locator);
            }
            else if (item == tvItem)
            {
                click(tv_more_information_button_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public void clickMoreInformationCloseButton(String item)
        {
            if (item == dividabillItem)
            {
                click(divida_bill_more_information_close_button_locator);
            }
            else if (item == gasItem)
            {
                click(gas_more_information_close_button_locator);
            }
            else if (item == waterItem)
            {
                click(water_more_information_close_button_locator);
            }
            else if (item == internetlItem)
            {
                click(internet_more_information_close_button_locator);
            }
            else if (item == phoneItem)
            {
                click(phone_more_information_close_button_locator);
            }
            else if (item == skyItem)
            {
                click(sky_more_information_close_button_locator);
            }
            else if (item == netflixItem)
            {
                click(netflix_more_information_close_button_locator);
            }
            else if (item == tvItem)
            {
                click(tv_more_information_close_button_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public String getArialExpandedAttributeValue(String item)
        {
            String value = null;
            pause(2000);
            if (item == dividabillItem)
            {
                value = driver.FindElement(divida_bill_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == gasItem)
            {
                value = driver.FindElement(gas_and_electricity_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == waterItem)
            {
                value = driver.FindElement(water_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == internetlItem)
            {
                value = driver.FindElement(internet_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == phoneItem)
            {
                value = driver.FindElement(landline_phone_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == skyItem)
            {
                value = driver.FindElement(sky_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == netflixItem)
            {
                value = driver.FindElement(netflix_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == tvItem)
            {
                value = driver.FindElement(tvlicense_item_locator).GetAttribute("aria-expanded").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public Boolean isMoreInformationModalDialogDisplayed(String item)
        {
            By element = null;

            if (item == dividabillItem)
            {
                element = divida_bill_more_information_popup_locator;
            }
            else if (item == gasItem)
            {
                element = gas_more_information_popup_locator;
            }
            else if (item == waterItem)
            {
                element = water_more_information_popup_locator;
            }
            else if (item == internetlItem)
            {
                element = internet_more_information_popup_locator;
            }
            else if (item == phoneItem)
            {
                element = phone_more_information_popup_locator;
            }
            else if (item == skyItem)
            {
                element = sky_more_information_popup_locator;
            }
            else if (item == netflixItem)
            {
                element = netflix_more_information_popup_locator;
            }
            else if (item == tvItem)
            {
                element = tv_more_information_popup_locator;
            }
            else throw new System.ArgumentException(itemError);

            pause(1000);

            return isElementDisplayed(element);
        }

        public void typePostCodeInForm(String code)
        {
            waitUntilElementIsNotVisible(gas_post_code_form_locator);
            clearAndType(gas_post_code_form_locator, code);
        }

        public void clickSearch()
        {
            jsClick(gas_post_code_submit_button_locator);
            pause(5000);
        }

        public String checkSearch(String nameOfField)
        {
            String value = null;

            if (nameOfField == "GasUnitRate")
            {
                value = getTextAttribute(gas_table_gas_unit_rate_locator).ToString();

            }
            else if (nameOfField == "GasStandingChange")
            {
                value = getTextAttribute(gas_table_gas_standing_change_locator).ToString();

            }
            else if (nameOfField == "ElectricityUnitRate")
            {
                value = getTextAttribute(gas_table_electricity_unit_rate_locator).ToString();

            }
            else if (nameOfField == "ElectricityStandingChange")
            {
                value = getTextAttribute(gas_table_electricity_standing_change_locator).ToString();

            }
            return value;
        }

        public SignUpPage clickSignUpLink(String item)
        {
            if (item == dividabillItem)
            {
                waitUntilElementIsNotVisible(divida_bill_sign_up_link_locator);
                click(divida_bill_sign_up_link_locator);
                return new SignUpPage(driver);
            }
            else if (item == tvItem)
            {
                waitUntilElementIsNotVisible(tv_sign_up_link_locator);
                click(tv_sign_up_link_locator);
                return new SignUpPage(driver);
            }
            else throw new System.ArgumentException(itemError);
        }
    }
}
