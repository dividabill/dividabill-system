﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class AboutUsPage : AbstractPage
	{
        private By main_page_picture_locator = By.CssSelector(".img-square");
        private By email_link_locator = By.CssSelector("[href='mailto:support@dividabill.com']");

        private By award_first_first = By.CssSelector("[src*='evnt.jpg']");
        private By award_first_second = By.CssSelector("[src*='VPlogo.jpg']");
        private By award_first_third = By.CssSelector("[src*='loanscomp.jpg']");
        private By award_first_fourth = By.CssSelector("[class='col-sm-2']>[src*='entrepreneur.jpg']");
        private By award_second_first = By.CssSelector(".col-sm-offset-2>[src*='entrepreneur.jpg']");
        private By award_second_second = By.CssSelector("[src*='SUlogo.jpg']");
        private By award_second_third = By.CssSelector("[src*='logo-lloyds-enterprise-awards-print.png']");
        private By award_second_fourth = By.CssSelector("[src*='Setsquared-square.jpg']");

        private By portrett_first_locator = By.CssSelector(".jon>.onimg-items");
        private By portrett_second_locator = By.CssSelector(".patch>.onimg-items");
        private By portrett_third_locator = By.CssSelector(".gavin>.onimg-items");

        private By text_portrett_first_locator = By.CssSelector(".jon .onimg-text");
        private By text_portrett_second_locator = By.CssSelector(".patch .onimg-text");
        private By text_portrett_third_locator = By.CssSelector(".gavin .onimg-text");

        public AboutUsPage(IWebDriver driver)
            : base(driver)
        {
        }

        public String getSrcAttributeMainPicture()
        {
            return driver.FindElement(main_page_picture_locator).GetAttribute("src").ToString();
        }

        public String getClassAttributeAwardPictures(String item) 
        {
            String value = null;

            if (item.Contains("LEN Dragons"))
            {
                value = driver.FindElement(award_first_first).GetAttribute("class").ToString();
            }
            else if (item.Contains("Finalists of"))
            {
                value = driver.FindElement(award_first_second).GetAttribute("class").ToString();
            }
            else if (item.Contains("Semi-Finalists for"))
            {
                value = driver.FindElement(award_first_third).GetAttribute("class").ToString();
            }
            else if (item.Contains("Winners of"))
            {
                value = driver.FindElement(award_first_fourth).GetAttribute("class").ToString();
            }
            else if (item.Contains("Shell LiveWire"))
            {
                value = driver.FindElement(award_second_first).GetAttribute("class").ToString();
            }
            else if (item.Contains("Take off"))
            {
                value = driver.FindElement(award_second_second).GetAttribute("class").ToString();
            }
            else if (item.Contains("Lloyds Enterprise"))
            {
                value = driver.FindElement(award_second_third).GetAttribute("class").ToString();
            }
            else if (item.Contains("Best Graduate"))
            {
                value = driver.FindElement(award_second_fourth).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getTextAttributeOnPicture(String item) 
        {
            String value = null;

            if (item == "first")
            {
                mouseOver(portrett_first_locator);
                pause(500);
                value = getTextAttribute(text_portrett_first_locator);
            }
            else if (item == "second")
            {
                mouseOver(portrett_second_locator);
                pause(500);
                value = getTextAttribute(text_portrett_second_locator);
            }
            else if (item == "third")
            {
                mouseOver(portrett_third_locator);
                pause(500);
                value = getTextAttribute(text_portrett_third_locator);
            }
            else throw new System.ArgumentException(itemError);
            
            return value;
        }

        public String getHrefAttributeEmail()
        {
            return driver.FindElement(email_link_locator).GetAttribute("href").ToString();
        }
	}
}
