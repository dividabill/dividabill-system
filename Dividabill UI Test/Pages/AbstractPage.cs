﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumTesting;

namespace SeleniumTesting
{
 
    public class AbstractPage : Init
    {

        // Header
        private By headerLogoLocator = By.CssSelector(".logo>img");
        private By whyItWorksButtonLocator = By.CssSelector("[href='/Home/WhyItWorks']");
        private By servicesButtonLocator = By.CssSelector("[href='/Home/Services']");
        private By faqButtonLocator = By.CssSelector("[href='/Home/Questions']");
        private By aboutUsButtonLocator = By.CssSelector("[href='/Home/AboutUs']");
        private By contactUsButtonLocator = By.CssSelector("[href='/Home/ContactUs']");
        private By signUpButtonLocator = By.CssSelector("[href='/Account/SignUp']");

        // Footer
        private By footerLogoLocator = By.CssSelector(".footer img");
        private By whyItWorksFooterLinkLocator = By.CssSelector("[href='/Home/WhyitWorks']");
        private By servicesFooterLinkLocator = By.CssSelector(".footer [href='/Home/Services']");
        private By faqFooterLinkLocator = By.CssSelector(".footer [href='/Home/Questions']");
        private By aboutUsFooterLinkLocator = By.CssSelector(".footer [href='/Home/AboutUs']");
        private By contactUsFooterLinkLocator = By.CssSelector(".footer [href='/Home/ContactUs']");
        private By termsOfUseFooterLinkLocator = By.CssSelector("[href='/Home/TermsOfUse']");
        private By blogFooterLinkLocator = By.CssSelector("[href='http://blog.dividabill.co.uk']");
        private By privacyFooterLinkLocator = By.CssSelector("[href='/Home/Privacy']");

        private By learnButtonLocator = By.CssSelector(".home-button");

        private By mainPageTitleLocator = By.CssSelector("h1");
        private By pageTitleLocator = By.CssSelector("h2");

        private IWebDriver _driver;
        Common common = new Common();

        public AbstractPage(IWebDriver driver){
            _driver = driver;
            PageFactory.InitElements(_driver, this);
        }

        public HomePage openHomePage(String source)
        {
            if (source == header)
            {
                click(headerLogoLocator);
            }
            else if (source == footer)
            {
                click(footerLogoLocator);
            }
            else throw new System.ArgumentException(sourceError);
       
            return new HomePage(driver);
        }

        public WhyItWorksPage openWhyItWorksPage(String source)
        {
            if (source == header)
            {
                click(whyItWorksButtonLocator);
            }
            else if (source == footer)
            {
                click(whyItWorksFooterLinkLocator);
            }
            else if (source == "learn")
            {
                click(learnButtonLocator);
            }
            else throw new System.ArgumentException(sourceError);

            return new WhyItWorksPage(driver);
        }

        public ServicesPage openServicesPage(String source)
        {
            if (source == header)
            {
                click(servicesButtonLocator);
            }
            else if (source == footer)
            {
                click(servicesFooterLinkLocator);
            }
            else throw new System.ArgumentException(sourceError);

            return new ServicesPage(driver);
        }

        public FaqPage openFaqPage(String source)
        {
            if (source == header)
            {
                click(faqButtonLocator);
            }
            else if (source == footer)
            {
                click(faqFooterLinkLocator);
            }
            else throw new System.ArgumentException(sourceError);

            return new FaqPage(driver);
        }

        public AboutUsPage openAboutUsPage(String source)
        {
            if (source == header)
            {
                click(aboutUsButtonLocator);
            }
            else if (source == footer)
            {
                click(aboutUsFooterLinkLocator);
            }
            else throw new System.ArgumentException(sourceError);
           
            return new AboutUsPage(driver);
        }

        public ContactUsPage openContactUsPage(String source)
        {
            if (source == header)
            {
                click(contactUsButtonLocator);
            }
            else if (source == footer)
            {
                click(contactUsFooterLinkLocator);
            }
            else throw new System.ArgumentException(sourceError);
            
            return new ContactUsPage(driver);
        }

        public SignUpPage openSignUpPage()
        {
            click(signUpButtonLocator);
            return new SignUpPage(driver);
        }

        public TermsOfUsePage openTermsOfUsePage()
        {
            click(termsOfUseFooterLinkLocator);
            return new TermsOfUsePage(driver);
        }

        public BlogPage openBlogPage()
        {
            click(blogFooterLinkLocator);
            return new BlogPage(driver);
        }

        public PrivacyPolicyPage openPrivacyPolicyPage()
        {
            click(privacyFooterLinkLocator);
            return new PrivacyPolicyPage(driver);
        }

        public String getMainPageTitle()
        {
            return getTextAttribute(mainPageTitleLocator);
        }

        public String getPageTitle()
        {
            pause(500);
            return getTextAttribute(pageTitleLocator);
        }
    }
}
