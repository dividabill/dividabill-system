﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class FaqPage : AbstractPage
	{

        private By general_item_list_locator = By.CssSelector("[href='#collapseGeneral']");
        private By payments_item_list_locator = By.CssSelector("[href='#collapsePayments']");
        private By gas_item_list_locator = By.CssSelector("[href='#collapseGas']");
        private By water_item_list_locator = By.CssSelector("[href='#collapseWater']");
        private By electricity_item_list_locator = By.CssSelector("[href='#collapseElectricity']");
        private By internet_item_list_locator = By.CssSelector("[href='#collapseInternet']");
        private By tv_item_list_locator = By.CssSelector("[href='#collapseTV']");

        /*Accordion locators General*/
        private By phone_general_one_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingOne'] [href='tel:02380 223 596']");
        private By phone_general_seven_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingSeven'] [href='tel:02380223596']");
        private By phone_general_ten_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingTen'] [href='tel:02380 223 596']");
        private By phone_general_eleven_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingEleven'] [href='tel:02380 223 596']");
        private By phone_general_thirteen_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingThirteen'] [href='tel:02380 223 596']");
        private By phone_general_sixteen_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingSixteen'] [href='tel:02380 223 596']");

        private By email_general_one_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingOne'] [href='mailto:support@dividabill.co.uk']");
        private By email_general_seven_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingSeven'] [href='mailto:support@dividabill.co.uk']");
        private By email_general_ten_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingTen'] [href='mailto:support@dividabill.co.uk ']");
        private By email_general_eleven_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingEleven'] [href='mailto:support@dividabill.co.uk']");
        private By email_general_thirteen_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingThirteen'] [href='mailto:support@dividabill.co.uk']");
        private By email_general_sixteen_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingSixteen'] [href='mailto:support@dividabill.co.uk']");

        private By sign_up_general_eight_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingEight'] [href='https://www.dividabill.co.uk/Account/SignUp']");
        private By sign_up_general_eleven_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingEleven'] [href='http://www.dividabill.co.uk/Account/SignUp']");

        private By terms_of_use_ten_locator = By.CssSelector("#accordionGeneral [aria-labelledby='headingTen'] [href='/Home/TermsOfUse']");

        /*Accordion locators Payments*/
        private By phone_payments_four_locator = By.CssSelector("#accordionPayments [aria-labelledby='headingPaymentsFour'] [href='tel:02380223596']");

        /*Acordion Locators Gas*/
        private By phone_gas_six_locator = By.CssSelector("#accordionGas [aria-labelledby='headingGasSix'] [href='tel:02380223596']");

        /*Accordion Locators Water*/
        private By phone_water_four_locator = By.CssSelector("#accordionWater [aria-labelledby='headingWaterFour'] [href='tel:02380223596']");

        private By contuct_us_water_four_locator = By.CssSelector("#accordionWater [aria-labelledby='headingWaterFour'] [href='/Home/ContactUs']");

        /*Accordion Locators Electricity*/
        private By phone_electricity_four_locator = By.CssSelector("#accordionElectricity [aria-labelledby='headingElSix'] [href='tel:02380223596']");

        /*Accordion Locator Internet*/
        private By phone_internet_six_locator = By.CssSelector("#accordionInternet [aria-labelledby='headingInternetSix'] [href='tel:02380223596']");

        private By contuct_us_internet_six_locator = By.CssSelector("#accordionInternet [aria-labelledby='headingInternetSix'] [href='/Home/ContactUs']");

        /*Accordion Locator TV*/
        private By phone_tv_three_locator = By.CssSelector("#accordionTV [aria-labelledby='headingTVThree'] [href='tel:02380223596']");
        private By phone_tv_four_locator = By.CssSelector("#accordionTV [aria-labelledby='headingTVFour'] [href='tel:02380223596']");

        private By contuct_us_tv_three_locator = By.CssSelector("#accordionTV [aria-labelledby='headingTVThree'] [href='/Home/ContactUs']");
        private By contuct_us_tv_four_locator = By.CssSelector("#accordionTV [aria-labelledby='headingTVFour'] [href='/Home/ContactUs']");

		public FaqPage(IWebDriver driver) : base(driver){
        }

        public String getArialExpandedAttributeValue(String item)
        {
            String value = null;

            if (item == generalItem)
            {
                value = driver.FindElement(general_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == paymentsItem)
            {
                value = driver.FindElement(payments_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == gasItem)
            {
                value = driver.FindElement(gas_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == waterItem)
            {
                value = driver.FindElement(water_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == electricityItem)
            {
                value = driver.FindElement(electricity_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == internetlItem)
            {
                value = driver.FindElement(internet_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == tvItem)
            {
                value = driver.FindElement(tv_item_list_locator).GetAttribute("aria-expanded").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public void selectItem(String item)
        {
            if (item == generalItem)
            {
                click(general_item_list_locator);
            }
            else if (item == paymentsItem)
            {
                click(payments_item_list_locator);
            }
            else if (item == gasItem)
            {
                click(gas_item_list_locator);
            }
            else if (item == waterItem)
            {
                click(water_item_list_locator);
            }
            else if (item == electricityItem)
            {
                click(electricity_item_list_locator);
            }
            else if (item == internetlItem)
            {
                click(internet_item_list_locator);
            }
            else if (item == tvItem)
            {
                click(tv_item_list_locator);
            }
            else throw new System.ArgumentException(itemError);
            pause(2000);
        }

        public String getAriaExpendedAttributeAccrordion(String item, String listItem)
        {
            String value = null;
            if (item == generalItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='general']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == paymentsItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='payments']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == gasItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='gas']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == waterItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='water']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == electricityItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='electricity']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == internetlItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='internet']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else if (item == tvItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='tv']//p[contains(.,'" + listItem + "')]/..")).GetAttribute("aria-expanded").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public void selectAccordeionItem(String item, String listItem)
        {
            if (item == generalItem)
            {
                click(By.XPath("//*[@aria-labelledby='general']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == paymentsItem)
            {
                click(By.XPath("//*[@aria-labelledby='payments']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == gasItem)
            {
                click(By.XPath("//*[@aria-labelledby='gas']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == waterItem)
            {
                click(By.XPath("//*[@aria-labelledby='water']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == electricityItem)
            {
                click(By.XPath("//*[@aria-labelledby='electricity']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == internetlItem)
            {
                click(By.XPath("//*[@aria-labelledby='internet']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else if (item == tvItem)
            {
                click(By.XPath("//*[@aria-labelledby='tv']//p[contains(.,'" + listItem + "')]/.."));
                pause(500);
            }
            else throw new System.ArgumentException(itemError);
        }

        public void moveToAccordionElement(String item, String listItem)
        {
            IWebElement element;
            if (item == generalItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='general']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == paymentsItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='payments']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == gasItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='gas']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == waterItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='water']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == electricityItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='electricity']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == internetlItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='internet']//p[contains(.,'" + listItem + "')]/.."));
            }
            else if (item == tvItem)
            {
                element = driver.FindElement(By.XPath("//*[@aria-labelledby='tv']//p[contains(.,'" + listItem + "')]/.."));
                
            }
            else throw new System.ArgumentException(itemError);

            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public String getClassAttributeAccordionArrow(String item, String listItem)
        {
            String value = null;

            if (item == generalItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='general']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == paymentsItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='payments']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == gasItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='gas']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == waterItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='water']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == electricityItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='electricity']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == internetlItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='internet']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else if (item == tvItem)
            {
                value = driver.FindElement(By.XPath("//*[@aria-labelledby='tv']//p[contains(.,'" + listItem + "')]//i[@class]")).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getHrefAttributePhone(String item, String listItem)
        {
            String value = null;
            if ((item == generalItem) && (listItem == "How do I contact you?"))
            {
                value = driver.FindElement(phone_general_one_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "What services are offered?"))
            {
                value = driver.FindElement(phone_general_seven_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How long is the contract?"))
            {
                value = driver.FindElement(phone_general_ten_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "What happens if tenants move in or out during our contract?"))
            {
                value = driver.FindElement(phone_general_eleven_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How are our bills divided?"))
            {
                value = driver.FindElement(phone_general_thirteen_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How do I make a complaint?"))
            {
                value = driver.FindElement(phone_general_sixteen_locator).GetAttribute("href").ToString();
            }
            else if (item == paymentsItem)
            {
                value = driver.FindElement(phone_payments_four_locator).GetAttribute("href").ToString();
            }
            else if (item == gasItem)
            {
                value = driver.FindElement(phone_gas_six_locator).GetAttribute("href").ToString();
            }
            else if (item == waterItem)
            {
                value = driver.FindElement(phone_water_four_locator).GetAttribute("href").ToString();
            }
            else if (item == electricityItem)
            {
                value = driver.FindElement(phone_electricity_four_locator).GetAttribute("href").ToString();
            }
            else if (item == internetlItem)
            {
                value = driver.FindElement(phone_internet_six_locator).GetAttribute("href").ToString();
            }
            else if ((item == tvItem) && (listItem == "What happens if one of my services stops working?"))
            {
                value = driver.FindElement(phone_tv_three_locator).GetAttribute("href").ToString();
            }
            else if ((item == tvItem) && (listItem == "What do I do if there are problems with Internet or sky?"))
            {
                value = driver.FindElement(phone_tv_four_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;

        }

        public String getHrefAttributeEmail(String item, String listItem)
        {
            String value = null;
            if ((item == generalItem) && (listItem == "How do I contact you?"))
            {
                value = driver.FindElement(email_general_one_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "What services are offered?"))
            {
                value = driver.FindElement(email_general_seven_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How long is the contract?"))
            {
                value = driver.FindElement(email_general_ten_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "What happens if tenants move in or out during our contract?"))
            {
                value = driver.FindElement(email_general_eleven_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How are our bills divided?"))
            {
                value = driver.FindElement(email_general_thirteen_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "How do I make a complaint?"))
            {
                value = driver.FindElement(email_general_sixteen_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public String getHrefAttributeSignUp(String item, String listItem)
        {
            String value = null;
            if ((item == generalItem) && (listItem == "Can I change my services at any time?"))
            {
                value = driver.FindElement(sign_up_general_eight_locator).GetAttribute("href").ToString();
            }
            else if ((item == generalItem) && (listItem == "What happens if tenants move in or out during our contract?"))
            {
                value = driver.FindElement(sign_up_general_eleven_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public String getHrefAttributeContactUs(String item, String listItem)
        {
            String value = null;

            if ((item == waterItem) && (listItem == "What happens if one of my services stops working?"))
            {
                value = driver.FindElement(contuct_us_water_four_locator).GetAttribute("href").ToString();
            }
            else if ((item == internetlItem) && (listItem == "What happens if one of my services stops working?"))
            {
                value = driver.FindElement(contuct_us_internet_six_locator).GetAttribute("href").ToString();
            }
            else if ((item == tvItem) && (listItem == "What happens if one of my services stops working?"))
            {
                value = driver.FindElement(contuct_us_tv_three_locator).GetAttribute("href").ToString();
            }
            else if ((item == tvItem) && (listItem == "What do I do if there are problems with Internet or sky?"))
            {
                value = driver.FindElement(contuct_us_tv_four_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public String getHrefAtributeTermAboutUs()
        {
            return driver.FindElement(terms_of_use_ten_locator).GetAttribute("href").ToString();
        }
	}
}
