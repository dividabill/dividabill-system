﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class AboutUs : Init
    {

        private String firstImageSrc = "https://dividabill-dev.azurewebsites.net/Content/Images/v4/aboutus-bigimg.jpg";
        private String[] awardTitles = new String[] { "LEN Dragons Investment Event Winners", "Finalists of the NACUE Varsity Pitch", "Semi-Finalists for the Big Start Up Loans Competititon ", "Winners of the Shell LiveWire Grand Ideas Award", "Shell LiveWire Young Entrepreneur of the Year of contestant", "Take off Southampton Winners", "Lloyds Enterprise Awards London & South East Shortlist", "Best Graduate Enterprise " };
        private String aboutUsTitle = "About DividaBill";
        private String classAttributePattern = "img-circle awards align-button";
        private String[] itemList = new String[] { "first", "second", "third" };
        private String textInJonPicture = "\"Jon,\r\nDirector and\r\nHead of Operations\"";
        private String textInPatchPicture = "“Patch,\r\nDirector and\r\nHead of Commercial”";
        private String textInGavinPicture = "“Gavin,\r\nChairman”";
        private String email = "mailto:support@dividabill.com";

        [Test]
        public void Open_AboutUsPage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutPage = homePage.openAboutUsPage("header");

            Assert.IsTrue(aboutPage.getPageTitle().Contains(aboutUsTitle));
        }

        [Test]
        public void Open_AboutUsPage_FromFooter()
        {

            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutPage = homePage.openAboutUsPage("footer");

            Assert.IsTrue(aboutPage.getPageTitle().Contains(aboutUsTitle));
        }

        [Test]
        public void MainPicture()
        {
            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutUs = homePage.openAboutUsPage(header);

            Assert.AreEqual(firstImageSrc, aboutUs.getSrcAttributeMainPicture());
        }

        [Test]
        public void AwardPictureChecker()
        {
            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutUs = homePage.openAboutUsPage(header);

            foreach (String item in awardTitles)
            {
                Assert.AreEqual(classAttributePattern, aboutUs.getClassAttributeAwardPictures(item));
            }

        }

        [Test]
        public void MouseOverPortraits()
        {
            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutUs = homePage.openAboutUsPage(header);

            Assert.AreEqual(textInJonPicture, aboutUs.getTextAttributeOnPicture(itemList[0]));
            Assert.AreEqual(textInPatchPicture, aboutUs.getTextAttributeOnPicture(itemList[1]));
            Assert.AreEqual(textInGavinPicture, aboutUs.getTextAttributeOnPicture(itemList[2]));
        }

        [Test]
        public void EmailLinkChecker()
        {
            HomePage homePage = new HomePage(driver);
            AboutUsPage aboutUs = homePage.openAboutUsPage(header);

            Assert.AreEqual(email, aboutUs.getHrefAttributeEmail());
        }
    }
}