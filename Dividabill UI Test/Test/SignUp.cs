﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class SignUp : Init
    {
        private String validPostCode = "SO17 1QG";
        private String[] nameDropDownLists = new String[]{"building", "tenats", "end date", "start date"};
        private String[] nameOfSteps = new String[] { "first", "second", "third" };
        private String disactiveSteps = "img-circle rounded";
        private String[] nameOfField = new String[] { "Selected address","City","Country","Selected address 2"};
        private String[] inputTextFields = new String[] { "18 Roselands Gardens", "Southampton", "Hampshire", "" };
        private String[] nameServices = new String[] {"electricity","gas","water","internet","tv","phone"};
        private String timeString = "15/08/2015";
        private String address = "18 Roselands Gardens, Southampton";
        private String house = "house";
        private String person = "person";
        private String[] nameOfRadioButtonsAndCheckBoxes = new String[] { "internet basic", "internet home", "tv sky", "tv netflix", "tv license", "phone basic", "phone medium" };
        private String[] idOfRadiobuttonsAndCheckBoxes = new String[] { "BroadbandTypeADSL20", "BroadbandTypeFibre40", "SkyTV", "NetflixTV", "TVLicense", "LandlineTypeBasic", "LandlineTypeMedium" };
        private String[] ratesOfServices = new String[] { "electricity rate", "gas rate", "water rate","internet base rate", "internet home rate", "tv sky rate", "tv netflix rate", "tv license rate", "phone basic rate", "phone medium rate", "gas massage"};
        [Test]
        public void Open_SignUpPage()
        {
            String signUpTitle = "House Information";

            HomePage homePage = new HomePage(driver);
            SignUpPage signUpPage = homePage.openSignUpPage();

            Assert.IsTrue(signUpPage.getPageTitle().Contains(signUpTitle));
        }

        [Test]
        public void FirstStep() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.getClassAttributeSteps(nameOfSteps[0]).Contains("active-step"));
        }

        [Test]
        public void Open_Buildings_DropDownList() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);

            Assert.AreEqual("true", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[0]));

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);

            Assert.AreEqual("false", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[0]));
        }

        [Test]
        public void CheckTextInputFields() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());
            Assert.AreEqual(inputTextFields[0], signUpPage.getFieldsDataValueAttribute(nameOfField[0]));
            Assert.AreEqual(inputTextFields[1], signUpPage.getFieldsDataValueAttribute(nameOfField[1]));
            Assert.AreEqual(inputTextFields[2], signUpPage.getFieldsDataValueAttribute(nameOfField[2]));
            Assert.AreEqual(inputTextFields[3], signUpPage.getFieldsDataValueAttribute(nameOfField[3]));
        }

        [Test]
        public void Open_Tanats_DropDownList()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);
            signUpPage.clickOnDropDownList(nameDropDownLists[1]);

            Assert.AreEqual("true", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[1]));

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);

            Assert.AreEqual("false", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[1]));
        }

        [Test]
        public void Open_EndDate_DropDownList()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);

            Assert.AreEqual("true", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[2]));

            signUpPage.clickOnDropDownList(nameDropDownLists[2]);

            Assert.AreEqual("false", signUpPage.getAriaExpandedAttributeDropDownList(nameDropDownLists[2]));
        }

        [Test]
        public void Check_OngoinCheckBox() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsFalse(signUpPage.getClassAttributeEndDate().Contains("disabled"));

            signUpPage.clickOngoingCheckBox();

            Assert.IsTrue(signUpPage.getClassAttributeEndDate().Contains("disabled"));
        }

        [Test]
        public void Open_StartDateDropDownList() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();
            DateTime currentTime = new DateTime();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);
            signUpPage.clickOnDropDownList(nameDropDownLists[3]);
            signUpPage.typeDayMounthYear(timeString);    
        }

        [Test]
        public void Click_Services() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);
            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "1");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");

            signUpPage.moveToElement();

            Assert.IsFalse(signUpPage.getClassAttributeServices(nameServices[0]).Contains("active"));

            signUpPage.clickServices(nameServices[0]);

            Assert.IsTrue(signUpPage.getClassAttributeServices(nameServices[0]).Contains("active"));

            signUpPage.clickServices(nameServices[0]);

            Assert.IsFalse(signUpPage.getClassAttributeServices(nameServices[0]).Contains("active"));

            signUpPage.clickServices(nameServices[0]);

            for (int i = 1; i < nameServices.Length; i++)
            {
                Assert.IsFalse(signUpPage.getClassAttributeServices(nameServices[i]).Contains("active"));

                signUpPage.clickServices(nameServices[i]);

                Assert.IsTrue(signUpPage.getClassAttributeServices(nameServices[i]).Contains("active"));

                signUpPage.clickServices(nameServices[i]);

                Assert.IsFalse(signUpPage.getClassAttributeServices(nameServices[i]).Contains("active"));
            }
        }

        [Test]
        public void Check_ElectricityBill() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[0]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£4.48", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£2.24", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_GasAndElectricityBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[0]);
            signUpPage.clickServices(nameServices[1]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£11.48", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£5.74", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_WaterBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[2]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£6.88", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£3.44", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_InternetBasicBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[3]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[0]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[0]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£8.77", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£4.39", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_InternetHomeBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[3]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[1]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[1]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£10.71", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£5.36", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_TvSkyBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[4]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[2]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[2]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£15.09", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£7.55", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_TvNetflixBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[4]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[3]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[3]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£2.07", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£1.04", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_TvLicenseBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[4]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[4]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[4]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£2.79", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£1.39", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_PhoneBasicBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[5]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[5]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[5]);
            
            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£4.19", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£2.10", signUpPage.getValueAttributeBill(person));
        }

        [Test]
        public void Check_PhoneMediumBill()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[5]);

            Assert.IsTrue(signUpPage.isRadioButtonOrCheckBoxPresent(nameOfRadioButtonsAndCheckBoxes[6]));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[6]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£4.85", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£2.42", signUpPage.getValueAttributeBill(person));
        }

        //[Test]
        public void Check_InternetTvPhoneBill() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[3]);
            signUpPage.clickServices(nameServices[4]);
            signUpPage.clickServices(nameServices[5]);

            Assert.IsTrue(signUpPage.isInputFieldsDisplayed());
            Assert.AreEqual("£0.0", signUpPage.getValueAttributeBill(house));
            Assert.AreEqual("£0.0", signUpPage.getValueAttributeBill(person));
        }

        //[Test]
        public void Check_GasMassage() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[1]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[10]).Contains("Sorry, you can't choose gas service"));
            Assert.IsFalse(signUpPage.isInputFieldsPresent());
        }

        //[Test]
        public void Check_ElectricityGasWaterRates() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[0]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[0]).Contains("2.24"));

            signUpPage.clickServices(nameServices[1]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[1]).Contains("3.50"));

            signUpPage.clickServices(nameServices[2]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[2]).Contains("3.44"));
        }
        
        //[Test]
        public void Check_InternetRates() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(validPostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[0]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[0], address);

            Assert.IsTrue(signUpPage.IsTextFieldPresent());

            signUpPage.clickOnDropDownList(nameDropDownLists[1]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[1], "2");
            signUpPage.clickOnDropDownList(nameDropDownLists[2]);
            signUpPage.clickElementOfDropDownList(nameDropDownLists[2], "9 Months");
            signUpPage.moveToElement();
            signUpPage.clickServices(nameServices[3]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[3]).Contains("0.0"));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[0]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[3]).Contains("4.39"));

            signUpPage.clickRadioButtonAndCheckBox(nameOfRadioButtonsAndCheckBoxes[1]);

            Assert.IsTrue(signUpPage.getTextAttribute(ratesOfServices[3]).Contains("5.36"));
        }
	}
}
