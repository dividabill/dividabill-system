﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class Home : Init
    {
        private String homeTitle = "We Simplify Your Life";
        private String postcode = "SO17 1QG";
        private String specialCharacters = "/*/*/*";
        private String spacePostCode = " ";
        private String firstCharSpacePostCode = " SO17 1QG";
        private String lastCharSpacePostCode = "SO17 1QG ";
        private String[] numberOfSlide = new String[] {"0","1","2","3","4","5","6"};
        private String[] imageSrcAttributePatterns = new String[] { "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/JadeRef.jpg?width=100&height=100&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/JoRegUniPortsmouth.jpg?width=200&height=200&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/ValRef.jpg?width=200&height=200&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/JoshRef.jpg?width=200&height=200&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/RachNaRef.jpg?width=200&height=200&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/AlexRefUniBristol.jpg?width=200&height=200&mode=max", "https://az754941.vo.msecnd.net/Content/Images/v4/Ref/GeorgieRef.jpg?width=200&height=200&mode=max" };
        private String[] caroselSrcPatterns = new String[] { "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/anglian%20water.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/British_Gas_logo.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/BT.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/GB%20energy%20.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Go-Cardless-1.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Netflix.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Northumbrian-Water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/scottish%20water.gif?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/severn%20trent.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/sky.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/south%20west%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/southern%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/thames%20water.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/TV%20licensing.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/united_utilities_logo.gif?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/welshwater-logo.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/wessex%20water.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/yorkshire%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/enactushqlogo.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/nasma.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/student%20loans.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Student%20money%20saver.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/student%20pad.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/the%20university%20paper.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Bournemouth-University-Logo.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/BSU-logo---generic-Cropped-transparent-233x317.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Portsmouth%20su.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Solent-LEP.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/SolentSU.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/susu.jpg?width=112" };
        private String activeStatement = "active";
        private String disactiveStatement = "";
        private String activeStatementCaroselList = "item item-back active";
        private String disactiveStatementCaroselList = "item item-back";
        private String[] hrefsArray = new String[] { "https://www.linkedin.com/company/dividabill", "http://twitter.com/dividabill/", "http://facebook.com/dividabill/", "https://plus.google.com/117719664197593106714/about", "tel:02380223596", "mailto:support@dividabill.co.uk" };
        private String[] invalidPostCodeArrayItems = new String[] { ".SO17 1QG", "SO17 1QG.", ".SO17 1QG.", "/*////SO17 1QG", "   SO17 1QG", "SO17 1QG         ", "SO17    1QG" };
        private String[] partnersCarosel = new String[] { "first", "second", "third"};
        private String[] whoWorkWithUsCarosel = new String[] { "first", "second"};
        [Test]
        public void Open_HomePage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage("header");
            homePage = faqPage.openHomePage("header");

            Assert.IsTrue(homePage.getPageTitle().Contains(homeTitle));
        }

        [Test]
        public void Open_HomePage_FromFooter()
        {

            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage("header");
            homePage = faqPage.openHomePage("footer");

            Assert.IsTrue(homePage.getPageTitle().Contains(homeTitle));
        }

        [Test]
        public void Search_ValidPostcode()
        {

            HomePage homePage = new HomePage(driver);
            homePage.fillPostcodeField(postcode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());
        }

        [Test]
        public void Search_ValidPostcode_ViaEnterButton()
        {

            HomePage homePage = new HomePage(driver);
            homePage.fillPostcodeField(postcode);
            SignUpPage signUpPage = homePage.clickEnterButton();

            Assert.IsTrue(signUpPage.isSelectBuildingDropdownPresent());
        }

        [Test]
        public void Search_EmptyPostcode()
        {

            HomePage homePage = new HomePage(driver);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsFalse(signUpPage.isBuildingDropdownPresent());
        }

        [Test]
        public void Open_WhyItWorksPage_UsingLearnButton()
        {
            String whyItWorksPageTitle = "Why it Works";

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage("learn");

            Assert.IsTrue(whyPage.getPageTitle().Contains(whyItWorksPageTitle));
        }

        [Test]
        public void Open_TermsOfUsePage()
        {
            String pageTitle = "Terms of Use";

            HomePage homePage = new HomePage(driver);
            TermsOfUsePage termsOfUsePage = homePage.openTermsOfUsePage();

            Assert.IsTrue(termsOfUsePage.getMainPageTitle().Contains(pageTitle));
        }

        [Test]
        public void Open_BlogPage()
        {

            HomePage homePage = new HomePage(driver);
            BlogPage blogPage = homePage.openBlogPage();

            Assert.IsTrue(blogPage.isTitleDisplayed());
        }

        [Test]
        public void Open_PrivacyPolicyPage()
        {
            String pageTitle = "Privacy Policy";

            HomePage homePage = new HomePage(driver);
            PrivacyPolicyPage privacyPage = homePage.openPrivacyPolicyPage();

            Assert.IsTrue(privacyPage.getMainPageTitle().Contains(pageTitle));
        }

        [Test]
        public void Slide_Clicker() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.clickToSlideElements(numberOfSlide[1]);

            foreach(String item in numberOfSlide)
            {
                Assert.AreEqual(disactiveStatement, homePage.getClassAttribute(item));

                homePage.clickToSlideElements(item);

                Assert.AreEqual(activeStatement, homePage.getClassAttribute(item));
            }
        }

        [Test]
        public void SlideGetSrcAttribute() 
        {
            HomePage homePage = new HomePage(driver);
            
            Assert.AreEqual(imageSrcAttributePatterns[0], homePage.getSrcAttribute(numberOfSlide[0]));
            Assert.AreEqual(imageSrcAttributePatterns[1], homePage.getSrcAttribute(numberOfSlide[1]));
            Assert.AreEqual(imageSrcAttributePatterns[2], homePage.getSrcAttribute(numberOfSlide[2]));
            Assert.AreEqual(imageSrcAttributePatterns[3], homePage.getSrcAttribute(numberOfSlide[3]));
            Assert.AreEqual(imageSrcAttributePatterns[4], homePage.getSrcAttribute(numberOfSlide[4]));
            Assert.AreEqual(imageSrcAttributePatterns[5], homePage.getSrcAttribute(numberOfSlide[5]));
            Assert.AreEqual(imageSrcAttributePatterns[6], homePage.getSrcAttribute(numberOfSlide[6]));
        }

        [Test]
        public void CaroselGetSrcAttribute() 
        {
            HomePage homePage = new HomePage(driver);
            foreach (String item in caroselSrcPatterns)
            {
                Assert.AreEqual(item, homePage.getSrcAttributeCarosel(item));
            }
        }

        [Test]
        public void FooterLinksChecker() 
        {
            HomePage homePage = new HomePage(driver);
            foreach(String item in hrefsArray)
            {
                Assert.AreEqual(item, homePage.getHrefAttributeFooter(item));
            }
        }
       
        [Test]
        public void SpecialCharectersPostcode()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(specialCharacters);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSpinnerPresent());
            Assert.IsFalse(signUpPage.isBuildingDropdownPresent());          
        }

        [Test]
        public void SpacePostcode()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(spacePostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSpinnerPresent());
            Assert.IsFalse(signUpPage.isBuildingDropdownPresent());
        }

        [Test]
        public void FirstCharacterSpacePostCode()
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(firstCharSpacePostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.True(signUpPage.isSpinnerPresent());
            Assert.IsFalse(signUpPage.isBuildingDropdownPresent());
        }
        
        [Test]
        public void LastCharecterSpacePostCode() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.typeInvalidPostCode(lastCharSpacePostCode);
            SignUpPage signUpPage = homePage.clickGetQuoteButton();

            Assert.IsTrue(signUpPage.isSpinnerPresent());
            Assert.IsFalse(signUpPage.isBuildingDropdownPresent());
        }

        [Test]
        public void FirstAndLastCharacterOfPostCode_AreSpacesOrCharacters() 
        {
            HomePage homePage = new HomePage(driver);
            foreach(String item in invalidPostCodeArrayItems)
            {
                homePage.typeInvalidPostCode(item);
                SignUpPage signUpPage = homePage.clickGetQuoteButton();

                signUpPage.clickAlertErrorInvalidPostCode();

                Assert.IsTrue(signUpPage.isSpinnerPresent());
                Assert.IsFalse(signUpPage.isBuildingDropdownPresent());

                
                signUpPage.openHomePage(header);
            }  
        }

        [Test]
        public void CheckAutomationSlider() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.clickToSlideElements(numberOfSlide[0]);

            foreach(String item in numberOfSlide)
            {
                Assert.AreEqual(activeStatement, homePage.getClassAttribute(item));
                pause(5000);
            }
        }

        [Test]
        public void CheckAutomation_Partners_Carosel() 
        {
            HomePage homePage = new HomePage(driver);
            homePage.moveToCarosel();

            foreach (String item in partnersCarosel)
            {
                Assert.IsTrue(homePage.getClassAttributePartnersCarosel(item).Contains("active"));
                pause(5000);
            }
        }

        [Test]
        public void CheckAutomation_WhoWorksWithUs_Carosel()
        {
            HomePage homePage = new HomePage(driver);
            homePage.moveToCarosel();

            foreach (String item in whoWorkWithUsCarosel)
            {
                Assert.IsTrue(homePage.getClassAttributeWhoWorkWithUsCarosel(item).Contains("active"));
                pause(5000);
            }
        }
	}
}