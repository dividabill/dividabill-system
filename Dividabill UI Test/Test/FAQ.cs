﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class FAQ : Init
    {

        private String faqTitle = "FAQ Sections";
        private String[] generalListAccordion = new String[] { "How do I contact you?", "Why should I choose DividaBill?", "OK, so how does DividaBill work in detail?", "How does the first bill work?", "How much can I save through using DividaBill?", "How much does DividaBill cost to use?", "What services are offered?", "Can I change my services at any time?", "How do I sign up to DividaBill?", "How long is the contract?", "What happens if tenants move in or out during our contract?", "What is the online portal?", "How are our bills divided?", "Do I need to contact our previous providers to cancel our old contracts?", "What happens if I am already tied in to a contract with a supplier?", "How do I make a complaint?", "How do I refer a friend?", "How much discount do I receive for making a referral?" };
        private String[] paymentListAccordion = new String[] { "How does the first bill work? ", "How do payments work?", "When do payments occur?", "From whose account does the payment come out of?", "How do I receive my bills?", "What happens if my housemate does not pay?", "Will my monthly payment for bills vary each month?" };
        private String[] gasListAccordion = new String[] { "How do I submit meter readings?", "How do I find my meter?", "How do I read my meter?", "How can I manage my usage?", "What should I do if I am still receiving bills from previous suppliers?", "What happens if one of my services stops working?", "What if I have received an energy bill from another supplier?", "The estimate bill seems quite high/low", "What if we have a pre-payment meter?" };
        private String[] waterListAccordion = new String[] { "How can I manage my usage?", "How do you calculate my water charge?", "What should I do if I am still receiving bills from previous suppliers?", "What happens if one of my services stops working?", "The estimate bill seems quite high/low" };
        private String[] electricityListAccordion = new String[] { "How do I submit meter readings?", "How do I find my meter?", "How do I read my meter?", "How can I manage my usage?", "What should I do if I am still receiving bills from previous suppliers?", "What happens if one of my services stops working?", "What if I have received an energy bill from another supplier?", "The estimate bill seems quite high/low", "What if we have a pre-payment meter?" };
        private String[] internetListAccordion = new String[] { "How fast is my broadband?", "Is my broadband unlimited?", "What does DividaBill broadband offer?", "Is set up, installation and equipment free of charge?", "What should I do if I am still receiving bills from previous suppliers?", "What happens if one of my services stops working?" };
        private String[] tvListAccordion = new String[] { "Is set up, installation and equipment free of charge?", "What should I do if I am still receiving bills from previous suppliers?", "What happens if one of my services stops working?", "What do I do if there are problems with Internet or sky?", "How does my TV license work", "What does Netflix give me", "What Sky packages do I get?" };
        private String phoneNumber_1 = "tel:02380 223 596";
        private String phoneNumber_2 = "tel:02380223596";
        private String email = "mailto:support@dividabill.co.uk";
        private String signUpHTTPS = "https://www.dividabill.co.uk/Account/SignUp";
        private String signUpHTTP = "http://www.dividabill.co.uk/Account/SignUp";
        private String contuctUsHTTPS = "https://dividabill-dev.azurewebsites.net/Home/ContactUs";
        private String termAboutUs = "https://dividabill-dev.azurewebsites.net/Home/TermsOfUse";

        [Test]
        public void Open_FaqPage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage("header");

            Assert.IsTrue(faqPage.getPageTitle().Contains(faqTitle));
        }

        [Test]
        public void Open_FaqPage_FromFooter()
        {

            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage("footer");

            Assert.IsTrue(faqPage.getPageTitle().Contains(faqTitle));
        }

        [Test]
        public void LeftSidebar_General_Item()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("true", faqPage.getArialExpandedAttributeValue(generalItem));
        }

        [Test]
        public void General_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("true", faqPage.getArialExpandedAttributeValue(generalItem));
            Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(generalItem, generalListAccordion[0]));

            faqPage.selectAccordeionItem(generalItem, generalListAccordion[0]);

            foreach (String listItem in generalListAccordion)
            {
                faqPage.moveToAccordionElement(generalItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(generalItem, listItem));

                faqPage.selectAccordeionItem(generalItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(generalItem, listItem));

                faqPage.selectAccordeionItem(generalItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(generalItem, listItem));
            }
        }

        [Test]
        public void Payment_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(paymentsItem));

            faqPage.selectItem(paymentsItem);

            foreach (String listItem in paymentListAccordion)
            {
                faqPage.moveToAccordionElement(paymentsItem, listItem);
                
                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(paymentsItem, listItem));

                faqPage.selectAccordeionItem(paymentsItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(paymentsItem, listItem));

                faqPage.selectAccordeionItem(paymentsItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(paymentsItem, listItem));
            }
        }

        [Test]
        public void Gas_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(gasItem));

            faqPage.selectItem(gasItem);

            foreach (String listItem in gasListAccordion)
            {
                faqPage.moveToAccordionElement(gasItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(gasItem, listItem));

                faqPage.selectAccordeionItem(gasItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(gasItem, listItem));

                faqPage.selectAccordeionItem(gasItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(gasItem, listItem));
            }
        }

        [Test]
        public void Water_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(waterItem));

            faqPage.selectItem(waterItem);

            foreach (String listItem in waterListAccordion)
            {
                faqPage.moveToAccordionElement(waterItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(waterItem, listItem));

                faqPage.selectAccordeionItem(waterItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(waterItem, listItem));

                faqPage.selectAccordeionItem(waterItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(waterItem, listItem));
            }
        }

        [Test]
        public void Electricity_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(electricityItem));

            faqPage.selectItem(electricityItem);

            foreach (String listItem in electricityListAccordion)
            {
                faqPage.moveToAccordionElement(electricityItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(electricityItem, listItem));

                faqPage.selectAccordeionItem(electricityItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(electricityItem, listItem));

                faqPage.selectAccordeionItem(electricityItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(electricityItem, listItem));
            }
        }

        [Test]
        public void Internet_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(internetlItem));

            faqPage.selectItem(internetlItem);

            foreach (String listItem in internetListAccordion)
            {
                faqPage.moveToAccordionElement(internetlItem, listItem);
                pause(500);
                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(internetlItem, listItem));

                faqPage.selectAccordeionItem(internetlItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(internetlItem, listItem));

                faqPage.selectAccordeionItem(internetlItem, listItem);
                pause(500);
                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(internetlItem, listItem));
            }
        }

        [Test]
        public void Tv_Accordion_List_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(tvItem));

            faqPage.selectItem(tvItem);

            foreach (String listItem in tvListAccordion)
            {
                faqPage.moveToAccordionElement(tvItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(tvItem, listItem));

                faqPage.selectAccordeionItem(tvItem, listItem);

                Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(tvItem, listItem));

                faqPage.selectAccordeionItem(tvItem, listItem);

                Assert.AreEqual("false", faqPage.getAriaExpendedAttributeAccrordion(tvItem, listItem));
            }

        }
            
        [Test]
        public void General_Accordion_Content_Checker() 
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("true", faqPage.getArialExpandedAttributeValue(generalItem));
            Assert.AreEqual("true", faqPage.getAriaExpendedAttributeAccrordion(generalItem, generalListAccordion[0]));

            Assert.AreEqual("fa fa-caret-up", faqPage.getClassAttributeAccordionArrow(generalItem, generalListAccordion[0]));

            faqPage.selectAccordeionItem(generalItem, generalListAccordion[0]);

            Assert.AreEqual("fa fa-caret-down", faqPage.getClassAttributeAccordionArrow(generalItem, generalListAccordion[0]));

            faqPage.selectAccordeionItem(generalItem, generalListAccordion[0]);

            Assert.AreEqual(phoneNumber_1, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[0]));
            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[6]));
            Assert.AreEqual(phoneNumber_1, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[9]));
            Assert.AreEqual(phoneNumber_1, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[10]));
            Assert.AreEqual(phoneNumber_1, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[12]));
            Assert.AreEqual(phoneNumber_1, faqPage.getHrefAttributePhone(generalItem, generalListAccordion[15]));
            
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[0]));
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[6]));
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[9]));
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[10]));
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[12]));
            Assert.AreEqual(email, faqPage.getHrefAttributeEmail(generalItem, generalListAccordion[15]));

            Assert.AreEqual(signUpHTTPS, faqPage.getHrefAttributeSignUp(generalItem, generalListAccordion[7]));
            Assert.AreEqual(signUpHTTP, faqPage.getHrefAttributeSignUp(generalItem, generalListAccordion[10]));

            Assert.AreEqual(termAboutUs, faqPage.getHrefAtributeTermAboutUs());
        }

        [Test]
        public void Payments_Accordion_Content_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(paymentsItem));
            
            faqPage.selectItem(paymentsItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(paymentsItem, paymentListAccordion[3]));
        }

        [Test]
        public void Gas_AccordionContent_Checker() 
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(gasItem));

            faqPage.selectItem(gasItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(gasItem, gasListAccordion[5]));
        }

        [Test]
        public void Water_AccordionContent_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(waterItem));

            faqPage.selectItem(waterItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(waterItem, waterListAccordion[3]));
            Assert.AreEqual(contuctUsHTTPS, faqPage.getHrefAttributeContactUs(waterItem, waterListAccordion[3]));
        }

        [Test]
        public void Electricity_AccordionContent_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(electricityItem));

            faqPage.selectItem(electricityItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(electricityItem, electricityListAccordion[3]));
        }

        [Test]
        public void Internet_AccordionContent_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(internetlItem));

            faqPage.selectItem(internetlItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(internetlItem, internetListAccordion[5]));
            Assert.AreEqual(contuctUsHTTPS, faqPage.getHrefAttributeContactUs(internetlItem, internetListAccordion[5]));
        }

        [Test]
        public void TV_AccordionContent_Checker()
        {
            HomePage homePage = new HomePage(driver);
            FaqPage faqPage = homePage.openFaqPage(header);

            Assert.AreEqual("false", faqPage.getArialExpandedAttributeValue(tvItem));

            faqPage.selectItem(tvItem);

            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(tvItem, tvListAccordion[2]));
            Assert.AreEqual(phoneNumber_2, faqPage.getHrefAttributePhone(tvItem, tvListAccordion[3]));
            Assert.AreEqual(contuctUsHTTPS, faqPage.getHrefAttributeContactUs(tvItem, tvListAccordion[2]));
            Assert.AreEqual(contuctUsHTTPS, faqPage.getHrefAttributeContactUs(tvItem, tvListAccordion[3]));
        }

	}
}
