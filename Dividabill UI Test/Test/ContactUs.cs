﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class ContactUs : Init
    {
        private String[] socialWebItemNames = new String[] { "Facebook", "Twitter", "Write us an email", "Call us", "Live Chat" };
        private String[] socialWebItemHrefAttributePatternArray = new String[] { "http://facebook.com/dividabill", "http://twitter.com/dividabill", "mailto:support@dividabill.co.uk", "tel:02380223596", "javascript:void(0);" };
        private String[] socialWebItemTextAttributePatternArray = new String[] { "Facebook\r\nLike us", "Twitter\r\nFollow us", "Write us an email\r\nsupport@dividabill.co.uk", "Call us\r\n02380223596", "Live Chat\r\nChat with us!" };
        private String[] socialWebItemSrcAttributePatternArray = new String[] { "https://dividabill-dev.azurewebsites.net/Content/Images/v4/contact/FBIcon.png", "https://dividabill-dev.azurewebsites.net/Content/Images/v4/contact/TwitterIcon.png", "https://dividabill-dev.azurewebsites.net/Content/Images/v4/contact/EmailIcon.png", "https://dividabill-dev.azurewebsites.net/Content/Images/v4/contact/PhoneIcon.png", "https://dividabill-dev.azurewebsites.net/Content/Images/v4/contact/LiveChatIcon.png" };
        private String contactUsTitle = "Contact Us";

        [Test]
        public void Open_ContactUsPage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            ContactUsPage contactPage = homePage.openContactUsPage("header");

            Assert.IsTrue(contactPage.getPageTitle().Contains(contactUsTitle));
        }

        [Test]
        public void Open_ContactUsPage_FromFooter()
        {

            HomePage homePage = new HomePage(driver);
            ContactUsPage contactPage = homePage.openContactUsPage("footer");

            Assert.IsTrue(contactPage.getPageTitle().Contains(contactUsTitle));
        }

        [Test]
        public void CheckSrcAttributeSocialWeb() 
        {
            HomePage homePage = new HomePage(driver);
            ContactUsPage contustUsPage = homePage.openContactUsPage(header);

            Assert.AreEqual(socialWebItemSrcAttributePatternArray[0], contustUsPage.getSrcAttribute(socialWebItemNames[0]));
            Assert.AreEqual(socialWebItemSrcAttributePatternArray[1], contustUsPage.getSrcAttribute(socialWebItemNames[1]));
            Assert.AreEqual(socialWebItemSrcAttributePatternArray[2], contustUsPage.getSrcAttribute(socialWebItemNames[2]));
            Assert.AreEqual(socialWebItemSrcAttributePatternArray[3], contustUsPage.getSrcAttribute(socialWebItemNames[3]));
            Assert.AreEqual(socialWebItemSrcAttributePatternArray[4], contustUsPage.getSrcAttribute(socialWebItemNames[4]));
        }

        [Test]
        public void CheckHrefAttributeSocialWeb()
        {
            HomePage homePage = new HomePage(driver);
            ContactUsPage contustUsPage = homePage.openContactUsPage(header);

            Assert.AreEqual(socialWebItemHrefAttributePatternArray[0], contustUsPage.getHrefAttribute(socialWebItemNames[0]));
            Assert.AreEqual(socialWebItemHrefAttributePatternArray[1], contustUsPage.getHrefAttribute(socialWebItemNames[1]));
            Assert.AreEqual(socialWebItemHrefAttributePatternArray[2], contustUsPage.getHrefAttribute(socialWebItemNames[2]));
            Assert.AreEqual(socialWebItemHrefAttributePatternArray[3], contustUsPage.getHrefAttribute(socialWebItemNames[3]));
            Assert.AreEqual(socialWebItemHrefAttributePatternArray[4], contustUsPage.getHrefAttribute(socialWebItemNames[4]));
        }

        [Test]
        public void CheckTextAttributeSocialWeb()
        {
            HomePage homePage = new HomePage(driver);
            ContactUsPage contustUsPage = homePage.openContactUsPage(header);

            Assert.AreEqual(socialWebItemTextAttributePatternArray[0], contustUsPage.getTextAttribute(socialWebItemNames[0]));
            Assert.AreEqual(socialWebItemTextAttributePatternArray[1], contustUsPage.getTextAttribute(socialWebItemNames[1]));
            Assert.AreEqual(socialWebItemTextAttributePatternArray[2], contustUsPage.getTextAttribute(socialWebItemNames[2]));
            Assert.AreEqual(socialWebItemTextAttributePatternArray[3], contustUsPage.getTextAttribute(socialWebItemNames[3]));
            Assert.AreEqual(socialWebItemTextAttributePatternArray[4], contustUsPage.getTextAttribute(socialWebItemNames[4]));
        }

        [Test]
        public void CheckQuickChatVisibility()
        {
            HomePage homePage = new HomePage(driver);
            ContactUsPage contuctUsPage = homePage.openContactUsPage(header);

            contuctUsPage.clickOnChatElement("icon");
           
            Assert.AreEqual("olrk-state-expanded",contuctUsPage.getClassAttributeQuickChat());

            contuctUsPage.clickOnChatElement("arrow");

            Assert.AreEqual("olrk-state-compressed", contuctUsPage.getClassAttributeQuickChat());
        }
        
        [Test]
        public void CheckGoogleMap()
        {
            HomePage homePage = new HomePage(driver);
            ContactUsPage contuctUsPage = homePage.openContactUsPage(header);
            
            Assert.IsTrue(contuctUsPage.isGoogleMapPresent());
        }
    }
}
