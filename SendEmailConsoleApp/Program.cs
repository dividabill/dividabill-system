﻿using System;
using DividaBill.DAL;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Security.Utilities;
using DividaBill.Services.AppConfiguration.Impl;
using DividaBill.Services.Emails.Impl;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Impl;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.ThirdPartyServices.Impl;
using SendGrid.API.api;

namespace SendEmailConsoleApp
{
    internal class Program
    {
        private static UnitOfWork _unitOfWork;
        private static HouseholdBillService _householdBillService;
        private static UtilityBillService _utilityBillService;
        private static BillingQueueTransactionLogService _billingQueueTransactionLogService;
        private static HousesToBeBilledService _housesToBeBilledService;
        private static HouseService _houseService;
        private static ContractService _contractService;
        private static ProviderService _providerService;
        private static TenantEncryptionService _tenantEncryptionService;
        private static AesEncryptionService _aesEncryptionService;
        private static HouseDetailsViewModelToHouseModelMappingFactory _houseDetailsViewModelToHouseModelMappingFactory;
        private static CouponService _couponService;
        private static MultiThreadingService _multiThreadingService;

        private static EmailSentMessageService _emailSentMessageService;
        private static EmailService _emailService;
        private static AppConfigurationReader _appConfigurationReader;
        private static EmailConfigurationReader _emailConfigurationReader;
        private static TenantService _tenantService;
        private static EmailThirdPartyService _emailThirdPartyService;
        private static SendGridApiClient _sendGridApiClient;
        private static EmailTemplateService _emailTemplateService;
        private static SendGridTemplateToEmailTemplateMappingFactory _sendGridTemplateToEmailTemplateMappingFactory;
        private static ISendBillEmailsService _sendBillEmailsService;

        private static void Main(string[] args)
        {
            _unitOfWork = new UnitOfWork();
            _utilityBillService = new UtilityBillService();
            _householdBillService = new HouseholdBillService(_utilityBillService, _unitOfWork);
            _billingQueueTransactionLogService = new BillingQueueTransactionLogService(_unitOfWork);
            _providerService = new ProviderService(_unitOfWork);
            _contractService = new ContractService(_unitOfWork, _providerService);
            _aesEncryptionService = new AesEncryptionService();
            _tenantEncryptionService = new TenantEncryptionService(_aesEncryptionService, new AddressEncryptionService(_aesEncryptionService));
            _couponService = new CouponService(_unitOfWork);
            _houseDetailsViewModelToHouseModelMappingFactory = new HouseDetailsViewModelToHouseModelMappingFactory(_couponService);
            _houseService = new HouseService(_unitOfWork, _contractService, _tenantEncryptionService, _houseDetailsViewModelToHouseModelMappingFactory, new TenancyService(_unitOfWork));
            _multiThreadingService = new MultiThreadingService();
            _housesToBeBilledService = new HousesToBeBilledService(_unitOfWork, _houseService, _multiThreadingService);


            _emailSentMessageService = new EmailSentMessageService(_unitOfWork);
            _appConfigurationReader = new AppConfigurationReader();
            _emailConfigurationReader = new EmailConfigurationReader(_appConfigurationReader);
            _tenantService = new TenantService(_unitOfWork, _tenantEncryptionService);
            _sendGridApiClient = new SendGridApiClient(_appConfigurationReader);
            _emailTemplateService = new EmailTemplateService(_unitOfWork);
            _sendGridTemplateToEmailTemplateMappingFactory = new SendGridTemplateToEmailTemplateMappingFactory();
            _emailThirdPartyService = new EmailThirdPartyService(_sendGridApiClient, _emailTemplateService, _sendGridTemplateToEmailTemplateMappingFactory, _emailSentMessageService, _tenantService, _appConfigurationReader);
            _emailService = new EmailService(_emailConfigurationReader, _tenantService, _houseService, _householdBillService, _emailThirdPartyService, _appConfigurationReader);
            _sendBillEmailsService = new SendBillEmailsService(_unitOfWork, _householdBillService, _utilityBillService, _billingQueueTransactionLogService, _housesToBeBilledService, _multiThreadingService, _emailSentMessageService, _emailService, _tenantEncryptionService, _aesEncryptionService);

            _sendBillEmailsService.SendBillEmailsAsync(5, null).Wait();

            Console.ReadLine();
        }
    }
}