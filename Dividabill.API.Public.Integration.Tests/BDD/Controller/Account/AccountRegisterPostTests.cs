﻿using Dividabill.API.Public.Integration.Tests.Infrastructure;
using Dividabill.API.Public.Integration.Tests.Infrastructure.API;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.ViewModels;
using NUnit.Framework;
using StoryQ;

namespace Dividabill.API.Public.Integration.Tests.BDD.Controller.Account
{

    public class BusinessLogic
    {
        public readonly IBusinessLogicAsserter Asserter;
        private readonly IApi _api;

        public BusinessLogic(IBusinessLogicAsserter asserter, IApi api)
        {
            Asserter = asserter;
            _api = api;
        }

        public void UserTriesToSignup()
        {
            var response = _api.EndPoint.PostJsonAsync("api/Account/Register", _api.InputOutputModels.InputModels.SignUpViewModel).Result;
            _api.InputOutputModels.OutputModels.SignupOutputModel = response;
        }
    }

    public interface IBusinessLogicAsserter
    {
        void UserHasSignedUp();
    }

    public class BusinessLogicAsserter : IBusinessLogicAsserter
    {
        private readonly IApi _api;
        private readonly IUnitOfWork _unitOfWork;

        public BusinessLogicAsserter(IApi api, IUnitOfWork unitOfWork = null)
        {
            _api = api;
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }

        public void UserHasSignedUp()
        {
            var email = _api.InputOutputModels.InputModels.SignUpViewModel.Email;
            var user = _unitOfWork.UserRepository.GetByEmail(email);
            Assert.That(user, Is.Not.Null);
        }
    }

    [Category("BDD")]
    [TestFixture]
    public class AccountRegisterPostTests
    {
        private Story _story;
        private Api _api;


        [SetUp]
        public void Setup()
        {
            _story = new Story("User signs up up to the dividabill service");
            _api = new Api();
        }

        [TestCase("TW19 7PH")]
        public void user_can_signup(string postCode)
        {
            var bl = new BusinessLogic(new BusinessLogicAsserter(_api), _api);
            _api.InputOutputModels.InputModels.SignUpViewModel =
                new SignUpViewModel
                {
                    Email = "jeff@pinkhamsoftware.com",
                    Password = "password",

                };

            _story
                .InOrderTo("Split my bills with my housemates")
                .AsA("User")
                .IWant("To be able to sign up to the dividabill service")
                    .WithScenario("1st user for house can signup for services")
                    .Given(bl.UserTriesToSignup)
                    .When(_api.Asserter.SignupPostReturnedOk)
                    .Then(bl.Asserter.UserHasSignedUp)
                    .And(UserHasBeenSentSignupEmails)
                    .And(UserIsAssignedToHouse)
                    .And(HouseHasCorrectContractsAssignedToIt)
                    .Execute();
        }



        private void UserHasBeenSentSignupEmails()
        {
            throw new System.NotImplementedException();
        }

        public void UserIsAssignedToHouse()
        {
            throw new System.NotImplementedException();
        }

        private void HouseHasCorrectContractsAssignedToIt()
        {
            throw new System.NotImplementedException();
        }


    }
}
