﻿namespace Dividabill.API.Public.Integration.Tests.Infrastructure.Models
{
    public class InputOutputModels
    {
        public InputModels InputModels { get; set; }
        public OutputModels OutputModels { get; set; }
    }
}