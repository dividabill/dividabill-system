﻿using System.Net.Http;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.Models
{
    public class OutputModels
    {
        public HttpResponseMessage SignupOutputModel { get; set; }
    }
}