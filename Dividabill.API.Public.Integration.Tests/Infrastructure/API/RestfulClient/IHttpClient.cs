﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient
{
    public interface IHttpClient
    {
        //HttpRequestHeaders DefaultRequestHeaders { get; }
        //Uri BaseUri { get; set; }

        void AddHeader(string key, string value);
        void SetBaseUri(Uri uri);

        Task<HttpResponseMessage> GetAsync(string uri, CancellationToken cancellationToken);
        Task<HttpResponseMessage> PostAsync(string uri, HttpContent content, CancellationToken cancellationToken);
        Task<HttpResponseMessage> PutAsync(string uri, HttpContent content, CancellationToken cancellationToken);
        Task<HttpResponseMessage> DeleteAsync(string uri, CancellationToken cancellationToken);
        Task<HttpResponseMessage> SendAsync(string uri, HttpMethod httpMethod, CancellationToken cancellationToken);
    }
}