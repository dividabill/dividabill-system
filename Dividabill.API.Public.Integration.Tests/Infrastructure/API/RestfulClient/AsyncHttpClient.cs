﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient
{
    public class AsyncHttpClient : IHttpClient
    {
        private HttpClient _httpClient;

        public AsyncHttpClient()
        {
            _httpClient = new HttpClient();
        }

        public AsyncHttpClient(HttpMessageHandler messageHandler)
        {
            _httpClient = new HttpClient(messageHandler);
        }

        public void AddHeader(string key, string value)
        {
            if (_httpClient != null && _httpClient.DefaultRequestHeaders != null)
                _httpClient.DefaultRequestHeaders.Add(key, value);
        }

        public void SetBaseUri(Uri uri)
        {
            if (_httpClient != null)
                _httpClient.BaseAddress = uri;
        }

        public void SetMessagehandler(HttpMessageHandler messageHandler)
        {
            _httpClient = new HttpClient(messageHandler);
        }

        public async Task<HttpResponseMessage> GetAsync(string uri, CancellationToken cancellationToken)
        {
            return await _httpClient.GetAsync(uri, cancellationToken);
        }
        public async Task<HttpResponseMessage> PostAsync(string uri, HttpContent content, CancellationToken cancellationToken)
        {
            return await _httpClient.PostAsync(uri, content, cancellationToken);
        }

        public async Task<HttpResponseMessage> PutAsync(string uri, HttpContent content, CancellationToken cancellationToken)
        {
            return await _httpClient.PutAsync(uri, content, cancellationToken);
        }

        public async Task<HttpResponseMessage> DeleteAsync(string uri, CancellationToken cancellationToken)
        {
            return await _httpClient.DeleteAsync(uri, cancellationToken);
        }

        public async Task<HttpResponseMessage> SendAsync(string uri, HttpMethod httpMethod, CancellationToken cancellationToken)
        {
            return await _httpClient.SendAsync(new HttpRequestMessage(httpMethod, uri), cancellationToken);
        }
    }
}