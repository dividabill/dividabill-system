﻿using Dividabill.API.Public.Integration.Tests.BDD.Controller.Account;
using Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient.Server;
using Dividabill.API.Public.Integration.Tests.Infrastructure.Asserter;
using Dividabill.API.Public.Integration.Tests.Infrastructure.Models;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API
{
    public interface IApi
    {
        InputOutputModels InputOutputModels { get; set; }
        InputOutputModelAsserter Asserter { get; }
        ServerEndPoint EndPoint { get; set; }
        string BaseUrl { get; }
        void OriginalSetup();
        void Teardown();
    }

    public class Api:BaseBDDTestFixture, IApi
    {
        public Api()
        {
            OriginalSetup();
            InputOutputModels = new InputOutputModels
            {
                InputModels = new InputModels(),
                OutputModels = new OutputModels()
            };
        }
        private InputOutputModels _inputOutputModels;
        public InputOutputModels InputOutputModels
        {
            get { return _inputOutputModels; }
            set
            {
                _inputOutputModels = value;
                Asserter = new InputOutputModelAsserter(value);
            }
        }
        public InputOutputModelAsserter Asserter { get; private set; }

        //public BusinessLogicAsserter LogicAsserter { get; private set; }

        ~Api()
        {
            Teardown();
        }

    }
}