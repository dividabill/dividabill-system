﻿using Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient;
using Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient.Server;
using NUnit.Framework;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API
{
    [TestFixture]
    public class BaseBDDTestFixture
    {
        public ServerEndPoint EndPoint { get; set; }

        public string BaseUrl
        {
            get { return ""; }
        }

        private IHttpClientFactory _asyncHttpClientFactory;

        [SetUp]
        public void OriginalSetup()
        {
            _asyncHttpClientFactory = new AsyncHttpClientFactory();
            EndPoint = new ServerEndPoint(_asyncHttpClientFactory);
        }

        [TearDown]
        public void Teardown()
        {
            _asyncHttpClientFactory = null;
            EndPoint?.Dispose();
        }
    }
}