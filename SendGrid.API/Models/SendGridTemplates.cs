﻿using System.Collections.Generic;

namespace SendGrid.API.Models
{
    public class SendGridTemplatesVersion
    {
        public string id { get; set; }
        public string template_id { get; set; }
        public int active { get; set; }
        public string name { get; set; }
        public string subject { get; set; }
        public string updated_at { get; set; }
    }

    public class SendGridTemplatesTemplate
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<SendGridTemplatesVersion> versions { get; set; }
    }

    public class SendGridTemplates
    {
        public List<SendGridTemplatesTemplate> templates { get; set; }
    }
}