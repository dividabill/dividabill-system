using System;
using System.Collections.Generic;
using System.Linq;

namespace SendGrid.API.Models
{
    public class SendGridTemplateVersion
    {
        public string id { get; set; }
        public int user_id { get; set; }
        public string template_id { get; set; }
        public int active { get; set; }
        public string name { get; set; }
        public string html_content { get; set; }
        public string plain_content { get; set; }
        public string subject { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class SendGridTemplate
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<SendGridTemplateVersion> versions { get; set; }

        public SendGridTemplateVersion GetActiveVersion()
        {
            return versions?.FirstOrDefault(x => x.active == 1);
        }
    }
}