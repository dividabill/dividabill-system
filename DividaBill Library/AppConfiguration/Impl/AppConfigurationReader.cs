using System.Configuration;
using DividaBill.AppConstants;

namespace DividaBill.Library.AppConfiguration.Impl
{
    public class AppConfigurationReader : IAppConfigurationReader
    {
        public string GetFromAppConfig(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public string GetThirdPartyConfig(ThirdPartyServicesEnum serviceType, ThirdPartyServicesConfigEnum configName)
        {
            var keyConcated = $"thirdParty:{serviceType}.{configName}";
            return GetFromAppConfig(keyConcated);
        }
		
        public string GetFromAppConfig(AppSettingsEnum keyEnum)
        {
            return GetFromAppConfig(keyEnum.ToString());
        }
    }
}