using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace DividaBill.Library
{
    public static class Extensions
    {
        /// <summary>
        ///     Generates a Stream object from the given string.
        /// </summary>
        public static Stream GenerateStream(this string s)
        {
            var stream = new MemoryStream();
            using (var writer = new StreamWriter(stream))
                writer.Write(s);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        ///     Replaces the format item in a specified string with the string representation
        ///     of a corresponding object in a specified array.
        /// </summary>
        /// <param name="format">A composite format string (see Remarks).</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <returns>
        ///     A copy of format in which the format items have been replaced by the string
        ///     representation of the corresponding objects in args.
        /// </returns>
        public static string F(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        public static V GetValueOrDefault<K, V>(this IDictionary<K, V> dict, K key, V defaultValue)
        {
            V value;
            return dict.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static int GetMonthsPassed(this DateTime dt)
        {
            return dt.Year*12 + dt.Month;
        }

        public static void ThrowIfNull<T>(this T val, string paramName)
            where T : class
        {
            if (val == null) throw new ArgumentNullException(paramName);
        }

        public static void ThrowIfNull<T>(this T val)
            where T : class
        {
            if (val == null)
                throw new ArgumentNullException("", "The given object of type '{0}' was null!".F(typeof (T).Name));
        }

        public static T ArgMax<T>(this IEnumerable<T> vals, Func<T, long> functor)
            where T : class
        {
            return vals.ArgMinMax(functor, (a, b) => a > b);
        }

        public static T ArgMin<T>(this IEnumerable<T> vals, Func<T, long> functor)
            where T : class
        {
            return vals.ArgMinMax(functor, (a, b) => a < b);
        }

        public static T ArgMax<T>(this IEnumerable<T> vals, Func<T, DateTime> functor)
            where T : class
        {
            return vals.ArgMinMax(e => functor(e).Ticks, (a, b) => a > b);
        }

        public static T ArgMin<T>(this IEnumerable<T> vals, Func<T, DateTime> functor)
            where T : class
        {
            return vals.ArgMinMax(e => functor(e).Ticks, (a, b) => a < b);
        }

        public static TEnum ArgMinMax<TEnum, TComp>(this IEnumerable<TEnum> vals, Func<TEnum, TComp> functor,
            Func<TComp, TComp, bool> selector)
            where TEnum : class
        {
            if (vals == null || !vals.Any())
                return null;

            return vals
                .Select(e => new {Key = e, Val = functor(e)})
                .Aggregate((a, b) => selector(a.Val, b.Val) ? a : b)
                .Key;
        }

        public static IEnumerable<T> DuplicateValues<T>(this IEnumerable<T> vals)
        {
            var ha = new HashSet<T>();
            var hb = new HashSet<T>();
            return vals.Where(v => !ha.Add(v) && hb.Add(v));
        }

        public static decimal BusinessRound(this decimal val)
        {
            return decimal.Ceiling(val*100)/100;
        }

        public static string ToStringDateFormat(this DateTime current)
        {
            var format = ConfigurationManager.AppSettings["DateFormat"];
            return current.ToString(format);
        }

        public static bool IsEmpty<T>(this IQueryable<T> list)
        {
            return list == null
                   || !list.Any();
        }

        public static bool IsEmpty<T>(this IEnumerable<T> list)
        {
            return list == null
                   || !list.Any();
        }

        public static bool IsEmpty<T>(this List<T> list)
        {
            return list == null
                   || !list.Any();
        }
    }
}