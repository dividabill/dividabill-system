﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DividaBill.Library
{
    /// <summary>
    /// The master of laziness. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IxLazy<T>
    {
        Func<T> generator;
        int _hasValue = 0;
        T _value;

        public bool HasValue { get { return _hasValue != 0; } }

        public T Value
        {
            get
            {
                if (Interlocked.Exchange(ref _hasValue, 1) == 0)
                {
                    _value = generator();
                    generator = null;
                }
                return _value;
            }
        }

        public static implicit operator T(IxLazy<T> lazy)
        {
            return lazy.Value;
        }

        public static implicit operator IxLazy<T> (Func<T> gen)
        {
            return new IxLazy<T>(gen);
        }


        public IxLazy(Func<T> gen)
        {
            this.generator = gen;
        }
    }
}
