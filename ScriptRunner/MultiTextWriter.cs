﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ScriptRunner
{
    class MultiTextWriter : TextWriter
    {
        readonly TextWriter sourceA;
        readonly TextWriter sourceB;

        public MultiTextWriter(TextWriter srcA, TextWriter srcB)
        {
            sourceA = srcA;
            sourceB = srcB;
        }

        public override Encoding Encoding
        {
            get { return sourceA.Encoding; }
        }

        public override void Write(char value)
        {
            sourceA.Write(value);
            sourceB.Write(value);
        }

        protected override void Dispose(bool disposing)
        {
            sourceB.Flush();
            sourceB.Dispose();
        }
    }
}
