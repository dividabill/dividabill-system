﻿using DividaBill.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ScriptRunner
{
    abstract class Script
    {
        public static DateTime PickDateTime(string prompt = "Pick a date: ")
        {
            DateTime dt;
            string input;
            do
            {
                Console.Write(prompt);
                input = Console.ReadLine();
            }
            while (!DateTime.TryParse(input, out dt));
            return dt;
        }


        public static T PickListItem<T>(T[] items, string itemTypeName, Func<T, string> nameFunc)
        {
            var id = 0;
            do
            {
                Console.WriteLine("Please pick one {0}:", itemTypeName);
                for (int i = 0; i < items.Length; i++)
                    Console.WriteLine("[{0}] {1}", i, nameFunc(items[i]));
                Console.Write(">");

                var input = Console.ReadLine();

                if (int.TryParse(input, out id) && id >= 0 && id < items.Length)
                    break;
            }
            while (true);

            var it = items[id];
            Console.WriteLine("Picked the '{0}' {1}", nameFunc(it), itemTypeName);
            return it;
        }

        public static T PickListItem<T>(T[] items)
        {
            return PickListItem(items, "item", i => i.ToString());
        }

        List<string> loggedLines = new List<string>();

        public UnitOfWork unitOfWork { get; private set; }

        public bool RequiresWriteAccess { get; set; }

        public void DoIt(string dbConnString)
        {
            if(RequiresWriteAccess)
            {
                Console.Write("WARNING: This script requires WRITE PERMISSIONS! Do you still want to run it? (type 'yes' to proceed)");
                Console.WriteLine(">");
                if (Console.ReadLine() != "yes")
                    return;
            }
            this.unitOfWork = new UnitOfWork(dbConnString, !RequiresWriteAccess);
            doIt();
        }

        protected abstract void doIt();
    }
}
