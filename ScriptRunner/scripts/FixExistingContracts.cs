﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Models.Bills;

namespace DividaBill.ScriptRunner
{
    /// <summary>
    /// Migrates the database to the newest format:
    ///  - get Contract.ServiceNew from Contracts.Service
    ///  - get Contract.RequestedStartDate from StartDate
    /// 
    /// NOT DONE / UNNEEDED:
    ///  - restore utility bills linking to household bills (reverse)
    ///  - restore utility bills linking to the contract
    ///  - fill HouseholdBill.Month, Year from HouseholdBills.StartDate
    /// 
    /// Date: 07.09.2015
    /// Author: Ix
    /// </summary>
    class FixExistingContracts : Script
    {
        Dictionary<ServiceType, Func<HouseholdBill, int?>> billGetters = new Dictionary<ServiceType, Func<HouseholdBill, int?>>
        {
            { ServiceType.Broadband, (b) => b.BroadbandBill_Id },
            { ServiceType.Electricity, (b) => b.ElectricityBill_Id },
            { ServiceType.Gas, (b) => b.GasBill_Id },
            { ServiceType.LandlinePhone, (b) => b.LandlinePhoneBill_Id },
            { ServiceType.LineRental, (b) => b.LineRentalBill_Id },
            { ServiceType.NetflixTV, (b) => b.NetflixTVBill_Id },
            { ServiceType.SkyTV, (b) => b.SkyTVBill_Id },
            { ServiceType.TVLicense, (b) => b.TVLicenseBill_Id },
            { ServiceType.Water, (b) => b.WaterBill_Id },
        };

        Dictionary<ServiceType, Func<HouseModel, bool>> houseServiceCheckers = new Dictionary<ServiceType, Func<HouseModel, bool>>
        {
            { ServiceType.Broadband, (h) => h.Broadband },
            { ServiceType.Electricity, (h) => h.Electricity },
            { ServiceType.Gas, (h) => h.Gas },
            { ServiceType.LandlinePhone, (h) => h.LandlinePhone },
            { ServiceType.LineRental, (h) => h.Broadband || h.LandlinePhone },
            { ServiceType.NetflixTV, (h) => h.NetflixTV },
            { ServiceType.SkyTV, (h) => h.SkyTV },
            { ServiceType.TVLicense, (h) => h.TVLicense },
            { ServiceType.Water, (h) => h.Water },
        };

        public FixExistingContracts()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            RestoreContractService();
            //CreateMissingContracts(); 
            //RestoreUtilityHouseholdLinks();
        }

        //seki contract da si ima ServiceNew
        void RestoreContractService()
        {
            //restore service type
            Func<int> noStartDateContracts = () => unitOfWork.ContractRepository.Get()
                .Where(c => c.RequestedStartDate < new DateTime(2000, 1, 1))
                .Count();



            Console.WriteLine("{0} contracts with no start date... ", noStartDateContracts());

            Console.WriteLine("Restoring..");
            unitOfWork.ExecuteSqlScript<Contract>("UPDATE Contracts SET RequestedStartDate = StartDate WHERE RequestedStartDate < '2000/01/01'");

            Console.WriteLine("{0} contracts with no start date left", noStartDateContracts());



            Func<int> noServiceContracts = () => unitOfWork.ContractRepository.Get()
                .Where(c => c.ServiceNew == null || c.ServiceNew.Id == 0)
                .Count();

            Console.WriteLine("{0} contracts with no service type... ", noServiceContracts());

            Console.WriteLine("Restoring..");
            unitOfWork.ExecuteSqlScript<Contract>("UPDATE Contracts SET ServiceNew_ID = Service WHERE ServiceNew_ID IS NULL AND Service <> 0");

            Console.WriteLine("{0} contracts with no service type left", noServiceContracts());



            var contractsBB = unitOfWork.ContractRepository.Get().Where(c => c.ServiceNew_ID == ServiceType.Broadband.Id).ToArray();
            foreach (var contract in contractsBB)
            {
                var house = contract.House;

                contract.PackageRaw = (int)house.BroadbandType;
            }
            unitOfWork.Save();

            var contractsPhone = unitOfWork.ContractRepository.Get().Where(c => c.ServiceNew_ID == ServiceType.LandlinePhone.Id).ToArray();
            foreach (var contract in contractsPhone)
            {
                var house = contract.House;

                contract.PackageRaw = (int)house.LandlineType;
            }
            unitOfWork.Save();

        }
    }
}
