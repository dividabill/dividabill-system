﻿using Dividabill.DAL;
using Dividabill.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class GetFirstTenantsForHouses : Script
    {
        public override void DoIt()
        {
            var service = new HouseService(unitOfWork);

            var allHouses = unitOfWork.ActiveHouses.Get().Where(s=> s.StartDate >= new DateTime(2015, 5, 1) && s.Tenants.Count()>0).OrderBy(h => h.StartDate).ToArray();

            var duda = service.GenerateListOfLeadTeannts(allHouses);

            File.WriteAllText("leadTenantInHouses.csv", duda);
        }
    }
}
