﻿using Dividabill;
using Dividabill.Models;
using Dividabill.Services;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill_Library;
using GoCardless_API.api;
using GoCardless_API.helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApplication1
{
    class AddBankAccountToGoCardless : Script
    {
        public override void DoIt()
        {
            
            var _houseService = new HouseService(unitOfWork);
            var _billService = new BillService(unitOfWork);
            TenantService _tenantService = new TenantService(unitOfWork);

            GoCardless_API.GoCardless.Environment = GoCardless_API.GoCardless.Environments.Sandbox;
            //TODO: Get only the currently generated bills
            ApiClient client = new ApiClient(
                "AC00000566K9MYBTX7ZA0HC6FA2DZE7R",
                "YR_7kK8oMovkxQLj77uQPdfDLOlMTyAlSHTKpq2i", "");

            var housesToAdd = _houseService.GetActiveHouses().Where(h => h.StartDate > new DateTime(2014, 07, 1));

            foreach (var house in housesToAdd.ToList())
            {
                foreach (var tenant in house.Tenants)
                {
                    _tenantService.AddToGoCardless(tenant, client);
                }
            }

            Console.WriteLine("Added all tenants to gocardless.");
        }
    }
}
