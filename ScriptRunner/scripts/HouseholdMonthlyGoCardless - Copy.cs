﻿using Dividabill;
using Dividabill.Models;
using Dividabill.Services;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill_Library;
using GoCardless_API.api;
using GoCardless_API.helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApplication1
{
    class HouseholdMonthlyGoCardless : Script
    {
        public override void DoIt()
        {
            
            var _houseService = new HouseService(unitOfWork);
            var _billService = new BillService(unitOfWork);
            Console.Write("Bills created at hour: ");
            var hour = Console.ReadLine();
            var hourInt = 0;
            var bills = _billService.GetHouseholdBillsAsQ().Where(b => b.CreatedDate.Date == DateTime.Today);
            if (int.TryParse(hour, out hourInt)) {
                bills.Where(b => b.CreatedDate.Hour == hourInt);
            }

            //TODO: Get only the currently generated bills
            ApiClient client = new ApiClient(
                "AC00000566K9MYBTX7ZA0HC6FA2DZE7R",
                "YR_7kK8oMovkxQLj77uQPdfDLOlMTyAlSHTKpq2i", "");


            //ApiClient client = new ApiClient(
            //    "AC00000AWK3YF85V2TTXV7ERJACS0D89",
            //    "LTYhMXqv8cQX5oqcpiH6B52SG3_X4ELR-xKcFft7", "");

            List<HouseModel> dbActiveHouses = unitOfWork.ActiveHouses.GetHouses(p => p.SetUpDate <= DateTime.Today, null, "Tenants").ToList();
            Dictionary<string, string> problemOnes = new Dictionary<string, string>();
            foreach (HouseModel house in dbActiveHouses)
            {
                TenantService _tenantService = new TenantService(unitOfWork);
                int i = 0;
                foreach (User tenant in house.Tenants)
                {
                    _tenantService.AddToGoCardless(tenant, client);
                    i++;

                    Console.WriteLine("Added user {0}/{1}: {2}...",i, house.Tenants.Count, tenant.FullName);
                }
                Console.WriteLine("Added {0} users to gocardless", i);
                i = 0;
                foreach (HouseholdBill bill in bills)
                {
                    foreach (User tenant in house.Tenants)
                    {
                        DateTime paymentDate;
                        //check if payment date is after 3 business days
                        if (bill.DeliveryBill.PaymentDate <= DateTime.Today.AddBusinessDays(4))
                        {
                            paymentDate = DateTime.Today.AddBusinessDays(4);
                        }
                        else
                        {
                            paymentDate = bill.DeliveryBill.PaymentDate;
                        }

                        Mandate mand = Retry.Do<Mandate>(() => client.GetMandate(tenant.MandateID), TimeSpan.FromSeconds(5), 3);
                        BankMandate mandate = unitOfWork.BankMandateRepository.GetByID(tenant.MandateID);
                        mandate.next_possible_charge_date = mand.next_possible_charge_date;
                        unitOfWork.BankMandateRepository.Update(mandate);


                        var chargeDate = (Convert.ToDateTime(mandate.next_possible_charge_date) > paymentDate ? mand.next_possible_charge_date : paymentDate.ToString("yyyy-MM-dd"));
                        var payment = new NewPayment
                        {
                            amount = Convert.ToInt32((Math.Truncate(bill.TotalBill / house.Tenants.Count() * 100) / 100) * 100),
                            charge_date = chargeDate,
                            currency = "GBP",
                            description = "",
                            reference = ""
                        };
                        payment.links.Add("mandate", tenant.MandateID);
                        try
                        {
                            Payment paym = Retry.Do<Payment>(() => client.CreatePayment(payment), TimeSpan.FromSeconds(5), 3);

                            DividaBill.Models.BankPayment domainBankPayment = new DividaBill.Models.BankPayment
                            {
                                amount = paym.amount,
                                amount_refunded = paym.amount_refunded,
                                charge_date = paym.charge_date,
                                created_at = paym.created_at,
                                currency = paym.currency,
                                description = paym.description,
                                id = paym.id,
                                Mandate_ID = tenant.MandateID,
                                reference = paym.reference,
                                status = paym.status
                            };

                            unitOfWork.BankPaymentRepository.Insert(domainBankPayment);
                            unitOfWork.Save();
                        }
                        catch (AggregateException ae)
                        {
                            ae.Handle((x) =>
          {
              if (x is ApiException) // This we know how to handle.
                  {
                  if (!problemOnes.ContainsKey(tenant.MandateID))
                  {
                      problemOnes.Add(tenant.MandateID, ((ApiException)x).RawContent);
                  }
              }
              return true; //if you do something like this all exceptions are marked as handled  
              });


                        }
                    }
                }

                Console.WriteLine("Added {0} payments to gocardless", i);

            }

            if (problemOnes.Count() > 0)
            {
                string s = string.Join(";\n", problemOnes.Select(x => x.Key + "=" + x.Value).ToArray());
                 File.WriteAllText("GoCardless.txt", s);
            }
            else
            {
                Console.WriteLine("All bills submitted successfully.");
            }







            var duda = bills.Aggregate("", (a,b) => a + "," + b);

            File.WriteAllText("bills-gocardless.csv", duda);
        }
    }
}
