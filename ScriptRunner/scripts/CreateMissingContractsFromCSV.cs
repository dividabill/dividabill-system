﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DividaBill.DAL.Interfaces;
using CsvHelper;
using DividaBill.Library;
using DividaBill.Services.Impl;

namespace DividaBill.ScriptRunner
{
    class CreateMissingContractsFromCSV : Script
    {
        public static Dictionary<ServiceType, Func<HouseModel, bool>> houseServiceGetter = new Dictionary<ServiceType, Func<HouseModel, bool>>
        {
            { ServiceType.Broadband, (m) => m.Broadband },
            { ServiceType.Electricity, (m) => m.Electricity },
            { ServiceType.Gas, (m) => m.Gas },
            { ServiceType.LandlinePhone, (m) => m.LandlinePhone },
            { ServiceType.LineRental, (m) => m.LandlinePhone || m.Broadband },
            { ServiceType.NetflixTV, (m) => m.NetflixTV },
            { ServiceType.SkyTV, (m) => m.SkyTV },
            { ServiceType.TVLicense, (m) => m.TVLicense },
            { ServiceType.Water, (m) => m.Water },
        };

        public static Dictionary<ServiceType, Func<HouseModel, int>> contractPackageGetter = new Dictionary<ServiceType, Func<HouseModel, int>>
        {
            { ServiceType.Broadband,        (m) => (int)m.BroadbandType },
            { ServiceType.Electricity,      (m) => 0 },
            { ServiceType.Gas,              (m) => 0 },
            { ServiceType.LandlinePhone,    (m) => (int)m.LandlineType },
            { ServiceType.LineRental,       (m) => 0 },
            { ServiceType.NetflixTV,        (m) => 0 },
            { ServiceType.SkyTV,            (m) => 0 },
            { ServiceType.TVLicense,        (m) => 0 },
            { ServiceType.Water,            (m) => 0 },
        };

        static Dictionary<ServiceType, Action<IUnitOfWork, int>> contractDeleteGuy = new Dictionary<ServiceType, Action<IUnitOfWork, int>>
        {
            { ServiceType.Broadband, (u, c) => u.BroadbandContractRepository.Delete(c) },
            { ServiceType.Electricity, (u, c) => u.ElectricityContractRepository.Delete(c) },
            { ServiceType.Gas, (u, c) => u.GasContractRepository.Delete(c) },
            { ServiceType.LandlinePhone, (u, c) => u.LandlinePhoneContractRepository.Delete(c) },
            { ServiceType.LineRental, (u, c) => u.LineRentalContractRepository.Delete(c) },
            { ServiceType.NetflixTV, (u, c) => u.NetflixTVContractRepository.Delete(c) },
            { ServiceType.SkyTV, (u, c) => u.SkyTVContractRepository.Delete(c) },
            { ServiceType.TVLicense, (u, c) => u.TVLicenseContractRepository.Delete(c) },
            { ServiceType.Water, (u, c) => u.WaterContractRepository.Delete(c) },
        };

        ContractService contractService;
        int n_created, n_deleted;

        public CreateMissingContractsFromCSV()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            n_created = 0;
            n_deleted = 0;

            var providerService = new ProviderService(unitOfWork);
            contractService = new ContractService(unitOfWork, providerService);
            var houses = unitOfWork.ActiveHouses.Get().ToArray();
            var allServices = ServiceType.All.Where(ty => ty != ServiceType.NetflixTV);

            using (TextReader reader = File.OpenText("addServices.csv"))
            {
                using (var csv = new CsvReader(reader))
                    
                    while (csv.Read())
                    {
                        var house_id = csv.GetField<int>(0);
                        var servicesToAdd = csv.GetField<string>(1);
                        var packagesTypes = csv.GetField<string>(2);

                        var i = 0;
                        foreach (var con in servicesToAdd)
                        {

                            var ty = int.Parse(con.ToString());
                            var raw = int.Parse(packagesTypes.ToCharArray()[i].ToString());
                            //Contract creation

                            //check for existing contracts
                            var existingContract = unitOfWork.ContractRepository.Get()
                                .FirstOrDefault(c =>
                                    c.House_ID == house_id &&
                                    (c.ServiceNew_ID == ty));
                            var hasTheContract = (existingContract != null);

                            var req = requestFromHouse(house_id, ty, raw);
                            var wantsTheContract = (req != null);

                            if (hasTheContract == wantsTheContract)
                            {
                                Console.Write(' ');
                                continue;
                            }

                            if (hasTheContract)
                            {
                                //has one, but does not want it
                                contractDeleteGuy[ty](unitOfWork, existingContract.ID);
                                unitOfWork.Save();

                                Console.Write('-');
                                n_deleted++;
                            }
                            else
                            {
                                //no contract, wants one
                                var newContract = contractService.Create(req);
                                unitOfWork.Save();

                                Console.Write('+');
                                n_created++;
                            }

                            i++;
                        }
                    }
            }
        






            Console.WriteLine("{0} contracts created!", n_created);
            Console.WriteLine("{0} contracts deleted!", n_deleted);

            Console.WriteLine("And saved!");
        }
        

        ContractRequest requestFromHouse(int houseId, ServiceType ty, int raw)
        {

            var h = unitOfWork.ActiveHouses.Get().Where(x => x.ID == houseId).First();



            var user = h.Tenants.First();
            var provider = contractService.GetDefaultProvider(user.House, ty);
            var conLength = (h.EndDate.GetMonthsPassed() - h.StartDate.GetMonthsPassed());

            if (conLength < 9 && h.ID > 150)
                Console.WriteLine("House {0} has a duration of {1}!", h.ID, conLength);

            return new ContractRequest
            {
                Duration = (ContractLength)conLength,
                Provider = provider,
                TriggeringTenant = user,
                RequestedStartDate = h.StartDate,
                Service = ty,
                PackageRaw = raw,
            };
        }
    }
}
