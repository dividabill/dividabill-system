﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ScriptRunner.scripts
{
    /// <summary>
    /// Checks if each house's utility bool fields correspond to having contracts
    /// </summary>
    //class HouseContractsChecker : Script
    //{
    //    const string logFileName = "consistency.log";

    //    protected override void doIt()
    //    {
    //        var houses = unitOfWork.ActiveHouses.Get().ToArray();
    //        foreach (var h in houses)
    //        {
    //            var houseStart = h.StartDate.Date;
    //            var contractStart = h.Contracts.OrderBy(c => c.StartDate).FirstOrDefault()?.StartDate.Date;
    //            var houseEnd = h.EndDate.Date;
    //            var contractEnd = h.Contracts.OrderBy(c => c.EndDate).FirstOrDefault()?.EndDate.Date;

    //            if (houseStart != contractStart)
    //                Log("House #{0} claims it starts on {1} but its earliest contract is from {2}!", h.ID, houseStart, contractStart);

    //            if (houseStart != contractStart)
    //                Log("House #{0} claims it ends on {1} but its latest contract is until {2}!", h.ID, houseEnd, contractEnd);
    //        }

    //        SaveLog(logFileName);
    //    }
    //}
}
