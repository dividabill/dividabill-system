﻿//using DividaBill.DAL;
//using DividaBill.Models;
//using DividaBill.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ConsoleApplication1
//{
//    class HouseholdBillFixer : Script
//    {
//        const int NServices = 9;

//        Func<UnitOfWork, HouseModel, IQueryable<BillBase>>[] tableGetters = new Func<UnitOfWork, HouseModel, IQueryable<BillBase>>[]
//        {
//            (u, h) => u.WaterBillRepository.Get(),
//            (u, h) => u.GasBillRepository.Get(),
//            (u, h) => u.BroadbandBillRepository.Get(),
//            (u, h) => u.LandlinePhoneBillRepository.Get(),

//            (u, h) => u.LineRentalBillRepository.Get(),

//            (u, h) => u.ElectricityBillRepository.Get(),
//            (u, h) => u.NetflixTVBillRepository.Get(),
//            (u, h) => u.SkyTVBillRepository.Get(),
//            (u, h) => u.TVLicenseBillRepository.Get(),
//        };

//        static Action<HouseholdBill, int?>[] billSetters = new Action<HouseholdBill, int?>[]
//        {
//            (h, b) => h.WaterBill_Id = b,
//            (h, b) => h.GasBill_Id = b,
//            (h, b) => h.BroadbandBill_Id = b,
//            (h, b) => h.LandlinePhoneBill_Id = b,

//            (h, b) => h.LineRentalBill_Id = b,

//            (h, b) => h.ElectricityBill_Id = b,
//            (h, b) => h.NetflixTVBill_Id = b,
//            (h, b) => h.SkyTVBill_Id = b,
//            (h, b) => h.TVLicenseBill_Id = b,

//        };

//        public override void DoIt()
//        {
//            var houses = unitOfWork.ActiveHouses.Get().ToArray();

//            foreach (var h in houses)
//            {
//                Console.WriteLine("Checking house #{0}, registered at {1}", h.ID, h.RegisteredDate);

//                //get its delivery bills
//                var dBills = unitOfWork.DeliveryBillRepository.Get()
//                    .Where(b => b.House_ID == h.ID)
//                    .OrderBy(b => b.StartDate)
//                    .ToList();

//                //continue if house has deliveries
//                if (!dBills.Any())
//                    continue;

//                //exception if any 2 dbills have same start month
//                if (!GetIsUnique(dBills, b => b.StartDate.Date))
//                    throw new Exception("Two delivery bills with same month!");


//                //and all typed bills
//                var otherBills = tableGetters
//                    .Select(g => g(unitOfWork, h)
//                        .Where(b => b.House_ID == h.ID)
//                        .OrderBy(b => b.StartDate)
//                        .ToArray())
//                    .ToArray();

//                //exception if any 2 othabills have same start month
//                if (otherBills.Any(billList => !GetIsUnique(billList, b => b.StartDate.Date)))
//                    throw new Exception("Two other-bills with same month!");

//                var nBills = dBills.Count();
//                var n = 1;

//                foreach (var db in dBills)
//                {
//                    //if (unit.HouseholdBillRepository.Get().Any(b => b.DeliveryBill_Id == db.Id))
//                    //    continue;

//                    var startTime = db.StartDate;
//                    var endTime = db.EndDate;
//                    var creationDate = startTime.AddDays(-7);

//                    //get typed bills starting before this delivery
//                    var nowBills = otherBills
//                        .Select(bs => bs
//                            .TakeWhile(b => b.StartDate.Date <= startTime.Date)
//                            .SingleOrDefault())
//                        .ToArray();

//                    //there should be at least one bill with this delivery bill
//                    if (!nowBills.Any(b => b != null))
//                        throw new Exception();

//                    // should have at most 1 typed bill per delivery bill
//                    // start and end dates should match
//                    if (nowBills.Any(b => b != null && (b.StartDate != startTime || b.EndDate != endTime)))
//                        throw new Exception();

//                    var houseBill = new HouseholdBill()
//                    {
//                        House_ID = h.ID,
//                        CreatedDate = creationDate,
//                        StartDate = startTime,
//                        EndDate = endTime,

//                        DeliveryBill = db,
//                    };

//                    for (int i = 0; i < billSetters.Length; i++)
//                        billSetters[i](houseBill, nowBills[i]?.Id);

//                    unitOfWork.HouseholdBillRepository.InsertAsync(houseBill);

//                    Console.CursorLeft = 0;
//                    Console.Write(n + "/" + nBills);
//                    n++;

//                    // remove them bills
//                    otherBills = otherBills.Select(bs => bs.SkipWhile(b => b.StartDate <= startTime).ToArray()).ToArray();
//                }
//                Console.WriteLine();

//            }
//            unitOfWork.Save();
//        }

//        /// <summary>
//        /// Gets whether all elements are unique
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="values"></param>
//        /// <returns></returns>
//        static bool GetIsUnique<T, U>(IEnumerable<T> values, Func<T, U> functor)
//        {
//            var set = new HashSet<U>();

//            foreach (T item in values)
//            {
//                if (!set.Add(functor(item)))
//                    return false;
//            }
//            return true;
//        }

//    }
//}
