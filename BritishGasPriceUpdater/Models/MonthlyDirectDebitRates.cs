using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class MonthlyDirectDebitRates
    {
        public String standingChargeExcludingVAT;
        public String standingCharges;
        public String tier2ExcludingVAT;
        public String tier2Rate;
        public String unitRate;
        public String unitRateExcludingVAT;

        public String getStandingChargeExcludingVAT()
        {
            return this.standingChargeExcludingVAT;
        }
        public void setStandingChargeExcludingVAT(String standingChargeExcludingVAT)
        {
            this.standingChargeExcludingVAT = standingChargeExcludingVAT;
        }
        public String getStandingCharges()
        {
            return this.standingCharges;
        }
        public void setStandingCharges(String standingCharges)
        {
            this.standingCharges = standingCharges;
        }
        public String getTier2ExcludingVAT()
        {
            return this.tier2ExcludingVAT;
        }
        public void setTier2ExcludingVAT(String tier2ExcludingVAT)
        {
            this.tier2ExcludingVAT = tier2ExcludingVAT;
        }
        public String getTier2Rate()
        {
            return this.tier2Rate;
        }
        public void setTier2Rate(String tier2Rate)
        {
            this.tier2Rate = tier2Rate;
        }
        public String getUnitRate()
        {
            return this.unitRate;
        }
        public void setUnitRate(String unitRate)
        {
            this.unitRate = unitRate;
        }
        public String getUnitRateExcludingVAT()
        {
            return this.unitRateExcludingVAT;
        }
        public void setUnitRateExcludingVAT(String unitRateExcludingVAT)
        {
            this.unitRateExcludingVAT = unitRateExcludingVAT;
        }
    }
}