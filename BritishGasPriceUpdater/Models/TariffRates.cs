using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class TariffRates
    {
        public CurrentUnitRates currentUnitRates;
        public PostUnitRates postUnitRates;

        public CurrentUnitRates getCurrentUnitRates()
        {
            return this.currentUnitRates;
        }
        public void setCurrentUnitRates(CurrentUnitRates currentUnitRates)
        {
            this.currentUnitRates = currentUnitRates;
        }
        public PostUnitRates getPostUnitRates()
        {
            return this.postUnitRates;
        }
        public void setPostUnitRates(PostUnitRates postUnitRates)
        {
            this.postUnitRates = postUnitRates;
        }
    }
}