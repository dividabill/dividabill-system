using System;

namespace BritishGasPriceUpdater.Models
{
    public class AveragePrices
    {
        public String monthly;
        public String postMonthly;
        public String postQuarterly;
        public String postYearly;
        public String quarterly;
        public String yearly;

        public String getMonthly()
        {
            return this.monthly;
        }
        public void setMonthly(String monthly)
        {
            this.monthly = monthly;
        }
        public String getPostMonthly()
        {
            return this.postMonthly;
        }
        public void setPostMonthly(String postMonthly)
        {
            this.postMonthly = postMonthly;
        }
        public String getPostQuarterly()
        {
            return this.postQuarterly;
        }
        public void setPostQuarterly(String postQuarterly)
        {
            this.postQuarterly = postQuarterly;
        }
        public String getPostYearly()
        {
            return this.postYearly;
        }
        public void setPostYearly(String postYearly)
        {
            this.postYearly = postYearly;
        }
        public String getQuarterly()
        {
            return this.quarterly;
        }
        public void setQuarterly(String quarterly)
        {
            this.quarterly = quarterly;
        }
        public String getYearly()
        {
            return this.yearly;
        }
        public void setYearly(String yearly)
        {
            this.yearly = yearly;
        }
    }
}