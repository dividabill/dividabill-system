using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class Prices
    {
        public bool currentTariff;
        public List<FuelTypes> fuelTypes;
        public bool isTouTariff;
        public TariffInformationDetails tariffInformationDetails;
        public String tariffName;

        public bool getCurrentTariff()
        {
            return this.currentTariff;
        }
        public void setCurrentTariff(bool currentTariff)
        {
            this.currentTariff = currentTariff;
        }
        public List<FuelTypes> getFuelTypes()
        {
            return this.fuelTypes;
        }
        public void setFuelTypes(List<FuelTypes> fuelTypes)
        {
            this.fuelTypes = fuelTypes;
        }
        public bool getIsTouTariff()
        {
            return this.isTouTariff;
        }
        public void setIsTouTariff(bool isTouTariff)
        {
            this.isTouTariff = isTouTariff;
        }
        public TariffInformationDetails getTariffInformationDetails()
        {
            return this.tariffInformationDetails;
        }
        public void setTariffInformationDetails(TariffInformationDetails tariffInformationDetails)
        {
            this.tariffInformationDetails = tariffInformationDetails;
        }
        public String getTariffName()
        {
            return this.tariffName;
        }
        public void setTariffName(String tariffName)
        {
            this.tariffName = tariffName;
        }
    }
}