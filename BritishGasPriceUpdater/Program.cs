﻿using BritishGasPriceUpdater.Models;
using CsvHelper;
using DividaBill.Models;
using Newtonsoft.Json;
using PostcodeLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BritishGasPriceUpdater
{
    // To learn more about Microsoft Azure WebJobs, please see http://go.microsoft.com/fwlink/?LinkID=401557
    class Program
    {
        static Dictionary<String, String> postcodes = new Dictionary<string, string>();
        static List<String> notAdded = new List<string>();
        static void Main()
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            string startupPath = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, "Dividabill-V3", "Postcodes", "postcodes.csv");

            Console.WriteLine("Location of postcodes.csv (Empty for default): ");
            string newStartupPath = Console.ReadLine();
            if (newStartupPath != "")
            {
                startupPath = newStartupPath;
            }

            using (TextReader reader = File.OpenText(startupPath))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.RegisterClassMap<PostCodeCSVMapper>();
                    while (csv.Read())
                    {
                        PostCode postcodeObject = csv.GetRecord<PostCode>();
                        string postcodeBegining = postcodeObject.Postcode.Split(' ')[0];
                        if (!postcodes.Keys.Contains(postcodeBegining))
                        {
                            Console.WriteLine(postcodeBegining);
                            postcodes.Add(postcodeBegining, postcodeObject.Postcode);
                        }
                    }


                }

            }

            foreach (var item in postcodes)
            {


                Console.WriteLine(item);

                var postData = new POSTModel
                {
                    paymentOption = "mdd",
                    postcode = item.Key,
                    fuelType = "gas,elecsr",
                    economySevenMeter = false,
                    dualFuel = false,
                    tariff = "Standard"
                };
                try
                {
                    var client = new HttpClient();
                    //client.Timeout = TimeSpan.FromMinutes(5);
                    var values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("paymentOptions", "mdd"));
                    values.Add(new KeyValuePair<string, string>("postcode", item.Value));
                    values.Add(new KeyValuePair<string, string>("fuelType", "gas,elecsr"));
                    values.Add(new KeyValuePair<string, string>("economySevenMeter", "false"));
                    values.Add(new KeyValuePair<string, string>("dualFuel", "false"));
                    values.Add(new KeyValuePair<string, string>("tariff", "Standard"));
                    var content = new FormUrlEncodedContent(values);

                    var r = client.PostAsync("http://www.britishgas.co.uk/apps/centrica/pricefinder/POST.servlet", content).Result;

                    var result = r.Content.ReadAsStringAsync().Result;



                    if (!result.Contains("error"))
                    {
                        BritishGasEstimatesModel model = r.Content.ReadAsAsync<BritishGasEstimatesModel>().Result;

                        //PostcodeEstimatedPrice dbModel = new PostcodeEstimatedPrice() {
                        //    ID
                        //}

                        //var currentPostcodes = client.GetAsync("https://www.dividabill.co.uk/odata/PostCodes?$filter=startswith(Postcode,'"+item.Key+"')").Result;
                        //currentPostcodes.EnsureSuccessStatusCode();
                        var container = new Default.Container(new Uri("https://www.dividabill.co.uk/odata/"));

                        var currentPostcodes = container.PostCodes.Where(p => p.Postcode.StartsWith(item.Key)).ToList();

                        PostcodeEstimatedPrice estimatedModel = new PostcodeEstimatedPrice();

                        foreach (Prices price in model.prices)
                        {
                            foreach (FuelTypes fuelType in price.fuelTypes)
                            {
                                if (fuelType.fuelType == "gas")
                                {
                                    estimatedModel.Gas = Convert.ToDecimal(fuelType.tariffRates.currentUnitRates.tariffComparisonRate);
                                }
                                else if (fuelType.fuelType == "elecsr")
                                {
                                    estimatedModel.Electricity = Convert.ToDecimal(fuelType.tariffRates.currentUnitRates.tariffComparisonRate);
                                }
                            }
                        }

                        foreach (var postcode in currentPostcodes)
                        {
                            var stopwatch = Stopwatch.StartNew();
                            //estimatedModel.PostCode_ID = postcode.ID;
                            //PostcodeEstimatedPrice price = container.Prices.Where()
                            var response = client.PutAsJsonAsync<PostcodeEstimatedPrice>("https://www.dividabill.co.uk/odata/Prices('"+postcode.Postcode+"')", estimatedModel).Result;

                            
                            //container.UpdateObject()
                            var reply = response.Content.ReadAsStringAsync().Result;

                            if ((response.StatusCode != HttpStatusCode.Created) && (response.StatusCode != HttpStatusCode.Conflict))
                            {
                                Console.WriteLine("Not added. Code: " + response.StatusCode);
                                Console.WriteLine(reply);
                                notAdded.Add(item.Key);
                            }
                            else
                            {
                                Console.WriteLine("Added {1} in {0}. Code: " + response.StatusCode, stopwatch.Elapsed.TotalSeconds, postcode.Postcode);
                            }
                        }
                    }


                    if ((r.StatusCode != HttpStatusCode.OK))
                    {
                        Console.WriteLine("Not added. Reason: " + r.StatusCode);
                        Console.WriteLine(result);
                        notAdded.Add(item.Key);
                    }

                }
                catch (Exception exception)
                {
                    Console.WriteLine("Not added. Reason: Exception");
                    notAdded.Add(item.Key);
                    Console.WriteLine(exception);
                }

            }

            //using (var client = new HttpClient())
            //{
            //    //Add client headers
            //    client.BaseAddress = new Uri("https://www.dividabill.co.uk/");
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    client.Timeout = new TimeSpan(1, 0, 0);
            //    Console.WriteLine("Timeout set to: " + client.Timeout);
            //    try
            //    {
            //        HttpResponseMessage response = await client.GetAsync("odata/PostCodes");
            //        response.EnsureSuccessStatusCode();
            //        if (response.IsSuccessStatusCode)
            //        {
            //            List<PostCode> postcodes = await response.Content.ReadAsAsync<List<PostCode>>();
            //            foreach (PostCode item in postcodes)
            //            {
            //                Console.WriteLine("{0}", item.Postcode);
            //            }
            //        }
            //    }
            //    catch (HttpRequestException e)
            //    {
            //        Console.WriteLine(e.StackTrace);
            //    }
            //}

            Console.ReadKey();
        }
    }
}
