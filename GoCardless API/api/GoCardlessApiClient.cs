﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using API.Library.Models;
using API.Library.Request.Impl;
using DividaBill.Models.GoCardless;
using GoCardless_API.Models;
using RestSharp;

namespace GoCardless_API.api
{
    public class GoCardlessApiClient : IGoCardlessApiClient, IDisposable
    {
        private readonly ApiRestRequest _apiRestRequest;

        public GoCardlessApiClient()
        {
            var apiKey = "ORG4Iy7eVRLanHq8qYR1ZTVhLDpiVfPpm8CcU1ds";
            var baseUri = new Uri(GoCardless.BaseUrl);
            var userAgent = GoCardless.UserAgent;
            var header = new Dictionary<string, string> {{"GoCardless-Version", "2015-07-06"}};
            var apiRestConfig = new ApiRestConfig(baseUri, userAgent, apiKey, header);
            _apiRestRequest = new ApiRestRequest(apiRestConfig);
            //var _apiKey = "AK000010DJF8T2";
            //var _apiSecret = "X_pd0QiOUBwdvNAAWCdFj4nSUzMS4XjjWomEG_Zq";

            //var apitoken = "ORG4Iy7eVRLanHq8qYR1ZTVhLDpiVfPpm8CcU1ds";
        }

        public void Dispose()
        {
            _apiRestRequest.Dispose();
        }

        public async Task<Customer> CreateCustomerAsync(NewCustomerInfo customer)
        {
            var customerRequest = new CustomerRequest {customers = customer};
            var restRequest = _apiRestRequest.CreateRestRequest("customers", Method.POST, customerRequest);

            var response = await _apiRestRequest.ExecuteAsync<CustomerResponse>(restRequest, HttpStatusCode.Created);
            return response.customers;
        }

        public async Task<GCBankAccount> CreateBankAccountAsync(NewBankAccount bankAccount)
        {
            var bankAccountRequest = new BankAccountRequest {customer_bank_accounts = bankAccount};
            var restRequest = _apiRestRequest.CreateRestRequest("customer_bank_accounts", Method.POST, bankAccountRequest);

            var response = await _apiRestRequest.ExecuteAsync<BankAccountResponse>(restRequest, HttpStatusCode.Created);
            return response.customer_bank_accounts;
        }

        public async Task<GCBankAccount> GetBankAccountAsync(string accountId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("customer_bank_accounts/{id}", Method.GET);
            restRequest.AddUrlSegment("id", accountId);

            var response = await _apiRestRequest.ExecuteAsync<BankAccountResponse>(restRequest);
            return response.customer_bank_accounts;
        }

        public async Task<List<GCExistingBankAccount>> GetBankAccountsAsync(string customerId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("customer_bank_accounts/?customer={customer}", Method.GET);
            restRequest.AddUrlSegment("customer", customerId);

            var response = await _apiRestRequest.ExecuteAsync<ExistingBankAccountResponse>(restRequest);
            return response.customer_bank_accounts;
        }

        public async Task<GCBankAccount> DisableBankAccountAsync(string bankAccountId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("customer_bank_accounts/{id}/actions/disable", Method.POST)
                .AddUrlSegment("id", bankAccountId);

            var response = await _apiRestRequest.ExecuteAsync<BankAccountResponse>(restRequest);
            return response.customer_bank_accounts;
        }

        public async Task<GCMandate> CreateMandateAsync(NewMandate mandate)
        {
            mandate.scheme = "bacs";
            mandate.links.Add("creditor", "CR0000379K4XJG");
            var bankAccountRequest = new MandateRequest {mandates = mandate};
            var restRequest = _apiRestRequest.CreateRestRequest("mandates", Method.POST, bankAccountRequest);

            var response = await _apiRestRequest.ExecuteAsync<MandateResponse>(restRequest, HttpStatusCode.Created);
            return response.Mandates;
        }

        public async Task<BankAccountDetailsLookupsOutputModel> CheckBankAccountDetails(GCBankAccountDetailsInputModel bankDetails)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("bank_details_lookups", Method.POST, bankDetails);

            var response = await _apiRestRequest.ExecuteAsync<GCBankAccountDetailsOutputModel>(restRequest, HttpStatusCode.OK);
            return response.bank_details_lookups;
        }

        public async Task<GCMandate> GetMandateAsync(string mandateId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("mandates/{id}", Method.GET)
                .AddUrlSegment("id", mandateId);

            var response = await _apiRestRequest.ExecuteAsync<MandateResponse>(restRequest);
            return response.Mandates;
        }

        public async Task<List<GCMandate>> GetMandatesForBankAccount(string bankAccountId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("mandates/?customer_bank_account={bankAccountId}", Method.GET);
            restRequest.AddUrlSegment("bankAccountId", bankAccountId);

            var response = await _apiRestRequest.ExecuteAsync<MandatesResponse>(restRequest);
            return response.Mandates;
        }

        public async Task<GCMandate> CancelMandateAsync(string mandateId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("mandates/{id}/actions/cancel", Method.POST)
                .AddUrlSegment("id", mandateId);

            var response = await _apiRestRequest.ExecuteAsync<MandateResponse>(restRequest);
            return response.Mandates;
        }

        public async Task<GCPayment> CreatePaymentAsync(NewPayment payment)
        {
            var data = new PaymentRequest {payments = payment};
            var restRequest = _apiRestRequest.CreateRestRequest("payments", Method.POST, data);

            var response = await _apiRestRequest.ExecuteAsync<PaymentResponse>(restRequest, HttpStatusCode.Created);
            return response.GcPayments;
        }

        public async Task<GCPayment> GetPaymentAsync(string paymentId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("payments/{id}", Method.GET)
                .AddUrlSegment("id", paymentId);

            var response = await _apiRestRequest.ExecuteAsync<PaymentResponse>(restRequest);
            return response.GcPayments;
        }

        public async Task<List<GCPayment>> GetPaymentsAsync()
        {
            var restRequest = _apiRestRequest.CreateRestRequest("payments", Method.GET);

            var response = await _apiRestRequest.ExecuteAsync<PaymentListResponse>(restRequest);
            return response.payments;
        }

        public async Task<GCPayment> CancelPaymentAsync(string paymentId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("payments/{id}/actions/cancel", Method.POST)
                .AddUrlSegment("id", paymentId);

            var response = await _apiRestRequest.ExecuteAsync<PaymentResponse>(restRequest);
            return response.GcPayments;
        }

        public void SetGoCardlessEnvironment(GoCardless.Environments goCardlessEnvironment)
        {
            GoCardless.Environment = goCardlessEnvironment;
        }
    }
}