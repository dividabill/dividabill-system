﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class CustomerResponse
    {
        public Customer customers { get; set; }
    }
}