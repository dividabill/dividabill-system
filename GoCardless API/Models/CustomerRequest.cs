﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class CustomerRequest
    {
        public NewCustomerInfo customers { get; set; }
    }
}