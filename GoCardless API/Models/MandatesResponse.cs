﻿using System.Collections.Generic;
using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class MandatesResponse
    {
        public List<GCMandate> Mandates { get; set; }
    }
}