﻿using System.Collections.Generic;
using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class PaymentListResponse
    {
        public List<GCPayment> payments { get; set; }
    }
}