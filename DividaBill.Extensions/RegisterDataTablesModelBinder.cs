using System.Web;
using System.Web.Mvc;
using DividaBill.Extensions;
using Mvc.JQuery.DataTables;

[assembly: PreApplicationStartMethod(typeof (RegisterDataTablesModelBinder), "Start")]

namespace DividaBill.Extensions
{
    public static class RegisterDataTablesModelBinder
    {
        public static void Start()
        {
            if (!ModelBinders.Binders.ContainsKey(typeof (DataTablesParam)))
                ModelBinders.Binders.Add(typeof (DataTablesParam), new DataTablesModelBinder());
        }
    }
}