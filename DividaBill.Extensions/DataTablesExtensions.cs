﻿using System;
using System.Linq;
using Mvc.JQuery.DataTables;
using Mvc.JQuery.DataTables.Models;

namespace DividaBill.Extensions
{
    public static class DataTablesExtensions
    {
        internal static DataTablesResponseData CopyTotalParams(this DataTablesResponseData data, DataTablesResponseData dataForCopy)
        {
            data.iTotalDisplayRecords = dataForCopy.iTotalDisplayRecords;
            data.iTotalRecords = dataForCopy.iTotalRecords;
            data.sEcho = dataForCopy.sEcho;
            return data;
        }

        public static IQueryable<TSource> ConvertResults<TSource>(this DataTablesResponseData data)
        {
            return data.aaData.Cast<TSource>().AsQueryable();
        }

        public static IQueryable<TResult> ConvertResults<TSource, TResult>(this DataTablesResponseData data, Func<TSource, TResult> transformRow)
        {
            var transformedData = data.Transform(transformRow);
            return transformedData.ConvertResults<TResult>();
        }

        public static DataTablesResult<TSource> Create<TSource, TTransform>(this DataTablesResponseData data, IQueryable<TSource> q, Func<TSource, TTransform> transform)
        {
            var transformedData = DataTablesResult.Create(q, new DataTablesParam(), transform);
            transformedData.Data = transformedData.Data.CopyTotalParams(data);
            return transformedData;
        }
    }
}