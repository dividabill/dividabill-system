﻿using Seterlund.CodeGuard;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace DividaBill
{
    public class BundleConfig
    {

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Styles.DefaultTagFormat = "<link href='https://az754941.vo.msecnd.net{0}' rel='stylesheet'/>";

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/signup").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/autocomplete").Include(
                        "~/Scripts/jquery.autocomplete.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
            "~/Scripts/jquery.unobtrusive-ajax.js"));


            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                    "~/Content/themes/base/all.css"));

            bundles.Add(new StyleBundle("~/Content/metronic-login").Include(
                "~/Content/metronic/pages/login-soft.css",
                "~/Content/select2.css"));


            bundles.Add(new ScriptBundle("~/bundles/metronic-login").Include(
                "~/Scripts/login.js",
                "~/Scripts/jquery.backstretch.js",
                "~/Scripts/select2.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css2").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/site.css",
                        "~/Content/font-awesome.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/FABSCheckbox.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                        "~/Scripts/site.js"
                ));

            bundles.Add(new StyleBundle("~/Content/signup").Include(
                        "~/Content/bootstrap-select.css",
                        "~/Content/bootstrap-datepicker3.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/postcodefinder").Include(
                        "~/Scripts/postcodefinder-{version}.js",
                        "~/Scripts/postcodeanywhere.js",
                        "~/Scripts/bootstrap-select.js",
                        "~/Scripts/bootstrap-datepicker.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/signup.js",
                        "~/Scripts/jquery.inputmask/inputmask.js",
                        "~/Scripts/jquery.inputmask/jquery.inputmask.js",
                        "~/Scripts/jquery.inputmask/inputmask.extensions.js",
                        "~/Scripts/jquery.inputmask/inputmask.phone.extensions.js"));

            bundles.Add(new ScriptBundle("~/bundles/postcodefinderOnly").Include(
                        "~/Scripts/postcodefinder-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/zxcvbn").Include(
                        "~/Scripts/zxcvbn/zxcvbn.js"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }

    public class ReplaceContentsBundleBuilder : IBundleBuilder
    {
        private readonly string _find;

        private readonly string _replaceWith;

        private readonly IBundleBuilder _builder;

        public ReplaceContentsBundleBuilder(string find, string replaceWith)
            : this(find, replaceWith, new DefaultBundleBuilder())
        {
        }

        public ReplaceContentsBundleBuilder(string find, string replaceWith, IBundleBuilder builder)
        {
            Guard.That(() => find).IsNotNullOrEmpty();
            Guard.That(() => replaceWith).IsNotNullOrEmpty();
            Guard.That(() => builder).IsNotNull();

            _find = find;
            _replaceWith = replaceWith;
            _builder = builder;
        }

        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<System.Web.Optimization.BundleFile> files)
        {
            string contents = _builder.BuildBundleContent(bundle, context, files);

            return contents.Replace(_find, _replaceWith);
        }
    }
}
