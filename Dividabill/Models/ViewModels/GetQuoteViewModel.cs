﻿using DividaBill.Models;
using DividaBill.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models
{
    [Serializable]
    public class GetQuoteViewModel
    {
        [Required]
        [Display(Name = "Postcode")]
        public String PostCode { get; set; }

        [Required]
        [Display(Name = "Number of people")]
        public UInt16 Housemates { get; set; }

        [Display(Name="Discount code")]
        public String Coupon { get; set; }
        public Boolean Gas { get; set; }

        public Boolean Water { get; set; }
        public Boolean Electricity { get; set; }
        public Boolean Media { get; set; }

        public Boolean Broadband { get; set; }

        public Boolean SkyTV { get; set; }

        public Boolean NetflixTV { get; set; }

        public Boolean LandlinePhone { get; set; }

        public Boolean TVLicense { get; set; }

        public BroadbandType BroadbandType { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }


        [Required]
        [Display(Name = "Name")]
        public string FullName { get; set; }

        public LandlineType LandlinePhoneType { get; set; }

        public bool TV { get; set; }
    }
}