﻿$(document).ready(function () {
    console.log(location.pathname);
	var string = location.search;
	var length = location.search.length;
	if (string != "") {
		var substr = string.substr(1);
		var query = substr.split("=");
		var ps_query = query[1].replace(/\+/g, "");
		if (query[0] == "postcode" && length < 20) {
			$("#PostCode").val(ps_query)
			getBuildings('#building', $('#PostCode').val());
		}
		if (query[0] == "coupon") {
			$("#Coupon").val(query[1]);
			$("#Coupon").attr("readonly", "readonly")
		}
		if (length > 20) {
			var query = substr.split("&");
			var ps_full = query[0].replace(/\+/g, "");
			var cp_full = query[1].replace(/\+/g, "");
			var coupon = cp_full.split("=");
			var postcode = ps_full.split("=");
			if (postcode[0] == "postcode") {
			    $("#PostCode").val(postcode[1]);
			    getBuildings('#building', $('#PostCode').val());
			}
			if (coupon[0] == "coupon") {
			    $("#Coupon").val(coupon[1]);
			    $("#Coupon").attr("readonly", "readonly")
			}
		}
	}
});

$(".step_1").ready(function () {
    if (location.pathname == "/Account/Register" && $(".field-validation-error").length > 0) {
        $("#step-one").removeClass("active-step");
        $("#step-one").addClass("active-green");
        $("#step-two").addClass("active-green");
        $("#step-three").addClass("active-step");
        $("#step-two").attr({ "href": "#signupOne" });
        $("#step-three").attr({ "href": "#signupTwo" });

    }
    else
    {
        $('#DoB').val('').datepicker('update');
    }
});

$("#PostCode").keydown(function (e) {
    if (e.keyCode == 13) {
        getBuildings('#building', $('#PostCode').val());
    }
});

$(".quick-find").click(function () {
    getBuildings('#building', $('#PostCode').val());
});

function getBuildings(buildingsContainer, postcode) {

    if (!$("#quick-find").hasClass("fa-spinner")) {
        $("#quick-find").addClass("fa-spinner fa-spin");
        $.getJSON(window.env.getPublicApiUrl('Address/Get/' + postcode)).done(
            function (result) {
                // Check if there were any items found
                $("#quick-find").removeClass("fa-spinner fa-spin");

                if (result.length == 0)
                    alert("Sorry, there were no results");
                else {
                    $(buildingsContainer).empty();
                    $(buildingsContainer).parent().parent().hide();

                    $(buildingsContainer)
                            .append($("<option></option>")
                            .text("Please Select Building"));

                    $.each(result, function (key, value) {
                        $(buildingsContainer)
                            .append($("<option></option>")
                            .attr("value", value.PAId)
                            .text(value.StreetAddress + ", " + value.Place));
                    });
                    $(buildingsContainer).selectpicker('refresh');
                    $(buildingsContainer).parent().parent().show();
                }
            })
        .fail(function () {
            $("#quick-find").removeClass("fa-spinner fa-spin");
            $('#postcodeError').modal('toggle');
        });
        ga('send', 'event', location.pathname, 'Click', 'Search postcode');
    }
}

function selectBuilding(premiseId) {
    $.getJSON(window.env.getPublicApiUrl('Building/Get/' + premiseId)).done(
		function (result) {
		    // Check if there were any items found
		    if (result.length == 0)
		        alert("Sorry, there were no results");
		    else {
		        $("#filled-signup-info").removeClass("hidden");
		        $('#PostCode').val(result.PostCode).prop('readonly', true);
		        $('#CountyTT').val(result.County).prop('readonly', true);
		        $('#CityTT').val(result.PostTown).prop('readonly', true);
		        $('#AddressTT1').val(result.Line1).prop('readonly', true);
		        $('#AddressTT2').val(result.Line2).prop('readonly', true);
		        $('#AddressTT3').val(result.Line3).prop('readonly', true);
		        $('#AddressTT4').val(result.Line4).prop('readonly', true);
		        $('#Udprn').val(result.Udprn);

		        $.getJSON(window.env.getPublicApiUrl('House/GetHouse/' + $("#Udprn").val()))
                    .done(function (h) { parseHouse(h); })
                    .fail(function () { parseHouse(null); });
		    }
		});
}

function parseHouse(house) {
    //Bind to fields
    $('.quote-checker').unbind();
    $('#choose-internet').unbind();
    $('#choose-telephone').unbind();
    $('#choose-television').unbind();


    $('.quote-checker').click(MarkChecked);
    $('#choose-internet .radio').change(MarkChecked);
    $('#choose-telephone .radio').change(MarkChecked);
    $('#choose-television :checkbox').change(MarkChecked);


    // Check if there were any items found
    if (house == null || house.length == 0) {

        $('.ischecked').change(function () {
            if ($('#form1').validate().checkForm()) {
                $("#signup-services").removeClass("hidden");
            }
        });

        $('#Housemates').prop('readonly', false).selectedIndex = 0;
        $('#Period').prop('disabled', false).selectedIndex = 0;
        $('.selectpicker').selectpicker('refresh');
        $('#StartTime').datepicker();
        $('#StartTime').prop("disabled", false);

        //$('#Coupon').val("").prop('readonly', false);

        //Unbind the click event trackers so that they dont stackup.


    } else {
        $('.ischecked').unbind();
        $('#myModal').modal();

        $('#Housemates').val(house.Housemates).prop('readonly', true);
        $('#StartTime').prop("disabled", true).val(house.StartDate);
        if (house.Period > 12) {
            $('#Period').prop('disabled', true).selectedIndex = 0;
            $('#Ongoing').prop("checked", true).prop('readonly', true);
        } else {
            $('#Period').val(house.Period).trigger("change");
            $("#Period").next().attr("disabled", true);
            $('#Ongoing').prop("checked", false).prop('disabled', true);
        }
        $('#Coupon').val(house.Coupon).prop('readonly', true);
        $('.selectpicker').selectpicker('refresh');



        if ($('#form1').validate().checkForm()) {
            $("#signup-services").removeClass("hidden");
        }

        if (house.Electricity && !$('#electricity').hasClass('electricity-active')) {
            $('#electricity').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.Gas && !$('#gas').hasClass('gas-active')) {
            $('#gas').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.Water && !$('#water').hasClass('water-active')) {
            $('#water').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.BroadbandType == 1) {
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
            $('#BroadbandTypeADSL20').prop('checked', true);
        } else if (house.BroadbandType == 2) {
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
            $('#BroadbandTypeFibre40').prop('checked', true);
        } else if ($('#internet').hasClass('internet-active')) {
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
        }

        if ((house.SkyTV || house.NetflixTV || house.TVLicense)
            && !$('#television').hasClass('television-active')) {
            $('#television').trigger("click", [{ checkPrice: 'false' }]);
        }
        $('#SkyTV').prop('checked', house.SkyTV);

        $('#NetflixTV').prop('checked', house.NetflixTV);

        $('#TVLicense').prop('checked', house.TVLicense);

        if (house.LandlinePhoneType == 1) {
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
            $('#LandlineTypeBasic').prop('checked', true);
        } else if (house.LandlinePhoneType == 2) {
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
            $('#LandlineTypeMedium').prop('checked', true);
        } else if ($('#telephone').hasClass('telephone-active')) {
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
        }

        //Remove bindings
        $('.quote-checker').unbind("click");
        $('#choose-internet').unbind();
        $('#choose-telephone').unbind();
        $('#choose-television').unbind();
        $('#choose-internet .radio').unbind();
        $('#choose-telephone .radio').unbind();
        $('#choose-television :checkbox').unbind();
        checkCoupon();

        $(".boxes").hide();
        $(".quote-checker").click(function () {
            $('#servicesInformation').modal('toggle');
        })
    }
}

function GetPostCodeByIp() {
    $.getJSON(window.env.getPublicApiUrl('Location/GetLocation/')).done(
		function (result) {
		    // Check if there were any items found
		    if (result.length == 0)
		        return null;
		    else {
		        return result.PostCode;
		    }
		});
}