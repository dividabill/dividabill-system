﻿$(document).ready(function () {
    $('.selectpicker').selectpicker({
        size: "10",
    });

    $("#BankAccountDetails_AccountSortCode").inputmask("99-99-99");
    $("#BankAccountDetails_AccountNumber").inputmask("99999999");
    $('#Phone').inputmask('(+44) 99999-9999[9]');
    $(".signup-nav").css({ "visibility": "hidden" });
    if ($('#Ongoing').is(':checked')) {
        $('#Period').prop('disabled', true);
        $('#Period').selectpicker('refresh');
    }

    $('#reset').click(function () {
        location.reload();
    });

    $('.bank-checkbox').click(function () {
        $($(this)).toggleClass("fa-circle-thin fa-check-circle");

        var checkBox = $('#' + $(this).data('checkbox-id'));
        checkBox.prop("checked", !checkBox.prop("checked"));
    })

    var form = $("#form1");
    var validator = form.data('validator');

   

    validator.settings.ignore = ":disabled,:hidden:not(.active_step select)";

    $.validator.unobtrusive.adapters.addBool("booleanrequired", "required");

    jQuery.validator.methods.date = function (value, element) {
        var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

        if (isChrome || isSafari) {
            var d = new Date();
            var secondValue = moment(value, "DD/MM/YYYY").toDate();
            return this.optional(element) || !/Invalid|NaN/.test(new Date(secondValue));
        } else {
            return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
        }
    };

    $("#reggedHouse").click(function () {
        window.location.href = "#signup-btn1";
    });

    if ($('.validation-summary-errors').length) {
        $("#filled-signup-info").removeClass("hidden");
        $("#signup-services").removeClass("hidden");

        $('.quote-checker').click(MarkChecked);
        $('#choose-internet .radio').change(MarkChecked);
        $('#choose-telephone .radio').change(MarkChecked);
        $('#choose-television :checkbox').change(MarkChecked);

        
        $("#signup-services").find("input[type=checkbox]:not(input[type=hidden]):checked").change(MarkChecked).each(function () {
            $(this).trigger("click", [{ checkPrice: 'false' }]);
        });
    }

    //Bank Details button opens modal only if form is valid
    $("#confirmBDetails").click(function(){
        if(form.valid()){
            $('#lastModal').modal('toggle');
            ga('send', 'event', location.pathname, 'Click', 'Save & Complete SignUp');}
    });

    $('.step_1 .next').click(function () {
            var size = $(".fa-circle").length;
            var buttons = "<button type='button' id='btnUnselect' class='btn btn-danger col-md-3 col-md-offset-3' data-dismiss='modal'>Unselect</button><button type='button' id='btnSelect' class='btn btn-success col-md-3' data-dismiss='modal'>Select</button>";

            var checks = ["#fee-internet", "#fee-tv", "#fee-phone"];
            var null_checked = [];
            for (i = 0; i < checks.length; i++) {
                if ($(checks[i]).find(".value").text() == "0.00" && $(checks[i]).parent().find(".quote-check").hasClass("fa-circle")) {
                    null_checked.push($(checks[i]).parent().find(".services").attr("id"));
                }
            }
            if (null_checked != "") {
                $("#services-error-msg").html("Please choose " + null_checked.join(', ') + " service or unselect it.");
                $("#error-btns").html(buttons);

                $("#btnUnselect").click(function () { unselectService(null_checked[0]) });
                $("#btnSelect").click(function () { selectService(null_checked[0]) });
                $('#servicesError').modal('toggle');
            }
            else if (size > 0 && !$(".nope").is(':checked') || size > 1 || size == 1 && $("#SkyTV").is(":checked")) {
                if ($("form").valid()) {
                    $('.step_1').removeClass('active_step');
                    $("#step-one").removeClass("active-step");
                    $("#step-one").addClass("active-green");
                    $("#step-two").attr({ "href": "#signupOne" });
                    if (!$("#step-two").hasClass("active-green")) {
                        $('#step-two').addClass('active-step');
                    };
                    $('.step_2').addClass('active_step');
                    $('.step_2').delay(500).fadeIn(500);
                    window.location.href = "#signupOne";
                    $(this).hide()
                    ga('send', 'pageview', '/Account/SignUp/Step2');
                }
            }
            else {
                $("#services-error-msg").html("You must choose one or more services to continue. <br/>  P.S. You can't choose Netflix and TV License only");
                $("#error-btns").html("<button type='button' class='btn btn-default col-sm-4 col-sm-offset-4' data-dismiss='modal'>Close</button>");
                $('#servicesError').modal('toggle');
            }
    });

    $('.step_2 .next').click(function () {
        if ($(".step_2 input,select").valid()) {
            $('.step_2').removeClass('active_step');
            $("#step-two").removeClass("active-step");
            $("#step-two").addClass("active-green");
            $('#step-three').addClass('active-step');
            $('.step_3').addClass('active_step');
            $("#step-three").attr({ "href": "#signupTwo" });
            $('.step_3').delay(500).fadeIn(500);
            window.location.href = "#signupTwo";
            $(this).hide();
            ga('send', 'pageview', '/Account/SignUp/Step3');
        }
        return false;
    });

    $('#lastModal').on('show.bs.modal', function (e) {
        if (form.valid()) {
            $('#AccountNameConfirmation').text($('#BankAccountDetails_AccountName').val());
            $('#AccountNumberConfirmation').text($('#BankAccountDetails_AccountNumber').val());
            $('#AccountSortCodeConfirmation').text($('#BankAccountDetails_AccountSortCode').val());
            if ($('#AccountStandalone').is(':checked')) {
                $('#aloneAccountValue').text("Yes");
            } else {
                $('#aloneAccountValue').text("No");
            }
            $('.confirm-bank').fadeIn();
        }
        // e.preventDefault()
    })

    $("#Ongoing").change(function () {
        $('#Period').prop('disabled', this.checked);
        $('#Period').selectpicker('refresh');
    });

    $('.step_3 .change').click(function () {
        $('.confirm-bank').fadeOut(500);
        window.location.href = "#main";
        return false;
    });

//Validation Error - show all steps
if (location.pathname == "/Account/Register") {
    $('.step_1').ready(function () {
        $('.step_1 .next').hide();
        $('.step_2 .next').hide();
        $('.step_2').show();
        $('.step_3').show();
        $(".info-box").hide();
    });
};
});
    
    /* /////////////////
    FUNCTION
    //////////////////// */
    function serviceHover(e) {
        checkbox = this;
        var id = $(".third-single-img", $(this)).attr('id');
        if ($(".third-single-img", $(checkbox)).hasClass(id + '-active')) {
            $(".third-single-img", $(checkbox)).css('color', 'white');
            $(".boxes").hide();
            $(checkbox).hover(function () {
                $(".boxes").hide();
                $("#choose-" + id).toggle();
            });
        } else {
            $(".third-single-img", $(checkbox)).css('color', '');
            $(checkbox).off("mouseenter mouseleave");
        }

    }

    
    //Change CSS To Checkbox
    function ChangeCssChck(id, checkbox) {
        $(".fee-per-week", $(checkbox)).toggle();
        $(".third-single-img", $(checkbox)).toggleClass(id + '-active');

        if ($(".third-single-img", $(checkbox)).hasClass(id + '-active')) {
            $(".third-single-img", $(checkbox)).css('color', 'white');

            $(checkbox).hover(function () {
                $(".boxes").hide();
                $("#choose-" + id).toggle();
            });
        } else {
            $(".third-single-img", $(checkbox)).css('color', '');
            $(checkbox).off("mouseenter mouseleave");
        }

        $(".quote-check", $(checkbox)).toggleClass('fa-circle');

       

    }


    //Show message why Gas service can be clicked
    $("#gas").parent().click(function gasChecked() {
        if ($("#gas").hasClass("gas-active")) {
            $(".gas-notshow-msg").hide();
        }
        else {
            $(".gas-notshow-msg").show();
        }

    });

    // checkbox
    var MarkChecked = function (e, checkPrice) {
        if (typeof (checkPrice) === 'undefined') checkPrice = true;
        var id = $(".third-single-img", $(this)).attr('id');

        ChangeCssChck(id, this);

        var checkBoxes = $(this).find("input[type=checkbox]:not(input[type=hidden])");
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        if (id == 'television') {
            $('#choose-television').toggle();
            $('#choose-television :input').prop("disabled", !checkBoxes.prop("checked"))

            if($("#choose-television").is(':hidden'))
            {
                $('#choose-television :input').prop('checked', false);
            }
        }

        if (id == 'internet') {
            $('#choose-internet').toggle();
            $('#choose-internet :input').prop("disabled", !checkBoxes.prop("checked"));
            if ($("#choose-internet").is(':hidden')) {
                $('#choose-internet :input').prop('checked', false);
            }
        }

        if (id == 'telephone') {
            $('#choose-telephone').toggle();
            $('#choose-telephone :input').prop("disabled", !checkBoxes.prop("checked"));
            if ($("#choose-telephone").is(':hidden')) {
                $('#choose-telephone :input').prop('checked', false);
            }
        }

        if (id == 'electricity') {
            $('#choose-electricity').toggle();
            $("#gas").parent().toggleClass("quote-checker");
            if ($("#gas").hasClass("gas-active")) {
                var checkBoxes = $("#gas").parent().find("input[type=checkbox]:not(input[type=hidden])");
                checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                $("#gas").removeClass("gas-active");
                $("#gas").css("color", "");
                ChangeCssChck("gas", "#gas");
               
                $("#gas").parent().find(".fee-per-week").hide();
                $("#gas").parent().find($(".quote-check")).toggleClass('fa-circle');
                
                $(".gas-notshow-msg").removeClass("hidden");
                $("#gas").parent().unbind();
                $("#gas").parent().click(function () {
                    $(".gas-notshow-msg").show();
                });
                
            }
            else {
                $(".gas-notshow-msg").hide()
                $('#gas').parent().click(MarkChecked);
            }
        }

        if (id == 'gas') {
            $('#choose-gas').toggle();
 
        }
        if (id == 'water') {
            $("#choose-water").toggle();
        }

        if (checkPrice) {
            checkCoupon();
        }
    };


    function checkCoupon() {
        // add the overlay with loading image to the page
        $('#overlay').fadeIn("slow");
        console.log("Check pricing...")
        if (typeof ($('input[name=BroadbandType]:checked').val()) === 'undefined') {
            broadband = "None";
        } else {
            broadband = $('input[name=BroadbandType]:checked:enabled').val()
        }
        if (typeof ($('input[name=LandlinePhoneType]:checked').val()) === 'undefined') {
            landline = "None";
        } else {
            landline = $('input[name=LandlinePhoneType]:checked:enabled').val()
        }
        
        var postCode =  $('#PostCode').val()
        var id = {
            'coupon': $('#Coupon').val(),
            'tenants': $('#Housemates').val(),
            'postcode': postCode,
            'water': $('#Water').is(":checked"),
            'gas': $('#Gas:enabled').is(":checked"),
            'electricity': $('#Electricity').is(":checked"),
            'broadband': broadband,
            'skytv': $('#SkyTV:enabled').is(":checked"),
            'tvlicense': $('#TVLicense:enabled').is(":checked"),
            'phoneline': landline,
            'netflixtv': $('#NetflixTV:enabled').is(":checked"),
        }
        
        $.getJSON(window.env.getPublicApiUrl('Price/UnitPricing/' + postCode)).done(
        function (price) {
            // Check if there were any items found
            if (price.length != 0) {
                var elec_unit = price.ElectrcityUnitRate.toFixed(2);
                var elec_st_ch = price.ElectricityStandingCharge.toFixed(2);
                var gas_unit = price.GasUnitRate.toFixed(2);
                var gas_st_ch = price.GasStandingCharge.toFixed(2);

                $('#tariff-elec-unit').find(".value").html(elec_unit + "p per KwH");
                $('#tariff-elec-charge').find(".value").html(elec_st_ch + "p per day");
                $('#tariff-gas-unit').find(".value").html(gas_unit + "p per KwH");
                $('#tariff-gas-charge').find(".value").html(gas_st_ch + "p per day");
            }
        });

        $.getJSON(window.env.getPublicApiUrl('Coupon'), id).done(
    function (price) {
        // Check if there were any items found
        if (price.length != 0) {


            $('#fee-electricity').find(".value").html(parseFloat(price.Electricity).toFixed(2));
            $('#fee-gas').find(".value").html(parseFloat(price.Gas).toFixed(2));
            $('#fee-water').find(".value").html(parseFloat(price.Water).toFixed(2));



            var broadbandPrice = 0;
            var landlinePrice = 0;

            if (broadband == "Fibre40") {
                broadbandPrice += price.BroadbandFibre40 + price.LineRental;
            } else if (broadband == "ADSL20") {
                broadbandPrice += price.BroadbandADSL + price.LineRental;
            }

            if ((landline == "Basic" || landline == "Medium") && (broadband != "Fibre40" && broadband != "ADSL20")) {
                landlinePrice += price.LineRental;
            }

            if (landline == "Basic") {
                landlinePrice += price.LandlinePhoneBasic;
            } else if (landline == "Medium") {
                landlinePrice += price.LandlinePhoneMedium;
            }



            $('#fee-internet').find(".value").html(parseFloat(broadbandPrice).toFixed(2));
            var tvprice = 0;
            tvprice += $('#SkyTV').is(":checked") ? price.SkyTV : 0;
            tvprice += $('#TVLicense').is(":checked") ? price.TVLicense : 0;
            tvprice += $('#NetflixTV').is(":checked") ? price.NetflixTV : 0;
            $('#fee-tv').find(".value").html(parseFloat(tvprice).toFixed(2));
            $('#fee-phone').find(".value").html(parseFloat(landlinePrice).toFixed(2));
            $('#housing-cost').val("£" + parseFloat(price.TotalBill * $('#Housemates').val()).toFixed(2));
            $('#person-cost').val("£" + parseFloat(price.TotalBill).toFixed(2));

            $('#overlay').fadeOut("slow");
            $("#prices").removeClass("hidden");
        }
    })
    };


    //jQuery Validator to Bootstrap class binder.
    jQuery.validator.setDefaults({
        highlight: function (element, errorClass, validClass) {
            if (element.type === 'radio') {
                this.findByName(element.name).addClass(errorClass).removeClass(validClass);
            } else {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            if (element.type === 'radio') {
                this.findByName(element.name).removeClass(errorClass).addClass(validClass);
            } else {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }
        }
    });



    $(function () {
        $("span.field-validation-valid, span.field-validation-error").addClass('help-block');
        $("div.form-group").has("span.field-validation-error").addClass('has-error');
        $("div.validation-summary-errors").has("li:visible").addClass("alert alert-block alert-danger");

        /*if ($("div.validation-summary-errors").hasClass("alert alert-block alert-danger")) {

            $('.step_1').hide();
            $('.step_1').removeClass('active_step');

            $('.step_2').hide();
            $('.step_2').removeClass('active_step');

            $('.step_3').addClass('active_step');
            $('.step_3').show();

            $('#step-one').toggleClass('active-step active-green');
            $('#step-two').addClass('active-green');
            $('#step-three').addClass('active-step');

        }*/
    });
    
    function selectService(service)
    {
        window.location.href = "#more-services-info";
        $(".boxes").hide();
        $("#choose-" + service).fadeIn();
        $("#choose-" + service).find(".info-box").addClass('addShadow');
        setTimeout(function () {
            $("#choose-" + service).find(".info-box").removeClass('addShadow', 2000);           
        }, 3000);
    }

    function unselectService(service)
    {
        var checkBoxes = $("#" + service).parent().find("input[type=checkbox]:not(input[type=hidden])");
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        //if (!checkBoxes.prop("checked"))
        //    
        if ($("#choose-"+service).is(':hidden')) {
            $('#choose-' + service + ' :input').prop('checked', false);
            $('#choose-' + service + ' :input').prop("disabled", true);
        }
        $('#choose-' + service).hide();
        $("#" + service).removeClass(service+"-active");
        $("#" + service).parent().find(".fee-per-week").hide();
        $("#" + service).parent().find($(".quote-check")).removeClass('fa-circle');
        $("#" + service).css("color", "");
        $("#" + service).parent().off("mouseenter mouseleave");
        
        window.location.href = "#signup-services";
    }

    //Check password strength
    function checkPassword()
    {
        var pass = $('#Password').val();

        var pass_score = 0;
        if (pass != "")
        {
            var res = zxcvbn.zxcvbn(pass);
            pass_score = res.score + 1;
        }

        var passStrengthBar = $('#PassMeter');
        passStrengthBar
            .css('width', (100 * pass_score / 5) + '%')
            .attr('aria-valuenow', pass_score)
            .attr('title', 'Password Strength')
            .removeClass('progress-bar-success progress-bar-warning progress-bar-danger');

        if(pass_score < 2)
            passStrengthBar.addClass('progress-bar-danger')
        else if (pass_score < 4)
            passStrengthBar.addClass('progress-bar-warning')
        else
            passStrengthBar.addClass('progress-bar-success')
    }
