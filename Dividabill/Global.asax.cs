﻿using System;
using System.Data.Entity.Migrations;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DividaBill.App_Start;
using DividaBill.DAL.Migrations;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Exceptions;

namespace DividaBill
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperWebConfiguration.Configure();

            //Scheduler.Start();
            var configuration = new Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            var exception = Server.GetLastError();

            ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(exception, typeof (MvcApplication).Assembly));
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}