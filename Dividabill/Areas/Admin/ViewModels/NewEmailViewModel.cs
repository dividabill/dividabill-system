﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class NewEmailViewModel
    {
        public String To { get; set; }
        public String Subject { get; set; }
        [AllowHtml]
        public String Message { get; set; }
        public int HouseId { get; set; }
    }
}