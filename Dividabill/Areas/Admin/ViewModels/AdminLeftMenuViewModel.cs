﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class AdminLeftMenuViewModel
    {
        public UserMinimalViewModel User { get; set; }
        public List<INotificationViewModel> Notifications { get; set; }
    }
}