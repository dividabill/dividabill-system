﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dividabill.Models;
using Dividabill.Areas.Admin.ViewModels;
using Dividabill.Helpers;
using Dividabill.Services;
using Dividabill.DAL;

namespace Dividabill.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class BillsController : AdminController
    {
        // GET: Admin/Bills
        public ActionResult Index(string id)
        {
            DateTime parsedSelectedTime = DateTime.Now;
            if (!DateTime.TryParse(id, out parsedSelectedTime))
            {
                parsedSelectedTime = DateTime.Now;
            }
            DateTime selectedMonthBegins = new DateTime(parsedSelectedTime.Year, parsedSelectedTime.Month, 1);
            DateTime selectedMonthEnds = selectedMonthBegins.AddMonths(1);

            //Define Bills list
            List<BillsViewModel> selectedBills = new List<BillsViewModel>();

            //Get the bills for the selected month
            List<Bill> bills = unitOfWork.BillRepository.Get(p => p.StartDate >= selectedMonthBegins && p.EndDate < selectedMonthEnds).ToList();

            foreach (Bill bill in bills)
            {
                selectedBills.Add(new BillsViewModel { House = bill.House, ElectricBill = bill.ElectricBill, GasBill = bill.GasBill, WaterBill = bill.WaterBill, Note = bill.Note, Adjustment = bill.Adjustment, BillID = bill.Id, ServiceCharge = bill.DeliveryFee });
            }
            return View(selectedBills);
        }

        // GET: Admin/Bills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = unitOfWork.BillRepository.GetByID(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // GET: Admin/Bills/Create
        public ActionResult Create()
        {

            var bill = new Bill();
            bill.Month = DateTime.Now;
            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Line1");
            return View(bill);
        }

        [RequireRequestValue("houseId")]
        public ActionResult Create(int houseId)
        {

            HouseModel house = unitOfWork.ActiveHouses.GetByID(houseId);
            if (house == null)
            {
                return HttpNotFound();
            }
            var bill = new Bill();
            bill.HouseModelID = houseId;
            bill.Month = DateTime.Now;
            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Line1");
            ViewBag.DisabledDropdownList = true;
            return View(bill);
        }

        // POST: Admin/Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bill mbill)
        {
            //if (bill.House == null)
            //{
            //    ModelState.AddModelError("House", "The house can't be empty.");
            //}
            if (ModelState.IsValid)
            {
                if (mbill.Type == BillType.Estimated)
                {
                    BillService _service = new BillService(unitOfWork);
                    _service.GenerateEstimatedBill(mbill.StartDate, mbill.EndDate, mbill.HouseModelID);
                }
                else
                {
                    unitOfWork.BillRepository.Insert(mbill);
                    unitOfWork.Save();
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Street");
            }

            return View(mbill);
        }

        // GET: Admin/Bills/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = unitOfWork.BillRepository.GetByID(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Line1");
            return View(bill);
        }

        // POST: Admin/Bills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ElectricBill,GasBill,WaterBill,Month,HouseModelID,Adjustment,Note,Status")] Bill bill)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.BillRepository.Update(bill);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(bill);
        }

        // GET: Admin/Bills/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = unitOfWork.BillRepository.GetByID(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Admin/Bills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bill bill = unitOfWork.BillRepository.GetByID(id);
            unitOfWork.BillRepository.Delete(bill);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
