﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dividabill.Models;
using Dividabill.DAL;

namespace Dividabill.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class MediaController : AdminController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Media
        public ActionResult Index()
        {
            return View(db.MediaRequests.ToList());
        }

        // GET: Admin/Media/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediaRequest mediaRequest = db.MediaRequests.Find(id);
            if (mediaRequest == null)
            {
                return HttpNotFound();
            }
            return View(mediaRequest);
        }

        // GET: Admin/Media/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Media/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Email,Phone,Address,RequestDate,provider,Created")] MediaRequest mediaRequest)
        {
            if (ModelState.IsValid)
            {
                db.MediaRequests.Add(mediaRequest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mediaRequest);
        }

        // GET: Admin/Media/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediaRequest mediaRequest = db.MediaRequests.Find(id);
            if (mediaRequest == null)
            {
                return HttpNotFound();
            }
            return View(mediaRequest);
        }

        // POST: Admin/Media/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Email,Phone,Address,RequestDate,provider,Created")] MediaRequest mediaRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mediaRequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mediaRequest);
        }

        // GET: Admin/Media/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MediaRequest mediaRequest = db.MediaRequests.Find(id);
            if (mediaRequest == null)
            {
                return HttpNotFound();
            }
            return View(mediaRequest);
        }

        // POST: Admin/Media/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MediaRequest mediaRequest = db.MediaRequests.Find(id);
            db.MediaRequests.Remove(mediaRequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
