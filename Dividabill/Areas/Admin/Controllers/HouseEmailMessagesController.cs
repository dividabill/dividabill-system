﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dividabill.DAL;
using Dividabill.Models;
using Dividabill.Areas.Admin.ViewModels;
using System.Net.Mail;
using S22.Mail;

namespace Dividabill.Areas.Admin.Controllers
{
    public class HouseEmailMessagesController : AdminController
    {

        // GET: Admin/HouseEmailMessages
        public async Task<ActionResult> Index(int id)
        {
            ViewBag.houseId = id;
            return View();
        }

        public async Task<ActionResult> Inbox(int id)
        {
            var houseEmails = unitOfWork.HouseEmailsRepository.Get(e => e.House_ID == id);
            List<EmailsViewModel> emails = new List<EmailsViewModel>();
            foreach(HouseEmailMessage message in houseEmails) {
                SerializableMailMessage email = Services.EmailService.GetMessageFromByteArray(message.Message);
                var emailMessage = new EmailsViewModel
                {
                    ID = message.ID,
                    Attachments = email.Attachments,
                    Sent = message.Sent,
                    Subject = email.Subject,
                    To = ""
                };
                emails.Add(emailMessage);
            }

            return PartialView("_Inbox", emails);
        }

        // GET: Admin/HouseEmailMessages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseEmailMessage message = unitOfWork.HouseEmailsRepository.GetByID(id);


            if (message == null)
            {
                return HttpNotFound();
            }



            SerializableMailMessage email = Services.EmailService.GetMessageFromByteArray(message.Message);
            EmailsViewModel emailMessage = new EmailsViewModel
            {
                ID = message.ID,
                //HasAttachement = (email.Attachments.Count() == 0) ? false : true,
                Sent = message.Sent,
                Attachments = email.Attachments,
                From = email.From,
                Message = email.Body,
                Subject = email.Subject,
                To = ""
            };


            return PartialView("_DetailsMessage", emailMessage);

        }
        // GET: Admin/HouseEmailMessages/Create
        public ActionResult Create(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            HouseModel house = unitOfWork.ActiveHouses.GetByID(id);


            if (house == null)
            {
                return HttpNotFound();
            }

            NewEmailViewModel email = new NewEmailViewModel
            {
                HouseId = house.ID
            };
            return PartialView("_ComposeMessage", email);
        }

        // POST: Admin/HouseEmailMessages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HouseId,To,Subject,Message")] NewEmailViewModel houseEmailMessage)
        {
            Services.EmailService _emailService = new Services.EmailService(unitOfWork);
            _emailService.SendCustomHouseEmail(houseEmailMessage.Message, houseEmailMessage.Subject, houseEmailMessage.To, houseEmailMessage.HouseId);

            return RedirectToAction("Index", new { id = houseEmailMessage.HouseId });
        }
    }
}
