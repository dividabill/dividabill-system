﻿using GoCardless.Webhook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GoCardless.Webhook.Controllers
{
    [RoutePrefix("suppliers/origin")]
    public class OriginController : ApiController
    {
        // GET: api/Supplier/5
        [Route("{id:int}")]
        public string Get(int id)
        {
            return "value";
        }

        // PUT: api/Supplier/5
        [Route("house/{id:int}")]
        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]OriginSharepoint value)
        {
            return Ok(value);
        }
    }
}
