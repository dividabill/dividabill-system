﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.Models.Common;

namespace DividaBill.BusinessLogic.MappingProfiles
{
    public class SimpleAddressProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Address, SimpleAddress>();
            Mapper.CreateMap<SimpleAddress, Address>()
                .ForMember(x => x.Position, opt => opt.Ignore());
        }
    }
}