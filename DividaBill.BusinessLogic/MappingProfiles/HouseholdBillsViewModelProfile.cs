﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.BusinessLogic.MappingProfiles
{
    public class HouseholdBillsViewModelProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<HouseholdBill, HouseHoldBillsViewModel>()
                .ForMember(dest => dest.PaymentDate, f => f.MapFrom(source => source.PaymentDate))
                .ForMember(dest => dest.HouseID, f => f.MapFrom(source => source.House_ID))
                .ForMember(dest => dest.Address, f => f.MapFrom(source => source.House.Address))
                .ForMember(dest => dest.StartDate, f => f.MapFrom(source => source.StartDate))
                .ForMember(dest => dest.EndDate, f => f.MapFrom(source => source.EndDate));
        }
    }
}