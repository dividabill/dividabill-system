﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.BusinessLogic.MappingProfiles
{
    public class ContractViewModelProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ContractViewModel, ContractRequest>()
                .ForMember(dto => dto.Duration, conf => conf.MapFrom(dm => dm.Length))
                .ForMember(dto => dto.Provider, conf => conf.Ignore());

            Mapper.CreateMap<Contract, ContractViewModel>()
                .ForMember(dto => dto.Service, conf => conf.MapFrom(dm => dm.ServiceNew_ID))
                .ForMember(dto => dto.RequestedTenantId, conf => conf.MapFrom(dm => dm.RequestedBy_ID))
                .ForMember(dto => dto.TriggeringTenant, conf => conf.MapFrom(dm => dm.RequestedBy));
            Mapper.CreateMap<ContractViewModel, Contract>()
                .ForMember(dto => dto.ServiceNew_ID, conf => conf.MapFrom(dm => dm.Service))
                .ForMember(dto => dto.SubmissionStatus, conf => conf.Ignore())
                .ForMember(dto => dto.RequestedBy_ID, conf => conf.MapFrom(dm => dm.RequestedTenantId))
                .ForMember(dto => dto.RequestedBy, conf => conf.MapFrom(dm => dm.TriggeringTenant));
        }
    }
}