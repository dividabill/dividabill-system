using System;
using System.Linq;
using AutoMapper;
using DividaBill.BusinessLogic.MappingProfiles;

namespace DividaBill.BusinessLogic
{
    public static class AutoMapperConfiguration
    {
        public static void GetConfiguration(IConfiguration configuration)
        {
            var profiles = typeof(HouseholdBillsViewModelProfile).Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));
            foreach (var profile in profiles)
            {
                configuration.AddProfile(Activator.CreateInstance(profile) as Profile);
            }
        }
    }
}
