﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails;
using DividaBill.Services.Emails.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.Services.Unity;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;

namespace SendUnsentEmails
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var container = new UnityContainer();

            container.RegisterType<IMappingEngine>(new InjectionFactory(_ => Mapper.Engine));
            container.RegisterDAL();
            container.RegisterServices();

            //UpdateAllMandates(container);

            //SendUnsentEmails(container);
        }

        private static void UpdateAllMandates(UnityContainer container)
        {
            var tenantService = container.Resolve<ITenantService>();
            var goCardlessBankMandateService = container.Resolve<IGoCardlessBankMandateService>();
            var unitOfWork = container.Resolve<IUnitOfWork>();

            var tenants = tenantService.GetAllTenants().ToList();

            foreach (var tenant in tenants)
            {
                goCardlessBankMandateService.UpdateAllExistingBankMandatesFromGc(tenant).Wait();
                unitOfWork.Save();
            }
        }

        private static void SendUnsentEmails(UnityContainer container)
        {
            var emailSentMessageService = container.Resolve<IEmailSentMessageService>();
            var emailTemplateService = container.Resolve<IEmailTemplateService>();
            var tenantService = container.Resolve<ITenantService>();
            var emailThirdPartyService = container.Resolve<IEmailThirdPartyService>();

            var unsentMessages = emailSentMessageService.GetActive().Where(x => x.SendToEmail == null).ToList();

            foreach (var item in unsentMessages)
            {
                var template = emailTemplateService.GetById(item.EmailTemplateId);
                if (template != null)
                {
                    var emailConfig = new EmailConfiguration
                    {
                        TemplateId = template.TemplateId,
                        Subject = item.Subject,
                        SendFromEmail = item.SentFromEmail,
                        SendFromEmailDisplayName = item.SentFromEmailDisplayName
                    };

                    var typeOfEmail = GetEmailTypeById(item.EmailTypeId);
                    var tenant = tenantService.GetTenant(item.UserId);

                    var substitutedValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(item.TemplateSubstitutedValues);

                    var emailToSend = new EmailMessageToSend(emailConfig, typeOfEmail, tenant, substitutedValues);
                    emailThirdPartyService.SendAndUpdate(emailToSend, item).Wait();
                }
            }
        }

        private static EmailType GetEmailTypeById(int id)
        {
            return EmailType.All.FirstOrDefault(item => item == id);
        }
    }
}