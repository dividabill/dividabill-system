﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DividaBill.Models;
using DividaBill.DAL;

namespace DividaBill.SysOp.Controllers
{
    public class ReferalsController : AdminController
    {

        // GET: Admin/Referals
        public ActionResult Index()
        {
            return View(unitOfWork.ReferalRepository.Get().ToList());
        }

        // GET: Admin/Referals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferalCode code = unitOfWork.ReferalRepository.GetByID(id);
            if (code == null)
            {
                return HttpNotFound();
            }
            return View(code);
        }

        // GET: Admin/Referals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Referals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Code,Text")] ReferalCode code)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.ReferalRepository.Insert(code);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(code);
        }

        // GET: Admin/Referals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferalCode code = unitOfWork.ReferalRepository.GetByID(id);
            if (code == null)
            {
                return HttpNotFound();
            }
            return View(code);
        }

        // POST: Admin/Referals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ReferalCode code)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.ReferalRepository.Update(code);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(code);
        }

        // GET: Admin/Referals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferalCode code = unitOfWork.ReferalRepository.GetByID(id);
            if (code == null)
            {
                return HttpNotFound();
            }
            return View(code);
        }

        // POST: Admin/Referals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReferalCode code = unitOfWork.ReferalRepository.GetByID(id);
            unitOfWork.ReferalRepository.Delete(code);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
