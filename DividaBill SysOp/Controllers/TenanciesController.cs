﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.SysOp.Controllers
{
    public class TenanciesController : Controller
    {
        public readonly IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel> _entityToViewModelFactory;
        private readonly IHouseService _houseService;
        private readonly ITenancyService _tenancyService;
        private readonly ITenantService _tenantService;
        public readonly IViewModelToEntityMappingFactory<TenancyViewModel, Tenancy> _viewModelToEntityFactory;

        // GET: Tenancy
        public TenanciesController(ITenancyService tenancyService, IHouseService houseService,
            IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel> entityToViewModelFactory,
            IViewModelToEntityMappingFactory<TenancyViewModel, Tenancy> viewModelToEntityMapping,
            ITenantService tenantService)
        {
            _tenantService = tenantService;
            _houseService = houseService;
            _viewModelToEntityFactory = viewModelToEntityMapping;
            _entityToViewModelFactory = entityToViewModelFactory;
            _tenancyService = tenancyService;
        }


        public ActionResult GetTenancies(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var tenanciesList = _tenancyService.GetTenanciesbyUserId(id);
                if (tenanciesList.Count == 0)
                {
                    ViewBag.noTenancy = "There is no tenancy for this tenant";
                    return View("~/Views/Tenancies/_TenanciesDetails.cshtml");
                }

                var viewModelList = new List<TenancyViewModel>();
                var viewModel = new TenancyViewModel();

                foreach (var tenancy in tenanciesList)
                {
                    viewModel = _entityToViewModelFactory.Map(tenancy);
                    viewModelList.Add(viewModel);
                }

                return View("~/Views/Tenancies/_TenanciesDetails.cshtml", viewModelList);
            }
            catch (TenancyNotFoundException)
            {
                return HttpNotFound();
            }
        }


        public ActionResult Edit(int tenancyId)
        {
            var entity = _tenancyService.GetTenancy(tenancyId);
            var viewModel = _entityToViewModelFactory.Map(entity);

            ViewBag.Houses = ActiveHouseList();

            return View("Edit", viewModel);
        }

        [HttpPost]
        public ActionResult Edit(TenancyViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var currentTenancy = _tenancyService.GetTenancy(viewModel.TenancyId);
                currentTenancy.JoiningDate = viewModel.JoiningDate;
                currentTenancy.LeavingDate = viewModel.LeavingDate;
                currentTenancy.Notes = viewModel.Notes;

                _tenancyService.UpdateTenancy(currentTenancy);
            }
            return RedirectToAction("Edit", "Tenants", new {id = viewModel.UserId});
        }


        public ActionResult Create(string id)
        {
            if (id != null)
            {
                var user = _tenantService.GetTenant(id);
                var viewModel = new TenancyViewModel
                {
                    UserId = id,
                    User = user
                };

                ViewBag.Houses = ActiveHouseList();

                return View("Create", viewModel);
            }
            return View("/Views/Tenants/All.cshtml");
        }

        [HttpPost]
        public ActionResult Create(TenancyViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entity = _viewModelToEntityFactory.Map(viewModel);
                _tenancyService.CreateTenancy(entity);
            }

            return RedirectToAction("Edit", "Tenants", new {id = viewModel.UserId});
        }


        public SelectList ActiveHouseList()
        {
            var houseList = _houseService.GetAllHouses();
            return new SelectList(houseList, "ID", "houseDisplay");
        }
    }
}