﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DividaBill.Extensions;
using DividaBill.Library;
using DividaBill.Library.DataTable;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using DividaBill.ViewModels.DataTables;
using Mvc.JQuery.DataTables;
using Mvc.JQuery.DataTables.Models;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/tenants")]
    public class TenantsApiController : ApiController
    {
        private readonly IHouseService _houseService;
        private readonly ITenantService _tenantService;
        private readonly IUserRegistrationService _userRegistrationService;
        private readonly IEntityToViewModelMappingFactory<User, TenantsDataTableViewModel> _userToTenantsDataTableViewModelMappingFactory;
        private readonly IViewModelEncryptionService<TenantsDataTableViewModel> _viewModelEncryptionService;

        public TenantsApiController(ITenantService tenantService, IEntityToViewModelMappingFactory<User, TenantsDataTableViewModel> userToTenantsDataTableViewModelMappingFactory,
            IViewModelEncryptionService<TenantsDataTableViewModel> viewModelEncryptionService
            , IUserRegistrationService userRegistrationService
            , IHouseService houseService
            )
        {
            _userRegistrationService = userRegistrationService;
            _userToTenantsDataTableViewModelMappingFactory = userToTenantsDataTableViewModelMappingFactory;
            _tenantService = tenantService;
            _viewModelEncryptionService = viewModelEncryptionService;
            _houseService = houseService;
        }


        [Route("TenantsAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetTenantsDataTable(
            [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var tenants =
                _tenantService.GetAllTenants()
                    .AsEnumerable()
                    .Select(x => _userToTenantsDataTableViewModelMappingFactory.Map(x))
                    .AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);

            var filteredResult = dataTableParam.GetDataTablesResponse(tenants);

            var convertedData = filteredResult.ConvertResults<TenantsDataTableViewModel>();

            var decryptedAndConvertedData =
                convertedData.AsEnumerable().Select(x => _viewModelEncryptionService.DecryptViewModel(x)).AsQueryable();

            return filteredResult.Create(decryptedAndConvertedData, uv =>
                new
                {
                    DOB = uv.DOB.ToStringDateFormat(),
                    Options =
                        string.Format(
                            "<a href=\"/Tenants/Edit/{0}\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-{0}\">Edit</a>",
                            uv.Id)
                }
                ).Data;
        }

        [ResponseType(typeof (DataTablesResponseData))]
        [HttpPost]
        [Route("ActiveTenantsAsDataTable")]
        public DataTablesResponseData GetActiveTenantsDataTable(
            [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var tenants =
                _tenantService.GetActiveTenants()
                    .AsEnumerable()
                    .Select(x => _userToTenantsDataTableViewModelMappingFactory.Map(x))
                    .AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);

            var filteredResult = dataTableParam.GetDataTablesResponse(tenants);

            var convertedData = filteredResult.ConvertResults<TenantsDataTableViewModel>();

            var decryptedAndConvertedData =
                convertedData.AsEnumerable().Select(x => _viewModelEncryptionService.DecryptViewModel(x)).AsQueryable();

            return filteredResult.Create(decryptedAndConvertedData, uv =>
                new
                {
                    DOB = uv.DOB.ToStringDateFormat(),
                    Options =
                        string.Format(
                            "<a href=\"/Tenants/Edit/{0}\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-{0}\">Edit</a>",
                            uv.Id)
                }
                ).Data;
        }

        [Route("")]
        [ResponseType(typeof (SignUpViewModel))]
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] SignUpViewModel viewModel)
        {
            var house = _houseService.GetHouse(viewModel.HouseId);
            var today = DateTime.Today;

            var bankdetails = new BankAccountDetails
            {
                AccountNumber = viewModel.AccountNumber,
                AccountName = viewModel.AccountName,
                AccountSortCode = viewModel.AccountSortCode
            };
            viewModel.BankAccountDetails = bankdetails;
            viewModel.Udprn = house.Address.UDPRN;
            viewModel.AddressTT1 = house.Address.Line1;
            viewModel.PostCode = house.Address.Postcode;
            viewModel.CityTT = house.Address.City;
            viewModel.CountyTT = house.Address.County;
            viewModel.StartTime = house.StartDate;
            viewModel.Housemates = (ushort) house.Housemates;
            viewModel.AccountStandalone = true;
            viewModel.Ongoing = house.IsActive(today);


            // Validate the SignUpViewModel with the Bank Account details. Execution stops if fails.
            ValidateViewModel(viewModel);

            try
            {
                var returnedViewModel = await _userRegistrationService.ProcessUserRegistration(viewModel);
                var dummy = returnedViewModel.HouseId;

                if (returnedViewModel.ValidationErrors == null)
                    return Ok(dummy);

                var errorList = new List<string>(returnedViewModel.ValidationErrors.Values);

                var msg = errorList.Aggregate("", (current, value) => current + "-" + value + "<br>");

                return BadRequest(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [ValidateModelState]
        public HttpActionContext ValidateViewModel(SignUpViewModel viewModel)
        {
            var action = new HttpActionContext();
            return action;
        }
    }
}