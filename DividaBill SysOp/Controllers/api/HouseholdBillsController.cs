﻿using AutoMapper;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels.DataTables;
using Mvc.JQuery.DataTables;
using System.Linq;
using System.Web.Http;

namespace DividaBill.SysOp.Controllers.api
{
    public class HouseholdBillsController : ApiController
    {

        private readonly IBillService _billService;

        public HouseholdBillsController(IBillService billService)
        {
            this._billService = billService;
        }

        public DataTablesResult<HouseHoldBillsViewModel> GetBill(DataTablesParam dataTableParam)
        {
            var bills = _billService.GetHouseholdBills();
            var viewModels = bills.Select(entity => Mapper.Map<HouseHoldBillsViewModel>(entity)).AsQueryable();
            return DataTablesResult.Create(viewModels, dataTableParam, Mvc.JQuery.DataTables.Models.ArrayOutputType.ArrayOfObjects);  //...and return a DataTablesResult
        }
    }
}
