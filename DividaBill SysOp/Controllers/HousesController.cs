﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using DividaBill.Areas.Admin.ViewModels;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Emails;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.SysOp.Controllers
{
    public class HousesController : AdminController
    {
        private readonly IEmailService _emailService;
        private readonly IHouseService _houseService;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IGoCardlessService _goCardlessService;
        private readonly IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel> _entityToViewModel;


        public HousesController(IHouseService houseService, IEmailService emailService, ITenantEncryptionService tenantEncryptionService, IGoCardlessService goCardlessService, IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel> entityToViewModel)
        {
            _entityToViewModel = entityToViewModel;
            _tenantEncryptionService = tenantEncryptionService;
            _goCardlessService = goCardlessService;
            _houseService = houseService;
            _emailService = emailService;
        }


        // GET: Admin/Houses
        public ActionResult Index()
        {
            return RedirectToActionPermanent(nameof(All));
        }


        // GET: Admin/Houses
        public ActionResult All()
        {
            return View();
        }

        // GET Admin/Houses/New
        public ActionResult Today()
        {
            return View();
        }

        // GET Admin/Houses/New
        public ActionResult Yesterday()
        {
            return View();
        }

        // GET Admin/Houses/New
        public ActionResult New()
        {
            return RedirectToActionPermanent(nameof(Today));
        }

        // GET Admin/Houses/Active
        public ActionResult Active()
        {
            return View();
        }

        // GET Admin/Houses/Active
        public ActionResult LastWeek()
        {
            return View();
        }

        public ActionResult Export(string FileType = "Excel", string Type = "Houses", int days = 100)
        {
            var houses = new DataTable("Houses");

            houses.Columns.Add("ID", typeof (int));
            houses.Columns.Add("Housemates", typeof (int));
            houses.Columns.Add("Address", typeof (string));
            houses.Columns.Add("Electricity", typeof (bool));
            houses.Columns.Add("Gas", typeof (bool));
            houses.Columns.Add("Water", typeof (bool));
            houses.Columns.Add("StartDate", typeof (DateTime));
            houses.Columns.Add("EndDate", typeof (DateTime));
            houses.Columns.Add("RegisteredDate", typeof (DateTime));
            houses.Columns.Add("CompletedDate", typeof (DateTimeOffset));
            houses.Columns.Add("Coupon", typeof (string));
            houses.Columns.Add("FirstName", typeof (string));
            houses.Columns.Add("LastName", typeof (string));
            houses.Columns.Add("Email", typeof (string));

            List<HouseModel> dbHouses;
            string Filename;
            switch (Type)
            {
                case "Active":
                    Filename = "ActiveHouses";
                    dbHouses = _houseService.GetActiveHouses().ToList();
                    break;
                default:
                    Filename = "Houses";
                    dbHouses = _houseService.GetAllHouses().ToList();
                    break;
            }

            foreach (var house in dbHouses)
            {
                _tenantEncryptionService.DecryptUsersList(house.Tenants.AsQueryable());
                foreach (var tenant in house.Tenants)
                {
                    houses.Rows.Add(house.ID, house.Housemates, house.Address, house.HasElectricity, house.HasGas,
                        house.HasWater,
                        house.StartDate, house.EndDate, house.RegisteredDate, house.ActualStartDate,
                        house.Coupon != null ? house.Coupon.Code : "", tenant.FirstName, tenant.LastName, tenant.Email);
                }
            }


            var grid = new GridView();
            grid.DataSource = houses;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index");
        }

        public ActionResult GenerateBGForm(int id)
        {
            var workbook = _houseService.GenerateBritishGasRegistrationForm(id);

            //Write-out the workbook
            var m = new MemoryStream();
            workbook.SaveToStream(m);
            m.Position = 0;

            return File(m, "application/vnd.ms-excel", "BritishGas-" + id + ".xls");
        }

        public ActionResult GenerateBTForm(int id)
        {
            var csv = _houseService.GenerateBTRegistrationForm(id);

            return File(new UTF8Encoding().GetBytes(csv), "text/csv", "BT-" + id + ".xls");
        }

        public ActionResult GenerateBTFormForHouses()
        {
            var housesWithBT =
                _houseService.GetActiveHouses()
                    .Where(
                        h =>
                            h.StartDate.Year == 2015 && h.StartDate.Month == 7 && h.HasBroadband && h.Tenants.Count >= 1)
                    .ToList();
            var csv = _houseService.GenerateBTRegistrationFormForHouses(housesWithBT);

            return File(new UTF8Encoding().GetBytes(csv), "text/csv", "BT-7.xls");
        }

        /// <summary>
        ///     Gets the GBEnergy CSV form of all houses for a specific month.
        /// </summary>
        /// <param name="sendDate">The date at which the form is to be sent. </param>
        /// <param name="startingMonth">The starting month of the included houses. </param>
        public ActionResult GenerateGBEnergyFormNew(DateTime sendDate, int startingMonth)
        {
            var houses = _houseService.GetActiveHouses().Where(x => x.HasElectricity || x.HasGas);
            var csv = _houseService.GenerateGBEnergyRegistrationsNew(houses);
            return File(new UTF8Encoding().GetBytes(csv), "text/csv", "GBEnergy-" + startingMonth + ".csv");
        }

        [HttpGet]
        public ActionResult SubmitPayments()
        {
            return View();
        }

        // GET: Admin/Houses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var house = _houseService.GetHouseIncluding(id.Value, "Tenancies,Contracts,HouseholdBills");

                var tenanciesViewModelList = new List<TenancyViewModel>();

                foreach (var tenancy in house.Tenancies)
                {
                    tenancy.User = _tenantEncryptionService.DecryptUser(tenancy.User);
                    tenanciesViewModelList.Add(_entityToViewModel.Map(tenancy));
                }

                ViewBag.TenanciesViewModel = tenanciesViewModelList.OrderByDescending(t => t.IsActive);

                ViewBag.HousematesCount = tenanciesViewModelList.Count(t => t.IsActive);

                return View(house);
            }
            catch
            {
                return HttpNotFound();
            }
        }

        // GET: Admin/Houses/Get/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                //ViewBag.Coupons = new SelectList(_houseService.GetAllCoupons(), "ID", "Code");

                var house = _houseService.GetHouseIncluding(id.Value, "Tenants,Contracts,HouseholdBills");
                _tenantEncryptionService.DecryptUsersList(house.Tenants.AsQueryable());

                return View(house);
            }
            catch
            {
                return HttpNotFound();
            }
        }

        // POST: Admin/Houses/Get/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(
                Include =
                    "Id,Housemates,Address,Electricity,Gas,Water,Broadband,BroadbandType,LandlinePhone,LandlineType,SkyTV,NetflixTV,TVLicense,Note,StartDate,EndDate,RegisteredDate,SetUpDate,Housemates"
                )] HouseModel houseModel)
        {
            if (ModelState.IsValid)
            {
                _houseService.UpdateHouse(houseModel);
                return RedirectToAction("Index");
            }
            return View(houseModel);
        }

        // POST: Admin/Houses/Archive/5
        [HttpPost]
        public ActionResult Archive(int id)
        {
            _houseService.ArchiveHouse(id);
            return Json("success");
        }


        // POST Admin/NewHouses
        [HttpPost]
        public JsonResult RemoveTenantFromHouse(string tenantID, int houseID)
        {
            _houseService.RemoveTenantFromHouse(tenantID, houseID);

            return Json(false);
        }


        // POST Admin/NewHouses
        [HttpPost]
        public JsonResult MergeHouses(int[] houses)
        {
            try
            {
                _houseService.MergeHouses(houses);

                return Json(true);
            }
            catch (Exception e)
            {
                return Json(e.ToString());
            }
        }

        [HttpPost]
        public async Task<JsonResult> AddToGoCardless(int id)
        {
            var tenants = _houseService.GetTenantsForHouse(id);
            foreach (var tenant in tenants)
            {
                var validationError = await _goCardlessService.AddToGoCardlessAsync(tenant);
                if (validationError != null)
                {
                    throw new Exception($"Key=[{validationError[0].Key}];ErrorMessage=[{validationError[0].ErrorMessage}]");
                }
            }
            return Json(true);
        }
    }
}