﻿$("#checkAll").change(function () {
    $("input[name=\"editCheckBox\"]").prop('checked', $(this).prop("checked"));
});

$("#updateJustMandateInfo").click(
    function() {
        var checkedIDs = GetCheckedIDs();
        if (checkedIDs.length <= 0) {
            alert("No rows selected");
            return;
        }
        var postData = { '': checkedIDs };
        $.post("/api/BankMandate/UpdateMandatesInfo/", postData, function(data) {
            alert("Updated IDs: " + data);

            ReloadTable();
        }).fail(function (xhr, textStatus, errorThrown) {
            alert("Error: " + xhr.responseText);
            ReloadTable();
        });
    }
);

$("#updateAllMandatesInfo").click(
    function () {
        if (confirm("Are you sure?")) {
            $.post("/api/BankMandate/UpdateAllMandatesInfo", null, function (data) {
                alert("Failed rows count is [" + data.Failed + "]\n" +
                    "Successful updated count is [" + data.Successful + "]");

                ReloadTable();
            }).fail(function (xhr, textStatus, errorThrown) {
                alert("Error: " + xhr.responseText);
                ReloadTable();
            });
        }
    }
);

$("#forceRegenerateMandate").click(
    function () {
        var checkedIDs = GetCheckedIDs();
        if (checkedIDs.length <= 0) {
            alert("No rows selected");
            return;
        }
        if (confirm("This action will force regenerate new Mandates for chosen users (even for Cancelled statuses). Are you sure to make this action?")) {
            var postData = { '': checkedIDs };
            $.post("/api/BankMandate/ForceRegenerateGcMandate/", postData, function (data) {
                var message = "";
                for (var i = 0; i < data.Successful.length; i++) {
                    message += data.Successful[i].Key + "- Successful";
                    message += "\n";
                }
                for (var j = 0; j < data.Failed.length; j++) {
                    message += data.Failed[j].Key + "-" + data.Failed[j].Value;
                    message += "\n";
                }
                alert(message);

                ReloadTable();
            }).fail(function (xhr, textStatus, errorThrown) {
                alert("Error: " + xhr.responseText);
                ReloadTable();
            });
        }
    }
);

function ReloadTable() {
    $("#table-houses").DataTable().ajax.reload();
    $(".se-pre-con").fadeOut("slow");
}

function GetCheckedIDs() {
    var checkedIDs = [];
    $('input[name="editCheckBox"]:checked').each(function() {
        checkedIDs.push(this.value);
    });
    return checkedIDs;
}

$body = $("body");
$(document).on({
    ajaxStart: function() {
        $body.addClass("loading");
        $(".se-pre-con").fadeIn("slow");
    },
    ajaxStop: function() {
        $(".se-pre-con").fadeOut("slow");
        $body.removeClass("loading");
    }
});
