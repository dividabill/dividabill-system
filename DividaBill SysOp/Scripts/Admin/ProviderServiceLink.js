﻿function selectProvidersByServiceTypeId(serviceTypeId) {
    $.getJSON("/api/providers/GetByServiceId/" + serviceTypeId).done(
        function(result) {
            var alertMessage = "Sorry, there is no linked Provider for this Service";
            var providersElement = $("#providers");
            providersElement.empty();
            if (result.length == 0)
                alert(alertMessage);
            else {
                providersElement.parent().parent().hide();
                //providersElement
                //        .append($("<option></option>")
                //        .attr("value", 0)
                //        .text("Select provider..."));
                var firstId = null;
                $.each(result, function(key, provider) {
                    if (firstId == null) {
                        firstId = provider.ID;
                    }
                    providersElement
                        .append($("<option></option>")
                            .attr("value", provider.ID)
                            .text(provider.Name));
                });
                if (firstId == null) {
                    alert(alertMessage);
                    providersElement.empty();
                } else {
                    providersElement.val(firstId).change();
                }
                providersElement.parent().parent().show();
            }
        });
}