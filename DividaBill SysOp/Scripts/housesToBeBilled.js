﻿var billingRunId = 0;

$("#checkAll").change(function () {
    $("input[name=\"editCheckBox\"]").prop('checked', $(this).prop("checked"));
});

$("#SetActiveHousesToBeBilled").click(
    function() {
        $.get("/billingrun/SetActiveHousesToBeBilled/" + billingRunId, function(data) {
            alert("All active houses added");
            ReloadTable();
        });
    }
);

$("#housesAdd").click(
    function () {
        var checkedHouseIDs = GetCheckedHouseIDs();
        if (checkedHouseIDs.length <= 0) {
            alert("No rows selected");
            return;
        }
        if (confirm("Do you really want to add objects: [" + checkedHouseIDs + "] ?")) {
            var postData = { '': checkedHouseIDs };
            $.post("/api/BillingQueue/AddHousesToBeBilled/" + billingRunId, postData, function (data) {
                alert("Added houseIDs: " + data);

                ReloadTable();
            });
        }
    }
);

$("#housesDelete").click(
    function() {
        var checkedHouseIDs = GetCheckedHouseIDs();
        if (checkedHouseIDs.length <= 0) {
            alert("No rows selected");
            return;
        }
        if (confirm("Do you really want to delete objects: [" + checkedHouseIDs + "] ?")) {
            DeleteHouseIDs();
        }
    }
);

function DeleteHouseIDs() {
    var postData = { '': GetCheckedHouseIDs() };
    $.post("/api/BillingQueue/DeleteHousesToBeBilled/" + billingRunId, postData, function(data) {
        alert("Deleted houseIDs: " + data);
        ReloadTable();
    });
}

function ReloadTable() {
    $("#table-houses").DataTable().ajax.reload();
}

function GetCheckedHouseIDs() {
    var houseIDs = [];
    $('input[name="editCheckBox"]:checked').each(function() {
        houseIDs.push(this.value);
    });
    return houseIDs;
}