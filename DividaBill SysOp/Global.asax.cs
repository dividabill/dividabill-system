﻿using System;
using System.Data.Entity.Migrations;
using System.IdentityModel.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DividaBill.DAL.Migrations;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Exceptions;
using DividaBill.SysOp.App_Start;
using Quartz.Impl;
using QuartzNetWebConsole;

namespace DividaBill.SysOp
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_BeginRequest()
        {
            //if  (!Context.Request.IsSecureConnection)
            //    Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //IdentityConfig.ConfigureIdentity();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperWebConfiguration.Configure();

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            Application["codename"] = "Avengers";

            var configuration = new Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();

            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();

            // This tells the QuartzNetWebConsole what scheduler to use
            Setup.Scheduler = () => scheduler;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            var exception = Server.GetLastError();

            ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(exception, typeof(MvcApplication).Assembly));
        }

        //{

        //private void WSFederationAuthenticationModule_RedirectingToIdentityProvider(object sender, RedirectingToIdentityProviderEventArgs e)
        //    if (!String.IsNullOrEmpty(IdentityConfig.Realm))
        //    {
        //        e.SignInRequestMessage.Realm = IdentityConfig.Realm;
        //    }
        //}
    }
}