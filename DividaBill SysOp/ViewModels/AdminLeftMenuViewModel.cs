﻿using System.Collections.Generic;

namespace DividaBill.Areas.Admin.ViewModels
{
    public class AdminLeftMenuViewModel
    {
        public UserMinimalViewModel User { get; set; }
        public List<INotificationViewModel> Notifications { get; set; }
    }
}