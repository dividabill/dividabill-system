﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DividaBill.SysOp.ViewModels
{
    public class HouseEditViewModel
    {
        public int ID { get; set; }

        public string Coupon { get; set; }

        public int Housemates { get; set; }

        public Address Address { get; set; }


        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public int Coupon_ID { get; set; }

        public virtual ICollection <User> Tenants { get; set; }

        public IList<Contract> Contracts { get; set; }

        public DateTime EndDate { get; set; }
        
    }
}