﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace DividaBill.Areas.Admin.ViewModels
{
    public class BillEditViewModel
    {
        public IEnumerable<SelectListItem> WarrantySelectListItems { get; set; }

    }
}