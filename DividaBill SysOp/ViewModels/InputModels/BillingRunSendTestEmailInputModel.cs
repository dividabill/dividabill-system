using System.ComponentModel.DataAnnotations;

namespace DividaBill.SysOp.ViewModels.InputModels
{
    public class BillingRunSendTestEmailInputModel
    {
        [Required]
        public int BillingRunId { get; set; }
        
        public int HouseId { get; set; }
        [Required]
        public string EmailAddress { get; set; }
    }
}