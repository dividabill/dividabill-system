using System.ComponentModel.DataAnnotations;

namespace DividaBill.SysOp.ViewModels.InputModels
{
    public class BillingRunAddTenantsToGoCardlessInputModel
    {
        [Required]
        public int BillingRunId { get; set; }

    }
}