﻿using DividaBill.Models;
using System;

namespace DividaBill.Areas.Admin.ViewModels
{
    public class ProblemHousesViewModel
    {
        public int ID { get; set; }
        public Address HouseAddress { get; set; }
        public int RegisteredTenants { get; set;}
        public int TotalTenants { get; set; }

        public DateTime RegisteredDate { get; set; }
    }
}