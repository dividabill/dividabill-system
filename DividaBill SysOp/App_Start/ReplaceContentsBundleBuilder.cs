using System.Collections.Generic;
using System.Web.Optimization;
using Seterlund.CodeGuard;

namespace DividaBill.SysOp
{
    public class ReplaceContentsBundleBuilder : IBundleBuilder
    {
        private readonly IBundleBuilder _builder;
        private readonly string _find;

        private readonly string _replaceWith;

        public ReplaceContentsBundleBuilder(string find, string replaceWith)
            : this(find, replaceWith, new DefaultBundleBuilder())
        {
        }

        public ReplaceContentsBundleBuilder(string find, string replaceWith, IBundleBuilder builder)
        {
            Guard.That(() => find).IsNotNullOrEmpty();
            Guard.That(() => replaceWith).IsNotNullOrEmpty();
            Guard.That(() => builder).IsNotNull();

            _find = find;
            _replaceWith = replaceWith;
            _builder = builder;
        }

        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<BundleFile> files)
        {
            var contents = _builder.BuildBundleContent(bundle, context, files);

            return contents.Replace(_find, _replaceWith);
        }
    }
}