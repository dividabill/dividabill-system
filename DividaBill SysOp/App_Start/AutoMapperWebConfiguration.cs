﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.ViewModels.DataTables;
using System;
using DividaBill.Services.AutoMapper;

namespace DividaBill.SysOp.App_Start
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                BusinessLogic.AutoMapperConfiguration.GetConfiguration(Mapper.Configuration);
            });
            ConfigureBankPaymentsMapping();
            //ConfigureHouseHoldBillsMapping();
            AutoMapperConfiguration.Configure();
        }

        private static void ConfigureBankPaymentsMapping()
        {
            Mapper.CreateMap<BankPayment, BankPaymentsViewModel>()
                .ForMember(dest => dest.Amount, f => f.MapFrom(source => decimal.Parse(((decimal)source.amount / 100).ToString())))
                .ForMember(dest => dest.CreatedDate, f => f.MapFrom(source => Convert.ToDateTime(source.created_at)))
                .ForMember(dest => dest.ChargeDate, f => f.MapFrom(source => Convert.ToDateTime(source.charge_date)));
            Mapper.CreateMap<BankMandate, BankPaymentsViewModel>().ForMember(dest => dest.ClientID, a => a.MapFrom(s => s.User_ID));
        }
    }
}