﻿using System.Web.Optimization;

namespace DividaBill.SysOp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery-ui.unobtrusive-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                "~/Scripts/jquery.unobtrusive-ajax.js"));


            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                "~/Content/themes/base/all.css"));

            bundles.Add(new ScriptBundle("~/bundles/metronic-core").Include(
                "~/Scripts/bootstrap-hover-dropdown.js",
                "~/Scripts/jquery.slimscroll.js",
                "~/Scripts/jquery.blockUI.js",
                "~/Scripts/jquery.cookie.js",
                "~/Scripts/jquery.uniform.js",
                "~/Scripts/Admin/app.js",
                "~/Scripts/select2.js",
                "~/Scripts/toastr.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                "~/Scripts/select2.js"
                ));

            bundles.Add(new StyleBundle("~/Content/metronic-core")
            {
                Builder = new ReplaceContentsBundleBuilder("url('font/", "url('/Content/font/")
            }.Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.css",
                "~/Content/metronic/style-metronic.css",
                "~/Content/metronic/style.css",
                "~/Content/metronic/style-responsive.css",
                "~/Content/metronic/plugins.css",
                "~/Content/metronic/pages/tasks.css",
                "~/Content/metronic/themes/default.css",
                "~/Content/metronic/custom.css",
                "~/Content/themes/uniformjs/default/css/uniform.default.css",
                "~/Content/select2.css",
                "~/Content/select2-metronic.css",
                "~/Content/toastr.css",
                "~/Content/bootstrap-datepicker.css",
                "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/metronic-login").Include(
                "~/Content/metronic/pages/login-soft.css",
                "~/Content/select2.css"));

            bundles.Add(new StyleBundle("~/Content/metronic-profile").Include(
                "~/Content/metronic/pages/profile.css"));

            bundles.Add(new ScriptBundle("~/bundles/metronic-login").Include(
                "~/Scripts/login.js",
                "~/Scripts/jquery.backstretch.js",
                "~/Scripts/select2.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/Site.css",
                "~/Content/preload.css"
                ));


            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                "~/Scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/adminTables").Include(
                "~/Content/select2.css"));

            bundles.Add(new StyleBundle("~/Content/dataTables").Include(
                "~/Content/DataTables-1.10.10/media/css/jquery.dataTables.css",
                "~/Content/DataTables-1.10.10/media/css/dataTables.bootstrap.css",
                "~/Content/DataTables-1.10.10/extensions/Buttons/css/buttons.dataTables.css",
                "~/Content/DataTables-1.10.10/extensions/Buttons/css/buttons.bootstrap.css",
                "~/Content/jquery-datatables-column-filter/media/js/jquery.dataTables.columnFilter.css",
                "~/Content/select2.css"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                "~/Scripts/select2.js",
                "~/Scripts/DataTables-1.10.10/jquery.dataTables.js",
                "~/Scripts/DataTables-1.10.10/dataTables.*"
                ));

            bundles.Add(new ScriptBundle("~/bundles/housesToBeBilled").Include(
                "~/Scripts/housesToBeBilled.js"));

            bundles.Add(new ScriptBundle("~/bundles/updateMandate").Include(
                "~/Scripts/updateMandate.js"));

            bundles.Add(new ScriptBundle("~/bundles/bankDetailsValidation").Include(
                "~/Scripts/bankDetailsValidation.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminTables").Include(
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery-ui.unobtrusive-{version}.js",
                "~/Scripts/jquery.validate*",
                "~/Scripts/DataTables-1.10.10/media/js/jquery.dataTables.js",
                "~/Scripts/DataTables-1.10.10/media/js/dataTables.bootstrap.js",
                "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/dataTables.buttons.js",
                "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/buttons.bootstrap.js",
                "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/buttons.html5.js",
                "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/buttons.flash.js",
                "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/buttons.print.js",
                "~/Content/jquery-datatables-column-filter/media/js/jquery.dataTables.columnFilter.js",
                "~/Content/jquery-datatables-column-filter/media/js/jquery.multiselect.js",
                "~/Scripts/pdfmake/pdfmake.min.js",
                "~/Scripts/pdfmake/vfs_fonts.js",
                "~/Scripts/jszip.js",
                "~/Scripts/moment.js",
                "~/Scripts/datatable.js",
                "~/Scripts/datatable-extensions.js",
                "~/Scripts/DT_bootstrap.js",
                "~/Scripts/select2.js",
                "~/Scripts/bootbox.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin-inbox").Include(
                "~/Scripts/inbox.js",
                "~/Scripts/wysihtml5/wysihtml5-0.3.0.js",
                "~/Scripts/bootstrap-wysihtml5/bootstrap-wysihtml5.js",
                "~/Scripts/blueimp/blueimp-gallery.js",
                "~/Scripts/blueimp/jquery.blueimp-gallery.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-process.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-image.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-audio.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-video.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-validate.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload-ui.js",
                "~/Scripts/tmpl.min.js",
                "~/Scripts/load-image.min.js",
                "~/Scripts/canvas-to-blob.min.js",
                "~/Scripts/jquery.fancybox.pack.js"));

            bundles.Add(new StyleBundle("~/Content/admin-inbox").Include(
                "~/Content/wysiwyg-color.css",
                "~/Content/bootstrap-wysihtml5.css",
                "~/Content/inbox.css",
                "~/Content/jQuery.FileUpload/css/jquery.fileupload.css",
                "~/Content/jQuery.FileUpload/css/jquery.fileupload-ui.css",
                "~/Content/blueimp-gallery2/blueimp-gallery.css",
                "~/Content/jquery.fancybox.css",
                "~/Content/jquery.fancybox-buttons.css",
                "~/Content/jquery.fancybox-thumbs.css"));

            bundles.Add(new ScriptBundle("~/bundles/admin-billsDetailsTable").Include(
                "~/Scripts/Admin/billsDetailsTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin-housesDetailsTable").Include(
                "~/Scripts/Admin/housesDetailsTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin-problemHousesDetailsTable").Include(
                "~/Scripts/Admin/problemHousesDetailsTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin-clientsDetailsTable").Include(
                "~/Scripts/Admin/clientsDetailsTable.js"));

            bundles.Add(new StyleBundle("~/Content/adminTables").Include(
                "~/Content/DT_bootstrap.css"));


            bundles.Add(new ScriptBundle("~/bundles/editHouse").Include(
                "~/Scripts/Admin/editHouse.js",
                "~/Scripts/Admin/providerServiceLink.js",
                "~/Scripts/bootbox.js",
                "~/Scripts/jquery.json.js",
                "~/Scripts/jquery.form-custom.js",
                "~/Scripts/moment.js",
                "~/Scripts/bootstrap-datepicker.js",
                 "~/Scripts/jquery.inputmask/inputmask.js",
                "~/Scripts/jquery.inputmask/jquery.inputmask.js",
                "~/Scripts/jquery.inputmask/inputmask.extensions.js",
                "~/Scripts/jquery.inputmask/inputmask.phone.extensions.js"));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include(
                "~/Scripts/Chart.js"));

            BundleTable.EnableOptimizations = false;
        }
    }
}