using System;
using System.Web.Mvc;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Exceptions;

namespace DividaBill.SysOp.Exceptions
{
    public class CustomExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var exceptionInfo = new ExceptionInfo(filterContext.Exception, typeof (MvcApplication).Assembly);
            try
            {
                var form = string.Empty;
                if (filterContext.HttpContext.Request.Form != null)
                {
                    var nameValueCollection = filterContext.HttpContext.Request.Form;
                    for (var i = 0; i < nameValueCollection.Count; i++)
                    {
                        form += $"{{{nameValueCollection.AllKeys[i]}: {nameValueCollection[i]}}}<br/>";
                    }
                }

                exceptionInfo.Form = form;
                exceptionInfo.Headers = filterContext.HttpContext.Request.Headers;
                exceptionInfo.HttpMethod = filterContext.HttpContext.Request.HttpMethod;
                exceptionInfo.UserHostAddress = filterContext.HttpContext.Request.UserHostAddress;
                if (filterContext.HttpContext.Request.Url != null)
                    exceptionInfo.Url = filterContext.HttpContext.Request.Url.ToString();
                exceptionInfo.UserAgent = filterContext.HttpContext.Request.UserAgent;
                exceptionInfo.UserHostName = filterContext.HttpContext.Request.UserHostName;
            }
            catch (Exception getValueException)
            {
                ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(getValueException, typeof (MvcApplication).Assembly));
            }

            ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), exceptionInfo);
        }
    }
}