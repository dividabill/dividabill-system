using System;
using System.Net.Http;
using System.Web.Http.Filters;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Exceptions;

namespace DividaBill.SysOp.Exceptions
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            try
            {
                var httpMethod = context.Request.Method;
                var exceptionInfo = new ExceptionInfo(context.Exception, typeof (MvcApplication).Assembly)
                {
                    Form = httpMethod == HttpMethod.Get ? context.Request.RequestUri.Query : context.Request.Content.ReadAsStringAsync().Result,
                    HttpMethod = httpMethod.Method,
                    Url = context.Request.RequestUri.AbsolutePath,
                    UserAgent = context.Request.Headers.UserAgent.ToString(),
                    UserHostAddress = context.Request.Headers.Host
                };
                ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), exceptionInfo);
            }
            catch (Exception exception)
            {
                ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(exception, typeof (MvcApplication).Assembly));
            }

            base.OnException(context);
        }
    }
}