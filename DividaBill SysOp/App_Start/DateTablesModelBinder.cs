﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using Mvc.JQuery.DataTables;

namespace DividaBill.SysOp
{
    public class DataTablesModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var ct = actionContext.Request.Content.ReadAsStringAsync().Result;
            var vals = ct.Split('&');

            var values = HttpUtility.ParseQueryString(ct);

            var valueProvider = bindingContext.ValueProvider;
            var xxx = values.Get("iColumns");
            var columns2 = bindingContext.PropertyMetadata["iColumns"];
            var columns = GetValue<int>(values, "iColumns");

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, bindingContext.ValueProvider.GetValue(bindingContext.ModelName));

            if (columns == 0)
            {
                return BindV10Model(bindingContext, values);
            }
            return BindLegacyModel(bindingContext, values, columns);
        }

        private bool BindV10Model(ModelBindingContext bindingContext, NameValueCollection valueProvider)
        {
            try
            {
                var obj = new DataTablesParam();
                var start = bindingContext.PropertyMetadata["start"];
                obj.iDisplayStart = GetValue<int>(valueProvider, "start");
                obj.iDisplayLength = GetValue<int>(valueProvider, "length");
                obj.sSearch = GetValue<string>(valueProvider, "search[value]");
                obj.bEscapeRegex = GetValue<bool>(valueProvider, "search[regex]");
                obj.sEcho = GetValue<int>(valueProvider, "draw");

                var colIdx = 0;
                while (true)
                {
                    var colPrefix = string.Format("columns[{0}]", colIdx);
                    var colName = GetValue<string>(valueProvider, colPrefix + "[data]");

                    var colName2 = bindingContext.PropertyMetadata[colPrefix + "[data]"];
                    if (string.IsNullOrWhiteSpace(colName))
                    {
                        break;
                    }
                    obj.sColumnNames.Add(colName);
                    obj.bSortable.Add(GetValue<bool>(valueProvider, colPrefix + "[orderable]"));
                    obj.bSearchable.Add(GetValue<bool>(valueProvider, colPrefix + "[searchable]"));
                    obj.sSearchValues.Add(GetValue<string>(valueProvider, colPrefix + "[search][value]"));
                    obj.bEscapeRegexColumns.Add(GetValue<bool>(valueProvider, colPrefix + "[searchable][regex]"));
                    colIdx++;
                }
                obj.iColumns = colIdx;
                colIdx = 0;
                while (true)
                {
                    var colPrefix = string.Format("order[{0}]", colIdx);
                    var orderColumn = GetValue<int?>(valueProvider, colPrefix + "[column]");
                    if (orderColumn.HasValue)
                    {
                        obj.iSortCol.Add(orderColumn.Value);
                        obj.sSortDir.Add(GetValue<string>(valueProvider, colPrefix + "[dir]"));
                        colIdx++;
                    }
                    else
                    {
                        break;
                    }
                }
                obj.iSortingCols = colIdx;

                bindingContext.Model = obj;
                return true;
            }
            catch (Exception)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("\"{0}\" is invalid.", bindingContext.ModelName));
                return false;
            }
        }

        private bool BindLegacyModel(ModelBindingContext bindingContext, NameValueCollection valueProvider, int columns)
        {
            var obj = new DataTablesParam(columns);

            var start = bindingContext.PropertyMetadata["iDisplayStart"];
            obj.iDisplayStart = GetValue<int>(valueProvider, "iDisplayStart");
            obj.iDisplayLength = GetValue<int>(valueProvider, "iDisplayLength");
            obj.sSearch = GetValue<string>(valueProvider, "sSearch");
            obj.bEscapeRegex = GetValue<bool>(valueProvider, "bEscapeRegex");
            obj.iSortingCols = GetValue<int>(valueProvider, "iSortingCols");
            obj.sEcho = GetValue<int>(valueProvider, "sEcho");

            for (var i = 0; i < obj.iColumns; i++)
            {
                //var iSortCol = bindingContext.PropertyMetadata["iSortCol_" + i];

                obj.bSortable.Add(GetValue<bool>(valueProvider, "bSortable_" + i));
                obj.bSearchable.Add(GetValue<bool>(valueProvider, "bSearchable_" + i));
                obj.sSearchValues.Add(GetValue<string>(valueProvider, "sSearch_" + i));
                obj.bEscapeRegexColumns.Add(GetValue<bool>(valueProvider, "bEscapeRegex_" + i));
                obj.iSortCol.Add(GetValue<int>(valueProvider, "iSortCol_" + i));
                obj.sSortDir.Add(GetValue<string>(valueProvider, "sSortDir_" + i));
            }

            bindingContext.Model = obj;
            return true;
        }

        private static T GetValue<T>(NameValueCollection valueProvider, string key)
        {
            var valueResult = valueProvider.Get(key);
            return valueResult == null
                ? default(T)
                : (T) Convert.ChangeType(valueResult, typeof (T));
        }
    }

    public class DateTimeModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var date = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue;

            if (string.IsNullOrEmpty(date))
                return false;

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, bindingContext.ValueProvider.GetValue(bindingContext.ModelName));
            try
            {
                bindingContext.Model = DateTime.Parse(date);
                return true;
            }
            catch (Exception)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("\"{0}\" is invalid.", bindingContext.ModelName));
                return false;
            }
        }
    }
}