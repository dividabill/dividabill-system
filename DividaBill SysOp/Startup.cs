﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(DividaBill.SysOp.Startup))]
namespace DividaBill.SysOp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}