﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoCardless.Webhook.Models
{
    public class WebhookResponse
    {
        public List<Event> Events { get; set; }
    }
}