﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GoCardless.Webhook.Models;
using System.Web;
using GoCardless_API.api;
using DividaBill.DAL;
using DividaBill.Models;
using Newtonsoft.Json;

namespace GoCardless.Webhook.Controllers
{
    public class ValuesController : ApiController
    {
        // POST api/gocardless
        public HttpResponseMessage Post(WebhookResponse events)
        {
            ApiClient client = new ApiClient("AK000010V7DPMY", "LwNkPfNiTKjr1xy81SEeKnl-oucvv6wA14bg5iqV", "");
            UnitOfWork _unitOfWork = new UnitOfWork();

            foreach (GoCardless.Webhook.Models.Event currentEvent in events.Events)
            {
                if (currentEvent.resource_type == "payments")
                {
                    if (_unitOfWork.BankPaymentEventRepository.Count(e => e.id == currentEvent.id) == 0)
                    {
                        DividaBill.Models.GoCardless.Payment gocardlessPayment = client.GetPayment(currentEvent.links["payment"]);
                        BankPayment payment = _unitOfWork.BankPaymentRepository.GetByID(currentEvent.links["payment"]);
                        if (payment == null)
                        {
                            return new HttpResponseMessage(HttpStatusCode.NotFound);
                        }
                        payment.amount = gocardlessPayment.amount;
                        payment.amount_refunded = gocardlessPayment.amount_refunded;
                        payment.charge_date = gocardlessPayment.charge_date;
                        payment.created_at = gocardlessPayment.created_at;
                        payment.currency = gocardlessPayment.currency;
                        payment.description = gocardlessPayment.description;
                        payment.status = gocardlessPayment.status;
                        payment.reference = gocardlessPayment.reference;

                        BankPaymentEvent paymentEvent = new BankPaymentEvent
                        {
                            id = currentEvent.id,
                            action = currentEvent.action,
                            created_at = currentEvent.created_at,
                            Payment_ID = currentEvent.links["payment"],
                            resource_type = currentEvent.resource_type,
                            details = JsonConvert.SerializeObject(currentEvent.details, Formatting.None)
                        };

                        _unitOfWork.BankPaymentRepository.Update(payment);
                        _unitOfWork.BankPaymentEventRepository.Insert(paymentEvent);
                        _unitOfWork.Save();
                    }
                }

                if (currentEvent.resource_type == "mandates")
                {
                    if (_unitOfWork.BankMandateEventRepository.Count(e => e.id == currentEvent.id) == 0)
                    {
                        DividaBill.Models.GoCardless.Mandate gocardlessMandate = client.GetMandate(currentEvent.links["mandate"]);
                        BankMandate mandate = _unitOfWork.BankMandateRepository.GetByID(currentEvent.links["mandate"]);
                        if (mandate == null)
                        {
                            return new HttpResponseMessage(HttpStatusCode.NotFound);
                        }
                        mandate.created_at = gocardlessMandate.created_at;
                        mandate.next_possible_charge_date = gocardlessMandate.next_possible_charge_date;
                        mandate.reference = gocardlessMandate.reference;
                        mandate.scheme = gocardlessMandate.scheme;
                        mandate.status = gocardlessMandate.status;

                        BankMandateEvent paymentEvent = new BankMandateEvent
                        {
                            id = currentEvent.id,
                            action = currentEvent.action,
                            created_at = currentEvent.created_at,
                            Mandate_ID = currentEvent.links["mandate"],
                            resource_type = currentEvent.resource_type,
                            details = JsonConvert.SerializeObject(currentEvent.details, Formatting.None)
                        };

                        _unitOfWork.BankMandateRepository.Update(mandate);
                        _unitOfWork.BankMandateEventRepository.Insert(paymentEvent);
                        _unitOfWork.Save();
                    }
                }
            }

            return new HttpResponseMessage(HttpStatusCode.OK);  // OK = 200
        }
    }
}
