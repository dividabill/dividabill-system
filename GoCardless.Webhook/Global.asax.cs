﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace GoCardless.Webhook
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy =
   IncludeErrorDetailPolicy.Always;

            //var configuration = new Dividabil.DAL.Migrations.Configuration();
            //var migrator = new DbMigrator(configuration);
            //migrator.Update();
        }
    }
}
